module.exports = {
  content: ["./src/**/*.tsx", "./src/**/*.ts"],
  theme: {
    container: (theme) => ({
      center: true,
      padding: {
        DEFAULT: theme("spacing.4"),
        lg: theme("spacing.8"),
        xl: theme("spacing.8"),
        "2xl": theme("spacing.8"),
      },
    }),
    minHeight: (theme) => ({
      0: "0",
      full: "100%",
      screen: "100vh",
      ...theme("spacing"),
    }),
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),
  ],
};
