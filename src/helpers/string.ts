import { startCase, camelCase } from "lodash";

export const pascalCase = (text: string) =>
  startCase(camelCase(text)).replace(/ /g, "");
