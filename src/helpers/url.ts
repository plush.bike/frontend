import ImageKit from "imagekit-javascript";

const imageKit = new ImageKit({
  urlEndpoint: process.env.NEXT_PUBLIC_IMAGEKIT_ENDPOINT,
});

export const getQueryParam = (param: string) => {
  const urlParams = new URLSearchParams(window.location.search);

  return urlParams.get(param);
};

export const imageUrl = ({
  url,
  width,
  height,
}: {
  url: string;
  width?: number;
  height?: number;
}) => {
  const transformation: { height?: string; width?: string } = {};

  if (height) {
    transformation.height = `${height}`;
  }

  if (width) {
    transformation.width = `${width}`;
  }

  return imageKit.url({
    src: url,
    transformation: [transformation],
  });
};
