import { twMerge } from "tailwind-merge";

export const buttonBaseSharedProps = ({
  loading = false,
  disabled = false,
  className = "",
}: {
  loading?: boolean;
  disabled?: boolean;
  className?: string;
}) => {
  return {
    disabled: loading || disabled,
    className: twMerge(
      "inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm",
      (loading || disabled) &&
        "disabled:cursor-not-allowed disabled:opacity-40",
      className
    ),
  };
};
