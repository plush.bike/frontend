import { uniqueId } from "lodash";

export class SavedFile {
  id: string;
  preview: string;
  key: string;
  loading: boolean;
  src: string;
  error: string;

  constructor({ id, src }) {
    this.id = id;
    this.preview = null;
    this.key = uniqueId();
    this.loading = false;
    this.src = src;
    this.error = null;
  }
}
