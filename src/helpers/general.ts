export const isPromise = (promise: any) =>
  !!promise && typeof promise.then === "function";

export const nextTick = () => new Promise((resolve) => setTimeout(resolve, 0));

export const classToObject = <ClassType>(classToConvert: any): ClassType => {
  const keys = Object.getOwnPropertyNames(
    Object.getPrototypeOf(classToConvert)
  );

  return keys.reduce((accumulator, key) => {
    if (key === "constructor") {
      return accumulator;
    }

    accumulator[key] = classToConvert[key];
    return accumulator;
  }, {}) as ClassType;
};
