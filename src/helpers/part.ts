import { AirOrCoilType, PartSettingsRecord } from "@/types/graphql";

export const formatSettingsRecordsForMutation = ({
  settingsRecords,
  airOrCoil,
}: {
  settingsRecords: Pick<
    PartSettingsRecord,
    | "__typename"
    | "id"
    | "psi"
    | "volumeSpacers"
    | "springRate"
    | "preloadAdjust"
  >[];
  airOrCoil: AirOrCoilType;
}) => {
  return settingsRecords.map(
    ({
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      __typename,
      psi,
      volumeSpacers,
      springRate,
      preloadAdjust,
      ...settingsRecord
    }) =>
      airOrCoil === AirOrCoilType.Air
        ? { psi, volumeSpacers, ...settingsRecord }
        : { springRate, preloadAdjust, ...settingsRecord }
  );
};
