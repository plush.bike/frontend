import { emailIsValid } from "@/helpers/form";
import { ListboxOption, ValidationRule } from "@/types/form";

export const validateListBoxRequired: ValidationRule<ListboxOption> = (
  value,
  message
) => !!value || message;

export const validateEmail: ValidationRule<string> = (value, message) =>
  emailIsValid(value) || message;

export const validateMatch: ValidationRule<string, { otherValue: string }> = (
  value,
  message,
  { otherValue }
) => value === otherValue || message;
