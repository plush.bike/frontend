const createFakeElement = (value: string) => {
  const isRTL = document.documentElement.getAttribute("dir") === "rtl";
  const fakeElement = document.createElement("textarea");
  // Prevent zooming on iOS
  fakeElement.style.fontSize = "12pt";
  // Reset box model
  fakeElement.style.border = "0";
  fakeElement.style.padding = "0";
  fakeElement.style.margin = "0";
  // Move element out of screen horizontally
  fakeElement.style.position = "absolute";
  fakeElement.style[isRTL ? "right" : "left"] = "-9999px";
  // Move element to the same position vertically
  const yPosition = window.pageYOffset || document.documentElement.scrollTop;
  fakeElement.style.top = `${yPosition}px`;

  fakeElement.setAttribute("readonly", "");
  fakeElement.value = value;

  return fakeElement;
};

export const copyToClipboard = (contentToCopy: string) => {
  const fakeElement = createFakeElement(contentToCopy);
  document.body.appendChild(fakeElement);
  fakeElement.select();
  fakeElement.setSelectionRange(0, contentToCopy.length);
  return new Promise<void>((resolve, reject) => {
    // here the magic happens
    document.execCommand("copy") ? resolve() : reject();
    fakeElement.remove();
  });
};
