import { ApolloError } from "@apollo/client";
import { useId } from "@reach/auto-id";
import { camelCase, isEmpty, kebabCase } from "lodash";
import { Dispatch, SetStateAction } from "react";
import {
  FieldError,
  FieldValues,
  Path,
  UseFormSetError,
} from "react-hook-form";

export const emailIsValid = (email: string) =>
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );

export const hasErrors = (errors: FieldError | FieldError[]) =>
  !isEmpty(errors);

export const errorInputAttributes = ({
  errors,
  id,
}: {
  errors: FieldError | FieldError[];
  id: string;
}) => ({
  "aria-invalid": hasErrors(errors) ? true : null,
  "aria-errormessage": hasErrors(errors) ? `${id}-error` : null,
});

export const descriptionInputAttributes = ({
  id,
  description,
}: {
  id: string;
  description: string | (() => JSX.Element);
}) => ({
  "aria-describedby": description ? `${id}-description` : null,
});

export const parseServerErrorFields = <TFieldValues extends FieldValues>(
  error: ApolloError
): Record<Path<TFieldValues>, string[]> => {
  const fields =
    (error?.graphQLErrors?.[0]?.extensions?.validation as TFieldValues) || {};

  return Object.keys(fields).reduce((accumulator, field) => {
    const errorMessage = fields[field]?.[0];
    const fieldKey = camelCase(field.match(/input\.([^.]+)/)[1]);

    if (Object.prototype.hasOwnProperty.call(accumulator, fieldKey)) {
      return {
        ...accumulator,
        [fieldKey]: [...accumulator[fieldKey], errorMessage],
      };
    } else {
      return {
        ...accumulator,
        [fieldKey]: [errorMessage],
      };
    }
  }, {}) as Record<Path<TFieldValues>, string[]>;
};

export const parseAlertErrorMessage = (error: ApolloError) => {
  const errors = error?.graphQLErrors?.[0];
  const extensions = errors?.extensions;

  if (extensions?.category === "validation") {
    return Object.values(extensions?.errors || {})[0];
  }

  return errors?.message;
};

export const parseAndSetServerErrors = <TFieldValues extends FieldValues>({
  error,
  setAlertErrorMessage,
  setError,
  customAlertErrorMessages = {},
}: {
  error: ApolloError;
  setAlertErrorMessage: Dispatch<SetStateAction<string>>;
  setError: UseFormSetError<TFieldValues>;
  customAlertErrorMessages?: { [key: string]: string };
}) => {
  const fields = parseServerErrorFields<TFieldValues>(error);

  if (isEmpty(fields)) {
    const errorMessage = parseAlertErrorMessage(error);

    if (errorMessage) {
      setAlertErrorMessage(
        customAlertErrorMessages[errorMessage] || errorMessage
      );
    } else {
      setAlertErrorMessage("An error occurred, please try again.");
    }

    return;
  }

  for (const fieldKey in fields) {
    const errorMessage = fields[fieldKey];
    setError(fieldKey as Path<TFieldValues>, {
      type: "server",
      message: errorMessage[0],
    });
  }
};

export const inputId = (label: string) => {
  const uid = useId();

  return `${kebabCase(label)}-${uid}`;
};
