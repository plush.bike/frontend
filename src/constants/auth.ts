import GoogleLogo from "@/svgs/google.svg";
import StravaLogo from "@/svgs/strava.svg";
import { SocialProviderType } from "@/types/graphql";

export const INCORRECT_SIGN_IN_CREDENTIALS_ERROR_MESSAGE =
  "Incorrect username or password.";

export const CURRENT_PASSWORD_IS_INCORRECT = "Current password is incorrect.";

export const INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE =
  "Please enter your password to continue.";

export const SOCIAL_PROVIDER_STRAVA = {
  type: SocialProviderType.Strava,
  buttonIcon: StravaLogo,
  label: "Strava",
};

export const SOCIAL_PROVIDER_GOOGLE = {
  type: SocialProviderType.Google,
  buttonIcon: GoogleLogo,
  label: "Google",
};

export const SOCIAL_PROVIDERS = [
  SOCIAL_PROVIDER_STRAVA,
  SOCIAL_PROVIDER_GOOGLE,
];
