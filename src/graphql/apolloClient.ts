import {
  ApolloClient,
  NormalizedCacheObject,
  InMemoryCache,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { RetryLink } from "@apollo/client/link/retry";
import { createUploadLink } from "apollo-upload-client";
import Cookies from "js-cookie";
import Router from "next/router";
import { useMemo } from "react";

import { fetchCsrfCookie } from "@/helpers/auth";
import store from "@/store";
import { updateAuthenticatedUser } from "@/store/slices/auth";

let apolloClient: ApolloClient<NormalizedCacheObject> | null;

const httpUploadLink = createUploadLink({
  uri: `${process.env.NEXT_PUBLIC_API_URL}/graphql`,
  credentials: "include",
});

const authLink = setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      "X-XSRF-TOKEN": Cookies.get("XSRF-TOKEN"),
    },
  };
});

const retryLink = new RetryLink({
  attempts: async (count, operation, error) => {
    if (
      error?.["statusCode"] === 419 &&
      error?.["result"].message === "CSRF token mismatch"
    ) {
      if (count > 1) {
        return false;
      }

      try {
        await fetchCsrfCookie();

        return true;
      } catch {
        return false;
      }
    }

    if (error?.["statusCode"] === 401) {
      store.dispatch(updateAuthenticatedUser(null));

      if (Router.route !== "/") {
        Router.push("/sign-in");
      }

      return false;
    }
  },
});

const createApolloClient = () => {
  apolloClient = new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: retryLink.concat(authLink.concat(httpUploadLink)),
    cache: new InMemoryCache(),
    connectToDevTools: true,
  });

  return apolloClient;
};

export const initializeApollo = (initialState = null) => {
  const _apolloClient = apolloClient ?? createApolloClient();

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();
    // Restore the cache using the data passed from getStaticProps/getServerSideProps
    // combined with the existing cached data
    _apolloClient.cache.restore({ ...existingCache, ...initialState });
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;
  return _apolloClient;
};

export const useApollo = (initialState = null) => {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
};
