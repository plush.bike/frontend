import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type PartsByTypeQueryVariables = Types.Exact<{
  type: Types.PartType;
  first?: Types.InputMaybe<Types.Scalars["Int"]>;
  page?: Types.InputMaybe<Types.Scalars["Int"]>;
}>;

export type PartsByTypeQuery = {
  __typename?: "Query";
  partsByType: {
    __typename?: "PartPaginator";
    data: Array<{
      __typename?: "Part";
      id: string;
      name: string;
      type: Types.PartType;
      manufacturer: { __typename?: "Manufacturer"; id: string; name: string };
      specificationsRecord?: {
        __typename?: "PartSpecificationsRecord";
        id: string;
        lscMaxClicks?: number | null;
        hscMaxClicks?: number | null;
        lsrMaxClicks?: number | null;
        hsrMaxClicks?: number | null;
        travel?: number | null;
        airOrCoil?: Types.AirOrCoilType | null;
      } | null;
      activeSettingsRecord?: {
        __typename?: "PartSettingsRecord";
        id: string;
        nickname?: string | null;
        lscClicks?: number | null;
        hscClicks?: number | null;
        lsrClicks?: number | null;
        hsrClicks?: number | null;
        volumeSpacers?: number | null;
        psi?: number | null;
        sag?: number | null;
        springRate?: number | null;
        preloadAdjust?: number | null;
        notes?: string | null;
      } | null;
      images?: Array<{
        __typename?: "PartImage";
        id: string;
        url?: string | null;
      }> | null;
    }>;
    paginatorInfo: {
      __typename?: "PaginatorInfo";
      lastItem?: number | null;
      total: number;
    };
  };
};

export const PartsByTypeDocument = gql`
  query PartsByType($type: PartType!, $first: Int = 25, $page: Int = 1) {
    partsByType(type: $type, first: $first, page: $page) {
      data {
        id
        name
        type
        manufacturer {
          id
          name
        }
        specificationsRecord {
          id
          lscMaxClicks
          hscMaxClicks
          lsrMaxClicks
          hsrMaxClicks
          travel
          airOrCoil
        }
        activeSettingsRecord {
          id
          nickname
          lscClicks
          hscClicks
          lsrClicks
          hsrClicks
          volumeSpacers
          psi
          sag
          springRate
          preloadAdjust
          notes
        }
        images {
          id
          url
        }
      }
      paginatorInfo {
        lastItem
        total
      }
    }
  }
`;

/**
 * __usePartsByTypeQuery__
 *
 * To run a query within a React component, call `usePartsByTypeQuery` and pass it any options that fit your needs.
 * When your component renders, `usePartsByTypeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePartsByTypeQuery({
 *   variables: {
 *      type: // value for 'type'
 *      first: // value for 'first'
 *      page: // value for 'page'
 *   },
 * });
 */
export function usePartsByTypeQuery(
  baseOptions: Apollo.QueryHookOptions<
    PartsByTypeQuery,
    PartsByTypeQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PartsByTypeQuery, PartsByTypeQueryVariables>(
    PartsByTypeDocument,
    options
  );
}
export function usePartsByTypeLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    PartsByTypeQuery,
    PartsByTypeQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PartsByTypeQuery, PartsByTypeQueryVariables>(
    PartsByTypeDocument,
    options
  );
}
export type PartsByTypeQueryHookResult = ReturnType<typeof usePartsByTypeQuery>;
export type PartsByTypeLazyQueryHookResult = ReturnType<
  typeof usePartsByTypeLazyQuery
>;
export type PartsByTypeQueryResult = Apollo.QueryResult<
  PartsByTypeQuery,
  PartsByTypeQueryVariables
>;
