import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type PartByIdQueryVariables = Types.Exact<{
  id: Types.Scalars["ID"];
}>;

export type PartByIdQuery = {
  __typename?: "Query";
  partById?: {
    __typename?: "Part";
    name: string;
    type: Types.PartType;
    manufacturer: { __typename?: "Manufacturer"; id: string; name: string };
    specificationsRecord?: {
      __typename?: "PartSpecificationsRecord";
      id: string;
      lscMaxClicks?: number | null;
      hscMaxClicks?: number | null;
      lsrMaxClicks?: number | null;
      hsrMaxClicks?: number | null;
      travel?: number | null;
      airOrCoil?: Types.AirOrCoilType | null;
    } | null;
    activeSettingsRecord?: {
      __typename?: "PartSettingsRecord";
      id: string;
    } | null;
    settingsRecords?: Array<{
      __typename?: "PartSettingsRecord";
      id: string;
      nickname?: string | null;
      lscClicks?: number | null;
      hscClicks?: number | null;
      lsrClicks?: number | null;
      hsrClicks?: number | null;
      volumeSpacers?: number | null;
      psi?: number | null;
      sag?: number | null;
      springRate?: number | null;
      preloadAdjust?: number | null;
      notes?: string | null;
    }> | null;
    images?: Array<{
      __typename?: "PartImage";
      id: string;
      url?: string | null;
    }> | null;
  } | null;
};

export const PartByIdDocument = gql`
  query PartById($id: ID!) {
    partById(id: $id) {
      name
      type
      manufacturer {
        id
        name
      }
      specificationsRecord {
        id
        lscMaxClicks
        hscMaxClicks
        lsrMaxClicks
        hsrMaxClicks
        travel
        airOrCoil
      }
      activeSettingsRecord {
        id
      }
      settingsRecords {
        id
        nickname
        lscClicks
        hscClicks
        lsrClicks
        hsrClicks
        volumeSpacers
        psi
        sag
        springRate
        preloadAdjust
        notes
      }
      images {
        id
        url
      }
    }
  }
`;

/**
 * __usePartByIdQuery__
 *
 * To run a query within a React component, call `usePartByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `usePartByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePartByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePartByIdQuery(
  baseOptions: Apollo.QueryHookOptions<PartByIdQuery, PartByIdQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<PartByIdQuery, PartByIdQueryVariables>(
    PartByIdDocument,
    options
  );
}
export function usePartByIdLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    PartByIdQuery,
    PartByIdQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<PartByIdQuery, PartByIdQueryVariables>(
    PartByIdDocument,
    options
  );
}
export type PartByIdQueryHookResult = ReturnType<typeof usePartByIdQuery>;
export type PartByIdLazyQueryHookResult = ReturnType<
  typeof usePartByIdLazyQuery
>;
export type PartByIdQueryResult = Apollo.QueryResult<
  PartByIdQuery,
  PartByIdQueryVariables
>;
