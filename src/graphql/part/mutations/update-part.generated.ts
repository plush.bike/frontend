import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type UpdatePartMutationVariables = Types.Exact<{
  id: Types.Scalars["ID"];
  name?: Types.InputMaybe<Types.Scalars["String"]>;
  type?: Types.InputMaybe<Types.PartType>;
  manufacturer?: Types.InputMaybe<Types.ManufacturerBelongsTo>;
  specificationsRecord?: Types.InputMaybe<Types.UpdateSpecificationsRecordHasOne>;
  activeSettingsRecord?: Types.InputMaybe<Types.UpdateActiveSettingsRecordBelongsTo>;
  settingsRecords?: Types.InputMaybe<Types.UpdateSettingsRecordsHasMany>;
  images?: Types.InputMaybe<Types.UpdatePartImagesHasMany>;
}>;

export type UpdatePartMutation = {
  __typename?: "Mutation";
  updatePart?: {
    __typename?: "Part";
    id: string;
    name: string;
    type: Types.PartType;
    specificationsRecord?: {
      __typename?: "PartSpecificationsRecord";
      id: string;
    } | null;
    activeSettingsRecord?: {
      __typename?: "PartSettingsRecord";
      id: string;
    } | null;
    settingsRecords?: Array<{
      __typename?: "PartSettingsRecord";
      id: string;
    }> | null;
    images?: Array<{
      __typename?: "PartImage";
      id: string;
      url?: string | null;
    }> | null;
  } | null;
};

export const UpdatePartDocument = gql`
  mutation UpdatePart(
    $id: ID!
    $name: String
    $type: PartType
    $manufacturer: ManufacturerBelongsTo
    $specificationsRecord: UpdateSpecificationsRecordHasOne
    $activeSettingsRecord: UpdateActiveSettingsRecordBelongsTo
    $settingsRecords: UpdateSettingsRecordsHasMany
    $images: UpdatePartImagesHasMany
  ) {
    updatePart(
      input: {
        id: $id
        name: $name
        type: $type
        manufacturer: $manufacturer
        specificationsRecord: $specificationsRecord
        activeSettingsRecord: $activeSettingsRecord
        settingsRecords: $settingsRecords
        images: $images
      }
    ) {
      id
      name
      type
      specificationsRecord {
        id
      }
      activeSettingsRecord {
        id
      }
      settingsRecords {
        id
      }
      images {
        id
        url
      }
    }
  }
`;
export type UpdatePartMutationFn = Apollo.MutationFunction<
  UpdatePartMutation,
  UpdatePartMutationVariables
>;

/**
 * __useUpdatePartMutation__
 *
 * To run a mutation, you first call `useUpdatePartMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePartMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePartMutation, { data, loading, error }] = useUpdatePartMutation({
 *   variables: {
 *      id: // value for 'id'
 *      name: // value for 'name'
 *      type: // value for 'type'
 *      manufacturer: // value for 'manufacturer'
 *      specificationsRecord: // value for 'specificationsRecord'
 *      activeSettingsRecord: // value for 'activeSettingsRecord'
 *      settingsRecords: // value for 'settingsRecords'
 *      images: // value for 'images'
 *   },
 * });
 */
export function useUpdatePartMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UpdatePartMutation,
    UpdatePartMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdatePartMutation, UpdatePartMutationVariables>(
    UpdatePartDocument,
    options
  );
}
export type UpdatePartMutationHookResult = ReturnType<
  typeof useUpdatePartMutation
>;
export type UpdatePartMutationResult =
  Apollo.MutationResult<UpdatePartMutation>;
export type UpdatePartMutationOptions = Apollo.BaseMutationOptions<
  UpdatePartMutation,
  UpdatePartMutationVariables
>;
