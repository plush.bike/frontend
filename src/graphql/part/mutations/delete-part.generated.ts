import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type DeletePartMutationVariables = Types.Exact<{
  id: Types.Scalars["ID"];
}>;

export type DeletePartMutation = {
  __typename?: "Mutation";
  deletePart?: { __typename?: "Part"; id: string } | null;
};

export const DeletePartDocument = gql`
  mutation DeletePart($id: ID!) {
    deletePart(input: { id: $id }) {
      id
    }
  }
`;
export type DeletePartMutationFn = Apollo.MutationFunction<
  DeletePartMutation,
  DeletePartMutationVariables
>;

/**
 * __useDeletePartMutation__
 *
 * To run a mutation, you first call `useDeletePartMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeletePartMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deletePartMutation, { data, loading, error }] = useDeletePartMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeletePartMutation(
  baseOptions?: Apollo.MutationHookOptions<
    DeletePartMutation,
    DeletePartMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<DeletePartMutation, DeletePartMutationVariables>(
    DeletePartDocument,
    options
  );
}
export type DeletePartMutationHookResult = ReturnType<
  typeof useDeletePartMutation
>;
export type DeletePartMutationResult =
  Apollo.MutationResult<DeletePartMutation>;
export type DeletePartMutationOptions = Apollo.BaseMutationOptions<
  DeletePartMutation,
  DeletePartMutationVariables
>;
