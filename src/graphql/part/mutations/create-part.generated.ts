import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type CreatePartMutationVariables = Types.Exact<{
  name: Types.Scalars["String"];
  type: Types.PartType;
  manufacturer: Types.ManufacturerBelongsTo;
  images?: Types.InputMaybe<Types.CreatePartImagesHasMany>;
  specificationsRecord?: Types.InputMaybe<Types.CreateSpecificationsRecordHasOne>;
  settingsRecords?: Types.InputMaybe<Types.CreateSettingsRecordsHasMany>;
}>;

export type CreatePartMutation = {
  __typename?: "Mutation";
  createPart?: {
    __typename?: "Part";
    id: string;
    name: string;
    type: Types.PartType;
    specificationsRecord?: {
      __typename?: "PartSpecificationsRecord";
      id: string;
    } | null;
    settingsRecords?: Array<{
      __typename?: "PartSettingsRecord";
      id: string;
    }> | null;
    images?: Array<{
      __typename?: "PartImage";
      id: string;
      url?: string | null;
    }> | null;
  } | null;
};

export const CreatePartDocument = gql`
  mutation CreatePart(
    $name: String!
    $type: PartType!
    $manufacturer: ManufacturerBelongsTo!
    $images: CreatePartImagesHasMany
    $specificationsRecord: CreateSpecificationsRecordHasOne
    $settingsRecords: CreateSettingsRecordsHasMany
  ) {
    createPart(
      input: {
        name: $name
        type: $type
        manufacturer: $manufacturer
        images: $images
        specificationsRecord: $specificationsRecord
        settingsRecords: $settingsRecords
      }
    ) {
      id
      name
      type
      specificationsRecord {
        id
      }
      settingsRecords {
        id
      }
      images {
        id
        url
      }
    }
  }
`;
export type CreatePartMutationFn = Apollo.MutationFunction<
  CreatePartMutation,
  CreatePartMutationVariables
>;

/**
 * __useCreatePartMutation__
 *
 * To run a mutation, you first call `useCreatePartMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePartMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPartMutation, { data, loading, error }] = useCreatePartMutation({
 *   variables: {
 *      name: // value for 'name'
 *      type: // value for 'type'
 *      manufacturer: // value for 'manufacturer'
 *      images: // value for 'images'
 *      specificationsRecord: // value for 'specificationsRecord'
 *      settingsRecords: // value for 'settingsRecords'
 *   },
 * });
 */
export function useCreatePartMutation(
  baseOptions?: Apollo.MutationHookOptions<
    CreatePartMutation,
    CreatePartMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreatePartMutation, CreatePartMutationVariables>(
    CreatePartDocument,
    options
  );
}
export type CreatePartMutationHookResult = ReturnType<
  typeof useCreatePartMutation
>;
export type CreatePartMutationResult =
  Apollo.MutationResult<CreatePartMutation>;
export type CreatePartMutationOptions = Apollo.BaseMutationOptions<
  CreatePartMutation,
  CreatePartMutationVariables
>;
