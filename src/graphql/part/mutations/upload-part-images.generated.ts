import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type UploadPartImagesMutationVariables = Types.Exact<{
  files: Array<Types.Scalars["Upload"]> | Types.Scalars["Upload"];
}>;

export type UploadPartImagesMutation = {
  __typename?: "Mutation";
  uploadPartImages?: Array<{
    __typename?: "UploadPartImagesResponse";
    src: string;
  } | null> | null;
};

export const UploadPartImagesDocument = gql`
  mutation UploadPartImages($files: [Upload!]!) {
    uploadPartImages(input: { files: $files }) {
      src
    }
  }
`;
export type UploadPartImagesMutationFn = Apollo.MutationFunction<
  UploadPartImagesMutation,
  UploadPartImagesMutationVariables
>;

/**
 * __useUploadPartImagesMutation__
 *
 * To run a mutation, you first call `useUploadPartImagesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadPartImagesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadPartImagesMutation, { data, loading, error }] = useUploadPartImagesMutation({
 *   variables: {
 *      files: // value for 'files'
 *   },
 * });
 */
export function useUploadPartImagesMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UploadPartImagesMutation,
    UploadPartImagesMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    UploadPartImagesMutation,
    UploadPartImagesMutationVariables
  >(UploadPartImagesDocument, options);
}
export type UploadPartImagesMutationHookResult = ReturnType<
  typeof useUploadPartImagesMutation
>;
export type UploadPartImagesMutationResult =
  Apollo.MutationResult<UploadPartImagesMutation>;
export type UploadPartImagesMutationOptions = Apollo.BaseMutationOptions<
  UploadPartImagesMutation,
  UploadPartImagesMutationVariables
>;
