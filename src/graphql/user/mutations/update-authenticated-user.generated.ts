import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import { UserBasicFragmentDoc } from "../../fragments/user-basic.generated";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type UpdateAuthenticatedUserMutationVariables = Types.Exact<{
  name?: Types.InputMaybe<Types.Scalars["String"]>;
  email?: Types.InputMaybe<Types.Scalars["String"]>;
  profileImage?: Types.InputMaybe<Types.Scalars["Upload"]>;
}>;

export type UpdateAuthenticatedUserMutation = {
  __typename?: "Mutation";
  updateAuthenticatedUser?: {
    __typename?: "User";
    id: string;
    name: string;
    email?: string | null;
    profileImageUrl?: string | null;
    hasEnabledTwoFactorAuthentication?: boolean | null;
    hasPasswordSet?: boolean | null;
    createdAt: any;
    updatedAt: any;
  } | null;
};

export const UpdateAuthenticatedUserDocument = gql`
  mutation UpdateAuthenticatedUser(
    $name: String
    $email: String
    $profileImage: Upload
  ) {
    updateAuthenticatedUser(
      input: { name: $name, email: $email, profileImage: $profileImage }
    ) {
      ...UserBasic
    }
  }
  ${UserBasicFragmentDoc}
`;
export type UpdateAuthenticatedUserMutationFn = Apollo.MutationFunction<
  UpdateAuthenticatedUserMutation,
  UpdateAuthenticatedUserMutationVariables
>;

/**
 * __useUpdateAuthenticatedUserMutation__
 *
 * To run a mutation, you first call `useUpdateAuthenticatedUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAuthenticatedUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAuthenticatedUserMutation, { data, loading, error }] = useUpdateAuthenticatedUserMutation({
 *   variables: {
 *      name: // value for 'name'
 *      email: // value for 'email'
 *      profileImage: // value for 'profileImage'
 *   },
 * });
 */
export function useUpdateAuthenticatedUserMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UpdateAuthenticatedUserMutation,
    UpdateAuthenticatedUserMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    UpdateAuthenticatedUserMutation,
    UpdateAuthenticatedUserMutationVariables
  >(UpdateAuthenticatedUserDocument, options);
}
export type UpdateAuthenticatedUserMutationHookResult = ReturnType<
  typeof useUpdateAuthenticatedUserMutation
>;
export type UpdateAuthenticatedUserMutationResult =
  Apollo.MutationResult<UpdateAuthenticatedUserMutation>;
export type UpdateAuthenticatedUserMutationOptions = Apollo.BaseMutationOptions<
  UpdateAuthenticatedUserMutation,
  UpdateAuthenticatedUserMutationVariables
>;
