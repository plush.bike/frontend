import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import { UserBasicFragmentDoc } from "../../../fragments/user-basic.generated";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type SocialProviderCallbackMutationVariables = Types.Exact<{
  code: Types.Scalars["String"];
  provider: Types.SocialProviderType;
}>;

export type SocialProviderCallbackMutation = {
  __typename?: "Mutation";
  socialProviderCallback: {
    __typename?: "SocialProviderCallbackResponse";
    twoFactor?: boolean | null;
    callbackType: Types.SocialProviderCallbackType;
    user?: {
      __typename?: "User";
      id: string;
      name: string;
      email?: string | null;
      profileImageUrl?: string | null;
      hasEnabledTwoFactorAuthentication?: boolean | null;
      hasPasswordSet?: boolean | null;
      createdAt: any;
      updatedAt: any;
    } | null;
  };
};

export const SocialProviderCallbackDocument = gql`
  mutation SocialProviderCallback(
    $code: String!
    $provider: SocialProviderType!
  ) {
    socialProviderCallback(input: { code: $code, provider: $provider }) {
      user {
        ...UserBasic
      }
      twoFactor
      callbackType
    }
  }
  ${UserBasicFragmentDoc}
`;
export type SocialProviderCallbackMutationFn = Apollo.MutationFunction<
  SocialProviderCallbackMutation,
  SocialProviderCallbackMutationVariables
>;

/**
 * __useSocialProviderCallbackMutation__
 *
 * To run a mutation, you first call `useSocialProviderCallbackMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSocialProviderCallbackMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [socialProviderCallbackMutation, { data, loading, error }] = useSocialProviderCallbackMutation({
 *   variables: {
 *      code: // value for 'code'
 *      provider: // value for 'provider'
 *   },
 * });
 */
export function useSocialProviderCallbackMutation(
  baseOptions?: Apollo.MutationHookOptions<
    SocialProviderCallbackMutation,
    SocialProviderCallbackMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    SocialProviderCallbackMutation,
    SocialProviderCallbackMutationVariables
  >(SocialProviderCallbackDocument, options);
}
export type SocialProviderCallbackMutationHookResult = ReturnType<
  typeof useSocialProviderCallbackMutation
>;
export type SocialProviderCallbackMutationResult =
  Apollo.MutationResult<SocialProviderCallbackMutation>;
export type SocialProviderCallbackMutationOptions = Apollo.BaseMutationOptions<
  SocialProviderCallbackMutation,
  SocialProviderCallbackMutationVariables
>;
