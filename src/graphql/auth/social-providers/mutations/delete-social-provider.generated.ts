import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type DeleteSocialProviderMutationVariables = Types.Exact<{
  id: Types.Scalars["ID"];
}>;

export type DeleteSocialProviderMutation = {
  __typename?: "Mutation";
  deleteSocialProvider?: {
    __typename?: "SocialProvider";
    id?: string | null;
    provider?: Types.SocialProviderType | null;
  } | null;
};

export const DeleteSocialProviderDocument = gql`
  mutation DeleteSocialProvider($id: ID!) {
    deleteSocialProvider(input: { id: $id }) {
      id
      provider
    }
  }
`;
export type DeleteSocialProviderMutationFn = Apollo.MutationFunction<
  DeleteSocialProviderMutation,
  DeleteSocialProviderMutationVariables
>;

/**
 * __useDeleteSocialProviderMutation__
 *
 * To run a mutation, you first call `useDeleteSocialProviderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSocialProviderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSocialProviderMutation, { data, loading, error }] = useDeleteSocialProviderMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteSocialProviderMutation(
  baseOptions?: Apollo.MutationHookOptions<
    DeleteSocialProviderMutation,
    DeleteSocialProviderMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    DeleteSocialProviderMutation,
    DeleteSocialProviderMutationVariables
  >(DeleteSocialProviderDocument, options);
}
export type DeleteSocialProviderMutationHookResult = ReturnType<
  typeof useDeleteSocialProviderMutation
>;
export type DeleteSocialProviderMutationResult =
  Apollo.MutationResult<DeleteSocialProviderMutation>;
export type DeleteSocialProviderMutationOptions = Apollo.BaseMutationOptions<
  DeleteSocialProviderMutation,
  DeleteSocialProviderMutationVariables
>;
