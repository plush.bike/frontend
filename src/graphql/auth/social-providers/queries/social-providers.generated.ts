import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type SocialProvidersQueryVariables = Types.Exact<{
  [key: string]: never;
}>;

export type SocialProvidersQuery = {
  __typename?: "Query";
  socialProviders: Array<{
    __typename?: "SocialProvider";
    id?: string | null;
    provider?: Types.SocialProviderType | null;
  }>;
};

export const SocialProvidersDocument = gql`
  query SocialProviders {
    socialProviders {
      id
      provider
    }
  }
`;

/**
 * __useSocialProvidersQuery__
 *
 * To run a query within a React component, call `useSocialProvidersQuery` and pass it any options that fit your needs.
 * When your component renders, `useSocialProvidersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSocialProvidersQuery({
 *   variables: {
 *   },
 * });
 */
export function useSocialProvidersQuery(
  baseOptions?: Apollo.QueryHookOptions<
    SocialProvidersQuery,
    SocialProvidersQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<SocialProvidersQuery, SocialProvidersQueryVariables>(
    SocialProvidersDocument,
    options
  );
}
export function useSocialProvidersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    SocialProvidersQuery,
    SocialProvidersQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    SocialProvidersQuery,
    SocialProvidersQueryVariables
  >(SocialProvidersDocument, options);
}
export type SocialProvidersQueryHookResult = ReturnType<
  typeof useSocialProvidersQuery
>;
export type SocialProvidersLazyQueryHookResult = ReturnType<
  typeof useSocialProvidersLazyQuery
>;
export type SocialProvidersQueryResult = Apollo.QueryResult<
  SocialProvidersQuery,
  SocialProvidersQueryVariables
>;
