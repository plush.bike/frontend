import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type TwoFactorAuthenticationQrCodeMutationVariables = Types.Exact<{
  [key: string]: never;
}>;

export type TwoFactorAuthenticationQrCodeMutation = {
  __typename?: "Mutation";
  twoFactorAuthenticationQrCode: {
    __typename?: "TwoFactorAuthenticationQrCodeResponse";
    svg: string;
  };
};

export const TwoFactorAuthenticationQrCodeDocument = gql`
  mutation TwoFactorAuthenticationQrCode {
    twoFactorAuthenticationQrCode {
      svg
    }
  }
`;
export type TwoFactorAuthenticationQrCodeMutationFn = Apollo.MutationFunction<
  TwoFactorAuthenticationQrCodeMutation,
  TwoFactorAuthenticationQrCodeMutationVariables
>;

/**
 * __useTwoFactorAuthenticationQrCodeMutation__
 *
 * To run a mutation, you first call `useTwoFactorAuthenticationQrCodeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useTwoFactorAuthenticationQrCodeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [twoFactorAuthenticationQrCodeMutation, { data, loading, error }] = useTwoFactorAuthenticationQrCodeMutation({
 *   variables: {
 *   },
 * });
 */
export function useTwoFactorAuthenticationQrCodeMutation(
  baseOptions?: Apollo.MutationHookOptions<
    TwoFactorAuthenticationQrCodeMutation,
    TwoFactorAuthenticationQrCodeMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    TwoFactorAuthenticationQrCodeMutation,
    TwoFactorAuthenticationQrCodeMutationVariables
  >(TwoFactorAuthenticationQrCodeDocument, options);
}
export type TwoFactorAuthenticationQrCodeMutationHookResult = ReturnType<
  typeof useTwoFactorAuthenticationQrCodeMutation
>;
export type TwoFactorAuthenticationQrCodeMutationResult =
  Apollo.MutationResult<TwoFactorAuthenticationQrCodeMutation>;
export type TwoFactorAuthenticationQrCodeMutationOptions =
  Apollo.BaseMutationOptions<
    TwoFactorAuthenticationQrCodeMutation,
    TwoFactorAuthenticationQrCodeMutationVariables
  >;
