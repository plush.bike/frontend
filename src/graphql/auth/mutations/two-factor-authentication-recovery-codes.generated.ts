import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type TwoFactorAuthenticationRecoveryCodesMutationVariables =
  Types.Exact<{ [key: string]: never }>;

export type TwoFactorAuthenticationRecoveryCodesMutation = {
  __typename?: "Mutation";
  twoFactorAuthenticationRecoveryCodes: {
    __typename?: "TwoFactorAuthenticationRecoveryCodesResponse";
    recoveryCodes?: Array<string | null> | null;
  };
};

export const TwoFactorAuthenticationRecoveryCodesDocument = gql`
  mutation TwoFactorAuthenticationRecoveryCodes {
    twoFactorAuthenticationRecoveryCodes {
      recoveryCodes
    }
  }
`;
export type TwoFactorAuthenticationRecoveryCodesMutationFn =
  Apollo.MutationFunction<
    TwoFactorAuthenticationRecoveryCodesMutation,
    TwoFactorAuthenticationRecoveryCodesMutationVariables
  >;

/**
 * __useTwoFactorAuthenticationRecoveryCodesMutation__
 *
 * To run a mutation, you first call `useTwoFactorAuthenticationRecoveryCodesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useTwoFactorAuthenticationRecoveryCodesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [twoFactorAuthenticationRecoveryCodesMutation, { data, loading, error }] = useTwoFactorAuthenticationRecoveryCodesMutation({
 *   variables: {
 *   },
 * });
 */
export function useTwoFactorAuthenticationRecoveryCodesMutation(
  baseOptions?: Apollo.MutationHookOptions<
    TwoFactorAuthenticationRecoveryCodesMutation,
    TwoFactorAuthenticationRecoveryCodesMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    TwoFactorAuthenticationRecoveryCodesMutation,
    TwoFactorAuthenticationRecoveryCodesMutationVariables
  >(TwoFactorAuthenticationRecoveryCodesDocument, options);
}
export type TwoFactorAuthenticationRecoveryCodesMutationHookResult = ReturnType<
  typeof useTwoFactorAuthenticationRecoveryCodesMutation
>;
export type TwoFactorAuthenticationRecoveryCodesMutationResult =
  Apollo.MutationResult<TwoFactorAuthenticationRecoveryCodesMutation>;
export type TwoFactorAuthenticationRecoveryCodesMutationOptions =
  Apollo.BaseMutationOptions<
    TwoFactorAuthenticationRecoveryCodesMutation,
    TwoFactorAuthenticationRecoveryCodesMutationVariables
  >;
