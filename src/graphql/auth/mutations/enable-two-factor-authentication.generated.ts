import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type EnableTwoFactorAuthenticationMutationVariables = Types.Exact<{
  [key: string]: never;
}>;

export type EnableTwoFactorAuthenticationMutation = {
  __typename?: "Mutation";
  enableTwoFactorAuthentication: {
    __typename?: "EnableTwoFactorAuthenticationResponse";
    status: string;
    message: string;
  };
};

export const EnableTwoFactorAuthenticationDocument = gql`
  mutation EnableTwoFactorAuthentication {
    enableTwoFactorAuthentication {
      status
      message
    }
  }
`;
export type EnableTwoFactorAuthenticationMutationFn = Apollo.MutationFunction<
  EnableTwoFactorAuthenticationMutation,
  EnableTwoFactorAuthenticationMutationVariables
>;

/**
 * __useEnableTwoFactorAuthenticationMutation__
 *
 * To run a mutation, you first call `useEnableTwoFactorAuthenticationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEnableTwoFactorAuthenticationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [enableTwoFactorAuthenticationMutation, { data, loading, error }] = useEnableTwoFactorAuthenticationMutation({
 *   variables: {
 *   },
 * });
 */
export function useEnableTwoFactorAuthenticationMutation(
  baseOptions?: Apollo.MutationHookOptions<
    EnableTwoFactorAuthenticationMutation,
    EnableTwoFactorAuthenticationMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    EnableTwoFactorAuthenticationMutation,
    EnableTwoFactorAuthenticationMutationVariables
  >(EnableTwoFactorAuthenticationDocument, options);
}
export type EnableTwoFactorAuthenticationMutationHookResult = ReturnType<
  typeof useEnableTwoFactorAuthenticationMutation
>;
export type EnableTwoFactorAuthenticationMutationResult =
  Apollo.MutationResult<EnableTwoFactorAuthenticationMutation>;
export type EnableTwoFactorAuthenticationMutationOptions =
  Apollo.BaseMutationOptions<
    EnableTwoFactorAuthenticationMutation,
    EnableTwoFactorAuthenticationMutationVariables
  >;
