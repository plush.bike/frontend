import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type ConfirmPasswordMutationVariables = Types.Exact<{
  password: Types.Scalars["String"];
}>;

export type ConfirmPasswordMutation = {
  __typename?: "Mutation";
  confirmPassword: {
    __typename?: "ConfirmPasswordResponse";
    status: string;
    message: string;
  };
};

export const ConfirmPasswordDocument = gql`
  mutation ConfirmPassword($password: String!) {
    confirmPassword(input: { password: $password }) {
      status
      message
    }
  }
`;
export type ConfirmPasswordMutationFn = Apollo.MutationFunction<
  ConfirmPasswordMutation,
  ConfirmPasswordMutationVariables
>;

/**
 * __useConfirmPasswordMutation__
 *
 * To run a mutation, you first call `useConfirmPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmPasswordMutation, { data, loading, error }] = useConfirmPasswordMutation({
 *   variables: {
 *      password: // value for 'password'
 *   },
 * });
 */
export function useConfirmPasswordMutation(
  baseOptions?: Apollo.MutationHookOptions<
    ConfirmPasswordMutation,
    ConfirmPasswordMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    ConfirmPasswordMutation,
    ConfirmPasswordMutationVariables
  >(ConfirmPasswordDocument, options);
}
export type ConfirmPasswordMutationHookResult = ReturnType<
  typeof useConfirmPasswordMutation
>;
export type ConfirmPasswordMutationResult =
  Apollo.MutationResult<ConfirmPasswordMutation>;
export type ConfirmPasswordMutationOptions = Apollo.BaseMutationOptions<
  ConfirmPasswordMutation,
  ConfirmPasswordMutationVariables
>;
