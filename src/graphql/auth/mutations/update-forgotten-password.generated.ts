import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type UpdateForgottenPasswordMutationVariables = Types.Exact<{
  email: Types.Scalars["String"];
  token: Types.Scalars["String"];
  password: Types.Scalars["String"];
  passwordConfirmation: Types.Scalars["String"];
}>;

export type UpdateForgottenPasswordMutation = {
  __typename?: "Mutation";
  updateForgottenPassword: {
    __typename?: "UpdateForgottenPasswordResponse";
    status: string;
    message: string;
  };
};

export const UpdateForgottenPasswordDocument = gql`
  mutation UpdateForgottenPassword(
    $email: String!
    $token: String!
    $password: String!
    $passwordConfirmation: String!
  ) {
    updateForgottenPassword(
      input: {
        email: $email
        token: $token
        password: $password
        password_confirmation: $passwordConfirmation
      }
    ) {
      status
      message
    }
  }
`;
export type UpdateForgottenPasswordMutationFn = Apollo.MutationFunction<
  UpdateForgottenPasswordMutation,
  UpdateForgottenPasswordMutationVariables
>;

/**
 * __useUpdateForgottenPasswordMutation__
 *
 * To run a mutation, you first call `useUpdateForgottenPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateForgottenPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateForgottenPasswordMutation, { data, loading, error }] = useUpdateForgottenPasswordMutation({
 *   variables: {
 *      email: // value for 'email'
 *      token: // value for 'token'
 *      password: // value for 'password'
 *      passwordConfirmation: // value for 'passwordConfirmation'
 *   },
 * });
 */
export function useUpdateForgottenPasswordMutation(
  baseOptions?: Apollo.MutationHookOptions<
    UpdateForgottenPasswordMutation,
    UpdateForgottenPasswordMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    UpdateForgottenPasswordMutation,
    UpdateForgottenPasswordMutationVariables
  >(UpdateForgottenPasswordDocument, options);
}
export type UpdateForgottenPasswordMutationHookResult = ReturnType<
  typeof useUpdateForgottenPasswordMutation
>;
export type UpdateForgottenPasswordMutationResult =
  Apollo.MutationResult<UpdateForgottenPasswordMutation>;
export type UpdateForgottenPasswordMutationOptions = Apollo.BaseMutationOptions<
  UpdateForgottenPasswordMutation,
  UpdateForgottenPasswordMutationVariables
>;
