import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import { UserBasicFragmentDoc } from "../../fragments/user-basic.generated";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type TwoFactorAuthenticationChallengeMutationVariables = Types.Exact<{
  code?: Types.InputMaybe<Types.Scalars["String"]>;
  recoveryCode?: Types.InputMaybe<Types.Scalars["String"]>;
}>;

export type TwoFactorAuthenticationChallengeMutation = {
  __typename?: "Mutation";
  twoFactorAuthenticationChallenge: {
    __typename?: "TwoFactorAuthenticationChallengeResponse";
    user?: {
      __typename?: "User";
      id: string;
      name: string;
      email?: string | null;
      profileImageUrl?: string | null;
      hasEnabledTwoFactorAuthentication?: boolean | null;
      hasPasswordSet?: boolean | null;
      createdAt: any;
      updatedAt: any;
    } | null;
  };
};

export const TwoFactorAuthenticationChallengeDocument = gql`
  mutation TwoFactorAuthenticationChallenge(
    $code: String
    $recoveryCode: String
  ) {
    twoFactorAuthenticationChallenge(
      input: { code: $code, recoveryCode: $recoveryCode }
    ) {
      user {
        ...UserBasic
      }
    }
  }
  ${UserBasicFragmentDoc}
`;
export type TwoFactorAuthenticationChallengeMutationFn =
  Apollo.MutationFunction<
    TwoFactorAuthenticationChallengeMutation,
    TwoFactorAuthenticationChallengeMutationVariables
  >;

/**
 * __useTwoFactorAuthenticationChallengeMutation__
 *
 * To run a mutation, you first call `useTwoFactorAuthenticationChallengeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useTwoFactorAuthenticationChallengeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [twoFactorAuthenticationChallengeMutation, { data, loading, error }] = useTwoFactorAuthenticationChallengeMutation({
 *   variables: {
 *      code: // value for 'code'
 *      recoveryCode: // value for 'recoveryCode'
 *   },
 * });
 */
export function useTwoFactorAuthenticationChallengeMutation(
  baseOptions?: Apollo.MutationHookOptions<
    TwoFactorAuthenticationChallengeMutation,
    TwoFactorAuthenticationChallengeMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    TwoFactorAuthenticationChallengeMutation,
    TwoFactorAuthenticationChallengeMutationVariables
  >(TwoFactorAuthenticationChallengeDocument, options);
}
export type TwoFactorAuthenticationChallengeMutationHookResult = ReturnType<
  typeof useTwoFactorAuthenticationChallengeMutation
>;
export type TwoFactorAuthenticationChallengeMutationResult =
  Apollo.MutationResult<TwoFactorAuthenticationChallengeMutation>;
export type TwoFactorAuthenticationChallengeMutationOptions =
  Apollo.BaseMutationOptions<
    TwoFactorAuthenticationChallengeMutation,
    TwoFactorAuthenticationChallengeMutationVariables
  >;
