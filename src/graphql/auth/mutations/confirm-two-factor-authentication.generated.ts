import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type ConfirmTwoFactorAuthenticationMutationVariables = Types.Exact<{
  code: Types.Scalars["String"];
}>;

export type ConfirmTwoFactorAuthenticationMutation = {
  __typename?: "Mutation";
  confirmTwoFactorAuthentication: {
    __typename?: "ConfirmTwoFactorAuthenticationResponse";
    status: string;
    message: string;
    recoveryCodes?: Array<string | null> | null;
  };
};

export const ConfirmTwoFactorAuthenticationDocument = gql`
  mutation ConfirmTwoFactorAuthentication($code: String!) {
    confirmTwoFactorAuthentication(input: { code: $code }) {
      status
      message
      recoveryCodes
    }
  }
`;
export type ConfirmTwoFactorAuthenticationMutationFn = Apollo.MutationFunction<
  ConfirmTwoFactorAuthenticationMutation,
  ConfirmTwoFactorAuthenticationMutationVariables
>;

/**
 * __useConfirmTwoFactorAuthenticationMutation__
 *
 * To run a mutation, you first call `useConfirmTwoFactorAuthenticationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmTwoFactorAuthenticationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmTwoFactorAuthenticationMutation, { data, loading, error }] = useConfirmTwoFactorAuthenticationMutation({
 *   variables: {
 *      code: // value for 'code'
 *   },
 * });
 */
export function useConfirmTwoFactorAuthenticationMutation(
  baseOptions?: Apollo.MutationHookOptions<
    ConfirmTwoFactorAuthenticationMutation,
    ConfirmTwoFactorAuthenticationMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    ConfirmTwoFactorAuthenticationMutation,
    ConfirmTwoFactorAuthenticationMutationVariables
  >(ConfirmTwoFactorAuthenticationDocument, options);
}
export type ConfirmTwoFactorAuthenticationMutationHookResult = ReturnType<
  typeof useConfirmTwoFactorAuthenticationMutation
>;
export type ConfirmTwoFactorAuthenticationMutationResult =
  Apollo.MutationResult<ConfirmTwoFactorAuthenticationMutation>;
export type ConfirmTwoFactorAuthenticationMutationOptions =
  Apollo.BaseMutationOptions<
    ConfirmTwoFactorAuthenticationMutation,
    ConfirmTwoFactorAuthenticationMutationVariables
  >;
