import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type DisableTwoFactorAuthenticationMutationVariables = Types.Exact<{
  [key: string]: never;
}>;

export type DisableTwoFactorAuthenticationMutation = {
  __typename?: "Mutation";
  disableTwoFactorAuthentication: {
    __typename?: "DisableTwoFactorAuthenticationResponse";
    status: string;
    message: string;
  };
};

export const DisableTwoFactorAuthenticationDocument = gql`
  mutation DisableTwoFactorAuthentication {
    disableTwoFactorAuthentication {
      status
      message
    }
  }
`;
export type DisableTwoFactorAuthenticationMutationFn = Apollo.MutationFunction<
  DisableTwoFactorAuthenticationMutation,
  DisableTwoFactorAuthenticationMutationVariables
>;

/**
 * __useDisableTwoFactorAuthenticationMutation__
 *
 * To run a mutation, you first call `useDisableTwoFactorAuthenticationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDisableTwoFactorAuthenticationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [disableTwoFactorAuthenticationMutation, { data, loading, error }] = useDisableTwoFactorAuthenticationMutation({
 *   variables: {
 *   },
 * });
 */
export function useDisableTwoFactorAuthenticationMutation(
  baseOptions?: Apollo.MutationHookOptions<
    DisableTwoFactorAuthenticationMutation,
    DisableTwoFactorAuthenticationMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    DisableTwoFactorAuthenticationMutation,
    DisableTwoFactorAuthenticationMutationVariables
  >(DisableTwoFactorAuthenticationDocument, options);
}
export type DisableTwoFactorAuthenticationMutationHookResult = ReturnType<
  typeof useDisableTwoFactorAuthenticationMutation
>;
export type DisableTwoFactorAuthenticationMutationResult =
  Apollo.MutationResult<DisableTwoFactorAuthenticationMutation>;
export type DisableTwoFactorAuthenticationMutationOptions =
  Apollo.BaseMutationOptions<
    DisableTwoFactorAuthenticationMutation,
    DisableTwoFactorAuthenticationMutationVariables
  >;
