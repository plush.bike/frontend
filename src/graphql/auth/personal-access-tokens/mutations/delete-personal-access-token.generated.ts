import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type DeletePersonalAccessTokenMutationVariables = Types.Exact<{
  id: Types.Scalars["ID"];
}>;

export type DeletePersonalAccessTokenMutation = {
  __typename?: "Mutation";
  deletePersonalAccessToken: {
    __typename?: "PersonalAccessToken";
    id: string;
    name: string;
    lastUsedAt?: any | null;
    createdAt: any;
    updatedAt: any;
  };
};

export const DeletePersonalAccessTokenDocument = gql`
  mutation DeletePersonalAccessToken($id: ID!) {
    deletePersonalAccessToken(input: { id: $id }) {
      id
      name
      lastUsedAt
      createdAt
      updatedAt
    }
  }
`;
export type DeletePersonalAccessTokenMutationFn = Apollo.MutationFunction<
  DeletePersonalAccessTokenMutation,
  DeletePersonalAccessTokenMutationVariables
>;

/**
 * __useDeletePersonalAccessTokenMutation__
 *
 * To run a mutation, you first call `useDeletePersonalAccessTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeletePersonalAccessTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deletePersonalAccessTokenMutation, { data, loading, error }] = useDeletePersonalAccessTokenMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeletePersonalAccessTokenMutation(
  baseOptions?: Apollo.MutationHookOptions<
    DeletePersonalAccessTokenMutation,
    DeletePersonalAccessTokenMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    DeletePersonalAccessTokenMutation,
    DeletePersonalAccessTokenMutationVariables
  >(DeletePersonalAccessTokenDocument, options);
}
export type DeletePersonalAccessTokenMutationHookResult = ReturnType<
  typeof useDeletePersonalAccessTokenMutation
>;
export type DeletePersonalAccessTokenMutationResult =
  Apollo.MutationResult<DeletePersonalAccessTokenMutation>;
export type DeletePersonalAccessTokenMutationOptions =
  Apollo.BaseMutationOptions<
    DeletePersonalAccessTokenMutation,
    DeletePersonalAccessTokenMutationVariables
  >;
