import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type CreatePersonalAccessTokenMutationVariables = Types.Exact<{
  name: Types.Scalars["String"];
}>;

export type CreatePersonalAccessTokenMutation = {
  __typename?: "Mutation";
  createPersonalAccessToken: {
    __typename?: "CreatePersonalAccessTokenResponse";
    id: string;
    name: string;
    token: string;
    lastUsedAt?: any | null;
    createdAt: any;
    updatedAt: any;
  };
};

export const CreatePersonalAccessTokenDocument = gql`
  mutation CreatePersonalAccessToken($name: String!) {
    createPersonalAccessToken(input: { name: $name }) {
      id
      name
      token
      lastUsedAt
      createdAt
      updatedAt
    }
  }
`;
export type CreatePersonalAccessTokenMutationFn = Apollo.MutationFunction<
  CreatePersonalAccessTokenMutation,
  CreatePersonalAccessTokenMutationVariables
>;

/**
 * __useCreatePersonalAccessTokenMutation__
 *
 * To run a mutation, you first call `useCreatePersonalAccessTokenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePersonalAccessTokenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPersonalAccessTokenMutation, { data, loading, error }] = useCreatePersonalAccessTokenMutation({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCreatePersonalAccessTokenMutation(
  baseOptions?: Apollo.MutationHookOptions<
    CreatePersonalAccessTokenMutation,
    CreatePersonalAccessTokenMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    CreatePersonalAccessTokenMutation,
    CreatePersonalAccessTokenMutationVariables
  >(CreatePersonalAccessTokenDocument, options);
}
export type CreatePersonalAccessTokenMutationHookResult = ReturnType<
  typeof useCreatePersonalAccessTokenMutation
>;
export type CreatePersonalAccessTokenMutationResult =
  Apollo.MutationResult<CreatePersonalAccessTokenMutation>;
export type CreatePersonalAccessTokenMutationOptions =
  Apollo.BaseMutationOptions<
    CreatePersonalAccessTokenMutation,
    CreatePersonalAccessTokenMutationVariables
  >;
