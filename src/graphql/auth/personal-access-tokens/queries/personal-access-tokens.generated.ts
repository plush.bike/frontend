import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type PersonalAccessTokensQueryVariables = Types.Exact<{
  first?: Types.InputMaybe<Types.Scalars["Int"]>;
  page?: Types.InputMaybe<Types.Scalars["Int"]>;
}>;

export type PersonalAccessTokensQuery = {
  __typename?: "Query";
  personalAccessTokens: {
    __typename?: "PersonalAccessTokenPaginator";
    data: Array<{
      __typename?: "PersonalAccessToken";
      id: string;
      name: string;
      lastUsedAt?: any | null;
      createdAt: any;
    }>;
    paginatorInfo: {
      __typename?: "PaginatorInfo";
      lastItem?: number | null;
      total: number;
    };
  };
};

export const PersonalAccessTokensDocument = gql`
  query PersonalAccessTokens($first: Int = 25, $page: Int = 1) {
    personalAccessTokens(first: $first, page: $page) {
      data {
        id
        name
        lastUsedAt
        createdAt
      }
      paginatorInfo {
        lastItem
        total
      }
    }
  }
`;

/**
 * __usePersonalAccessTokensQuery__
 *
 * To run a query within a React component, call `usePersonalAccessTokensQuery` and pass it any options that fit your needs.
 * When your component renders, `usePersonalAccessTokensQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePersonalAccessTokensQuery({
 *   variables: {
 *      first: // value for 'first'
 *      page: // value for 'page'
 *   },
 * });
 */
export function usePersonalAccessTokensQuery(
  baseOptions?: Apollo.QueryHookOptions<
    PersonalAccessTokensQuery,
    PersonalAccessTokensQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<
    PersonalAccessTokensQuery,
    PersonalAccessTokensQueryVariables
  >(PersonalAccessTokensDocument, options);
}
export function usePersonalAccessTokensLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    PersonalAccessTokensQuery,
    PersonalAccessTokensQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    PersonalAccessTokensQuery,
    PersonalAccessTokensQueryVariables
  >(PersonalAccessTokensDocument, options);
}
export type PersonalAccessTokensQueryHookResult = ReturnType<
  typeof usePersonalAccessTokensQuery
>;
export type PersonalAccessTokensLazyQueryHookResult = ReturnType<
  typeof usePersonalAccessTokensLazyQuery
>;
export type PersonalAccessTokensQueryResult = Apollo.QueryResult<
  PersonalAccessTokensQuery,
  PersonalAccessTokensQueryVariables
>;
