import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import { UserBasicFragmentDoc } from "../../fragments/user-basic.generated";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type AuthenticatedUserQueryVariables = Types.Exact<{
  [key: string]: never;
}>;

export type AuthenticatedUserQuery = {
  __typename?: "Query";
  authenticatedUser?: {
    __typename?: "User";
    id: string;
    name: string;
    email?: string | null;
    profileImageUrl?: string | null;
    hasEnabledTwoFactorAuthentication?: boolean | null;
    hasPasswordSet?: boolean | null;
    createdAt: any;
    updatedAt: any;
  } | null;
};

export const AuthenticatedUserDocument = gql`
  query AuthenticatedUser {
    authenticatedUser {
      ...UserBasic
    }
  }
  ${UserBasicFragmentDoc}
`;

/**
 * __useAuthenticatedUserQuery__
 *
 * To run a query within a React component, call `useAuthenticatedUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useAuthenticatedUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAuthenticatedUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useAuthenticatedUserQuery(
  baseOptions?: Apollo.QueryHookOptions<
    AuthenticatedUserQuery,
    AuthenticatedUserQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<
    AuthenticatedUserQuery,
    AuthenticatedUserQueryVariables
  >(AuthenticatedUserDocument, options);
}
export function useAuthenticatedUserLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    AuthenticatedUserQuery,
    AuthenticatedUserQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    AuthenticatedUserQuery,
    AuthenticatedUserQueryVariables
  >(AuthenticatedUserDocument, options);
}
export type AuthenticatedUserQueryHookResult = ReturnType<
  typeof useAuthenticatedUserQuery
>;
export type AuthenticatedUserLazyQueryHookResult = ReturnType<
  typeof useAuthenticatedUserLazyQuery
>;
export type AuthenticatedUserQueryResult = Apollo.QueryResult<
  AuthenticatedUserQuery,
  AuthenticatedUserQueryVariables
>;
