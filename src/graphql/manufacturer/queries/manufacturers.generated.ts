import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
const defaultOptions = {} as const;
export type ManufacturersQueryVariables = Types.Exact<{
  first?: Types.InputMaybe<Types.Scalars["Int"]>;
}>;

export type ManufacturersQuery = {
  __typename?: "Query";
  manufacturers: {
    __typename?: "ManufacturerPaginator";
    data: Array<{ __typename?: "Manufacturer"; id: string; name: string }>;
  };
};

export const ManufacturersDocument = gql`
  query Manufacturers($first: Int = 25) {
    manufacturers(first: $first) {
      data {
        id
        name
      }
    }
  }
`;

/**
 * __useManufacturersQuery__
 *
 * To run a query within a React component, call `useManufacturersQuery` and pass it any options that fit your needs.
 * When your component renders, `useManufacturersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useManufacturersQuery({
 *   variables: {
 *      first: // value for 'first'
 *   },
 * });
 */
export function useManufacturersQuery(
  baseOptions?: Apollo.QueryHookOptions<
    ManufacturersQuery,
    ManufacturersQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<ManufacturersQuery, ManufacturersQueryVariables>(
    ManufacturersDocument,
    options
  );
}
export function useManufacturersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    ManufacturersQuery,
    ManufacturersQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<ManufacturersQuery, ManufacturersQueryVariables>(
    ManufacturersDocument,
    options
  );
}
export type ManufacturersQueryHookResult = ReturnType<
  typeof useManufacturersQuery
>;
export type ManufacturersLazyQueryHookResult = ReturnType<
  typeof useManufacturersLazyQuery
>;
export type ManufacturersQueryResult = Apollo.QueryResult<
  ManufacturersQuery,
  ManufacturersQueryVariables
>;
