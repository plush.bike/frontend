import * as Types from "@/types/graphql";

import { gql } from "@apollo/client";
export type UserBasicFragment = {
  __typename?: "User";
  id: string;
  name: string;
  email?: string | null;
  profileImageUrl?: string | null;
  hasEnabledTwoFactorAuthentication?: boolean | null;
  hasPasswordSet?: boolean | null;
  createdAt: any;
  updatedAt: any;
};

export const UserBasicFragmentDoc = gql`
  fragment UserBasic on User {
    id
    name
    email
    profileImageUrl
    hasEnabledTwoFactorAuthentication
    hasPasswordSet
    createdAt
    updatedAt
  }
`;
