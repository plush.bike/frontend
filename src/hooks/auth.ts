import { useSelector } from "react-redux";

import { RootState } from "@/types/store";

export const useAuth = () => {
  const auth = useSelector((state: RootState) => state.auth);

  return {
    ...auth,
    isAuthenticated: auth.authenticatedUser !== null,
  };
};
