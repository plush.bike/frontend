import BrowserSpriteSymbol from "svg-baker-runtime/browser-symbol";

export default class CustomBrowserSpriteSymbol extends BrowserSpriteSymbol {
  constructor({ id, viewBox, content }) {
    super({
      id,
      viewBox,
      content: content.replace(
        /viewBox="\d+ \d+ \d+ \d+"/,
        'viewBox="-0.25 -0.25 20.5 20.5"'
      ),
    });
  }
}
