import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import { useDispatch } from "react-redux";

import TwoFactorAuthenticationChallengeForm from "@/components/auth/TwoFactorAuthenticationChallengeForm";
import Head from "@/components/Head";
import Loader from "@/components/Loader";
import { useSocialProviderCallbackMutation } from "@/graphql/auth/social-providers/mutations/social-provider-callback.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import {
  SocialProviderCallbackType,
  SocialProviderType,
} from "@/types/graphql";
import { FunctionComponent } from "@/types/react";

type Props = {
  providerType: SocialProviderType;
  providerTitle: string;
};

const SocialSignInCallback: FunctionComponent<Props> = ({
  providerType,
  providerTitle,
}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [socialProviderCallback] = useSocialProviderCallbackMutation();
  const [
    showTwoFactorAuthenticationChallenge,
    setShowTwoFactorAuthenticationChallenge,
  ] = useState(false);

  const code = useMemo(() => {
    return router.query.code as string;
  }, [router]);

  useEffect(() => {
    if (!code) {
      return;
    }

    const fetchData = async () => {
      try {
        const {
          data: {
            socialProviderCallback: {
              user = null,
              twoFactor = false,
              callbackType,
            },
          },
        } = await socialProviderCallback({
          variables: {
            provider: providerType,
            code,
          },
        });

        if (twoFactor) {
          setShowTwoFactorAuthenticationChallenge(true);

          return;
        }

        if (callbackType === SocialProviderCallbackType.SignIn) {
          router.push("/parts/forks");
          dispatch(updateAuthenticatedUser(user));
        } else {
          router.push("/account-settings");
          dispatch(
            updateToastSuccess({
              show: true,
              message: `${providerTitle} successfully connected.`,
            })
          );
        }
      } catch (error) {
        const errors = error?.graphQLErrors?.[0] || {};
        const { callbackType = SocialProviderCallbackType.SignIn } =
          errors?.extensions || {};

        if (callbackType === SocialProviderCallbackType.SignIn) {
          router.push("/sign-in");
        } else {
          router.push("/account-settings");
        }
        dispatch(
          updateToastError({
            show: true,
            message: errors?.message || "An error occurred.",
          })
        );
      }
    };

    fetchData();
  }, [code, providerType]);

  return (
    <>
      <Head pageTitle={`Signing in with ${providerTitle}`} />
      {showTwoFactorAuthenticationChallenge ? (
        <TwoFactorAuthenticationChallengeForm />
      ) : (
        <div className="absolute -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 text-center">
          <Loader className="mx-auto" size={16} />
          <h1 className="mt-3">Authorizing with {providerTitle}</h1>
        </div>
      )}
    </>
  );
};

export default SocialSignInCallback;
