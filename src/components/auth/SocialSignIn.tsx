import ButtonWhite from "@/components/buttons/ButtonWhite";
import { SOCIAL_PROVIDERS } from "@/constants/auth";
import { FunctionComponent } from "@/types/react";

const SocialSignIn: FunctionComponent = () => {
  return (
    <div className="mt-4">
      <div className="relative">
        <div className="absolute inset-0 flex items-center">
          <div className="w-full border-t border-gray-300" />
        </div>
        <div className="relative flex justify-center text-sm">
          <span className="bg-white px-2 text-gray-500">Or continue with</span>
        </div>
      </div>
      <div className="mt-2 grid grid-cols-4 gap-3">
        {SOCIAL_PROVIDERS.map(({ type, buttonIcon: ButtonIcon, label }) => (
          <form
            key={type}
            action={`${
              process.env.NEXT_PUBLIC_API_URL
            }/social-provider/${type.toLowerCase()}/redirect`}
          >
            <ButtonWhite type="submit" className="w-full">
              <ButtonIcon className="h-6 mr-2 -ml-1" />
              {label}
            </ButtonWhite>
          </form>
        ))}
      </div>
    </div>
  );
};

export default SocialSignIn;
