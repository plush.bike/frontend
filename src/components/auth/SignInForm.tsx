import { useRouter } from "next/router";
import { useState, useRef } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import SocialSignIn from "@/components/auth/SocialSignIn";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import Link from "@/components/Link";
import { INCORRECT_SIGN_IN_CREDENTIALS_ERROR_MESSAGE } from "@/constants/auth";
import { useSignInMutation } from "@/graphql/auth/mutations/sign-in.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { validateEmail } from "@/helpers/validation-rules";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  email: string;
  password: string;
};

type Props = {
  onTwoFactor: () => void;
};

const SignInForm: FunctionComponent<Props> = ({ onTwoFactor }) => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm<Inputs>();

  const [signIn] = useSignInMutation();
  const dispatch = useDispatch();
  const router = useRouter();

  const passwordInputRef = useRef<HTMLInputElement | null>(null);
  const { ref: passwordInputRefCallBack, ...registerPassword } = register(
    "password",
    {
      required: { value: true, message: "Please enter your password." },
    }
  );

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({ email, password }) => {
    setLoading(true);

    try {
      const {
        data: {
          signIn: { user = null, twoFactor },
        },
      } = await signIn({
        variables: {
          email,
          password,
        },
      });

      if (twoFactor) {
        onTwoFactor();

        return;
      }

      dispatch(updateAuthenticatedUser(user));
      router.push("/parts/forks");
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
        customAlertErrorMessages: {
          [INCORRECT_SIGN_IN_CREDENTIALS_ERROR_MESSAGE]:
            "Incorrect email or password, please try again.",
        },
      });

      setValue("password", "");
      passwordInputRef.current?.focus();
    }

    setLoading(false);
  };

  return (
    <div className="container-sm">
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        <h1>Sign in</h1>
        <FormWrapper
          className="grid grid-cols-1 gap-4 mt-4"
          onSubmit={handleSubmit(onSubmit)}
        >
          <FormInput
            label="Email"
            type="email"
            errors={errors.email}
            {...register("email", {
              required: { value: true, message: "Please enter your email." },
              validate: {
                email: (value) =>
                  validateEmail(value, "Please enter a valid email."),
              },
            })}
          />
          <FormInput
            label="Password"
            type="password"
            errors={errors.password}
            {...registerPassword}
            ref={(e) => {
              passwordInputRefCallBack(e);
              passwordInputRef.current = e;
            }}
          />
          <div className="flex items-center justify-between">
            <ButtonPrimary type="submit" loading={loading}>
              Sign in
            </ButtonPrimary>
            <Link
              href="/forgot-password"
              className="underline-secondary-default"
            >
              Forgot password?
            </Link>
          </div>
        </FormWrapper>
        <SocialSignIn />
      </div>
    </div>
  );
};

export default SignInForm;
