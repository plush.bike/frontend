import { useRouter } from "next/router";
import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import { useTwoFactorAuthenticationChallengeMutation } from "@/graphql/auth/mutations/two-factor-authentication-challenge.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  code: string;
  recoveryCode: string;
};

const CODE_TYPE_ONE_TIME_PASSWORD = "oneTimePassword";
const CODE_TYPE_RECOVERY = "recovery";

const TwoFactorAuthenticationChallengeForm: FunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);
  const [codeType, setCodeType] = useState<string>(CODE_TYPE_ONE_TIME_PASSWORD);

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm<Inputs>();

  const [twoFactorAuthenticationChallenge] =
    useTwoFactorAuthenticationChallengeMutation();
  const dispatch = useDispatch();
  const router = useRouter();

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({ code, recoveryCode }) => {
    setLoading(true);

    try {
      const variables =
        codeType === CODE_TYPE_ONE_TIME_PASSWORD ? { code } : { recoveryCode };
      const {
        data: {
          twoFactorAuthenticationChallenge: { user },
        },
      } = await twoFactorAuthenticationChallenge({
        variables,
      });

      dispatch(updateAuthenticatedUser(user));
      router.push("/parts/forks");
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    } finally {
      setLoading(false);
    }
  };

  const handleSetCodeType = (
    codeTypeToSet:
      | typeof CODE_TYPE_ONE_TIME_PASSWORD
      | typeof CODE_TYPE_RECOVERY
  ) => {
    setCodeType(codeTypeToSet);

    setValue("code", "");
    setValue("recoveryCode", "");
  };

  return (
    <div className="container-sm">
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        <h1>Two factor authentication</h1>
        <FormWrapper
          className="grid grid-cols-1 gap-4 mt-4"
          onSubmit={handleSubmit(onSubmit)}
        >
          {codeType === CODE_TYPE_ONE_TIME_PASSWORD ? (
            <FormInput
              label="One time password"
              autoComplete="one-time-code"
              autoFocus
              errors={errors.code}
              {...register("code", {
                required: {
                  value: true,
                  message: "Please enter your one time password.",
                },
              })}
            />
          ) : (
            <FormInput
              label="Recovery code"
              errors={errors.recoveryCode}
              {...register("recoveryCode", {
                required: {
                  value: true,
                  message: "Please enter your recovery code.",
                },
              })}
            />
          )}
          <div className="flex items-center justify-between">
            <ButtonPrimary type="submit" loading={loading}>
              Sign in
            </ButtonPrimary>
            {codeType === CODE_TYPE_ONE_TIME_PASSWORD ? (
              <button
                type="button"
                onClick={() => handleSetCodeType(CODE_TYPE_RECOVERY)}
              >
                Use recovery code
              </button>
            ) : (
              <button
                type="button"
                onClick={() => handleSetCodeType(CODE_TYPE_ONE_TIME_PASSWORD)}
              >
                Use one time code
              </button>
            )}
          </div>
        </FormWrapper>
      </div>
    </div>
  );
};

export default TwoFactorAuthenticationChallengeForm;
