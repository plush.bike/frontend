import { twMerge } from "tailwind-merge";

import BadgeBase from "@/components/badges/BadgeBase";
import { BadgeProps } from "@/types/badges";
import { FunctionComponent } from "@/types/react";

const BadgePrimary: FunctionComponent<BadgeProps> = ({
  className,
  children,
  ...props
}) => {
  return (
    <BadgeBase
      {...props}
      className={twMerge("bg-indigo-100 text-indigo-800", className)}
    >
      {children}
    </BadgeBase>
  );
};

export default BadgePrimary;
