import { twMerge } from "tailwind-merge";

import BadgeBase from "@/components/badges/BadgeBase";
import { BadgeProps } from "@/types/badges";
import { FunctionComponent } from "@/types/react";

const BadgeGray: FunctionComponent<BadgeProps> = ({
  className,
  children,
  ...props
}) => {
  return (
    <BadgeBase
      {...props}
      className={twMerge("bg-gray-200 text-gray-800", className)}
    >
      {children}
    </BadgeBase>
  );
};

export default BadgeGray;
