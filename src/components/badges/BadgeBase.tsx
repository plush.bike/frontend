import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import closeIcon from "@/icons/close.svg";
import { BadgeProps } from "@/types/badges";
import { FunctionComponent } from "@/types/react";

const BadgeBase: FunctionComponent<BadgeProps> = ({
  onRemove,
  className,
  children,
  ...props
}) => {
  return (
    <span
      {...props}
      className={twMerge(
        "inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium",
        className
      )}
    >
      {onRemove && (
        <button aria-label="Remove" onClick={onRemove} className="mr-2">
          <Icon svg={closeIcon} />
        </button>
      )}
      {children}
    </span>
  );
};

export default BadgeBase;
