import { throttle } from "lodash";
import PhotoswipeJS, { Item as PhotoswipeItem, Options } from "photoswipe";
import PhotoSwipeUI_Default from "photoswipe/dist/photoswipe-ui-default";
import React, { useRef } from "react";

import GalleryThumbnail from "@/components/gallery/GalleryThumbnail";
import { GalleryItem } from "@/types/gallery";
import { FunctionComponent } from "@/types/react";

import "photoswipe/dist/photoswipe.css";
import "photoswipe/dist/default-skin/default-skin.css";

type Props = {
  items: GalleryItem[];
  options?: Options;
};

const Gallery: FunctionComponent<Props> = ({ items, options = {} }) => {
  const pswpElement = useRef<HTMLDivElement>(null);
  const galleryThumbnailRefs = useRef<HTMLImageElement[]>([]);

  const openPhotoSwipe = (index, disableAnimation = false) => {
    const item = items[index];

    const photoswipeOptions = {
      galleryUID: item.id,
      getThumbBoundsFn: function (index) {
        const thumbnail = galleryThumbnailRefs.current[index];
        const pageYScroll =
          window.pageYOffset || document.documentElement.scrollTop;
        const rect = thumbnail.getBoundingClientRect();

        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      },
      showHideOpacity: true,
      index,
      ...(disableAnimation ? { showAnimationDuration: 0 } : {}),
      ...options,
    };

    const gallery = new PhotoswipeJS(
      pswpElement.current,
      PhotoSwipeUI_Default,
      items.map(({ src, thumbnailSrc }) => ({
        src,
        msrc: thumbnailSrc,
        w: window.innerWidth,
        h: window.innerHeight,
      })),
      photoswipeOptions
    );

    gallery.init();

    const updateItemsDimensions = (items: PhotoswipeItem[]) => {
      items.forEach((item) => {
        item.w = window.innerWidth;
        item.h = window.innerHeight;
      });
    };

    const updateItemsDimensionsWithBinding = throttle(
      updateItemsDimensions.bind(this, gallery.items),
      200
    );

    window.addEventListener("resize", updateItemsDimensionsWithBinding);

    gallery.listen("destroy", () => {
      window.removeEventListener("resize", updateItemsDimensionsWithBinding);
    });
  };

  return (
    <>
      <ul
        role="list"
        className="grid grid-cols-2 gap-4 lg:grid-cols-4 xl:gap-x-8"
      >
        {items.map((item, index) => (
          <GalleryThumbnail
            key={item.id}
            ref={(el) => (galleryThumbnailRefs.current[index] = el)}
            item={item}
            index={index}
            onClick={openPhotoSwipe}
          />
        ))}
      </ul>
      <div
        className="pswp"
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
        ref={pswpElement}
      >
        <div className="pswp__bg"></div>

        <div className="pswp__scroll-wrap">
          <div className="pswp__container">
            <div className="pswp__item"></div>
            <div className="pswp__item"></div>
            <div className="pswp__item"></div>
          </div>

          <div className="pswp__ui pswp__ui--hidden">
            <div className="pswp__top-bar">
              <div className="pswp__counter"></div>

              <button
                className="pswp__button pswp__button--close"
                title="Close (Esc)"
              ></button>

              <button
                className="pswp__button pswp__button--share"
                title="Share"
              ></button>

              <button
                className="pswp__button pswp__button--fs"
                title="Toggle fullscreen"
              ></button>

              <button
                className="pswp__button pswp__button--zoom"
                title="Zoom in/out"
              ></button>

              <div className="pswp__preloader">
                <div className="pswp__preloader__icn">
                  <div className="pswp__preloader__cut">
                    <div className="pswp__preloader__donut"></div>
                  </div>
                </div>
              </div>
            </div>

            <div className="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
              <div className="pswp__share-tooltip"></div>
            </div>

            <button
              className="pswp__button pswp__button--arrow--left"
              title="Previous (arrow left)"
            ></button>

            <button
              className="pswp__button pswp__button--arrow--right"
              title="Next (arrow right)"
            ></button>

            <div className="pswp__caption">
              <div className="pswp__caption__center"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Gallery;
