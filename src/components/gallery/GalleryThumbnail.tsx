import React, { forwardRef, ForwardRefRenderFunction } from "react";

import "photoswipe/dist/photoswipe.css";
import "photoswipe/dist/default-skin/default-skin.css";
import { GalleryItem } from "@/types/gallery";

type Props = {
  item: GalleryItem;
  index: number;
  onClick: (index: number) => void;
};

const GalleryThumbnail: ForwardRefRenderFunction<HTMLImageElement, Props> = (
  { item, index, onClick },
  ref
) => {
  const handleOnClick = () => {
    onClick(index);
  };

  return (
    <li className="relative">
      <div className="block w-full overflow-hidden bg-gray-100 rounded-lg group aspect-w-1 aspect-h-1 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500">
        <img
          ref={ref}
          src={item.thumbnailSrc}
          className="object-cover w-full h-full pointer-events-none group-hover:opacity-75"
        />
        <button
          type="button"
          className="absolute inset-0 w-full focus:outline-none focus-ring-none"
          onClick={handleOnClick}
        >
          <span className="sr-only">View image</span>
        </button>
      </div>
    </li>
  );
};

export default forwardRef(GalleryThumbnail);
