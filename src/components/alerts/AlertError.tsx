import React from "react";
import { twMerge } from "tailwind-merge";

import AlertBase from "@/components/alerts/AlertBase";
import closeSolidIcon from "@/icons/close-solid.svg";
import { BaseProps as Props } from "@/types/alerts";
import { FunctionComponent } from "@/types/react";

const AlertError: FunctionComponent<Props> = ({
  onClose,
  className,
  children,
  ...props
}) => {
  return (
    <AlertBase
      {...props}
      className={twMerge("bg-red-50 text-red-800", className)}
      icon={closeSolidIcon}
      onClose={onClose}
    >
      {children}
    </AlertBase>
  );
};

export default AlertError;
