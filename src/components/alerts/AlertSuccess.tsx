import React from "react";
import { twMerge } from "tailwind-merge";

import AlertBase from "@/components/alerts/AlertBase";
import checkmarkOutlineIcon from "@/icons/checkmark-outline.svg";
import { BaseProps as Props } from "@/types/alerts";
import { FunctionComponent } from "@/types/react";

const AlertSuccess: FunctionComponent<Props> = ({
  onClose,
  className,
  children,
  ...props
}) => {
  return (
    <AlertBase
      {...props}
      className={twMerge("bg-emerald-50 text-emerald-800", className)}
      icon={checkmarkOutlineIcon}
      onClose={onClose}
    >
      {children}
    </AlertBase>
  );
};

export default AlertSuccess;
