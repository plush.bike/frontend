import React from "react";
import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import closeIcon from "@/icons/close.svg";
import { BaseProps } from "@/types/alerts";
import { FunctionComponent } from "@/types/react";
import { SvgIcon } from "@/types/svg";

type Props = BaseProps & {
  icon: SvgIcon;
};

const AlertBase: FunctionComponent<Props> = ({
  icon,
  className = "",
  onClose,
  children,
  ...props
}) => {
  return (
    <div
      {...props}
      className={twMerge(
        "relative flex items-center px-3 py-2 my-2 rounded",
        className
      )}
    >
      <div className="mr-3" data-testid="alertIcon">
        <Icon svg={icon} size={5} />
      </div>
      {onClose && (
        <div className="absolute top-0 right-0 p-2">
          <button aria-label="Close alert" onClick={onClose}>
            <Icon svg={closeIcon} />
          </button>
        </div>
      )}
      {children}
    </div>
  );
};

export default AlertBase;
