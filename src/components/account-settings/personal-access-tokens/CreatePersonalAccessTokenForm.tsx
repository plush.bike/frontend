import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormInputCopyReveal from "@/components/form/FormInputCopyReveal";
import FormWrapper from "@/components/form/FormWrapper";
import { useCreatePersonalAccessTokenMutation } from "@/graphql/auth/personal-access-tokens/mutations/create-personal-access-token.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  name: string;
};

const CreatePersonalAccessTokenForm: FunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);
  const [newPersonalAccessToken, setNewPersonalAccessToken] =
    useState<string>(null);

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm<Inputs>();

  const [createPersonalAccessToken] = useCreatePersonalAccessTokenMutation();

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({ name }) => {
    setLoading(true);

    try {
      const {
        data: {
          createPersonalAccessToken: { token },
        },
      } = await createPersonalAccessToken({
        variables: { name },
        update: (cache) => {
          cache.evict({
            id: "ROOT_QUERY",
            fieldName: "personalAccessTokens",
          });
        },
      });

      setNewPersonalAccessToken(token);
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    } finally {
      setLoading(false);
      setValue("name", "");
    }
  };

  return (
    <>
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      {newPersonalAccessToken && (
        <div className="max-w-lg mt-4">
          <FormInputCopyReveal
            label="Your new personal access token"
            description="Copy and save this personal access token in a safe place. You will not be able to access it again."
            value={newPersonalAccessToken}
          />
        </div>
      )}
      <FormWrapper onSubmit={handleSubmit(onSubmit)} className="mt-4">
        <div className="max-w-lg">
          <FormInput
            label="Name"
            errors={errors.name}
            {...register("name", {
              required: {
                value: true,
                message: "Please enter a name for your personal access token.",
              },
            })}
          />
        </div>
        <ButtonPrimary type="submit" className="mt-4" loading={loading}>
          Create personal access token
        </ButtonPrimary>
      </FormWrapper>
    </>
  );
};

export default CreatePersonalAccessTokenForm;
