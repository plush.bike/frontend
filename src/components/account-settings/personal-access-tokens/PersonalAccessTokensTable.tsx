import { createColumnHelper } from "@tanstack/react-table";
import { useRouter } from "next/router";
import { useMemo } from "react";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import ButtonRed from "@/components/buttons/ButtonRed";
import DateTime from "@/components/DateTime";
import ModalConfirm from "@/components/modals/ModalConfirm";
import Pagination from "@/components/pagination/Pagination";
import Table from "@/components/table/Table";
import { MODAL_VARIANT_RED } from "@/constants/modal";
import { DEFAULT_PER_PAGE } from "@/constants/pagination";
import { useDeletePersonalAccessTokenMutation } from "@/graphql/auth/personal-access-tokens/mutations/delete-personal-access-token.generated";
import {
  PersonalAccessTokensDocument,
  PersonalAccessTokensQuery,
  usePersonalAccessTokensQuery,
} from "@/graphql/auth/personal-access-tokens/queries/personal-access-tokens.generated";
import { nextTick } from "@/helpers/general";
import { updateToastError } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

const PersonalAccessTokensTable: FunctionComponent = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const page = useMemo(() => {
    return parseInt(router.query.patPage as string) || 1;
  }, [router]);

  const { data, loading, error } = usePersonalAccessTokensQuery({
    variables: {
      first: DEFAULT_PER_PAGE,
      page,
    },
  });

  const itemsCount = useMemo(() => {
    return data?.personalAccessTokens?.paginatorInfo?.total || 0;
  }, [data]);

  const [deletePersonalAccessToken] = useDeletePersonalAccessTokenMutation();

  const handleDeleteModalContinue = async (id: string) => {
    try {
      await deletePersonalAccessToken({
        variables: { id },
        update: async (cache) => {
          const personalAccessTokensQuery = cache.readQuery({
            query: PersonalAccessTokensDocument,
          }) as PersonalAccessTokensQuery;

          if (!personalAccessTokensQuery?.personalAccessTokens?.data) {
            return;
          }

          // Wait for modal to close before updating cache to avoid console errors
          await nextTick();

          cache.writeQuery({
            query: PersonalAccessTokensDocument,
            data: {
              personalAccessTokens: {
                ...personalAccessTokensQuery.personalAccessTokens,
                data: personalAccessTokensQuery.personalAccessTokens.data.filter(
                  (existingPersonalAccessToken) =>
                    id !== existingPersonalAccessToken.id
                ),
              },
            },
          });
        },
      });
    } catch (error) {
      dispatch(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    }
  };

  const tableData = useMemo(() => {
    return (data?.personalAccessTokens?.data || []).map(
      (personalAccessToken) => ({
        ...personalAccessToken,
        lastUsedAt: personalAccessToken.lastUsedAt || "Never",
      })
    );
  }, [data]);

  const columnHelper =
    createColumnHelper<(typeof data.personalAccessTokens.data)[0]>();

  const columns = useMemo(
    () => [
      columnHelper.accessor("name", {
        header: "Name",
      }),
      columnHelper.accessor("lastUsedAt", {
        header: "Last used",
        cell: (info) => <DateTime dateTime={info.getValue()} />,
      }),
      columnHelper.accessor("createdAt", {
        header: "Created",
        cell: (info) => <DateTime dateTime={info.getValue()} />,
      }),
      columnHelper.accessor("id", {
        header: "Actions",
        cell: (info) => (
          <ModalConfirm
            title="Delete personal access token"
            variant={MODAL_VARIANT_RED}
            modalTrigger={({ onClick }) => (
              <ButtonRed onClick={onClick}>Delete</ButtonRed>
            )}
            continueButton="Delete"
            onContinue={() => handleDeleteModalContinue(info.getValue())}
          >
            <p className="text-sm text-gray-500">
              Are you sure you want to delete this personal access token?
            </p>
          </ModalConfirm>
        ),
        meta: {
          className: "lg:w-px lg:whitespace-nowrap",
        },
      }),
    ],
    []
  );

  return (
    <>
      {error && (
        <AlertError>
          An error occurred loading your personal access tokens, please try
          again.
        </AlertError>
      )}
      <Table
        columns={columns}
        data={tableData}
        loading={loading}
        className="mt-4"
        breakpoint="lg"
        emptyContent="No personal access tokens found."
      />
      <Pagination
        className="mt-4"
        itemsCount={itemsCount}
        perPage={DEFAULT_PER_PAGE}
        page={page}
        queryParamName="patPage"
      />
    </>
  );
};

export default PersonalAccessTokensTable;
