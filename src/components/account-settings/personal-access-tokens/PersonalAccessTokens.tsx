import CreatePersonalAccessTokenForm from "@/components/account-settings/personal-access-tokens/CreatePersonalAccessTokenForm";
import PersonalAccessTokensTable from "@/components/account-settings/personal-access-tokens/PersonalAccessTokensTable";
import { FunctionComponent } from "@/types/react";

const PersonalAccessTokens: FunctionComponent = () => {
  return (
    <div className="p-6 mt-6 bg-white rounded-md sm:rounded-lg sm:shadow">
      <h3>Personal access tokens</h3>
      <p>
        Personal access tokens can be used to authenticate the GraphQL API.{" "}
        <a href="https://docs.plush.bike/docs/graphql">Learn more</a>.
      </p>
      <CreatePersonalAccessTokenForm />
      <PersonalAccessTokensTable />
    </div>
  );
};

export default PersonalAccessTokens;
