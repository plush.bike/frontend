import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { FieldError } from "react-hook-form";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Icon from "@/components/Icon";
import { inputId } from "@/helpers/form";
import { imageUrl } from "@/helpers/url";
import closeIcon from "@/icons/close.svg";
import photoIcon from "@/icons/photo.svg";
import { FunctionComponent } from "@/types/react";

type Props = {
  label: string;
  initialValue?: string;
  errors?: FieldError | FieldError[];
  onChange?: (event: File | null) => void;
};

const UploadProfileImage: FunctionComponent<Props> = ({
  label,
  initialValue,
  errors,
  onChange,
}) => {
  const id = inputId(label);

  const [preview, setPreview] = useState<string>();

  useEffect(() => {
    if (initialValue) {
      setPreview(imageUrl({ url: initialValue, height: 400, width: 400 }));
    }
  }, []);

  const handleOnDrop = async ([newFile]: File[]) => {
    if (!newFile) {
      return;
    }

    onChange(newFile);

    if (preview !== "") {
      URL.revokeObjectURL(preview);
    }

    setPreview(URL.createObjectURL(newFile));
  };

  const handleOnRemove = () => {
    setPreview("");
    onChange(null);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleOnDrop,
    accept: {
      "image/*": [".jpg", ".jpeg", ".png", ".bmp", ".gif", ".svg", ".webp"],
    },
    noClick: true,
    maxFiles: 1,
  });

  return (
    <FormInputWrapper id={id} errors={errors}>
      <span className="block text-sm font-medium text-gray-700">{label}</span>
      {preview ? (
        <div
          data-testid="profileImage"
          className="relative mt-1 bg-center bg-cover border border-gray-300 rounded-full w-36 h-36 xl:w-48 xl:h-48"
          style={{
            backgroundImage: `url(${preview})`,
          }}
        >
          <button
            aria-label="Remove profile image"
            className="absolute inline-flex items-center p-1 text-white bg-indigo-600 border border-transparent rounded-full shadow-sm top-3 right-3 xl:top-4 xl:right-4 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={handleOnRemove}
            type="button"
          >
            <Icon svg={closeIcon} size={4} />
          </button>
        </div>
      ) : (
        <div
          {...getRootProps()}
          data-testid="dropzone"
          className="relative flex items-center justify-center mt-1 border border-gray-300 border-dashed rounded-full w-36 h-36 xl:w-48 xl:h-48"
        >
          <div>
            {isDragActive ? (
              <p className="py-8 text-sm text-gray-600">
                Drop the files here...
              </p>
            ) : (
              <>
                <Icon
                  className="mx-auto text-gray-400"
                  svg={photoIcon}
                  size={10}
                />
                <div className="text-sm text-center text-gray-600">
                  <label
                    htmlFor={id}
                    className="relative px-2 font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                  >
                    <span>Upload a file</span>
                    <input
                      id={id}
                      name={id}
                      className="sr-only"
                      {...{ ...getInputProps(), style: null }}
                    />
                  </label>
                  <p className="pl-1">or drag and drop</p>
                </div>
                <p className="text-xs text-center text-gray-500">
                  PNG, JPG, GIF up to 10MB
                </p>
              </>
            )}
          </div>
        </div>
      )}
    </FormInputWrapper>
  );
};

export default UploadProfileImage;
