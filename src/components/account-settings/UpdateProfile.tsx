import React, { useState } from "react";
import {
  Controller,
  FieldError,
  SubmitHandler,
  useForm,
} from "react-hook-form";
import { useDispatch } from "react-redux";

import UploadProfileImage from "@/components/account-settings/UploadProfileImage";
import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import { useUpdateAuthenticatedUserMutation } from "@/graphql/user/mutations/update-authenticated-user.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { validateEmail } from "@/helpers/validation-rules";
import { useAuth } from "@/hooks/auth";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastSuccess } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  name: string;
  email: string;
  profileImage: string;
};

const UpdateProfile: FunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);
  const { authenticatedUser } = useAuth();
  const dispatch = useDispatch();
  const [updateAuthenticatedUserMutation] =
    useUpdateAuthenticatedUserMutation();

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    control,
    formState: { errors },
  } = useForm<Inputs>({
    defaultValues: {
      name: authenticatedUser?.name || "",
      email: authenticatedUser?.email || "",
      profileImage: "",
    },
  });

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({
    name,
    email,
    profileImage,
  }) => {
    setLoading(true);

    try {
      const {
        data: { updateAuthenticatedUser: user },
      } = await updateAuthenticatedUserMutation({
        variables: {
          name,
          email,
          ...(profileImage !== "" ? { profileImage } : {}),
        },
      });

      setValue("profileImage", "");

      dispatch(
        updateToastSuccess({
          show: true,
          message: "Profile successfully updated.",
        })
      );
      dispatch(updateAuthenticatedUser(user));
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    }

    setLoading(false);
  };

  return (
    <>
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 mt-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        <h3>Profile</h3>
        <FormWrapper
          className="grid grid-cols-1 gap-4 mt-4 lg:grid-cols-5"
          onSubmit={handleSubmit(onSubmit)}
        >
          <Controller<Inputs, "profileImage">
            name="profileImage"
            control={control}
            render={({ field: { onChange }, fieldState: { error } }) => {
              return (
                <UploadProfileImage
                  initialValue={authenticatedUser?.profileImageUrl}
                  label="Profile image"
                  onChange={onChange}
                  errors={error as unknown as FieldError}
                />
              );
            }}
          />
          <FormInput
            wrapperClassName="col-span-2"
            label="Name"
            type="text"
            errors={errors.name}
            {...register("name", {
              required: { value: true, message: "Please enter your name." },
            })}
          />
          <FormInput
            wrapperClassName="col-span-2"
            label="Email"
            type="email"
            errors={errors.email}
            {...register("email", {
              required: { value: true, message: "Please enter your email." },
              validate: {
                email: (value) =>
                  validateEmail(value, "Please enter a valid email."),
              },
            })}
          />
          <ButtonPrimary
            className="justify-self-start"
            type="submit"
            loading={loading}
          >
            Update
          </ButtonPrimary>
        </FormWrapper>
      </div>
    </>
  );
};

export default UpdateProfile;
