import React, { useMemo, useState } from "react";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import ButtonWhite from "@/components/buttons/ButtonWhite";
import SkeletonLoader from "@/components/SkeletonLoader";
import Tooltip from "@/components/Tooltip";
import { SOCIAL_PROVIDERS } from "@/constants/auth";
import { useDeleteSocialProviderMutation } from "@/graphql/auth/social-providers/mutations/delete-social-provider.generated";
import { useSocialProvidersQuery } from "@/graphql/auth/social-providers/queries/social-providers.generated";
import { useAuth } from "@/hooks/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

const SocialProviders: FunctionComponent = () => {
  const { loading, error, data } = useSocialProvidersQuery();
  const [deleteSocialProvider] = useDeleteSocialProviderMutation();
  const { authenticatedUser } = useAuth();
  const [socialProviderLoadingId, setSocialProviderLoadingId] =
    useState<string>(null);
  const dispatch = useDispatch();

  const handleDisconnect = async ({
    id,
    label,
  }: {
    id: string;
    label: string;
  }) => {
    try {
      setSocialProviderLoadingId(id);

      await deleteSocialProvider({
        variables: { id: id },
        update: (cache) => {
          cache.modify({
            id: "ROOT_QUERY",
            fields: {
              socialProviders(existingSocialProviderRefs, { readField }) {
                return existingSocialProviderRefs.filter(
                  (existingSocialProviderRef) => {
                    return readField("id", existingSocialProviderRef) !== id;
                  }
                );
              },
            },
          });
        },
      });

      setSocialProviderLoadingId(null);

      dispatch(
        updateToastSuccess({
          show: true,
          message: `${label} successfully disconnected.`,
        })
      );
    } catch (error) {
      dispatch(
        updateToastError({
          show: true,
          message: `An error occurred disconnecting ${label}, please try again.`,
        })
      );
      setSocialProviderLoadingId(null);
    }
  };

  const socialProviders = useMemo(() => {
    if (loading) {
      return (
        <>
          <SkeletonLoader className="h-10 w-full rounded-md" />
          <SkeletonLoader className="h-10 w-full rounded-md" />
          <SkeletonLoader className="h-10 w-full rounded-md" />
        </>
      );
    }

    if (!data) {
      return [];
    }

    return SOCIAL_PROVIDERS.map(({ type, buttonIcon: ButtonIcon, label }) => {
      const socialProvider = data.socialProviders.find(
        ({ provider }) => type === provider
      );

      if (socialProvider === undefined) {
        return (
          <form
            key={type}
            action={`${
              process.env.NEXT_PUBLIC_API_URL
            }/social-provider/${type.toLowerCase()}/redirect`}
          >
            <ButtonWhite type="submit" className="w-full">
              <ButtonIcon className="h-6 mr-2 -ml-1" />
              Connect {label}
            </ButtonWhite>
          </form>
        );
      } else {
        return !authenticatedUser?.hasPasswordSet &&
          data?.socialProviders?.length === 1 ? (
          <Tooltip
            content="This service can not be disconnected until you set a password or connect another service."
            key={type}
          >
            <ButtonWhite disabled className="w-full">
              <ButtonIcon className="h-6 mr-2 -ml-1" />
              Disconnect {label}
            </ButtonWhite>
          </Tooltip>
        ) : (
          <ButtonWhite
            key={type}
            className="w-full"
            onClick={() => handleDisconnect({ id: socialProvider.id, label })}
            loading={socialProviderLoadingId === socialProvider.id}
          >
            <ButtonIcon className="h-6 mr-2 -ml-1" />
            Disconnect {label}
          </ButtonWhite>
        );
      }
    });
  }, [
    data,
    loading,
    SOCIAL_PROVIDERS,
    authenticatedUser,
    socialProviderLoadingId,
  ]);

  return (
    <div className="p-6 mt-6 bg-white rounded-md sm:rounded-lg sm:shadow">
      <h3>Sign in services</h3>
      <p>You can use these services to sign in.</p>
      {error ? (
        <AlertError>
          An error occurred loading sign in services, please refresh the page to
          try again.
        </AlertError>
      ) : (
        <div className="mt-3 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-3">
          {socialProviders}
        </div>
      )}
    </div>
  );
};

export default SocialProviders;
