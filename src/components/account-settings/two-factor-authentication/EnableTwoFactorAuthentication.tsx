import React, { Dispatch, SetStateAction, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";

import RecoveryCodes from "@/components/account-settings/two-factor-authentication/RecoveryCodes";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import { useConfirmTwoFactorAuthenticationMutation } from "@/graphql/auth/mutations/confirm-two-factor-authentication.generated";
import { useEnableTwoFactorAuthenticationMutation } from "@/graphql/auth/mutations/enable-two-factor-authentication.generated";
import { useTwoFactorAuthenticationQrCodeMutation } from "@/graphql/auth/mutations/two-factor-authentication-qr-code.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { useAuth } from "@/hooks/auth";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

type Props = {
  setAlertErrorMessage: Dispatch<SetStateAction<string>>;
};

type Inputs = {
  code: string;
};

const EnableTwoFactorAuthentication: FunctionComponent<Props> = ({
  setAlertErrorMessage,
}) => {
  const [qrCode, setQrCode] = useState<string>(null);
  const [recoveryCodes, setRecoveryCodes] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const { authenticatedUser } = useAuth();

  const [enableTwoFactorAuthentication] =
    useEnableTwoFactorAuthenticationMutation();
  const [twoFactorAuthenticationQrCode] =
    useTwoFactorAuthenticationQrCodeMutation();
  const [confirmTwoFactorAuthentication] =
    useConfirmTwoFactorAuthenticationMutation();

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<Inputs>();

  const handleEnableTwoFactorAuthenticationButtonClick = async () => {
    setLoading(true);

    try {
      await enableTwoFactorAuthentication();
      const {
        data: {
          twoFactorAuthenticationQrCode: { svg },
        },
      } = await twoFactorAuthenticationQrCode();
      setQrCode(svg);
    } catch (error) {
      dispatch(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    } finally {
      setLoading(false);
    }
  };

  const onSubmit: SubmitHandler<Inputs> = async ({ code }) => {
    setLoading(true);

    try {
      const {
        data: {
          confirmTwoFactorAuthentication: { recoveryCodes },
        },
      } = await confirmTwoFactorAuthentication({ variables: { code } });

      setRecoveryCodes(recoveryCodes);
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    } finally {
      setLoading(false);
    }
  };

  const handleContinue = () => {
    dispatch(
      updateToastSuccess({
        show: true,
        message: "Two factor authentication enabled successfully.",
      })
    );
    dispatch(
      updateAuthenticatedUser({
        ...authenticatedUser,
        hasEnabledTwoFactorAuthentication: true,
      })
    );
  };

  if (recoveryCodes.length) {
    return (
      <RecoveryCodes
        recoveryCodes={recoveryCodes}
        onContinue={handleContinue}
      />
    );
  }

  if (qrCode) {
    return (
      <FormWrapper className="mt-4" onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 gap-4 lg:grid-flow-col lg:grid-cols-none lg:auto-cols-min">
          <div className="max-w-full w-52">
            <img
              className="w-full"
              src={`data:image/svg+xml;utf8,${encodeURIComponent(qrCode)}`}
            />
          </div>
          <div className="max-w-full w-60">
            <FormInput
              label="One time password"
              description="Scan the QR code and enter your one time password."
              autoComplete="one-time-code"
              errors={errors.code}
              {...register("code", {
                required: {
                  value: true,
                  message:
                    "Please scan the QR code and enter your one time password.",
                },
              })}
            />
          </div>
        </div>
        <ButtonPrimary className="mt-4" type="submit" loading={loading}>
          Confirm
        </ButtonPrimary>
      </FormWrapper>
    );
  }

  return (
    <ButtonPrimary
      onClick={handleEnableTwoFactorAuthenticationButtonClick}
      loading={loading}
    >
      Enable two factor authentication
    </ButtonPrimary>
  );
};

export default EnableTwoFactorAuthentication;
