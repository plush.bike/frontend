import React, { useState } from "react";

import DisableTwoFactorAuthentication from "@/components/account-settings/two-factor-authentication/DisableTwoFactorAuthentication";
import EnableTwoFactorAuthentication from "@/components/account-settings/two-factor-authentication/EnableTwoFactorAuthentication";
import ViewRecoveryCodes from "@/components/account-settings/two-factor-authentication/ViewRecoveryCodes";
import AlertError from "@/components/alerts/AlertError";
import { useAuth } from "@/hooks/auth";
import { FunctionComponent } from "@/types/react";

const TwoFactorAuthentication: FunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);

  const { authenticatedUser } = useAuth();

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  return (
    <div className="p-6 mt-6 bg-white rounded-md sm:rounded-lg sm:shadow">
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <h3>Two factor authentication</h3>
      <div className="mt-4">
        {authenticatedUser?.hasEnabledTwoFactorAuthentication ? (
          <>
            <ViewRecoveryCodes />
            <DisableTwoFactorAuthentication />
          </>
        ) : (
          <EnableTwoFactorAuthentication
            setAlertErrorMessage={setAlertErrorMessage}
          />
        )}
      </div>
    </div>
  );
};

export default TwoFactorAuthentication;
