import React, { useMemo } from "react";

import ButtonCopyToClipboard from "@/components/buttons/ButtonCopyToClipboard";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import { FunctionComponent } from "@/types/react";

type Props = {
  recoveryCodes: string[];
  onContinue: () => void;
};

const RecoveryCodes: FunctionComponent<Props> = ({
  recoveryCodes,
  onContinue,
}) => {
  const recoveryCodesString = useMemo(
    () => recoveryCodes.join("\n"),
    [recoveryCodes]
  );

  if (!recoveryCodes.length) {
    return null;
  }

  return (
    <div className="max-w-2xl" data-testid="recoveryCodes">
      <code className="block p-4 bg-gray-100 rounded">
        {recoveryCodes.map((recoveryCode) => (
          <div key={recoveryCode}>{recoveryCode}</div>
        ))}
      </code>
      <p className="mt-6">
        Copy and save these recovery codes in a safe place.
      </p>
      <div className="flex justify-between mt-4">
        <ButtonCopyToClipboard contentToCopy={recoveryCodesString} />
        <ButtonPrimary onClick={onContinue}>Continue</ButtonPrimary>
      </div>
    </div>
  );
};

export default RecoveryCodes;
