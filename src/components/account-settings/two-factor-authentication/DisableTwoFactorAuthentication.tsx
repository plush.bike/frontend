import { noop } from "lodash";
import React, { useRef, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";

import ButtonRed from "@/components/buttons/ButtonRed";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import ModalConfirm from "@/components/modals/ModalConfirm";
import { INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE } from "@/constants/auth";
import { MODAL_VARIANT_RED } from "@/constants/modal";
import { useConfirmPasswordMutation } from "@/graphql/auth/mutations/confirm-password.generated";
import { useDisableTwoFactorAuthenticationMutation } from "@/graphql/auth/mutations/disable-two-factor-authentication.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { nextTick } from "@/helpers/general";
import { useAuth } from "@/hooks/auth";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  password: string;
};

const DisableTwoFactorAuthentication: FunctionComponent = () => {
  const [requirePasswordConfirmation, setRequirePasswordConfirmation] =
    useState(false);
  const dispatch = useDispatch();

  const { authenticatedUser } = useAuth();

  const fireModalContinue = useRef<() => void>(noop);

  const [disableTwoFactorAuthentication] =
    useDisableTwoFactorAuthenticationMutation();
  const [confirmPasswordMutation] = useConfirmPasswordMutation();

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors },
  } = useForm<Inputs>();

  const confirmPassword: SubmitHandler<Inputs> = async ({ password }) => {
    try {
      await confirmPasswordMutation({ variables: { password } });
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage: noop,
        setError,
      });

      setValue("password", "");

      throw new Error();
    }
  };

  const onSubmit = () => {
    fireModalContinue.current();
  };

  const handleConfirmDisableTwoFactorAuthentication = async () => {
    if (requirePasswordConfirmation) {
      try {
        await handleSubmit(confirmPassword)();
      } catch (error) {
        return Promise.resolve({ shouldHide: false });
      }
    }

    try {
      await disableTwoFactorAuthentication();

      dispatch(
        updateToastSuccess({
          show: true,
          message: "Two factor authentication disabled successfully.",
        })
      );

      // Wait for modal to close before unmounting the component
      (async () => {
        await nextTick();
        dispatch(
          updateAuthenticatedUser({
            ...authenticatedUser,
            hasEnabledTwoFactorAuthentication: false,
          })
        );
      })();
    } catch (error) {
      if (error.message === INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE) {
        setRequirePasswordConfirmation(true);

        return Promise.resolve({ shouldHide: false });
      }

      dispatch(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    }
  };

  return (
    <ModalConfirm
      title="Disable two factor authentication"
      variant={MODAL_VARIANT_RED}
      continueButton="Disable"
      modalTrigger={({ onClick, fireContinue }) => {
        fireModalContinue.current = fireContinue;

        return (
          <ButtonRed onClick={onClick}>
            Disable two factor authentication
          </ButtonRed>
        );
      }}
      onContinue={handleConfirmDisableTwoFactorAuthentication}
    >
      <p className="text-sm text-gray-500">
        Are you sure you want to disable two factor authentication?
      </p>
      {requirePasswordConfirmation && (
        <FormWrapper className="mt-4" onSubmit={onSubmit}>
          <FormInput
            label="Password"
            type="password"
            description="Please confirm your password before continuing."
            errors={errors.password}
            {...register("password", {
              required: {
                value: true,
                message: "Please enter your password.",
              },
            })}
          />
        </FormWrapper>
      )}
    </ModalConfirm>
  );
};

export default DisableTwoFactorAuthentication;
