import React, { useState } from "react";
import { useDispatch } from "react-redux";

import RecoveryCodes from "@/components/account-settings/two-factor-authentication/RecoveryCodes";
import ButtonSecondary from "@/components/buttons/ButtonSecondary";
import ModalConfirmPassword from "@/components/modals/ModalConfirmPassword";
import { useTwoFactorAuthenticationRecoveryCodesMutation } from "@/graphql/auth/mutations/two-factor-authentication-recovery-codes.generated";
import { updateToastError } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

const ViewRecoveryCodes: FunctionComponent = () => {
  const [loading, setLoading] = useState(false);
  const [recoveryCodes, setRecoveryCodes] = useState<string[]>([]);

  const [twoFactorAuthenticationRecoveryCodes] =
    useTwoFactorAuthenticationRecoveryCodesMutation();

  const dispatch = useDispatch();

  const handleContinue = () => {
    setRecoveryCodes([]);
  };

  const viewRecoveryCodes = async (onError: (error: any) => boolean) => {
    setLoading(true);

    try {
      const {
        data: {
          twoFactorAuthenticationRecoveryCodes: { recoveryCodes },
        },
      } = await twoFactorAuthenticationRecoveryCodes();

      setRecoveryCodes(recoveryCodes);
    } catch (error) {
      if (onError(error)) {
        // Password confirmation is required
        return;
      }

      // An error occurred viewing recovery codes
      dispatch(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    } finally {
      setLoading(false);
    }
  };

  const handlePasswordConfirmed = () => {
    return viewRecoveryCodes(() => false);
  };

  return (
    <div className="mb-4">
      <RecoveryCodes
        recoveryCodes={recoveryCodes}
        onContinue={handleContinue}
      />
      <ModalConfirmPassword
        modalTrigger={({ onError }) => {
          return !recoveryCodes.length ? (
            <ButtonSecondary
              loading={loading}
              onClick={() => viewRecoveryCodes(onError)}
            >
              View recovery codes
            </ButtonSecondary>
          ) : null;
        }}
        onPasswordConfirmed={handlePasswordConfirmed}
      ></ModalConfirmPassword>
    </div>
  );
};

export default ViewRecoveryCodes;
