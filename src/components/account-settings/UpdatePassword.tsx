import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { twMerge } from "tailwind-merge";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import { CURRENT_PASSWORD_IS_INCORRECT } from "@/constants/auth";
import { useUpdatePasswordMutation } from "@/graphql/auth/mutations/update-password.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { validateMatch } from "@/helpers/validation-rules";
import { useAuth } from "@/hooks/auth";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastSuccess } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  currentPassword: string;
  password: string;
  passwordConfirmation: string;
};

const UpdatePassword: FunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const { authenticatedUser } = useAuth();

  const [updatePassword] = useUpdatePasswordMutation();

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    watch,
    formState: { errors },
  } = useForm<Inputs>();

  const watchPasswordConfirmation = watch("passwordConfirmation", "");

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({
    currentPassword,
    password,
    passwordConfirmation,
  }) => {
    setLoading(true);

    try {
      await updatePassword({
        variables: {
          currentPassword,
          password,
          passwordConfirmation,
        },
      });

      dispatch(
        updateToastSuccess({
          show: true,
          message: "Password successfully updated.",
        })
      );
      dispatch(
        updateAuthenticatedUser({
          ...authenticatedUser,
          hasPasswordSet: true,
        })
      );
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
        customAlertErrorMessages: {
          [CURRENT_PASSWORD_IS_INCORRECT]:
            "Your current password is incorrect, please try again.",
        },
      });
    }

    setLoading(false);
    setValue("currentPassword", "");
    setValue("password", "");
    setValue("passwordConfirmation", "");
  };

  return (
    <>
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 mt-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        <h3>Password</h3>
        <FormWrapper
          className={twMerge(
            "grid grid-cols-1 gap-4 mt-4",
            authenticatedUser?.hasPasswordSet
              ? "lg:grid-cols-3"
              : "lg:grid-cols-2"
          )}
          onSubmit={handleSubmit(onSubmit)}
        >
          {authenticatedUser?.hasPasswordSet && (
            <FormInput
              label="Current password"
              type="password"
              autoComplete="current-password"
              errors={errors.currentPassword}
              {...register("currentPassword", {
                required: {
                  value: true,
                  message: "Please enter your current password.",
                },
              })}
            />
          )}
          <FormInput
            label="New password"
            type="password"
            autoComplete="new-password"
            errors={errors.password}
            {...register("password", {
              required: {
                value: true,
                message: "Please enter your new password.",
              },
              minLength: {
                value: 8,
                message: "Your password must be at least 8 characters long.",
              },
              validate: {
                match: (value) =>
                  validateMatch(value, "Your passwords do not match.", {
                    otherValue: watchPasswordConfirmation,
                  }),
              },
            })}
          />
          <FormInput
            label="Confirm new password"
            type="password"
            autoComplete="new-password"
            errors={errors.passwordConfirmation}
            {...register("passwordConfirmation", {
              required: {
                value: true,
                message: "Please confirm your new password.",
              },
            })}
          />
          <ButtonPrimary
            className="justify-self-start"
            type="submit"
            loading={loading}
          >
            Update
          </ButtonPrimary>
        </FormWrapper>
      </div>
    </>
  );
};

export default UpdatePassword;
