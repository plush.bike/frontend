import React, {
  forwardRef,
  ForwardRefRenderFunction,
  ComponentPropsWithRef,
} from "react";
import TextareaAutosize from "react-textarea-autosize";
import { twMerge } from "tailwind-merge";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import Icon from "@/components/Icon";
import {
  descriptionInputAttributes,
  errorInputAttributes,
  hasErrors,
  inputId,
} from "@/helpers/form";
import exclamationSolidIcon from "@/icons/exclamation-solid.svg";
import { SharedInputProps } from "@/types/form";

type Props = ComponentPropsWithRef<"textarea"> &
  SharedInputProps & {
    minRows?: number;
  };

const Textarea: ForwardRefRenderFunction<HTMLTextAreaElement, Props> = (
  {
    label,
    description,
    labelHidden = false,
    className,
    wrapperClassName,
    errors,
    minRows = 5,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    style,
    ...props
  },
  ref
) => {
  const id = inputId(label);

  return (
    <FormInputWrapper
      id={id}
      errors={errors}
      className={wrapperClassName}
      description={description}
    >
      <Label htmlFor={id} hidden={labelHidden}>
        {label}
      </Label>
      <div className="relative mt-1">
        <TextareaAutosize
          {...props}
          minRows={minRows}
          ref={ref}
          id={id}
          className={twMerge(
            "shadow-sm block w-full sm:text-sm rounded-md focus:ring-1 focus:ring-offset-0",
            hasErrors(errors)
              ? "border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500"
              : "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300",
            className
          )}
          {...descriptionInputAttributes({ id, description })}
          {...errorInputAttributes({ errors, id })}
        />
        {hasErrors(errors) && (
          <div className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
            <Icon
              svg={exclamationSolidIcon}
              size={5}
              className="text-red-500 "
            />
          </div>
        )}
      </div>
    </FormInputWrapper>
  );
};

export default forwardRef(Textarea);
