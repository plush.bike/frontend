import React, {
  forwardRef,
  ForwardRefRenderFunction,
  ComponentPropsWithRef,
} from "react";
import { twMerge } from "tailwind-merge";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import Icon from "@/components/Icon";
import {
  descriptionInputAttributes,
  errorInputAttributes,
  hasErrors,
  inputId,
} from "@/helpers/form";
import exclamationSolidIcon from "@/icons/exclamation-solid.svg";
import { SharedInputProps } from "@/types/form";
import { SvgIcon } from "@/types/svg";

type Props = ComponentPropsWithRef<"input"> &
  SharedInputProps & {
    icon?: SvgIcon;
  };

const FormInput: ForwardRefRenderFunction<HTMLInputElement, Props> = (
  {
    label,
    description,
    labelHidden = false,
    type = "text",
    className,
    wrapperClassName,
    errors,
    icon,
    ...props
  },
  ref
) => {
  const id = inputId(label);

  return (
    <FormInputWrapper
      id={id}
      errors={errors}
      description={description}
      className={wrapperClassName}
    >
      <Label htmlFor={id} hidden={labelHidden}>
        {label}
      </Label>
      <div className={twMerge("relative", !labelHidden && "mt-1")}>
        {icon && (
          <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3 text-gray-400">
            <Icon svg={icon} />
          </div>
        )}
        <input
          {...props}
          ref={ref}
          id={id}
          type={type}
          className={twMerge(
            "shadow-sm block w-full sm:text-sm rounded-md",
            icon && "pl-10",
            hasErrors(errors)
              ? "border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500"
              : "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300",
            className
          )}
          {...descriptionInputAttributes({ id, description })}
          {...errorInputAttributes({ errors, id })}
        />
        {hasErrors(errors) && (
          <div className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
            <Icon svg={exclamationSolidIcon} className="w-5 h-5 text-red-500" />
          </div>
        )}
      </div>
    </FormInputWrapper>
  );
};

export default forwardRef(FormInput);
