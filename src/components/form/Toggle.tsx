import { Switch } from "@headlessui/react";
import { twMerge } from "tailwind-merge";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import { inputId } from "@/helpers/form";
import { SharedInputProps } from "@/types/form";
import { FunctionComponent } from "@/types/react";

type Props = SharedInputProps & {
  onChange(event: { target: { value: boolean } }): void;
  className?: string;
  value: boolean;
};

const Toggle: FunctionComponent<Props> = ({
  label,
  labelHidden,
  errors,
  onChange,
  className,
  wrapperClassName,
  value = false,
}) => {
  const id = inputId(label);

  const handleOnChange = () => {
    onChange({ target: { value: !value } });
  };

  return (
    <FormInputWrapper id={id} errors={errors} className={wrapperClassName}>
      <Label htmlFor={id} hidden={labelHidden}>
        {label}
      </Label>
      <Switch
        checked={value}
        onChange={handleOnChange}
        className={twMerge(
          value ? "bg-indigo-600" : "bg-gray-200",
          "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 mt-1",
          className
        )}
      >
        <span className="sr-only">{label}</span>
        <span
          aria-hidden="true"
          className={twMerge(
            value ? "translate-x-5" : "translate-x-0",
            "pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
          )}
        />
      </Switch>
    </FormInputWrapper>
  );
};

export default Toggle;
