import React, { ComponentPropsWithRef } from "react";
import { twMerge } from "tailwind-merge";

import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"label"> & {
  hidden?: boolean;
};

const Label: FunctionComponent<Props> = ({
  className,
  hidden = false,
  children,
  ...props
}) => {
  return (
    <label
      {...props}
      className={twMerge(
        "block text-sm font-medium text-gray-700",
        hidden && "sr-only",
        className
      )}
    >
      {children}
    </label>
  );
};

export default Label;
