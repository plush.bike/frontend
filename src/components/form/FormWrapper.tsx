import React, {
  FormEvent,
  ComponentPropsWithRef,
  ForwardRefRenderFunction,
  forwardRef,
} from "react";

type Props = ComponentPropsWithRef<"form">;

const FormWrapper: ForwardRefRenderFunction<HTMLFormElement, Props> = (
  { onSubmit, className, children, ...props },
  ref
) => {
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (onSubmit) {
      onSubmit(event);
    }
  };

  return (
    <form
      {...props}
      ref={ref}
      className={className}
      noValidate
      onSubmit={handleSubmit}
    >
      {children}
    </form>
  );
};

export default forwardRef(FormWrapper);
