import { Listbox, Transition } from "@headlessui/react";
import { range } from "lodash";
import React, { Fragment } from "react";
import { twMerge } from "tailwind-merge";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Icon from "@/components/Icon";
import SkeletonLoader from "@/components/SkeletonLoader";
import {
  descriptionInputAttributes,
  errorInputAttributes,
  inputId,
} from "@/helpers/form";
import checkmarkIcon from "@/icons/checkmark.svg";
import selectArrow from "@/icons/select-arrow.svg";
import {
  ListboxChangeEvent,
  ListboxOption,
  SharedInputProps,
} from "@/types/form";
import { FunctionComponent } from "@/types/react";

type Props = SharedInputProps & {
  disabled?: boolean;
  value?: ListboxOption;
  onChange(event: ListboxChangeEvent): void;
  buttonLabel: string;
  options: ListboxOption[];
  loading?: boolean;
};

const Select: FunctionComponent<Props> = ({
  label,
  description,
  buttonLabel,
  errors,
  value,
  onChange,
  disabled,
  options,
  loading = false,
}) => {
  const id = inputId(label);

  const handleOnChange = (value: number | string) => {
    const option = options.find((option) => option.id === value);

    onChange({
      target: {
        value: option,
      },
    });
  };

  return (
    <FormInputWrapper id={id} errors={errors} description={description}>
      <Listbox
        defaultValue={value?.id}
        onChange={handleOnChange}
        disabled={disabled}
      >
        {({ open }) => (
          <>
            <Listbox.Label className="block text-sm font-medium text-gray-700">
              {label}
            </Listbox.Label>
            <div className="relative mt-1">
              <Listbox.Button
                {...descriptionInputAttributes({ id, description })}
                {...errorInputAttributes({ errors, id })}
                className="relative w-full py-2 pl-3 pr-10 text-left bg-white border border-gray-300 rounded-md shadow-sm cursor-default focus:outline-none focus:ring-1 focus:ring-offset-0 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              >
                <span className="block truncate">
                  {value?.name || buttonLabel}
                </span>
                <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                  <Icon svg={selectArrow} size={6} />
                </span>
              </Listbox.Button>

              <Transition
                show={open}
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options
                  static
                  className="absolute z-10 w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm"
                >
                  {loading
                    ? range(0, 6).map((option) => (
                        <Listbox.Option
                          key={option}
                          className="relative py-2 pl-3 cursor-default select-none pr-9 "
                          disabled
                          value={option}
                        >
                          {() => (
                            <SkeletonLoader className="w-full h-4 rounded" />
                          )}
                        </Listbox.Option>
                      ))
                    : options.map((option) => (
                        <Listbox.Option
                          key={option.id}
                          className={({ active }) =>
                            twMerge(
                              active
                                ? "text-white bg-indigo-600"
                                : "text-gray-900",
                              "cursor-default select-none relative py-2 pl-3 pr-9"
                            )
                          }
                          value={option.id}
                        >
                          {({ selected, active }) => (
                            <>
                              <span
                                className={twMerge(
                                  selected ? "font-semibold" : "font-normal",
                                  "block truncate"
                                )}
                              >
                                {option.name}
                              </span>

                              {selected ? (
                                <span
                                  className={twMerge(
                                    active ? "text-white" : "text-indigo-600",
                                    "absolute inset-y-0 right-0 flex items-center pr-4"
                                  )}
                                >
                                  <Icon svg={checkmarkIcon} />
                                </span>
                              ) : null}
                            </>
                          )}
                        </Listbox.Option>
                      ))}
                </Listbox.Options>
              </Transition>
            </div>
          </>
        )}
      </Listbox>
    </FormInputWrapper>
  );
};

export default Select;
