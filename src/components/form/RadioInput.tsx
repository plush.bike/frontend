import { forwardRef, ForwardRefRenderFunction } from "react";
import { twMerge } from "tailwind-merge";

import { inputId } from "@/helpers/form";
import { RadioInputProps } from "@/types/form";

const RadioInput: ForwardRefRenderFunction<
  HTMLInputElement,
  RadioInputProps
> = ({ label, className, ...props }, ref) => {
  const id = inputId(label);

  return (
    <div className="flex items-center">
      <input
        {...props}
        id={id}
        ref={ref}
        type="radio"
        className={twMerge(
          "h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-500",
          className
        )}
      />
      <label
        htmlFor={id}
        className="ml-3 block text-sm font-medium text-gray-700"
      >
        {label}
      </label>
    </div>
  );
};

export default forwardRef(RadioInput);
