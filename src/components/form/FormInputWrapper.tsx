import { isArray, isFunction, uniqueId } from "lodash";
import React, { ComponentPropsWithRef } from "react";
import { FieldError } from "react-hook-form";

import Description from "@/components/form/Description";
import { hasErrors } from "@/helpers/form";
import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"div"> & {
  id: string;
  errors?: FieldError | FieldError[];
  description?: string | (() => JSX.Element);
  descriptionClassName?: string;
};

const FormInputWrapper: FunctionComponent<Props> = ({
  id,
  errors,
  description,
  descriptionClassName,
  children,
  ...props
}) => {
  const renderErrors = () => {
    if (isArray(errors)) {
      return (
        <div id={`${id}-error`}>
          {errors.map((error) => (
            <p key={uniqueId()} className="mt-2 text-sm text-red-600">
              {error.message}
            </p>
          ))}
        </div>
      );
    }

    return (
      <p id={`${id}-error`} className="mt-2 text-sm text-red-600">
        {errors.message}
      </p>
    );
  };

  return (
    <div {...props}>
      {children}
      {description && (
        <Description id={`${id}-description`} className={descriptionClassName}>
          {isFunction(description) ? description() : description}
        </Description>
      )}
      {hasErrors(errors) && renderErrors()}
    </div>
  );
};

export default FormInputWrapper;
