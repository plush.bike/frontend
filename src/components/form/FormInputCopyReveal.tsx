import React, {
  forwardRef,
  ForwardRefRenderFunction,
  ComponentPropsWithRef,
  useState,
  useMemo,
  FocusEvent,
  ClipboardEvent,
} from "react";
import { twMerge } from "tailwind-merge";

import ButtonCopyToClipboard from "@/components/buttons/ButtonCopyToClipboard";
import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import Icon from "@/components/Icon";
import {
  descriptionInputAttributes,
  errorInputAttributes,
  hasErrors,
  inputId,
} from "@/helpers/form";
import clipboardIcon from "@/icons/clipboard.svg";
import viewHideIcon from "@/icons/view-hide.svg";
import viewShowIcon from "@/icons/view-show.svg";
import { SharedInputProps } from "@/types/form";

type Props = ComponentPropsWithRef<"input"> &
  SharedInputProps & {
    value: string;
  };

const FormInputCopyReveal: ForwardRefRenderFunction<HTMLInputElement, Props> = (
  { label, description, className, wrapperClassName, errors, value, ...props },
  ref
) => {
  const id = inputId(label);

  const [isValueVisible, setIsValueVisible] = useState(false);

  const computedValue = useMemo(() => {
    if (isValueVisible) {
      return value;
    }

    return "*".repeat(20);
  }, [value, isValueVisible]);

  const handleRevealButtonClick = () => {
    setIsValueVisible(!isValueVisible);
  };

  const handleInputFocus = (event: FocusEvent<HTMLInputElement>) => {
    event.target.select();
  };

  const handleManualCopy = (event: ClipboardEvent<HTMLInputElement>) => {
    if (isValueVisible) {
      return;
    }

    event.clipboardData.setData("text/plain", value);
    event.preventDefault();
  };

  return (
    <FormInputWrapper
      id={id}
      errors={errors}
      description={description}
      className={wrapperClassName}
    >
      <Label htmlFor={id}>{label}</Label>
      <div className="flex mt-1 rounded-md shadow-sm">
        <div className="relative flex items-stretch flex-grow focus-within:z-10">
          <input
            {...props}
            ref={ref}
            id={id}
            value={computedValue}
            type="text"
            readOnly
            className={twMerge(
              "shadow-sm block w-full sm:text-sm rounded-none rounded-l-md font-mono",
              hasErrors(errors)
                ? "border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500"
                : "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300",
              className
            )}
            onCopy={handleManualCopy}
            onFocus={handleInputFocus}
            {...descriptionInputAttributes({ id, description })}
            {...errorInputAttributes({ errors, id })}
          />
        </div>
        <button
          onClick={handleRevealButtonClick}
          type="button"
          className="relative inline-flex items-center px-4 py-2 -ml-px space-x-2 text-sm font-medium text-gray-700 border border-gray-300 bg-gray-50 hover:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
          aria-label={isValueVisible ? "Hide value" : "Reveal value"}
        >
          <Icon svg={isValueVisible ? viewHideIcon : viewShowIcon} size={5} />
        </button>
        <ButtonCopyToClipboard
          contentToCopy={value}
          renderButton={({ onClick }) => (
            <button
              onClick={onClick}
              type="button"
              className="relative inline-flex items-center px-4 py-2 -ml-px space-x-2 text-sm font-medium text-gray-700 border border-gray-300 rounded-r-md bg-gray-50 hover:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
              aria-label="Copy"
            >
              <Icon svg={clipboardIcon} />
            </button>
          )}
        />
      </div>
    </FormInputWrapper>
  );
};

export default forwardRef(FormInputCopyReveal);
