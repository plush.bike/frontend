import { parseInt, isUndefined, isNaN, toString, isString } from "lodash";
import React, {
  forwardRef,
  ForwardRefRenderFunction,
  ComponentPropsWithRef,
  useRef,
  useImperativeHandle,
  useState,
  ChangeEvent,
  useEffect,
  useMemo,
} from "react";
import { twMerge } from "tailwind-merge";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import Icon from "@/components/Icon";
import Tooltip from "@/components/Tooltip";
import {
  descriptionInputAttributes,
  errorInputAttributes,
  hasErrors,
  inputId,
} from "@/helpers/form";
import minusIcon from "@/icons/minus.svg";
import plusIcon from "@/icons/plus.svg";
import { SharedInputProps } from "@/types/form";
import { SvgIcon } from "@/types/svg";

type Props = ComponentPropsWithRef<"input"> &
  SharedInputProps & {
    min?: number | string;
    max?: number | string;
    step?: number;
    decrementLabel?: string;
    incrementLabel?: string;
    decrementTooltipContent?: string;
    incrementTooltipContent?: string;
    decrementIcon?: SvgIcon;
    incrementIcon?: SvgIcon;
  };

const NumberInput: ForwardRefRenderFunction<HTMLInputElement, Props> = (
  {
    label,
    description,
    descriptionClassName,
    labelHidden = false,
    className,
    wrapperClassName,
    errors,
    min,
    max,
    step = 1,
    decrementLabel,
    incrementLabel,
    decrementTooltipContent,
    incrementTooltipContent,
    decrementIcon,
    incrementIcon,
    onChange,
    ...props
  },
  ref
) => {
  const id = inputId(label);

  const inputRef = useRef<HTMLInputElement>(null);
  useImperativeHandle(ref, () => inputRef.current, []);

  const [value, setValue] = useState<string>("0");

  const valueAsInt = useMemo(() => {
    if (isUndefined(value) || value === "") {
      return 0;
    }

    return parseInt(value);
  }, [value]);

  const isMinSet = useMemo(() => {
    return !isUndefined(min) && !isNaN(min);
  }, [min]);

  const isMaxSet = useMemo(() => {
    return !isUndefined(max) && !isNaN(max);
  }, [max]);

  const minAsInt = useMemo(() => {
    if (!isMinSet) return undefined;

    return isString(min) ? parseInt(min) : min;
  }, [min]);

  const maxAsInt = useMemo(() => {
    if (!isMaxSet) return undefined;

    return isString(max) ? parseInt(max) : max;
  }, [max]);

  const isDecrementButtonDisabled = useMemo(() => {
    if (value === "") {
      return true;
    }

    return valueAsInt <= minAsInt;
  }, [value, valueAsInt, minAsInt]);

  const isIncrementButtonDisabled = useMemo(() => {
    if (value === "") {
      return true;
    }

    return valueAsInt >= maxAsInt;
  }, [value, valueAsInt, maxAsInt]);

  useEffect(() => {
    setValue(inputRef.current?.value || "0");
  }, []);

  useEffect(() => {
    if (!isMaxSet || valueAsInt <= maxAsInt) {
      return;
    }

    triggerChangeEvent(toString(max));
  }, [max]);

  useEffect(() => {
    if (!isMinSet || valueAsInt >= minAsInt) {
      return;
    }

    triggerChangeEvent(toString(min));
  }, [min]);

  const triggerChangeEvent = (value: string) => {
    const nativeInputValueSetter = Object.getOwnPropertyDescriptor(
      window.HTMLInputElement.prototype,
      "value"
    ).set;
    nativeInputValueSetter.call(inputRef.current, value);

    inputRef.current.dispatchEvent(new Event("change", { bubbles: true }));
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const eventTargetValueAsInt = parseInt(event.target.value);

    if (isMaxSet && eventTargetValueAsInt > maxAsInt) {
      event.target.value = toString(max);
      setValue(toString(max));
      onChange(event);

      return;
    }

    if (isMinSet && eventTargetValueAsInt < minAsInt) {
      event.target.value = toString(min);
      setValue(toString(min));
      onChange(event);

      return;
    }

    setValue(event.target.value);
    onChange(event);
  };

  const handleMinusClick = () => {
    if (!inputRef.current) {
      return;
    }

    triggerChangeEvent(`${Math.round((valueAsInt - step) / step) * step}`);
  };

  const handlePlusClick = () => {
    if (!inputRef.current) {
      return;
    }

    triggerChangeEvent(`${Math.round((valueAsInt + step) / step) * step}`);
  };

  return (
    <FormInputWrapper
      id={id}
      errors={errors}
      description={description}
      descriptionClassName={twMerge("text-center", descriptionClassName)}
      className={wrapperClassName}
    >
      <Label htmlFor={id} hidden={labelHidden}>
        {label}
      </Label>
      <div className="flex mt-1">
        <Tooltip content={decrementTooltipContent}>
          <button
            type="button"
            className="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-tl rounded-bl shadow-sm hover:bg-indigo-700"
            onClick={handleMinusClick}
            disabled={isDecrementButtonDisabled}
            aria-label={!decrementLabel ? `Decrement by ${step}` : null}
          >
            {decrementLabel || <Icon svg={minusIcon} />}
            {decrementIcon && (
              <Icon className="ml-2 -mr-0.5" size={5} svg={decrementIcon} />
            )}
          </button>
        </Tooltip>
        <input
          {...props}
          ref={inputRef}
          id={id}
          type="number"
          className={twMerge(
            "shadow-sm block sm:text-sm w-24 text-center",
            hasErrors(errors)
              ? "border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500"
              : "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300",
            className
          )}
          min={isMinSet ? min : undefined}
          max={isMaxSet ? max : undefined}
          step={step}
          onChange={handleChange}
          {...descriptionInputAttributes({ id, description })}
          {...errorInputAttributes({ errors, id })}
        />
        <Tooltip content={incrementTooltipContent}>
          <button
            type="button"
            className="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-tr rounded-br shadow-sm hover:bg-indigo-700"
            onClick={handlePlusClick}
            disabled={isIncrementButtonDisabled}
            aria-label={!incrementLabel ? `Increment by ${step}` : null}
          >
            {incrementIcon && (
              <Icon className="-ml-0.5 mr-2" size={5} svg={incrementIcon} />
            )}
            {incrementLabel || <Icon svg={plusIcon} />}
          </button>
        </Tooltip>
      </div>
    </FormInputWrapper>
  );
};

export default forwardRef(NumberInput);
