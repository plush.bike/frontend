import { uniqueId } from "lodash";
import React, { useState, useEffect } from "react";
import { useDropzone, DropzoneOptions } from "react-dropzone";
import { FieldError } from "react-hook-form";

import FileThumbnail from "@/components/form/file-upload/FileThumbnail";
import FormInputWrapper from "@/components/form/FormInputWrapper";
import Icon from "@/components/Icon";
import { SavedFile } from "@/helpers/file-upload";
import { inputId, parseServerErrorFields } from "@/helpers/form";
import { classToObject } from "@/helpers/general";
import photoIcon from "@/icons/photo.svg";
import {
  FileUploadMutationHook,
  FileUploadResponse,
  FileExtended,
  FileUploadChangeEvent,
} from "@/types/file-upload";
import { SharedInputProps } from "@/types/form";
import { FunctionComponent } from "@/types/react";

type Props = SharedInputProps & {
  initialFiles?: SavedFile[];
  useMutation: FileUploadMutationHook;
  mutationKey: string;
  dropzoneOptions?: DropzoneOptions;
  onChange?: (event: FileUploadChangeEvent) => void;
};

const FileUpload: FunctionComponent<Props> = ({
  initialFiles = [],
  label,
  useMutation,
  mutationKey,
  errors: errorsProp,
  dropzoneOptions = {},
  onChange,
}) => {
  const id = inputId(label);

  const [files, setFiles] =
    useState<(FileExtended | SavedFile)[]>(initialFiles);
  const [error, setError] = useState<FieldError[]>([]);
  const [uploadFiles] = useMutation();

  const handleOnDrop = async (acceptedFiles: File[]) => {
    const acceptedFilesExtended = acceptedFiles.map((file) => ({
      ...classToObject<File>(file),
      id: null,
      preview: URL.createObjectURL(file),
      key: uniqueId(),
      loading: true,
      src: null,
      error: null,
    }));

    const alreadyUploadedFiles = [...files];
    setFiles([...files, ...acceptedFilesExtended]);

    try {
      const { data } = await uploadFiles({
        variables: {
          files: acceptedFiles,
        },
      });

      const newFiles = [
        ...alreadyUploadedFiles,
        ...acceptedFilesExtended.map((file, index) => ({
          ...file,
          loading: false,
          src: (data[mutationKey]?.[index] as FileUploadResponse)?.src || null,
        })),
      ];

      setFiles(newFiles);
      onChange(newFiles);
    } catch (error) {
      const filesErrors = parseServerErrorFields(error).files;

      if (filesErrors) {
        setError(
          filesErrors.map((fileError, index) => {
            const file = acceptedFilesExtended[index];

            if (file) {
              return {
                type: "server",
                message: `${file.name}: ${fileError}`,
              };
            }

            return {
              type: "server",
              message: fileError,
            };
          })
        );
      } else {
        setError([
          {
            type: "server",
            message: "An error occurred, please try again.",
          },
        ]);
      }

      setFiles(alreadyUploadedFiles);
    }
  };

  const handleOnRemoveFile = (fileToRemove: FileExtended) => {
    const filteredFiles = files.filter((file) => file.key !== fileToRemove.key);

    setFiles(filteredFiles);
    onChange(filteredFiles);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleOnDrop,
    accept: {
      "image/*": [".jpg", ".jpeg", ".png", ".bmp", ".gif", ".svg", ".webp"],
    },
    noClick: true,
    ...dropzoneOptions,
  });

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <FormInputWrapper id={id} errors={error || errorsProp}>
      <span className="block text-sm font-medium text-gray-700">{label}</span>
      <div
        {...getRootProps()}
        className="flex items-center justify-center px-6 pt-5 pb-6 mt-1 border-2 border-gray-300 border-dashed rounded-md min-h-44"
      >
        <div className="w-full space-y-1 text-center">
          {isDragActive ? (
            <p className="py-8 text-sm text-gray-600">Drop the files here...</p>
          ) : (
            <>
              <Icon
                className="mx-auto text-gray-400"
                svg={photoIcon}
                size={10}
              />
              <div className="flex justify-center text-sm text-gray-600">
                <label
                  htmlFor={id}
                  className="relative px-2 font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                >
                  <span>Upload a file</span>
                  <input
                    id={id}
                    name={id}
                    className="sr-only"
                    {...{ ...getInputProps(), style: null }}
                  />
                </label>
                <p className="pl-1">or drag and drop</p>
              </div>
              <p className="text-xs text-gray-500">PNG, JPG, GIF up to 10MB</p>
            </>
          )}
          <aside className="flex flex-wrap -m-4">
            {files.map((file, index) => (
              <FileThumbnail
                key={file.key}
                file={file}
                onRemove={handleOnRemoveFile}
                data-testid={`fileThumbnailContainer${index}`}
              />
            ))}
          </aside>
        </div>
      </div>
    </FormInputWrapper>
  );
};

export default FileUpload;
