import React, { ComponentPropsWithRef, MouseEvent } from "react";
import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import Loader from "@/components/Loader";
import { SavedFile } from "@/helpers/file-upload";
import checkmarkIcon from "@/icons/checkmark.svg";
import closeIcon from "@/icons/close.svg";
import { FileExtended } from "@/types/file-upload";
import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"div"> & {
  file: FileExtended | SavedFile;
  onRemove: (file: FileExtended | SavedFile) => void;
};

const FileThumbnail: FunctionComponent<Props> = ({
  file,
  onRemove,
  ...props
}) => {
  const handleOnClick = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault;

    onRemove(file);
  };

  return (
    <div {...props} className="p-4">
      <div
        key={file.key}
        className="relative w-16 h-16 bg-center bg-cover border border-gray-300 rounded-md"
        style={{ backgroundImage: `url(${file.preview || file.src})` }}
        data-testid="fileThumbnail"
      >
        <div
          className={twMerge(
            "absolute bottom-0.5 left-0.5 rounded-full p-1",
            file.loading ? "bg-indigo-600" : "bg-emerald-500"
          )}
        >
          {file.loading ? (
            <Loader size={2} className="text-white" />
          ) : (
            <Icon size={2} className="text-white" svg={checkmarkIcon} />
          )}
        </div>
        <button
          aria-label="Remove file"
          className="absolute inline-flex items-center p-1 text-white bg-indigo-600 border border-transparent rounded-full shadow-sm -top-2 -right-2 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={handleOnClick}
          type="button"
        >
          <Icon svg={closeIcon} size={3} />
        </button>
      </div>
    </div>
  );
};

export default FileThumbnail;
