import ReCAPTCHA from "react-google-recaptcha";
import { FieldError } from "react-hook-form";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import { inputId } from "@/helpers/form";
import { FunctionComponent } from "@/types/react";

type Props = {
  onChange(event: { target: { value: string } }): void;
  errors?: FieldError | FieldError[];
};

const Recaptcha: FunctionComponent<Props> = ({ errors, onChange }) => {
  const id = inputId("recaptcha");

  const handleOnChange = (value: string) => {
    onChange({ target: { value } });
  };

  return (
    <FormInputWrapper id={id} errors={errors}>
      <ReCAPTCHA
        sitekey="6Lfff0wfAAAAAJRD1t9tnpngbWbh4du0sktWENbV"
        onChange={handleOnChange}
        className="md:scale-100 scale-[0.8] origin-top-left"
      />
    </FormInputWrapper>
  );
};

export default Recaptcha;
