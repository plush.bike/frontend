import React, {
  forwardRef,
  ForwardRefRenderFunction,
  ComponentPropsWithRef,
  useState,
} from "react";
// eslint-disable-next-line import/no-named-as-default
import ReactDatePicker from "react-datepicker";
import { twMerge } from "tailwind-merge";
import "react-datepicker/dist/react-datepicker.css";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import Icon from "@/components/Icon";
import { errorInputAttributes, hasErrors, inputId } from "@/helpers/form";
import calendarIcon from "@/icons/calendar.svg";
import exclamationSolidIcon from "@/icons/exclamation-solid.svg";
import { SharedInputProps } from "@/types/form";

type Props = ComponentPropsWithRef<"input"> & SharedInputProps;

const Datepicker: ForwardRefRenderFunction<HTMLInputElement, Props> = (
  {
    label,
    labelHidden = false,
    type = "text",
    className,
    wrapperClassName,
    errors,
    ...props
  },
  ref
) => {
  const id = inputId(label);

  const [startDate, setStartDate] = useState(new Date());

  const handleChange = (date: Date) => {
    setStartDate(date);
  };

  return (
    <FormInputWrapper id={id} errors={errors} className={wrapperClassName}>
      <Label htmlFor={id} hidden={labelHidden}>
        {label}
      </Label>
      <div className="relative mt-1">
        <ReactDatePicker
          selected={startDate}
          onChange={handleChange}
          customInput={
            <input
              {...props}
              ref={ref}
              id={id}
              type={type}
              className={twMerge(
                "shadow-sm block w-full sm:text-sm rounded-md",
                hasErrors(errors)
                  ? "border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500"
                  : "focus:ring-indigo-500 focus:border-indigo-500 border-gray-300",
                className
              )}
              {...errorInputAttributes({ errors, id })}
            />
          }
        />
        {hasErrors(errors) && (
          <div className="absolute inset-y-0 flex items-center pr-3 pointer-events-none right-8">
            <Icon
              svg={exclamationSolidIcon}
              size={5}
              className="text-red-500"
            />
          </div>
        )}
        <div className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
          <Icon svg={calendarIcon} size={5} className="text-gray-400" />
        </div>
      </div>
    </FormInputWrapper>
  );
};

export default forwardRef(Datepicker);
