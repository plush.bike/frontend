import { ComponentPropsWithRef } from "react";
import { twMerge } from "tailwind-merge";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import Label from "@/components/form/Label";
import {
  descriptionInputAttributes,
  errorInputAttributes,
  inputId,
} from "@/helpers/form";
import { SharedInputProps } from "@/types/form";
import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"div"> & SharedInputProps;

const RadioGroup: FunctionComponent<Props> = ({
  label,
  description,
  labelHidden = false,
  descriptionClassName,
  errors,
  children,
  ...props
}) => {
  const id = inputId(label);
  const labelId = `${id}-label`;

  return (
    <div
      id={id}
      role="radiogroup"
      aria-labelledby={labelId}
      {...descriptionInputAttributes({ id, description })}
      {...errorInputAttributes({ errors, id })}
      {...props}
    >
      <Label id={labelId} hidden={labelHidden}>
        {label}
      </Label>
      <FormInputWrapper
        id={id}
        errors={errors}
        description={description}
        descriptionClassName={twMerge("mt-0", descriptionClassName)}
      />
      <div className="space-y-2 mt-1">{children}</div>
    </div>
  );
};

export default RadioGroup;
