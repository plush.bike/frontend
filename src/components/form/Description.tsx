import React, { ComponentPropsWithRef } from "react";
import { twMerge } from "tailwind-merge";

import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"p">;

const Description: FunctionComponent<Props> = ({
  children,
  className,
  ...props
}) => {
  return (
    <p {...props} className={twMerge("mt-2 text-sm text-gray-500", className)}>
      {children}
    </p>
  );
};

export default Description;
