import { noop } from "lodash";
import { Dispatch, SetStateAction, useRef } from "react";
import { SubmitErrorHandler, SubmitHandler, useForm } from "react-hook-form";

import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import ModalConfirm from "@/components/modals/ModalConfirm";
import { INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE } from "@/constants/auth";
import { MODAL_VARIANT_PRIMARY, MODAL_VARIANT_RED } from "@/constants/modal";
import { useConfirmPasswordMutation } from "@/graphql/auth/mutations/confirm-password.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { FunctionComponent } from "@/types/react";

type Props = {
  modalTrigger: (props: {
    onError: (error: any) => boolean;
  }) => JSX.Element | null;
  continueButton?: string;
  cancelButton?: string;
  variant?: typeof MODAL_VARIANT_PRIMARY | typeof MODAL_VARIANT_RED;
  onPasswordConfirmed: () => Promise<void>;
};

type Inputs = {
  password: string;
};

const ModalConfirmPassword: FunctionComponent<Props> = ({
  children,
  modalTrigger,
  continueButton,
  cancelButton,
  variant,
  onPasswordConfirmed,
}) => {
  const setModalShow = useRef<Dispatch<SetStateAction<boolean>>>(null);
  const fireModalContinue = useRef<() => void>(noop);

  const [confirmPasswordMutation] = useConfirmPasswordMutation();

  const {
    register,
    handleSubmit,
    setValue,
    setError,
    formState: { errors },
  } = useForm<Inputs>();

  const handleOnError = (error: any) => {
    if (error.message !== INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE) {
      return false;
    }

    setModalShow.current(true);
    return true;
  };

  const confirmPassword: SubmitHandler<Inputs> = async ({ password }) => {
    try {
      await confirmPasswordMutation({ variables: { password } });
      await onPasswordConfirmed();
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage: noop,
        setError,
      });

      setValue("password", "");

      throw new Error();
    }
  };

  const handleSubmitError: SubmitErrorHandler<Inputs> = (errors) => {
    if (errors.password) {
      setError("password", errors.password);
    }

    throw new Error();
  };

  const handleContinue = async () => {
    try {
      await handleSubmit(confirmPassword, handleSubmitError)();
    } catch (error) {
      return Promise.resolve({ shouldHide: false });
    }
  };

  const onSubmit = () => {
    fireModalContinue.current();
  };

  return (
    <ModalConfirm
      title="Confirm password"
      modalTrigger={({ setShow, fireContinue }) => {
        setModalShow.current = setShow;
        fireModalContinue.current = fireContinue;

        return modalTrigger({ onError: handleOnError });
      }}
      continueButton={continueButton}
      cancelButton={cancelButton}
      variant={variant}
      onContinue={handleContinue}
    >
      {children}
      <FormWrapper className="mt-4" onSubmit={onSubmit}>
        <FormInput
          label="Password"
          type="password"
          description="Please confirm your password before continuing."
          errors={errors.password}
          {...register("password", {
            required: {
              value: true,
              message: "Please enter your password.",
            },
          })}
        />
      </FormWrapper>
    </ModalConfirm>
  );
};

export default ModalConfirmPassword;
