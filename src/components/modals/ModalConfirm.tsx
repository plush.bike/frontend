import { Dialog, Transition } from "@headlessui/react";
import { Dispatch, Fragment, SetStateAction, useMemo, useState } from "react";
import { twMerge } from "tailwind-merge";

import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import ButtonRed from "@/components/buttons/ButtonRed";
import ButtonWhite from "@/components/buttons/ButtonWhite";
import Icon from "@/components/Icon";
import { MODAL_VARIANT_PRIMARY, MODAL_VARIANT_RED } from "@/constants/modal";
import { isPromise } from "@/helpers/general";
import closeIcon from "@/icons/close.svg";
import warningIcon from "@/icons/warning.svg";
import { FunctionComponent } from "@/types/react";

type Props = {
  title: string;
  modalTrigger: (props: {
    onClick: () => void;
    setShow: Dispatch<SetStateAction<boolean>>;
    fireContinue: () => void;
  }) => JSX.Element | null;
  continueButton?: string;
  cancelButton?: string;
  onContinue: () => void | Promise<{ shouldHide?: boolean } | void>;
  variant?: typeof MODAL_VARIANT_PRIMARY | typeof MODAL_VARIANT_RED;
};

const ModalConfirm: FunctionComponent<Props> = ({
  title,
  children,
  modalTrigger,
  continueButton = "Continue",
  cancelButton = "Cancel",
  onContinue,
  variant = MODAL_VARIANT_PRIMARY,
}) => {
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);

  const closeModal = () => {
    setShow(false);
  };

  const openModal = () => {
    setShow(true);
  };

  const handleContinue = async () => {
    const onContinueResult = onContinue();

    if (isPromise(onContinueResult)) {
      setLoading(true);

      const result = await (onContinueResult as Promise<{
        shouldHide?: boolean;
      }>);

      const { shouldHide = true } = result || {};

      if (shouldHide) {
        setShow(false);
      }

      setLoading(false);
    } else {
      setShow(false);
    }
  };

  const ButtonContinue = useMemo(
    () => (variant === MODAL_VARIANT_PRIMARY ? ButtonPrimary : ButtonRed),
    [variant]
  );

  return (
    <>
      {modalTrigger({
        onClick: openModal,
        setShow,
        fireContinue: handleContinue,
      })}

      <Transition.Root show={show} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={closeModal}
        >
          <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="inline-block px-4 pt-5 pb-4 overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
                <div className="absolute top-0 right-0 hidden pt-4 pr-4 sm:block">
                  <button
                    type="button"
                    className="text-gray-400 bg-white rounded-md hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    onClick={closeModal}
                  >
                    <span className="sr-only">Close</span>
                    <Icon size={6} svg={closeIcon} />
                  </button>
                </div>
                <div className="sm:flex sm:items-start">
                  <div
                    className={twMerge(
                      "flex items-center justify-center shrink-0 w-12 h-12 mx-auto rounded-full sm:mx-0 sm:h-10 sm:w-10",
                      variant === MODAL_VARIANT_PRIMARY
                        ? "bg-indigo-100"
                        : "bg-red-100"
                    )}
                  >
                    <Icon
                      svg={warningIcon}
                      size={6}
                      className={
                        variant === MODAL_VARIANT_PRIMARY
                          ? "text-indigo-500"
                          : "text-red-500"
                      }
                    />
                  </div>
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left grow">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-gray-900"
                    >
                      {title}
                    </Dialog.Title>
                    <div className="mt-2">{children}</div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
                  <ButtonContinue
                    className="w-full sm:ml-3 sm:w-auto"
                    loading={loading}
                    onClick={handleContinue}
                  >
                    {continueButton}
                  </ButtonContinue>
                  <ButtonWhite
                    className="w-full mt-3 sm:mt-0 sm:w-auto"
                    onClick={closeModal}
                  >
                    {cancelButton}
                  </ButtonWhite>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
};

export default ModalConfirm;
