import React, { ComponentPropsWithRef } from "react";
import { twMerge } from "tailwind-merge";

import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"div"> & {
  wrapperClassName?: string;
};

const SkeletonLoader: FunctionComponent<Props> = ({
  wrapperClassName,
  className,
}) => {
  return (
    <div
      className={twMerge("animate-pulse", wrapperClassName)}
      data-testid="skeletonLoader"
    >
      <div className={twMerge("bg-gray-300", className)} />
    </div>
  );
};

export default SkeletonLoader;
