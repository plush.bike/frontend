import React from "react";
import { twMerge } from "tailwind-merge";

import ToastBase from "@/components/toasts/ToastBase";
import closeSolidIcon from "@/icons/close-solid.svg";
import { FunctionComponent } from "@/types/react";
import { BaseProps as Props } from "@/types/toasts";

const ToastError: FunctionComponent<Props> = ({
  show,
  className,
  children,
  ...props
}) => {
  return (
    <ToastBase
      {...props}
      show={show}
      icon={closeSolidIcon}
      className={twMerge("text-red-700 bg-red-50 border-red-400", className)}
      dataTestid="toastError"
    >
      {children}
    </ToastBase>
  );
};

export default ToastError;
