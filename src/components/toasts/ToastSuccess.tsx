import React from "react";
import { twMerge } from "tailwind-merge";

import ToastBase from "@/components/toasts/ToastBase";
import checkmarkOutlineIcon from "@/icons/checkmark-outline.svg";
import { FunctionComponent } from "@/types/react";
import { BaseProps as Props } from "@/types/toasts";

const ToastSuccess: FunctionComponent<Props> = ({
  show,
  className,
  children,
  ...props
}) => {
  return (
    <ToastBase
      {...props}
      show={show}
      icon={checkmarkOutlineIcon}
      className={twMerge(
        "text-emerald-700 bg-emerald-50 border-green-400",
        className
      )}
      dataTestid="toastSuccess"
    >
      {children}
    </ToastBase>
  );
};

export default ToastSuccess;
