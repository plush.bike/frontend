import { Transition } from "@headlessui/react";
import React, { useEffect, useRef } from "react";
import { createPortal } from "react-dom";
import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import closeIcon from "@/icons/close.svg";
import { FunctionComponent } from "@/types/react";
import { SvgIcon } from "@/types/svg";
import { BaseProps } from "@/types/toasts";

type Props = BaseProps & {
  icon: SvgIcon;
};

const ToastBase: FunctionComponent<Props> = ({
  icon,
  show,
  showFor = 5000,
  onClose,
  className,
  dataTestid = "toastBase",
  children,
  ...props
}) => {
  const timeout = useRef(null);
  useEffect(() => {
    if (!show || !showFor) {
      if (timeout.current !== null) {
        clearTimeout(timeout.current);
      }

      return;
    }

    timeout.current = setTimeout(() => {
      if (!onClose) {
        return;
      }

      onClose();
    }, showFor);

    return () => {
      clearTimeout(timeout.current);
    };
  }, [show]);

  return process.browser
    ? createPortal(
        <Transition
          appear={false}
          show={show}
          enter="transition duration-500"
          enterFrom="opacity-0 toast-transform"
          enterTo="opacity-100 transform-none"
          leave="transition duration-500"
          leaveFrom="opacity-100 transform-none"
          leaveTo="opacity-0 toast-transform"
          {...props}
          className={twMerge(
            "items-center pl-3 pr-10 py-2 border-l-4 shadow-md fixed top-28 right-4 flex toast",
            className
          )}
          data-testid={dataTestid}
        >
          <div className="mr-3" data-testid="toastIcon">
            <Icon svg={icon} size={5} />
          </div>
          <div className="absolute top-0 right-0 p-2">
            <button aria-label="Close notification" onClick={onClose}>
              <Icon svg={closeIcon} />
            </button>
          </div>
          <div className="max-w-xs">{children}</div>
        </Transition>,
        document.body
      )
    : null;
};

export default ToastBase;
