import { Menu } from "@headlessui/react";
import { useRouter } from "next/router";
import React from "react";
import { useDispatch } from "react-redux";

import Dropdown from "@/components/dropdown/Dropdown";
import DropdownItem from "@/components/dropdown/DropdownItem";
import Icon from "@/components/Icon";
import Image from "@/components/Image";
import { useApollo } from "@/graphql/apolloClient";
import { useSignOutMutation } from "@/graphql/auth/mutations/sign-out.generated";
import { useAuth } from "@/hooks/auth";
import userSolidCircleIcon from "@/icons/user-solid-circle.svg";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError } from "@/store/slices/toasts";
import { FunctionComponent } from "@/types/react";

const AvatarDropdown: FunctionComponent = () => {
  const { authenticatedUser } = useAuth();
  const [signOut] = useSignOutMutation();
  const dispatch = useDispatch();
  const router = useRouter();
  const apolloClient = useApollo();

  const handleSignOutClick = async () => {
    try {
      await signOut();
      dispatch(updateAuthenticatedUser(null));
      router.push("/sign-in");
      apolloClient.clearStore();
    } catch (error) {
      dispatch(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    }
  };

  const buttonRender = () => (
    <Menu.Button className="flex items-center max-w-xs text-sm bg-white rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      <span className="sr-only">Open user menu</span>
      {authenticatedUser.profileImageUrl ? (
        <Image
          className="w-8 h-8 rounded-full"
          url={authenticatedUser.profileImageUrl}
          height={64}
          width={64}
        />
      ) : (
        <Icon svg={userSolidCircleIcon} className="text-gray-400" size={8} />
      )}
    </Menu.Button>
  );

  return (
    <Dropdown buttonRender={buttonRender}>
      <div className="px-4 py-3">
        <p className="text-sm">Signed in as</p>
        <p className="text-sm font-medium text-gray-900 truncate">
          {authenticatedUser.name}
        </p>
      </div>
      <DropdownItem href="/account-settings">Account settings</DropdownItem>
      <DropdownItem onClick={handleSignOutClick}>Sign out</DropdownItem>
    </Dropdown>
  );
};

export default AvatarDropdown;
