import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";

import Icon from "@/components/Icon";
import Link from "@/components/Link";
import SidebarLinks from "@/components/nav/SidebarLinks";
import closeIcon from "@/icons/close.svg";
import Logo from "@/svgs/logo.svg";
import { FunctionComponent } from "@/types/react";

type Props = {
  open: boolean;
  onClose: () => void;
};

const Sidebar: FunctionComponent<Props> = ({ open, onClose }) => {
  return (
    <>
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 z-40 flex md:hidden"
          data-testid="mobileNav"
          open={open}
          onClose={onClose}
        >
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-600 bg-opacity-75" />
          </Transition.Child>
          <Transition.Child
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
            <div className="relative flex flex-col flex-1 w-full max-w-xs pt-5 pb-4 bg-indigo-700">
              <Transition.Child
                as={Fragment}
                enter="ease-in-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in-out duration-300"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <div className="absolute top-0 right-0 pt-2 -mr-12">
                  <button
                    type="button"
                    className="flex items-center justify-center w-10 h-10 ml-1 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                    onClick={onClose}
                  >
                    <span className="sr-only">Close sidebar</span>
                    <Icon svg={closeIcon} className="text-white" />
                  </button>
                </div>
              </Transition.Child>
              <div className="flex items-center justify-center flex-shrink-0 px-4">
                <Link href="/parts/forks" aria-label="Home" onClick={onClose}>
                  <Logo className="w-32 h-auto" />
                </Link>
              </div>
              <div className="flex-1 h-0 mt-5 overflow-y-auto">
                <nav className="px-2 space-y-1">
                  <SidebarLinks onClick={onClose} />
                </nav>
              </div>
            </div>
          </Transition.Child>
          <div className="flex-shrink-0 w-14" aria-hidden="true">
            {/* Dummy element to force sidebar to shrink to fit close icon */}
          </div>
        </Dialog>
      </Transition.Root>

      {/* Static sidebar for desktop */}
      <div
        className="hidden bg-indigo-700 md:flex md:flex-col md:w-64 md:inset-y-0 md:fixed"
        data-testid="desktopNav"
      >
        <div className="flex flex-col w-64">
          {/* Sidebar component, swap this element with another sidebar if you like */}
          <div className="flex flex-col flex-grow pt-2 pb-4 overflow-y-auto">
            <div className="flex items-center justify-center flex-shrink-0 px-4">
              <Link href="/parts/forks" aria-label="Home">
                <Logo className="w-32 h-auto" />
              </Link>
            </div>
            <div className="flex flex-col flex-1 mt-5">
              <nav className="flex-1 px-2 space-y-1">
                <SidebarLinks />
              </nav>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Sidebar;
