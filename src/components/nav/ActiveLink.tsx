import { useRouter } from "next/router";
import React, { ComponentPropsWithRef } from "react";
import { twMerge } from "tailwind-merge";

import Link from "@/components/Link";
import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"a"> & {
  activeClassName?: string;
};

const ActiveLink: FunctionComponent<Props> = ({
  href,
  className = "",
  activeClassName = "",
  onClick,
  children,
}) => {
  const router = useRouter();

  return (
    <Link
      href={href}
      className={twMerge(
        className,
        router.pathname === href && activeClassName
      )}
      onClick={onClick}
    >
      {children}
    </Link>
  );
};

export default ActiveLink;
