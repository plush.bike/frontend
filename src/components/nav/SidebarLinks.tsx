import React, { MouseEventHandler } from "react";

import BadgePrimary from "@/components/badges/BadgePrimary";
import ActiveLink from "@/components/nav/ActiveLink";
import { FunctionComponent } from "@/types/react";

type Props = {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
};

const SidebarLinks: FunctionComponent<Props> = ({ onClick }) => {
  return (
    <>
      <ActiveLink
        href="/parts/forks"
        className="flex items-center px-2 py-2 text-base font-medium text-indigo-100 rounded-md group hover:bg-indigo-600"
        activeClassName="bg-indigo-800 text-white"
        onClick={onClick}
      >
        Forks
      </ActiveLink>
      <div className="flex items-center w-full px-2 py-2 text-base font-medium text-indigo-100 rounded-md cursor-not-allowed group hover:bg-indigo-600">
        Shocks
        <BadgePrimary className="ml-2">Coming soon</BadgePrimary>
      </div>
    </>
  );
};

export default SidebarLinks;
