import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import Loader from "@/components/Loader";
import { useAuthenticatedUserLazyQuery } from "@/graphql/auth/queries/authenticated-user.generated";
import { useAuth } from "@/hooks/auth";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { FunctionComponent } from "@/types/react";

const AuthenticationLayout: FunctionComponent = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const { csrfCookieSet, isAuthenticated } = useAuth();
  const dispatch = useDispatch();
  const [getAuthenticatedUser] = useAuthenticatedUserLazyQuery({
    fetchPolicy: "no-cache",
    onCompleted: ({ authenticatedUser }) => {
      dispatch(updateAuthenticatedUser(authenticatedUser));
      setLoading(false);
    },
    onError: () => {
      setLoading(false);
    },
  });

  const authenticate = () => {
    if (isAuthenticated) {
      setLoading(false);

      return;
    }

    getAuthenticatedUser();
  };

  useEffect(() => {
    if (csrfCookieSet) {
      authenticate();
    }
  }, [csrfCookieSet]);

  return <>{loading ? <Loader fullPage /> : children}</>;
};

export default AuthenticationLayout;
