import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { twMerge } from "tailwind-merge";

import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import ButtonSecondaryLink from "@/components/buttons/ButtonSecondaryLink";
import Icon from "@/components/Icon";
import Link from "@/components/Link";
import AvatarDropdown from "@/components/nav/AvatarDropdown";
import Sidebar from "@/components/nav/Sidebar";
import ToastError from "@/components/toasts/ToastError";
import ToastSuccess from "@/components/toasts/ToastSuccess";
import { fetchCsrfCookie } from "@/helpers/auth";
import { useAuth } from "@/hooks/auth";
import menuIcon from "@/icons/menu.svg";
import { updateCsrfCookieSet } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import LogoIndigo from "@/svgs/logo-indigo.svg";
import { FunctionComponent } from "@/types/react";
import { RootState } from "@/types/store";

type Props = {
  showNavigation: boolean;
  mainShouldHavePadding: boolean;
};

const PageLayout: FunctionComponent<Props> = ({
  showNavigation,
  mainShouldHavePadding,
  children,
}) => {
  const { isAuthenticated } = useAuth();
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const toastSuccess = useSelector((state: RootState) => state.toasts.success);
  const toastError = useSelector((state: RootState) => state.toasts.error);
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      await fetchCsrfCookie();
      dispatch(updateCsrfCookieSet(true));
    })();
  }, []);

  const handleToastSuccessClose = () => {
    dispatch(updateToastSuccess({ show: false }));
  };

  const handleToastErrorClose = () => {
    dispatch(updateToastError({ show: false }));
  };

  const handleSidebarClose = () => {
    setSidebarOpen(false);
  };

  const handleSidebarOpen = () => {
    setSidebarOpen(true);
  };

  return (
    <>
      <ToastSuccess show={toastSuccess.show} onClose={handleToastSuccessClose}>
        {toastSuccess.message}
      </ToastSuccess>
      <ToastError show={toastError.show} onClose={handleToastErrorClose}>
        {toastError.message}
      </ToastError>
      <div className="flex flex-col min-h-full">
        {showNavigation && isAuthenticated && (
          <Sidebar open={sidebarOpen} onClose={handleSidebarClose} />
        )}
        <div
          className={twMerge(
            "flex flex-col flex-1",
            showNavigation && isAuthenticated && "md:pl-64"
          )}
        >
          {showNavigation && (
            <div
              className="sticky top-0 z-10 flex flex-shrink-0 h-16 bg-white shadow"
              data-testid="topNavigation"
            >
              {isAuthenticated ? (
                <button
                  type="button"
                  className="px-4 text-gray-500 border-r border-gray-200 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 md:hidden"
                  onClick={handleSidebarOpen}
                >
                  <span className="sr-only">Open sidebar</span>
                  <Icon svg={menuIcon} size={6} />
                </button>
              ) : (
                <Link href="/" className="px-4" aria-label="Home">
                  <LogoIndigo className="h-auto w-28" />
                </Link>
              )}
              <div className="flex items-center justify-end flex-1 px-4 space-x-4">
                {isAuthenticated ? (
                  <AvatarDropdown />
                ) : (
                  <>
                    <div>
                      <ButtonPrimaryLink href="/sign-in">
                        Sign in
                      </ButtonPrimaryLink>
                    </div>
                    <div>
                      <ButtonSecondaryLink href="/sign-up">
                        Sign up
                      </ButtonSecondaryLink>
                    </div>
                  </>
                )}
              </div>
            </div>
          )}
          <main
            className={twMerge(
              "relative flex flex-col flex-1 overflow-y-auto focus:outline-none",
              mainShouldHavePadding && "py-6"
            )}
          >
            {children}
          </main>
        </div>
      </div>
    </>
  );
};

export default PageLayout;
