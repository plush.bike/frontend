import { ApolloProvider } from "@apollo/client";
import { Provider as ReduxProvider } from "react-redux";

import AuthenticationLayout from "@/components/layouts/AuthenticationLayout";
import PageLayout from "@/components/layouts/PageLayout";
import { BaseLayoutProps } from "@/types/layouts";
import { FunctionComponent } from "@/types/react";

const BaseLayout: FunctionComponent<BaseLayoutProps> = ({
  reduxStore,
  apolloClient,
  showNavigation = true,
  requireAuthentication = true,
  mainShouldHavePadding = true,
  children,
}) => {
  return (
    <ReduxProvider store={reduxStore}>
      <ApolloProvider client={apolloClient}>
        <PageLayout
          showNavigation={showNavigation}
          mainShouldHavePadding={mainShouldHavePadding}
        >
          {requireAuthentication ? (
            <AuthenticationLayout>{children}</AuthenticationLayout>
          ) : (
            children
          )}
        </PageLayout>
      </ApolloProvider>
    </ReduxProvider>
  );
};

export default BaseLayout;
