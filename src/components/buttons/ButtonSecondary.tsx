import React, { forwardRef, ForwardRefRenderFunction } from "react";
import { twMerge } from "tailwind-merge";

import ButtonBase from "@/components/buttons/ButtonBase";
import { ButtonProps } from "@/types/buttons";

const ButtonSecondary: ForwardRefRenderFunction<
  HTMLButtonElement,
  ButtonProps
> = ({ className, children, ...props }, ref) => {
  return (
    <ButtonBase
      {...props}
      ref={ref}
      className={twMerge(
        "text-indigo-700 bg-indigo-100 hover:bg-indigo-200",
        className
      )}
    >
      {children}
    </ButtonBase>
  );
};

export default forwardRef(ButtonSecondary);
