import React, { forwardRef, ForwardRefRenderFunction } from "react";
import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import Loader from "@/components/Loader";
import { buttonBaseSharedProps } from "@/helpers/buttons";
import { ButtonProps } from "@/types/buttons";

type Props = ButtonProps & {
  loaderClassName?: string;
};

const ButtonBase: ForwardRefRenderFunction<HTMLButtonElement, Props> = (
  {
    className,
    loaderClassName,
    type = "button",
    loading = false,
    disabled = false,
    icon,
    children,
    ...props
  },
  ref
) => {
  return (
    <button
      {...props}
      {...buttonBaseSharedProps({ loading, disabled, className })}
      ref={ref}
      type={type}
    >
      {loading && (
        <Loader
          size={5}
          className={twMerge("mr-2 -ml-1 text-white", loaderClassName)}
        />
      )}
      {icon && <Icon className="mr-2 -ml-1" svg={icon} />}
      {children}
    </button>
  );
};

export default forwardRef(ButtonBase);
