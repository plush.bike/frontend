import React from "react";

import Icon from "@/components/Icon";
import Link from "@/components/Link";
import Loader from "@/components/Loader";
import { buttonBaseSharedProps } from "@/helpers/buttons";
import { ButtonLinkProps } from "@/types/buttons";
import { FunctionComponent } from "@/types/react";

const ButtonBaseLink: FunctionComponent<ButtonLinkProps> = ({
  className,
  href,
  loading = false,
  icon,
  children,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ref,
  ...props
}) => {
  return (
    <Link
      {...props}
      {...buttonBaseSharedProps({ loading, className })}
      href={href}
    >
      {loading && <Loader size={5} className="mr-2 -ml-1 text-white" />}
      {icon && <Icon className="mr-2 -ml-1" svg={icon} />}
      {children}
    </Link>
  );
};

export default ButtonBaseLink;
