import React, { forwardRef, ForwardRefRenderFunction } from "react";
import { twMerge } from "tailwind-merge";

import ButtonBase from "@/components/buttons/ButtonBase";
import { ButtonProps } from "@/types/buttons";

const ButtonPrimary: ForwardRefRenderFunction<
  HTMLButtonElement,
  ButtonProps
> = ({ className, children, ...props }, ref) => {
  return (
    <ButtonBase
      {...props}
      ref={ref}
      className={twMerge(
        "text-white bg-indigo-600 hover:bg-indigo-700",
        className
      )}
    >
      {children}
    </ButtonBase>
  );
};

export default forwardRef(ButtonPrimary);
