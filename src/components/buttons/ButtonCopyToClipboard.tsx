import React, {
  ComponentPropsWithRef,
  forwardRef,
  ForwardRefRenderFunction,
  useRef,
  useState,
} from "react";
import { Instance } from "tippy.js";

import ButtonSecondary from "@/components/buttons/ButtonSecondary";
import Tooltip from "@/components/Tooltip";
import { copyToClipboard } from "@/helpers/copy-to-clipboard";
import clipboardIcon from "@/icons/clipboard.svg";

type Props = ComponentPropsWithRef<"button"> & {
  contentToCopy: string;
  renderButton?: (props: { onClick: () => void }) => JSX.Element | null;
};

const ButtonCopyToClipboard: ForwardRefRenderFunction<
  HTMLButtonElement,
  Props
> = ({ className, contentToCopy, renderButton, ...props }, ref) => {
  const [tooltipVisible, setTooltipVisible] = useState(false);
  const tooltipTimeout = useRef(null);

  const handleButtonClick = async () => {
    if (tooltipTimeout.current !== null) {
      clearTimeout(tooltipTimeout.current);
    }

    await copyToClipboard(contentToCopy);

    setTooltipVisible(true);

    tooltipTimeout.current = setTimeout(() => {
      setTooltipVisible(false);
    }, 1000);
  };

  const handleOnShow = (instance: Instance) => {
    (instance.reference as HTMLButtonElement).focus();
  };

  return (
    <Tooltip content="Copied" visible={tooltipVisible} onShow={handleOnShow}>
      {renderButton ? (
        renderButton({ onClick: handleButtonClick })
      ) : (
        <ButtonSecondary
          icon={clipboardIcon}
          onClick={handleButtonClick}
          {...props}
          ref={ref}
          className={className}
        >
          Copy
        </ButtonSecondary>
      )}
    </Tooltip>
  );
};

export default forwardRef(ButtonCopyToClipboard);
