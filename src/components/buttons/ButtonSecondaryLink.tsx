import React from "react";
import { twMerge } from "tailwind-merge";

import ButtonBaseLink from "@/components/buttons/ButtonBaseLink";
import { ButtonLinkProps } from "@/types/buttons";
import { FunctionComponent } from "@/types/react";

const ButtonSecondaryLink: FunctionComponent<ButtonLinkProps> = ({
  className,
  children,
  ...props
}) => {
  return (
    <ButtonBaseLink
      {...props}
      className={twMerge(
        "text-indigo-700 bg-indigo-100 hover:bg-indigo-200",
        className
      )}
    >
      {children}
    </ButtonBaseLink>
  );
};

export default ButtonSecondaryLink;
