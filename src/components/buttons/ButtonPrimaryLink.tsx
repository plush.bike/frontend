import React from "react";
import { twMerge } from "tailwind-merge";

import ButtonBaseLink from "@/components/buttons/ButtonBaseLink";
import { ButtonLinkProps } from "@/types/buttons";
import { FunctionComponent } from "@/types/react";

const ButtonPrimaryLink: FunctionComponent<ButtonLinkProps> = ({
  className,
  children,
  ...props
}) => {
  return (
    <ButtonBaseLink
      {...props}
      className={twMerge(
        "text-white bg-indigo-600 hover:bg-indigo-700",
        className
      )}
    >
      {children}
    </ButtonBaseLink>
  );
};

export default ButtonPrimaryLink;
