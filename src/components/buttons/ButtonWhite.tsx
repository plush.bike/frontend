import React, { forwardRef, ForwardRefRenderFunction } from "react";
import { twMerge } from "tailwind-merge";

import ButtonBase from "@/components/buttons/ButtonBase";
import { ButtonProps } from "@/types/buttons";

const ButtonWhite: ForwardRefRenderFunction<HTMLButtonElement, ButtonProps> = (
  { className, children, ...props },
  ref
) => {
  return (
    <ButtonBase
      {...props}
      ref={ref}
      className={twMerge(
        "text-gray-700 bg-white hover:bg-gray-50 border-gray-300",
        className
      )}
      loaderClassName="text-gray-900"
    >
      {children}
    </ButtonBase>
  );
};

export default forwardRef(ButtonWhite);
