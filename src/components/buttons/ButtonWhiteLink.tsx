import React from "react";
import { twMerge } from "tailwind-merge";

import ButtonBaseLink from "@/components/buttons/ButtonBaseLink";
import { ButtonLinkProps } from "@/types/buttons";
import { FunctionComponent } from "@/types/react";

const ButtonWhiteLink: FunctionComponent<ButtonLinkProps> = ({
  className,
  children,
  ...props
}) => {
  return (
    <ButtonBaseLink
      {...props}
      className={twMerge(
        "text-gray-700 bg-white hover:bg-gray-50 border-gray-300",
        className
      )}
    >
      {children}
    </ButtonBaseLink>
  );
};

export default ButtonWhiteLink;
