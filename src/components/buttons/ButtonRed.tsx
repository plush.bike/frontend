import React, { forwardRef, ForwardRefRenderFunction } from "react";
import { twMerge } from "tailwind-merge";

import ButtonBase from "@/components/buttons/ButtonBase";
import { ButtonProps } from "@/types/buttons";

const ButtonRed: ForwardRefRenderFunction<HTMLButtonElement, ButtonProps> = (
  { className, children, ...props },
  ref
) => {
  return (
    <ButtonBase
      {...props}
      ref={ref}
      className={twMerge("text-white bg-red-600 hover:bg-red-700", className)}
    >
      {children}
    </ButtonBase>
  );
};

export default forwardRef(ButtonRed);
