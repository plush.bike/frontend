import React, { MouseEventHandler } from "react";
import { RequireAtLeastOne } from "type-fest";

import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import Icon from "@/components/Icon";
import plusIcon from "@/icons/plus.svg";
import { FunctionComponent } from "@/types/react";
import { SvgIcon } from "@/types/svg";

type Props = {
  icon?: SvgIcon;
  title: string;
  buttonText: string;
  onButtonClick?: MouseEventHandler<HTMLButtonElement>;
  href?: string;
};

const EmptyState: FunctionComponent<
  RequireAtLeastOne<Props, "onButtonClick" | "href">
> = ({ icon, title, buttonText, onButtonClick, href, children }) => {
  return (
    <div className="text-center">
      {icon && <Icon size={12} svg={icon} className="mx-auto text-gray-400" />}
      <h3 className="mt-2 font-medium text-gray-900 text-md">{title}</h3>
      <p className="mt-1 text-sm text-gray-500">{children}</p>
      <div className="mt-6">
        {onButtonClick ? (
          <ButtonPrimary icon={plusIcon} onClick={onButtonClick}>
            {buttonText}
          </ButtonPrimary>
        ) : (
          <ButtonPrimaryLink icon={plusIcon} href={href}>
            {buttonText}
          </ButtonPrimaryLink>
        )}
      </div>
    </div>
  );
};

export default EmptyState;
