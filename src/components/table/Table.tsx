import {
  useReactTable,
  getCoreRowModel,
  ColumnDef,
  flexRender,
  Cell,
} from "@tanstack/react-table";
import { range, isFunction } from "lodash";
import React, { useCallback, useMemo } from "react";
import { twMerge } from "tailwind-merge";

import SkeletonLoader from "@/components/SkeletonLoader";
import { FunctionComponent } from "@/types/react";

type Props<RowData> = {
  columns: ColumnDef<RowData, any>[];
  data: RowData[];
  breakpoint?: "sm" | "md" | "lg" | "xl";
  loading?: boolean;
  emptyContent?: string;
  className?: string;
};

type TableComponent = <RowData>(
  props: Props<RowData>
) => ReturnType<FunctionComponent<Props<RowData>>>;

const Table: TableComponent = ({
  columns,
  data,
  breakpoint = "md",
  loading = false,
  emptyContent = "No results found.",
  className,
}) => {
  const computedColumns = useMemo(() => {
    if (loading) {
      return columns.map((column) => ({
        ...column,
        cell: () => <SkeletonLoader className="w-full h-4 rounded" />,
      }));
    }

    return columns;
  }, [columns, loading]);

  const computedData = useMemo(() => {
    if (loading) {
      return range(0, 10).map(() => ({})) as Array<(typeof data)[0]>;
    }

    return data;
  }, [data, loading]);

  const breakpointClasses = useCallback(
    (classes: { sm: string; md: string; lg: string; xl: string }) => {
      return classes[breakpoint];
    },
    [breakpoint]
  );

  const { getHeaderGroups, getRowModel } = useReactTable<(typeof data)[0]>({
    columns: computedColumns,
    data: computedData,
    getCoreRowModel: getCoreRowModel(),
  });

  const renderMobileHeader = (cell: Cell<(typeof data)[0], unknown>) => {
    const { header } = cell.column.columnDef;

    return isFunction(header) ? header({} as any) : header;
  };

  return (
    <div
      className={twMerge(
        "rounded-lg shadow-0 ring-0 ring-black ring-opacity-5",
        breakpointClasses({
          sm: "sm:ring-1 sm:shadow sm:overflow-auto",
          md: "md:ring-1 md:shadow md:overflow-auto",
          lg: "lg:ring-1 lg:shadow lg:overflow-auto",
          xl: "xl:ring-1 xl:shadow xl:overflow-auto",
        }),
        className
      )}
    >
      <table className="min-w-full table-fixed align-middle">
        <thead
          className={twMerge(
            "bg-gray-50 sr-only border-b border-gray-300",
            breakpointClasses({
              sm: "sm:not-sr-only",
              md: "md:not-sr-only",
              lg: "lg:not-sr-only",
              xl: "xl:not-sr-only",
            })
          )}
        >
          {getHeaderGroups().map((headerGroup) => {
            return (
              <tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <th
                      key={header.id}
                      className={twMerge(
                        "px-3 py-3.5 text-left text-sm font-semibold text-gray-900",
                        (header.column.columnDef?.meta as any)?.className
                      )}
                    >
                      {flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                    </th>
                  );
                })}
              </tr>
            );
          })}
        </thead>
        <tbody
          className={twMerge(
            "bg-white space-y-3",
            breakpointClasses({
              sm: "sm:divide-y sm:divide-gray-300",
              md: "md:divide-y md:divide-gray-300",
              lg: "lg:divide-y lg:divide-gray-300",
              xl: "xl:divide-y xl:divide-gray-300",
            })
          )}
        >
          {getRowModel().rows.length ? (
            getRowModel().rows.map((row) => {
              return (
                <tr
                  key={row.id}
                  className={twMerge(
                    "block divide-y divide-gray-300 rounded-lg shadow ring-1 ring-black ring-opacity-5",
                    breakpointClasses({
                      sm: "sm:table-row sm:divide-none sm:shadow-0 sm:ring-0",
                      md: "md:table-row md:divide-none md:shadow-0 md:ring-0",
                      lg: "lg:table-row lg:divide-none lg:shadow-0 lg:ring-0",
                      xl: "xl:table-row xl:divide-none xl:shadow-0 xl:ring-0",
                    })
                  )}
                >
                  {row.getVisibleCells().map((cell) => {
                    return (
                      <td
                        key={cell.id}
                        className={twMerge(
                          "px-3 py-4 block",
                          breakpointClasses({
                            sm: "sm:table-cell",
                            md: "md:table-cell",
                            lg: "lg:table-cell",
                            xl: "xl:table-cell",
                          }),
                          (cell.column.columnDef?.meta as any)?.cellClassName
                        )}
                      >
                        <div className="flex justify-between items-center w-full">
                          <div
                            className={twMerge(
                              "text-left text-sm font-semibold text-gray-900 w-1/2",
                              breakpointClasses({
                                sm: "sm:hidden",
                                md: "md:hidden",
                                lg: "lg:hidden",
                                xl: "xl:hidden",
                              }),
                              (cell.column.columnDef?.meta as any)
                                ?.cellInnerClassName
                            )}
                          >
                            {renderMobileHeader(cell)}
                          </div>
                          <div
                            className={twMerge(
                              "text-sm text-gray-500 w-1/2 text-right",
                              breakpointClasses({
                                sm: "sm:w-full sm:justify-start sm:text-left",
                                md: "md:w-full md:justify-start md:text-left",
                                lg: "lg:w-full lg:justify-start lg:text-left",
                                xl: "xl:w-full xl:justify-start xl:text-left",
                              }),
                              (cell.column.columnDef?.meta as any)
                                ?.cellInnerClassName
                            )}
                          >
                            {flexRender(
                              cell.column.columnDef.cell,
                              cell.getContext()
                            )}
                          </div>
                        </div>
                      </td>
                    );
                  })}
                </tr>
              );
            })
          ) : (
            <tr>
              <td colSpan={100} className="px-3 py-4 text-center">
                {emptyContent}
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
