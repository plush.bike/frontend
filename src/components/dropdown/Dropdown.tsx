import { Menu, Transition } from "@headlessui/react";
import React, {
  Fragment,
  ComponentPropsWithRef,
  KeyboardEvent,
  useRef,
} from "react";
import { twMerge } from "tailwind-merge";
import { RequireAtLeastOne } from "type-fest";

import Icon from "@/components/Icon";
import { KEY_VALUES } from "@/constants/keyboard";
import cheveronDown from "@/icons/cheveron-down.svg";
import { FunctionComponent } from "@/types/react";
import { SvgIcon } from "@/types/svg";

type Props = ComponentPropsWithRef<"div"> & {
  buttonRender?: () => JSX.Element;
  buttonText?: string;
  buttonIcon?: SvgIcon;
  buttonIconText?: string;
  alignment?: "left" | "right";
  preventCloseWhenItemIsSelected?: boolean;
  disabled?: boolean;
};

const Dropdown: FunctionComponent<
  RequireAtLeastOne<Props, "buttonRender" | "buttonText" | "buttonIcon">
> = ({
  buttonRender,
  buttonText,
  buttonIcon,
  buttonIconText = "Open options",
  className,
  children,
  alignment = "right",
  preventCloseWhenItemIsSelected = false,
  disabled = false,
}) => {
  const menuItemsRef = useRef<HTMLDivElement>(null);

  const handleKeyDown = (event: KeyboardEvent<HTMLDivElement>) => {
    if (!preventCloseWhenItemIsSelected) {
      return;
    }

    if (event.key === KEY_VALUES.Enter) {
      const activeItemId = menuItemsRef?.current?.getAttribute(
        "aria-activedescendant"
      );

      if (!activeItemId) {
        return;
      }

      const activeItem = document.getElementById(activeItemId);

      if (!activeItem) {
        return;
      }

      event.preventDefault();
      event.stopPropagation();

      activeItem.click();
    }
  };

  return (
    <Menu
      as="div"
      className={twMerge("inline-block text-left relative", className)}
    >
      <div>
        {buttonRender ? (
          buttonRender()
        ) : (
          <Menu.Button
            disabled={disabled}
            className={twMerge(
              "focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",
              buttonText &&
                "inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50",
              buttonIcon &&
                "bg-gray-100 rounded-full flex items-center justify-center text-gray-400 hover:text-gray-600 w-8 h-8"
            )}
          >
            {buttonText ? (
              <>
                {buttonText}
                <Icon svg={cheveronDown} size={5} className="ml-2 -mr-1" />
              </>
            ) : (
              <>
                <span className="sr-only">{buttonIconText}</span>
                <Icon svg={buttonIcon} size={5} />
              </>
            )}
          </Menu.Button>
        )}
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items
          ref={menuItemsRef}
          onKeyDown={handleKeyDown}
          className={twMerge(
            "absolute w-56 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none overflow-hidden",
            alignment === "right" ? "right-0" : "left-0"
          )}
        >
          {children}
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default Dropdown;
