import { Menu } from "@headlessui/react";
import React, {
  forwardRef,
  ForwardRefRenderFunction,
  MouseEventHandler,
  MouseEvent,
  ReactNode,
} from "react";
import { twMerge } from "tailwind-merge";
import { RequireAtLeastOne } from "type-fest";

import Icon from "@/components/Icon";
import Link from "@/components/Link";
import { SvgIcon } from "@/types/svg";

type Props = {
  icon?: SvgIcon;
  href?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  preventCloseWhenSelected?: boolean;
  className?: string;
  children?: ReactNode;
};

const DropdownItem: ForwardRefRenderFunction<
  HTMLButtonElement,
  RequireAtLeastOne<Props, "onClick" | "href">
> = (
  {
    className,
    icon,
    href,
    onClick,
    preventCloseWhenSelected = false,
    children,
  },
  ref
) => {
  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    if (onClick) {
      onClick(event);
    }

    if (!preventCloseWhenSelected) {
      return;
    }

    event.preventDefault();
    event.stopPropagation();
  };

  return (
    <Menu.Item>
      {({ active }) => {
        const computedClassName = twMerge(
          active ? "bg-gray-100 text-gray-900" : "text-gray-700",
          "group flex items-center px-4 py-2 text-sm w-full",
          className
        );

        const iconClassName = "mr-3 text-gray-400 group-hover:text-gray-500";

        return href ? (
          <Link href={href} className={computedClassName}>
            {icon && <Icon size={4} className={iconClassName} svg={icon} />}
            {children}
          </Link>
        ) : (
          <button ref={ref} className={computedClassName} onClick={handleClick}>
            {icon && <Icon size={4} className={iconClassName} svg={icon} />}
            {children}
          </button>
        );
      }}
    </Menu.Item>
  );
};

export default forwardRef(DropdownItem);
