import { Transition } from "@headlessui/react";
import { ComponentPropsWithRef, useMemo, useState } from "react";
import { twMerge } from "tailwind-merge";

import DropdownItem from "@/components//dropdown/DropdownItem";
import BadgeGray from "@/components/badges/BadgeGray";
import ButtonBase from "@/components/buttons/ButtonBase";
import Dropdown from "@/components/dropdown/Dropdown";
import Icon from "@/components/Icon";
import chevronLeftIcon from "@/icons/cheveron-left.svg";
import chevronRightIcon from "@/icons/cheveron-right.svg";
import { FunctionComponent } from "@/types/react";

type FilterOption = { label: string; key: string };
type FilterCategory = { label: string; key: string };

type AvailableFilter = {
  category: FilterCategory;
  options: FilterOption[];
};

type AvailableFilters = AvailableFilter[];

type SelectedFilter = {
  category: FilterCategory;
  option: FilterOption;
};

type SelectedFilters = SelectedFilter[];

type Props = ComponentPropsWithRef<"div"> & {
  availableFilters: AvailableFilters;
};

const Filter: FunctionComponent<Props> = ({ availableFilters, ...props }) => {
  const [selectedCategory, setSelectedCategory] =
    useState<FilterCategory>(null);
  const [selectedFilters, setSelectedFilters] = useState<SelectedFilters>([]);

  const handleCategoryClick = (category: FilterCategory) => {
    setSelectedCategory(category);
  };

  const handleOptionClick = (option: FilterOption) => {
    setSelectedFilters([
      ...selectedFilters,
      { category: selectedCategory, option },
    ]);
    setSelectedCategory(null);
  };

  const handleBackButtonClick = () => {
    setSelectedCategory(null);
  };

  const handleBadgeRemove = (selectedFilterToRemove: SelectedFilter) => {
    setSelectedFilters(
      selectedFilters.filter(
        (selectedFilter) => selectedFilter !== selectedFilterToRemove
      )
    );
  };

  const availableFiltersWithoutSelected = useMemo(() => {
    const selectedCategoryKeys = selectedFilters.map(
      (selectedFilter) => selectedFilter.category.key
    );

    return availableFilters.filter(
      (availableFilter) =>
        !selectedCategoryKeys.includes(availableFilter.category.key)
    );
  }, [availableFilters, selectedFilters]);

  const isCategorySelected = useMemo(() => {
    return selectedCategory !== null;
  }, [selectedCategory]);

  const options = useMemo(() => {
    if (selectedCategory === null) {
      return [];
    }

    return availableFilters.find(
      (availableFilter) => availableFilter.category.key === selectedCategory.key
    ).options;
  }, [selectedCategory]);

  return (
    <div {...props}>
      <div className="-m-1 flex flex-wrap items-center">
        <div className="p-1">
          <Dropdown
            buttonText="Filter"
            alignment="left"
            preventCloseWhenItemIsSelected={!isCategorySelected}
            disabled={!availableFiltersWithoutSelected.length}
          >
            <div className="px-4 py-3 relative">
              <Transition
                onClick={handleBackButtonClick}
                className="left-2 top-1/2 transform -translate-y-1/2 absolute p-1 shadow-none"
                as={ButtonBase}
                show={isCategorySelected}
                aria-label="Back to filters"
                enter="transition ease-in-out duration-500"
                enterFrom="-translate-x-12 opacity-0"
                enterTo="translate-x-0 opacity-100"
                leave="transition ease-in-out duration-500"
                leaveFrom="translate-x-0 opacity-100"
                leaveTo="-translate-x-12 opacity-0"
              >
                <Icon svg={chevronLeftIcon} />
              </Transition>
              <p
                className={twMerge(
                  "text-sm transition ease-in-out duration-500",
                  isCategorySelected && "translate-x-6"
                )}
              >
                {isCategorySelected ? (
                  <>
                    <span className="font-bold">Filters:</span>{" "}
                    {selectedCategory.label}
                  </>
                ) : (
                  <span className="font-bold">Filters</span>
                )}
              </p>
            </div>
            {!isCategorySelected &&
              availableFiltersWithoutSelected.map(({ category }) => (
                <DropdownItem
                  onClick={() => handleCategoryClick(category)}
                  key={category.key}
                  className="relative pr-10"
                  preventCloseWhenSelected
                >
                  {category.label}
                  <Icon
                    svg={chevronRightIcon}
                    className="right-4 top-1/2 transform -translate-y-1/2 absolute"
                  />
                </DropdownItem>
              ))}
            <Transition
              show={isCategorySelected}
              enter="transform transition ease-in-out duration-500"
              enterFrom="translate-x-full opacity-0"
              enterTo="translate-x-0 opacity-100"
              leave="transform transition ease-in-out duration-500"
              leaveFrom="translate-x-0 opacity-100"
              leaveTo="translate-x-full opacity-0"
            >
              {options.map((option) => (
                <DropdownItem
                  onClick={() => handleOptionClick(option)}
                  key={option.key}
                >
                  {option.label}
                </DropdownItem>
              ))}
            </Transition>
          </Dropdown>
        </div>
        {selectedFilters.map((selectedFilter) => (
          <div
            className="p-1"
            key={`${selectedFilter.category.key}-${selectedFilter.option.key}`}
          >
            <BadgeGray onRemove={() => handleBadgeRemove(selectedFilter)}>
              {selectedFilter.category.label} is&nbsp;
              <span className="font-bold">{selectedFilter.option.label}</span>
            </BadgeGray>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Filter;
