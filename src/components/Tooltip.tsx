import Tippy, { TippyProps } from "@tippyjs/react";
import { useMemo } from "react";

import { FunctionComponent } from "@/types/react";

import "tippy.js/dist/tippy.css";

const displayClassNames = [
  "block",
  "inline-block",
  "inline",
  "flex",
  "inline-flex",
];

const Tooltip: FunctionComponent<TippyProps> = ({
  children,
  content,
  ...props
}) => {
  if (!content) {
    return children;
  }

  const wrapperClassName = useMemo(() => {
    const { disabled, className } = children.props;

    if (!disabled || !className) {
      return null;
    }

    const childrenClassNames = className.split(" ");

    return (
      displayClassNames.find((displayClassName) =>
        childrenClassNames.includes(displayClassName)
      ) || null
    );
  }, [children]);

  return (
    <Tippy content={content} {...props}>
      {children.props.disabled ? (
        <div className={wrapperClassName}>{children}</div>
      ) : (
        children
      )}
    </Tippy>
  );
};

export default Tooltip;
