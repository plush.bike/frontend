import { IKImage } from "imagekitio-react";
import React, { ImgHTMLAttributes } from "react";

import { FunctionComponent } from "@/types/react";

type Props = ImgHTMLAttributes<HTMLImageElement> & {
  url: string;
  height?: number;
  width?: number;
  alt?: string;
};

const Image: FunctionComponent<Props> = ({
  url,
  height,
  width,
  className,
  alt,
}) => {
  return (
    <IKImage
      urlEndpoint={process.env.NEXT_PUBLIC_IMAGEKIT_ENDPOINT}
      className={className}
      src={url}
      transformation={[{ height: height.toString(), width: width.toString() }]}
      lqip={{ active: true }}
      loading="lazy"
      height={height}
      width={width}
      alt={alt}
    />
  );
};

export default Image;
