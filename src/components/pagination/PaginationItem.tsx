import { isString } from "lodash";
import React from "react";
import { twMerge } from "tailwind-merge";

import { FunctionComponent } from "@/types/react";

type Props = {
  page: number;
  item: number | string;
  onClick: (page: number) => void;
};

const PaginationItem: FunctionComponent<Props> = ({ page, item, onClick }) => {
  const handleOnClick = () => {
    onClick(item as number);
  };

  if (isString(item)) {
    return (
      <span className="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300">
        {item}
      </span>
    );
  }

  return (
    <button
      aria-current={page === item ? "page" : null}
      onClick={handleOnClick}
      className={twMerge(
        "relative items-center hidden px-4 py-2 text-sm font-medium md:inline-flex border",
        page === item
          ? "z-10 bg-indigo-50 border-indigo-500 text-indigo-600"
          : "text-gray-500 bg-white border-gray-300 hover:bg-gray-50"
      )}
    >
      {item}
    </button>
  );
};

export default PaginationItem;
