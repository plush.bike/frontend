import { max, min, range } from "lodash";
import { useRouter } from "next/router";
import React, { useMemo } from "react";
import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import PaginationItem from "@/components/pagination/PaginationItem";
import {
  BREAKPOINT,
  DEFAULT_PER_PAGE,
  ITEMS_PER_PAGE,
} from "@/constants/pagination";
import chevronLeftIcon from "@/icons/cheveron-left.svg";
import chevronRightIcon from "@/icons/cheveron-right.svg";
import { FunctionComponent } from "@/types/react";

type Props = {
  itemsCount: number;
  perPage?: number;
  page: number;
  className?: string;
  queryParamName?: string;
};

const Pagination: FunctionComponent<Props> = ({
  itemsCount,
  perPage = DEFAULT_PER_PAGE,
  page,
  className,
  queryParamName = "page",
}) => {
  const router = useRouter();

  const pageCount = useMemo(() => {
    if (itemsCount % perPage > 0) {
      return Math.floor(itemsCount / perPage) + 1;
    } else {
      return Math.floor(itemsCount / perPage);
    }
  }, [itemsCount, perPage]);

  const showingFloor = useMemo(
    () => max([(page - 1) * perPage + 1, 1]),
    [page, perPage]
  );

  const showingCeiling = useMemo(
    () => min([page * perPage, itemsCount]),
    [page, perPage, itemsCount]
  );

  const pages = () => {
    if (pageCount <= ITEMS_PER_PAGE) {
      return range(1, pageCount + 1);
    }

    if (pageCount > ITEMS_PER_PAGE) {
      if (page <= BREAKPOINT || page + BREAKPOINT > pageCount) {
        return [...range(1, 8), "...", pageCount];
      }

      return [1, "...", ...range(page - 2, page + 3), "...", pageCount];
    }

    if (page + BREAKPOINT > pageCount) {
      return ["...", ...range(pageCount - BREAKPOINT, pageCount + 1)];
    }
  };

  const updateQueryParam = (page: number) => {
    if (page === 1) {
      router.push(router.pathname, undefined, { shallow: true });
    } else {
      router.push({ query: { [queryParamName]: page } }, undefined, {
        shallow: true,
      });
    }
  };

  const handleNextClick = () => {
    updateQueryParam(page + 1);
  };

  const handlePrevClick = () => {
    updateQueryParam(page - 1);
  };

  const handlePageClick = (page: number) => {
    updateQueryParam(page);
  };

  if (itemsCount <= perPage) {
    return null;
  }

  return (
    <div
      className={twMerge(
        "flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6",
        className
      )}
    >
      <div
        className="flex justify-between flex-1 sm:hidden"
        data-testid="mobilePagination"
      >
        <button
          onClick={handlePrevClick}
          disabled={page === 1}
          className="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md hover:bg-gray-50"
        >
          Previous
        </button>
        <button
          onClick={handleNextClick}
          disabled={page === pageCount}
          className="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md hover:bg-gray-50"
        >
          Next
        </button>
      </div>
      <div
        className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between"
        data-testid="desktopPagination"
      >
        <div>
          <p className="text-sm text-gray-700" data-testid="showingText">
            Showing{" "}
            {showingFloor === showingCeiling ? (
              <>
                <span className="font-medium">{showingCeiling}</span> of{" "}
              </>
            ) : (
              <>
                <span className="font-medium">{showingFloor}</span> to{" "}
                <span className="font-medium">{showingCeiling}</span> of{" "}
              </>
            )}
            <span className="font-medium">{itemsCount}</span> results
          </p>
        </div>
        <nav
          className="relative z-0 inline-flex -space-x-px rounded-md shadow-sm"
          aria-label="Pagination"
        >
          <button
            onClick={handlePrevClick}
            disabled={page === 1}
            className="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-l-md hover:bg-gray-50"
          >
            <span className="sr-only">Previous</span>
            <Icon size={5} svg={chevronLeftIcon} />
          </button>
          {pages().map((item, index) => (
            <PaginationItem
              key={`${item}-${index}`}
              page={page}
              item={item}
              onClick={handlePageClick}
            />
          ))}
          <button
            onClick={handleNextClick}
            disabled={page === pageCount}
            className="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-r-md hover:bg-gray-50"
          >
            <span className="sr-only">Next</span>
            <Icon size={5} svg={chevronRightIcon} />
          </button>
        </nav>
      </div>
    </div>
  );
};

export default Pagination;
