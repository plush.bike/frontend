import React, { ComponentPropsWithRef } from "react";
import { twMerge } from "tailwind-merge";

import { AVAILABLE_LINES } from "@/constants/truncate";
import { FunctionComponent } from "@/types/react";

type Props = ComponentPropsWithRef<"div"> & {
  lines?: keyof typeof AVAILABLE_LINES;
};

const Truncate: FunctionComponent<Props> = ({
  lines = 2,
  className,
  children,
  ...props
}) => {
  return (
    <div className={twMerge(AVAILABLE_LINES[lines], className)} {...props}>
      {children}
    </div>
  );
};

export default Truncate;
