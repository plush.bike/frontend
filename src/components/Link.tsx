import NextLink from "next/link";
import React, {
  ComponentPropsWithRef,
  forwardRef,
  ForwardRefRenderFunction,
} from "react";

type Props = ComponentPropsWithRef<"a">;

const Link: ForwardRefRenderFunction<HTMLAnchorElement, Props> = (
  { href, children, ...props },
  ref
) => {
  return (
    <NextLink href={href} ref={ref} {...props}>
      {children}
    </NextLink>
  );
};

export default forwardRef(Link);
