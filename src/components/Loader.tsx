import React, { SVGProps } from "react";
import { twMerge } from "tailwind-merge";

import Icon from "@/components/Icon";
import { AVAILABLE_SIZES } from "@/constants/icon";
import loadingIcon from "@/icons/loading.svg";
import { FunctionComponent } from "@/types/react";

type Props = SVGProps<SVGSVGElement> & {
  size?: keyof typeof AVAILABLE_SIZES;
  fullPage?: boolean;
};

const Loader: FunctionComponent<Props> = ({
  size: sizeProp,
  fullPage = false,
  className,
  ...props
}) => {
  const size = sizeProp ? sizeProp : fullPage ? 16 : 5;

  if (fullPage) {
    return (
      <div className="absolute -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2">
        <Icon
          {...props}
          svg={loadingIcon}
          size={size}
          className={twMerge("animate-spin", className)}
        />
      </div>
    );
  } else {
    return (
      <Icon
        {...props}
        svg={loadingIcon}
        size={size}
        className={twMerge("animate-spin", className)}
      />
    );
  }
};

export default Loader;
