import React, { useEffect } from "react";
import { FieldError } from "react-hook-form";
import { useDispatch } from "react-redux";

import Select from "@/components/form/Select";
import { useManufacturersQuery } from "@/graphql/manufacturer/queries/manufacturers.generated";
import { updateToastError } from "@/store/slices/toasts";
import { ListboxChangeEvent, ListboxOption } from "@/types/form";
import { FunctionComponent } from "@/types/react";

type Props = {
  errors?: FieldError | FieldError[];
  value?: ListboxOption;
  onChange(event: ListboxChangeEvent): void;
};

const Manufacturer: FunctionComponent<Props> = ({
  onChange,
  value,
  errors,
}) => {
  const dispatch = useDispatch();
  const {
    data,
    error: manufacturerQueryError,
    loading: manufacturersLoading,
  } = useManufacturersQuery();

  const manufacturers = data?.manufacturers?.data;

  useEffect(() => {
    if (manufacturerQueryError) {
      dispatch(
        updateToastError({
          show: true,
          message:
            "An error occurred loading the manufacturers, please try again.",
        })
      );
    }
  }, [manufacturerQueryError]);

  return (
    <Select
      value={value}
      errors={errors}
      label="Manufacturer"
      buttonLabel="Select a manufacturer"
      onChange={onChange}
      options={manufacturers ?? []}
      loading={manufacturersLoading}
    />
  );
};

export default Manufacturer;
