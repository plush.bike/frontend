import { useState } from "react";
import {
  useForm,
  SubmitHandler,
  Controller,
  FieldError,
  useFieldArray,
  UseFormReturn,
  UseFieldArrayReturn,
} from "react-hook-form";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FileUpload from "@/components/form/file-upload/FileUpload";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import NumberInput from "@/components/form/NumberInput";
import RadioGroup from "@/components/form/RadioGroup";
import RadioInput from "@/components/form/RadioInput";
import Manufacturer from "@/components/parts/Manufacturer";
import SettingsRecords from "@/components/parts/SettingsRecords";
import { useUploadPartImagesMutation } from "@/graphql/part/mutations/upload-part-images.generated";
import { SavedFile } from "@/helpers/file-upload";
import { parseAndSetServerErrors } from "@/helpers/form";
import { validateListBoxRequired } from "@/helpers/validation-rules";
import { FileExtended } from "@/types/file-upload";
import { ListboxOption } from "@/types/form";
import {
  AirOrCoilType,
  Manufacturer as ManufacturerType,
} from "@/types/graphql";
import { FunctionComponent } from "@/types/react";

type Inputs = {
  name: string;
  manufacturer: ListboxOption;
  lscMaxClicks: number;
  hscMaxClicks: number;
  lsrMaxClicks: number;
  hsrMaxClicks: number;
  travel: number;
  airOrCoil: AirOrCoilType;
  images: (FileExtended | SavedFile)[];
  settingsRecords: {
    nickname: string;
    lscClicks: number;
    hscClicks: number;
    lsrClicks: number;
    hsrClicks: number;
    volumeSpacers: number;
    psi: number;
    sag: number;
    springRate: number;
    preloadAdjust: number;
    notes: string;
  }[];
  activeSettingsRecord: number;
};

type Props = {
  defaultValues?: {
    name?: string;
    manufacturer?: Pick<ManufacturerType, "id" | "name">;
    lscMaxClicks?: number;
    hscMaxClicks?: number;
    lsrMaxClicks?: number;
    hsrMaxClicks?: number;
    travel?: number;
    airOrCoil?: AirOrCoilType;
    images?: SavedFile[];
    settingsRecords: {
      nickname?: string;
      lscClicks?: number;
      hscClicks?: number;
      lsrClicks?: number;
      hsrClicks?: number;
      volumeSpacers?: number;
      psi?: number;
      sag?: number;
      springRate?: number;
      preloadAdjust?: number;
      notes?: string;
    }[];
    activeSettingsRecord?: number;
  };
  onSubmit: (inputs: Inputs) => Promise<void>;
};

const ForksCreateEdit: FunctionComponent<Props> = ({
  defaultValues = {
    lscMaxClicks: 0,
    hscMaxClicks: 0,
    lsrMaxClicks: 0,
    hsrMaxClicks: 0,
    travel: 150,
    airOrCoil: AirOrCoilType.Air,
    settingsRecords: [
      {
        nickname: "",
        lscClicks: 0,
        hscClicks: 0,
        lsrClicks: 0,
        hsrClicks: 0,
        volumeSpacers: 0,
        psi: 0,
        sag: 0,
        springRate: 400,
        preloadAdjust: 0,
        notes: "",
      },
    ],
    activeSettingsRecord: 0,
  },
  onSubmit: onSubmitProp,
}) => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);

  const useFormReturn = useForm<Inputs>({
    defaultValues,
  });
  const {
    register,
    control,
    handleSubmit,
    watch,
    setError,
    formState: { errors },
  } = useFormReturn;

  const useFieldArrayReturn = useFieldArray<Inputs, "settingsRecords", "key">({
    control,
    name: "settingsRecords",
    keyName: "key",
  });

  const watchImages = watch("images");

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async (inputs) => {
    setLoading(true);

    try {
      await onSubmitProp(inputs);
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <FormWrapper onSubmit={handleSubmit(onSubmit)}>
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 mt-4 bg-white rounded-md sm:rounded-lg sm:shadow">
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
          <Controller<Inputs, "manufacturer">
            name="manufacturer"
            control={control}
            rules={{
              validate: {
                required: (value) =>
                  validateListBoxRequired(
                    value,
                    "Please select a manufacturer."
                  ),
              },
            }}
            render={({ field: { onChange, value }, fieldState: { error } }) => {
              return (
                <Manufacturer
                  onChange={onChange}
                  value={value}
                  errors={error as FieldError}
                />
              );
            }}
          />
          <FormInput
            label="Name"
            errors={errors.name}
            {...register("name", {
              required: {
                value: true,
                message: "Please enter the name of your fork.",
              },
            })}
          />
          <NumberInput
            label="Low speed compression max clicks"
            errors={errors.lscMaxClicks}
            min={0}
            {...register("lscMaxClicks", { valueAsNumber: true })}
          />
          <NumberInput
            label="High speed compression max clicks"
            errors={errors.hscMaxClicks}
            min={0}
            {...register("hscMaxClicks", { valueAsNumber: true })}
          />
          <NumberInput
            label="Low speed rebound max clicks"
            errors={errors.lsrMaxClicks}
            min={0}
            {...register("lsrMaxClicks", { valueAsNumber: true })}
          />
          <NumberInput
            label="High speed rebound max clicks"
            errors={errors.hsrMaxClicks}
            min={0}
            {...register("hsrMaxClicks", { valueAsNumber: true })}
          />
          <NumberInput
            label="Travel (mm)"
            errors={errors.travel}
            min={0}
            step={5}
            {...register("travel", { valueAsNumber: true })}
          />
          <RadioGroup
            label="Air or coil?"
            description="test"
            errors={errors.airOrCoil}
          >
            <RadioInput
              label="Air"
              value={AirOrCoilType.Air}
              {...register("airOrCoil", {
                required: {
                  value: true,
                  message: "Please choose air or coil.",
                },
              })}
            />
            <RadioInput
              label="Coil"
              value={AirOrCoilType.Coil}
              {...register("airOrCoil", {
                required: {
                  value: true,
                  message: "Please choose air or coil.",
                },
              })}
            />
          </RadioGroup>
        </div>
        <div className="grid grid-cols-1 gap-4 mt-4 lg:grid-cols-2">
          <Controller<Inputs, "images">
            name="images"
            control={control}
            render={({ field: { onChange, value }, fieldState: { error } }) => {
              return (
                <FileUpload
                  initialFiles={value}
                  label="Images"
                  useMutation={useUploadPartImagesMutation}
                  mutationKey="uploadPartImages"
                  onChange={onChange}
                  errors={error as unknown as FieldError}
                />
              );
            }}
          />
        </div>
      </div>
      <SettingsRecords
        useFieldArrayReturn={
          useFieldArrayReturn as unknown as UseFieldArrayReturn<
            Record<string, unknown>,
            never,
            "key"
          >
        }
        useFormReturn={useFormReturn as unknown as UseFormReturn}
      />
      <div className="mt-4">
        <ButtonPrimary
          type="submit"
          loading={loading}
          disabled={watchImages?.some((image) => image.loading)}
        >
          Submit
        </ButtonPrimary>
      </div>
    </FormWrapper>
  );
};

export default ForksCreateEdit;
