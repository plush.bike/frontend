import React, { MouseEventHandler } from "react";
import { UseFieldArrayReturn, UseFormReturn } from "react-hook-form";

import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import EmptyState from "@/components/EmptyState";
import FormInput from "@/components/form/FormInput";
import NumberInput from "@/components/form/NumberInput";
import Textarea from "@/components/form/Textarea";
import Toggle from "@/components/form/Toggle";
import Icon from "@/components/Icon";
import ModalConfirm from "@/components/modals/ModalConfirm";
import { MODAL_VARIANT_RED } from "@/constants/modal";
import clockwiseIcon from "@/icons/clockwise.svg";
import closeIcon from "@/icons/close.svg";
import cogIcon from "@/icons/cog.svg";
import counterclockwiseIcon from "@/icons/counterclockwise.svg";
import plusIcon from "@/icons/plus.svg";
import { AirOrCoilType } from "@/types/graphql";
import { FunctionComponent } from "@/types/react";

type Props = {
  useFieldArrayReturn: UseFieldArrayReturn<
    Record<string, unknown>,
    never,
    "key"
  >;
  useFormReturn: UseFormReturn;
};

const SettingsRecords: FunctionComponent<Props> = ({
  useFieldArrayReturn: { fields, remove, append },
  useFormReturn: { register, watch, setValue },
}) => {
  const watchActiveSettingsRecord = watch("activeSettingsRecord");
  const [lscMaxClicks, hscMaxClicks, lsrMaxClicks, hsrMaxClicks, airOrCoil] =
    watch([
      "lscMaxClicks",
      "hscMaxClicks",
      "lsrMaxClicks",
      "hsrMaxClicks",
      "airOrCoil",
    ]);

  const handleDeleteSettingsRecord = (index: number) => {
    remove(index);

    if (index === watchActiveSettingsRecord) {
      setValue("activeSettingsRecord", null);
    }
  };

  const addNewSettingsRecord: MouseEventHandler<HTMLButtonElement> = () => {
    append({
      nickname: "",
      lscClicks: 0,
      hscClicks: 0,
      lsrClicks: 0,
      hsrClicks: 0,
      volumeSpacers: 0,
      psi: 0,
      sag: 0,
    });
  };

  const handleToggleChange = (event: { target: { value: boolean } }, index) => {
    if (!event.target.value) {
      setValue("activeSettingsRecord", null);

      return;
    }

    setValue("activeSettingsRecord", index);
  };

  return (
    <div className="mt-4">
      <h3>Settings Records</h3>
      {fields.length ? (
        <>
          <div className="mt-2 space-y-8">
            {fields.map((field, index) => (
              <div
                key={field.key}
                className="relative p-6 bg-white rounded-md sm:rounded-lg sm:shadow"
                data-testid={`settingsRecord${index}`}
              >
                <ModalConfirm
                  title="Delete settings record"
                  variant={MODAL_VARIANT_RED}
                  continueButton="Delete"
                  modalTrigger={({ onClick }) => (
                    <button
                      onClick={onClick}
                      type="button"
                      aria-label="Delete settings record"
                      className="absolute inline-flex items-center p-2 text-white bg-red-600 border border-transparent rounded-full shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 -top-3 -right-3"
                    >
                      <Icon size={4} svg={closeIcon} />
                    </button>
                  )}
                  onContinue={() => handleDeleteSettingsRecord(index)}
                >
                  <p className="text-sm text-gray-500">
                    Are you sure you want to delete this settings record?
                  </p>
                </ModalConfirm>
                <div className="grid grid-cols-1 gap-4 justify-items-center mb-4 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4">
                  <div className="flex space-x-4 justify-self-stretch">
                    <Toggle
                      wrapperClassName="flex-shrink-0"
                      label="Active"
                      onChange={(event) => handleToggleChange(event, index)}
                      value={watchActiveSettingsRecord === index}
                    />
                    <FormInput
                      wrapperClassName="flex-grow-1 w-full"
                      label="Nickname"
                      {...register(`settingsRecords.${index}.nickname`)}
                    />
                  </div>
                  {airOrCoil === AirOrCoilType.Air ? (
                    <>
                      <NumberInput
                        label="PSI"
                        key="psi"
                        min={0}
                        {...register(`settingsRecords.${index}.psi`, {
                          valueAsNumber: true,
                        })}
                      />
                      <NumberInput
                        label="Volume spacers"
                        key="volumeSpacers"
                        min={0}
                        {...register(`settingsRecords.${index}.volumeSpacers`, {
                          valueAsNumber: true,
                        })}
                      />
                    </>
                  ) : (
                    <>
                      <NumberInput
                        label="Spring rate"
                        key="springRate"
                        min={0}
                        step={25}
                        {...register(`settingsRecords.${index}.springRate`, {
                          valueAsNumber: true,
                        })}
                      />
                      <NumberInput
                        label="Preload adjust"
                        key="preloadAdjust"
                        min={0}
                        {...register(`settingsRecords.${index}.preloadAdjust`, {
                          valueAsNumber: true,
                        })}
                      />
                    </>
                  )}
                  <NumberInput
                    label="Sag (mm)"
                    min={0}
                    {...register(`settingsRecords.${index}.sag`, {
                      valueAsNumber: true,
                    })}
                  />
                  <NumberInput
                    label="Low speed compression clicks"
                    description={() => (
                      <>
                        Clicks from fully clockwise (closed)
                        <Icon className="ml-2" svg={clockwiseIcon} size={5} />
                      </>
                    )}
                    descriptionClassName="flex items-center justify-center"
                    decrementLabel="Firmer"
                    incrementLabel="Softer"
                    decrementTooltipContent="Clockwise"
                    incrementTooltipContent="Counterclockwise"
                    decrementIcon={clockwiseIcon}
                    incrementIcon={counterclockwiseIcon}
                    min={0}
                    max={lscMaxClicks}
                    {...register(`settingsRecords.${index}.lscClicks`, {
                      valueAsNumber: true,
                    })}
                  />
                  <NumberInput
                    label="High speed compression clicks"
                    description={() => (
                      <>
                        Clicks from fully clockwise (closed)
                        <Icon className="ml-2" svg={clockwiseIcon} size={5} />
                      </>
                    )}
                    descriptionClassName="flex items-center justify-center"
                    decrementLabel="Firmer"
                    incrementLabel="Softer"
                    decrementTooltipContent="Clockwise"
                    incrementTooltipContent="Counterclockwise"
                    decrementIcon={clockwiseIcon}
                    incrementIcon={counterclockwiseIcon}
                    min={0}
                    max={hscMaxClicks}
                    {...register(`settingsRecords.${index}.hscClicks`, {
                      valueAsNumber: true,
                    })}
                  />
                  <NumberInput
                    label="Low speed rebound clicks"
                    description={() => (
                      <>
                        Clicks from fully clockwise (closed)
                        <Icon className="ml-2" svg={clockwiseIcon} size={5} />
                      </>
                    )}
                    descriptionClassName="flex items-center justify-center"
                    decrementLabel="Slower"
                    incrementLabel="Faster"
                    decrementTooltipContent="Clockwise"
                    incrementTooltipContent="Counterclockwise"
                    decrementIcon={clockwiseIcon}
                    incrementIcon={counterclockwiseIcon}
                    min={0}
                    max={lsrMaxClicks}
                    {...register(`settingsRecords.${index}.lsrClicks`, {
                      valueAsNumber: true,
                    })}
                  />
                  <NumberInput
                    label="High speed rebound clicks"
                    description={() => (
                      <>
                        Clicks from fully clockwise (closed)
                        <Icon className="ml-2" svg={clockwiseIcon} size={5} />
                      </>
                    )}
                    descriptionClassName="flex items-center justify-center"
                    decrementLabel="Slower"
                    incrementLabel="Faster"
                    decrementTooltipContent="Clockwise"
                    incrementTooltipContent="Counterclockwise"
                    decrementIcon={clockwiseIcon}
                    incrementIcon={counterclockwiseIcon}
                    min={0}
                    max={hsrMaxClicks}
                    {...register(`settingsRecords.${index}.hsrClicks`, {
                      valueAsNumber: true,
                    })}
                  />
                  <Textarea
                    wrapperClassName="col-span-full justify-self-stretch"
                    label="Notes"
                    {...register(`settingsRecords.${index}.notes`)}
                  />
                </div>
              </div>
            ))}
          </div>
          <div className="flex justify-end mt-4">
            <ButtonPrimary icon={plusIcon} onClick={addNewSettingsRecord}>
              New settings record
            </ButtonPrimary>
          </div>
        </>
      ) : (
        <EmptyState
          icon={cogIcon}
          title="No settings records"
          buttonText="New settings record"
          onButtonClick={addNewSettingsRecord}
        >
          Create a settings record to help keep track of the settings you like.
        </EmptyState>
      )}
    </div>
  );
};

export default SettingsRecords;
