import { chunk, kebabCase, range } from "lodash";
import { Dispatch, SetStateAction, useMemo, useRef } from "react";
import { useDispatch } from "react-redux";
import { twMerge } from "tailwind-merge";

import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import ButtonSecondaryLink from "@/components/buttons/ButtonSecondaryLink";
import Dropdown from "@/components/dropdown/Dropdown";
import DropdownItem from "@/components/dropdown/DropdownItem";
import Image from "@/components/Image";
import ModalConfirm from "@/components/modals/ModalConfirm";
import SkeletonLoader from "@/components/SkeletonLoader";
import { MODAL_VARIANT_RED } from "@/constants/modal";
import { useDeletePartMutation } from "@/graphql/part/mutations/delete-part.generated";
import {
  PartsByTypeDocument,
  PartsByTypeQuery,
} from "@/graphql/part/queries/parts-by-type.generated";
import { nextTick } from "@/helpers/general";
import dotsHorizontalTripleIcon from "@/icons/dots-horizontal-triple.svg";
import editPencilIcon from "@/icons/edit-pencil.svg";
import trashIcon from "@/icons/trash.svg";
import viewListIcon from "@/icons/view-list.svg";
import { updateToastSuccess, updateToastError } from "@/store/slices/toasts";
import { Manufacturer, Part, PartImage } from "@/types/graphql";
import { FunctionComponent } from "@/types/react";
import { SvgrComponent } from "@/types/svg";

type Props = {
  part?: Pick<Part, "id" | "name" | "type"> & {
    images?: Pick<PartImage, "url">[];
    manufacturer: Pick<Manufacturer, "name">;
  };
  path?: string;
  placeholder?: SvgrComponent;
  loading?: boolean;
  activeSettings?: { label: string; value: string | number }[];
};

const PartCard: FunctionComponent<Props> = ({
  part = {},
  path = "",
  placeholder: Placeholder = null,
  loading = false,
  activeSettings = [],
}) => {
  const setModalShow = useRef<Dispatch<SetStateAction<boolean>>>(null);
  const computedName = useMemo(
    () => `${part?.manufacturer?.name} ${part?.name}`,
    [part]
  );
  const [deletePart] = useDeletePartMutation();
  const dispatch = useDispatch();

  const handleDeleteButtonClick = () => {
    if (!setModalShow.current) {
      return;
    }

    setModalShow.current(true);
  };

  const handleModalContinue = async () => {
    try {
      await deletePart({
        variables: {
          id: part.id,
        },
        update: async (cache) => {
          const partsByTypeQuery = cache.readQuery({
            query: PartsByTypeDocument,
            variables: {
              type: part.type,
            },
          }) as PartsByTypeQuery;

          if (!partsByTypeQuery?.partsByType?.data) {
            return;
          }

          // Wait for modal to close before updating cache to avoid console errors
          await nextTick();

          cache.writeQuery({
            query: PartsByTypeDocument,
            data: {
              partsByType: {
                ...partsByTypeQuery.partsByType,
                data: partsByTypeQuery.partsByType.data.filter(
                  (existingPart) => part.id !== existingPart.id
                ),
              },
            },
            variables: {
              type: part.type,
            },
          });
        },
      });

      dispatch(
        updateToastSuccess({
          show: true,
          message: `${computedName} successfully deleted.`,
        })
      );
    } catch (error) {
      dispatch(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    }
  };

  return (
    <li
      key={part.id}
      className="flex flex-col col-span-1 text-center bg-white rounded-lg shadow"
    >
      <div className="relative flex flex-col flex-1 p-8">
        {!loading && (
          <>
            <ModalConfirm
              title={`Delete ${computedName}`}
              variant={MODAL_VARIANT_RED}
              continueButton="Delete"
              modalTrigger={({ setShow }) => {
                setModalShow.current = setShow;

                return null;
              }}
              onContinue={handleModalContinue}
            >
              <p className="text-sm text-gray-500">{`Are you sure you want to delete ${computedName}?`}</p>
            </ModalConfirm>
            <Dropdown
              buttonIcon={dotsHorizontalTripleIcon}
              buttonIconText="Open part options"
              className="absolute right-4 top-4"
            >
              <DropdownItem href={`${path}/${part.id}`} icon={viewListIcon}>
                View
              </DropdownItem>
              <DropdownItem
                href={`${path}/${part.id}/edit`}
                icon={editPencilIcon}
              >
                Edit
              </DropdownItem>
              <DropdownItem
                onClick={handleDeleteButtonClick}
                icon={trashIcon}
                className="text-red-800"
              >
                Delete
              </DropdownItem>
            </Dropdown>
          </>
        )}
        {loading ? (
          <SkeletonLoader className="w-16 h-16 mx-auto rounded-full" />
        ) : part.images?.length ? (
          <Image
            className="flex-shrink-0 w-32 h-32 mx-auto rounded-full"
            url={part.images[0].url}
            width={300}
            height={300}
            alt={`${computedName} image`}
          />
        ) : (
          <div className="flex items-center justify-center flex-shrink-0 w-32 h-32 p-2 mx-auto bg-gray-100 rounded-full">
            <Placeholder
              className="w-full max-w-full max-h-full"
              data-testid="partCardPlaceholder"
            />
          </div>
        )}

        {loading ? (
          <SkeletonLoader className="w-full h-8 mt-6 rounded" />
        ) : (
          <h3 className="mt-6 text-lg font-medium text-gray-900">
            {computedName}
          </h3>
        )}
      </div>
      <div className="py-4">
        {loading ? (
          <div className="space-y-6">
            {range(0, 2).map((row) => (
              <SkeletonLoader key={row} className="w-full h-14" />
            ))}
          </div>
        ) : (
          <dl className="text-sm">
            {chunk(activeSettings, 2).map((twoSettings, index) => (
              <div
                key={twoSettings
                  .map((setting) => kebabCase(setting.label))
                  .join("-")}
                className={twMerge(
                  "grid grid-cols-2 gap-2 py-2",
                  index % 2 === 0 && "bg-gray-50"
                )}
              >
                {twoSettings.map(({ label, value }) => (
                  <div key={label} className="px-2">
                    <dt className="text-sm font-medium text-gray-500">
                      {label}
                    </dt>
                    <dd className="mt-1 text-sm text-gray-900">{value}</dd>
                  </div>
                ))}
              </div>
            ))}
          </dl>
        )}
      </div>
      <div className="grid grid-cols-2">
        {loading ? (
          <>
            <div className="justify-center w-full h-10 bg-gray-300 rounded-bl-lg"></div>
            <div className="justify-center w-full h-10 bg-gray-400 rounded-br-lg"></div>
          </>
        ) : (
          <>
            <ButtonPrimaryLink
              className="justify-center w-full rounded-tl-none rounded-tr-none rounded-br-none"
              href={`${path}/${part.id}`}
            >
              View
            </ButtonPrimaryLink>
            <ButtonSecondaryLink
              className="justify-center w-full rounded-tl-none rounded-tr-none rounded-bl-none"
              href={`${path}/${part.id}/edit`}
            >
              Edit
            </ButtonSecondaryLink>
          </>
        )}
      </div>
    </li>
  );
};

export default PartCard;
