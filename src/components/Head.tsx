import Head from "next/head";
import React from "react";

import { FunctionComponent } from "@/types/react";

type Props = {
  pageTitle?: string;
};

const CustomHead: FunctionComponent<Props> = ({ pageTitle, children }) => {
  const title = pageTitle ? `Plush.bike - ${pageTitle}` : "Plush.bike";

  return (
    <Head>
      <title>{title}</title>
      <meta name="robots" content="noindex, nofollow" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="/favicon-32x32.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="/favicon-16x16.png"
      />
      <link rel="manifest" href="/manifest.json" />
      <link rel="apple-touch-icon" href="/icon-192x192.png" />
      <meta name="theme-color" content="#4338CA" />
      {children}
    </Head>
  );
};

export default CustomHead;
