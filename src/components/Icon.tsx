import React, { SVGProps } from "react";
import { twMerge } from "tailwind-merge";

import { AVAILABLE_SIZES } from "@/constants/icon";
import { pascalCase } from "@/helpers/string";
import { FunctionComponent } from "@/types/react";
import { SvgIcon } from "@/types/svg";

type Props = SVGProps<SVGSVGElement> & {
  svg: SvgIcon;
  size?: keyof typeof AVAILABLE_SIZES;
};

const Icon: FunctionComponent<Props> = ({
  svg,
  size = 4,
  className,
  ...props
}) => {
  return (
    <svg
      {...props}
      className={twMerge(`${AVAILABLE_SIZES[size]} fill-current`, className)}
      xmlns="http://www.w3.org/2000/svg"
      data-testid={`iconSvg${pascalCase(svg.id)}`}
      aria-hidden="true"
    >
      <use
        xlinkHref={`#${svg.id}`}
        xmlnsXlink="http://www.w3.org/1999/xlink"
        data-testid="iconUse"
      />
    </svg>
  );
};

export default Icon;
