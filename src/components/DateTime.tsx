import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";

import { FunctionComponent } from "@/types/react";

type Props = {
  dateTime: string;
  nullValue?: string;
};

// eslint-disable-next-line import/no-named-as-default-member
dayjs.extend(utc);
// eslint-disable-next-line import/no-named-as-default-member
dayjs.extend(timezone);

const DateTime: FunctionComponent<Props> = ({
  dateTime,
  nullValue = "Never",
}) => {
  if (!dayjs(dateTime).isValid()) {
    return <>{nullValue}</>;
  }

  return (
    <>
      {dayjs
        .tz(dateTime, "UTC")
        .tz(dayjs.tz.guess())
        .format("MMM D, YYYY, h:mm A")}
    </>
  );
};

export default DateTime;
