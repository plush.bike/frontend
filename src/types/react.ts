import { FunctionComponent as ReactFunctionComponent, ReactNode } from "react";
import { Except } from "type-fest";

import { BaseLayoutProps } from "@/types/layouts";

export type GetLayoutProps = {
  getLayoutProps?: () => Except<BaseLayoutProps, "apolloClient" | "reduxStore">;
};

export type FunctionComponent<Props = Record<string, unknown>> =
  ReactFunctionComponent<Props & { children?: ReactNode }>;

export type PageFunctionComponent = FunctionComponent & GetLayoutProps;
