import { ComponentPropsWithRef } from "react";
import { FieldError, ValidateResult } from "react-hook-form";

export type ValidationRule<ValueType, Options = { [key: string]: any }> = (
  value: ValueType,
  message: string,
  options?: Options
) => ValidateResult;

export type ValidationRules<ValueType> = ReturnType<
  ValidationRule<ValueType>
>[];

export type ListboxOption = {
  id: string | number;
  name: string;
  disabled?: boolean;
};

export type ListboxChangeEvent = { target: { value: ListboxOption } };

export type SharedInputProps = {
  wrapperClassName?: string;
  label: string;
  description?: string | (() => JSX.Element);
  descriptionClassName?: string;
  labelHidden?: boolean;
  errors?: FieldError | FieldError[];
};

export type RadioInputProps = ComponentPropsWithRef<"input"> & {
  label: string;
  value: string;
};
