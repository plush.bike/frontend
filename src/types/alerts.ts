import { MouseEvent, ComponentPropsWithRef } from "react";

export type BaseProps = ComponentPropsWithRef<"div"> & {
  onClose?: (event?: MouseEvent<HTMLButtonElement>) => void;
};
