export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
  DateTime: any;
  Upload: any;
};

/** Air or coil type */
export enum AirOrCoilType {
  /** Air */
  Air = 'AIR',
  /** Coil */
  Coil = 'COIL'
}

export type ConfirmPasswordInput = {
  password?: InputMaybe<Scalars['String']>;
};

export type ConfirmPasswordResponse = {
  __typename?: 'ConfirmPasswordResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type ConfirmTwoFactorAuthenticationInput = {
  code?: InputMaybe<Scalars['String']>;
};

export type ConfirmTwoFactorAuthenticationResponse = {
  __typename?: 'ConfirmTwoFactorAuthenticationResponse';
  message: Scalars['String'];
  recoveryCodes?: Maybe<Array<Maybe<Scalars['String']>>>;
  status: Scalars['String'];
};

export type ConnectSocialProviderCallbackInput = {
  code?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<SocialProviderType>;
};

export type CreatePartImageInput = {
  src: Scalars['String'];
};

export type CreatePartImagesHasMany = {
  create: Array<CreatePartImageInput>;
};

export type CreatePartInput = {
  images?: InputMaybe<CreatePartImagesHasMany>;
  manufacturer: ManufacturerBelongsTo;
  name?: InputMaybe<Scalars['String']>;
  settingsRecords?: InputMaybe<CreateSettingsRecordsHasMany>;
  specificationsRecord?: InputMaybe<CreateSpecificationsRecordHasOne>;
  type?: InputMaybe<PartType>;
};

export type CreatePartSettingsRecordInput = {
  hscClicks?: InputMaybe<Scalars['Int']>;
  hsrClicks?: InputMaybe<Scalars['Int']>;
  lscClicks?: InputMaybe<Scalars['Int']>;
  lsrClicks?: InputMaybe<Scalars['Int']>;
  nickname?: InputMaybe<Scalars['String']>;
  notes?: InputMaybe<Scalars['String']>;
  preloadAdjust?: InputMaybe<Scalars['Int']>;
  psi?: InputMaybe<Scalars['Float']>;
  sag?: InputMaybe<Scalars['Float']>;
  springRate?: InputMaybe<Scalars['Int']>;
  volumeSpacers?: InputMaybe<Scalars['Float']>;
};

export type CreatePartSpecificationsRecordInput = {
  airOrCoil?: InputMaybe<AirOrCoilType>;
  eyeToEye?: InputMaybe<Scalars['Float']>;
  hscMaxClicks?: InputMaybe<Scalars['Int']>;
  hsrMaxClicks?: InputMaybe<Scalars['Int']>;
  lscMaxClicks?: InputMaybe<Scalars['Int']>;
  lsrMaxClicks?: InputMaybe<Scalars['Int']>;
  strokeLength?: InputMaybe<Scalars['Float']>;
  travel?: InputMaybe<Scalars['Float']>;
};

export type CreatePersonalAccessTokenInput = {
  name?: InputMaybe<Scalars['String']>;
};

export type CreatePersonalAccessTokenResponse = {
  __typename?: 'CreatePersonalAccessTokenResponse';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  lastUsedAt?: Maybe<Scalars['DateTime']>;
  name: Scalars['String'];
  token: Scalars['String'];
  updatedAt: Scalars['DateTime'];
};

export type CreateSettingsRecordsHasMany = {
  create: Array<CreatePartSettingsRecordInput>;
};

export type CreateSpecificationsRecordHasOne = {
  create: CreatePartSpecificationsRecordInput;
};

export type DeletePartInput = {
  id: Scalars['ID'];
};

export type DeletePersonalAccessTokenInput = {
  id: Scalars['ID'];
};

export type DeleteSocialProviderInput = {
  id: Scalars['ID'];
};

export type DisableTwoFactorAuthenticationResponse = {
  __typename?: 'DisableTwoFactorAuthenticationResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type EnableTwoFactorAuthenticationResponse = {
  __typename?: 'EnableTwoFactorAuthenticationResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type ForgotPasswordInput = {
  email?: InputMaybe<Scalars['String']>;
};

export type ForgotPasswordResponse = {
  __typename?: 'ForgotPasswordResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type Manufacturer = {
  __typename?: 'Manufacturer';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  parts: Array<Part>;
  updatedAt: Scalars['DateTime'];
};

export type ManufacturerBelongsTo = {
  connect: Scalars['ID'];
};

/** A paginated list of Manufacturer items. */
export type ManufacturerPaginator = {
  __typename?: 'ManufacturerPaginator';
  /** A list of Manufacturer items. */
  data: Array<Manufacturer>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type Mutation = {
  __typename?: 'Mutation';
  confirmPassword: ConfirmPasswordResponse;
  confirmTwoFactorAuthentication: ConfirmTwoFactorAuthenticationResponse;
  createPart?: Maybe<Part>;
  createPersonalAccessToken: CreatePersonalAccessTokenResponse;
  deletePart?: Maybe<Part>;
  deletePersonalAccessToken: PersonalAccessToken;
  deleteSocialProvider?: Maybe<SocialProvider>;
  disableTwoFactorAuthentication: DisableTwoFactorAuthenticationResponse;
  enableTwoFactorAuthentication: EnableTwoFactorAuthenticationResponse;
  forgotPassword: ForgotPasswordResponse;
  signIn: SignInResponse;
  signOut: SignOutResponse;
  signUp: SignUpResponse;
  socialProviderCallback: SocialProviderCallbackResponse;
  twoFactorAuthenticationChallenge: TwoFactorAuthenticationChallengeResponse;
  twoFactorAuthenticationQrCode: TwoFactorAuthenticationQrCodeResponse;
  twoFactorAuthenticationRecoveryCodes: TwoFactorAuthenticationRecoveryCodesResponse;
  updateAuthenticatedUser?: Maybe<User>;
  updateForgottenPassword: UpdateForgottenPasswordResponse;
  updatePart?: Maybe<Part>;
  updatePassword: UpdatePasswordResponse;
  uploadPartImages?: Maybe<Array<Maybe<UploadPartImagesResponse>>>;
};


export type MutationConfirmPasswordArgs = {
  input: ConfirmPasswordInput;
};


export type MutationConfirmTwoFactorAuthenticationArgs = {
  input: ConfirmTwoFactorAuthenticationInput;
};


export type MutationCreatePartArgs = {
  input: CreatePartInput;
};


export type MutationCreatePersonalAccessTokenArgs = {
  input: CreatePersonalAccessTokenInput;
};


export type MutationDeletePartArgs = {
  input: DeletePartInput;
};


export type MutationDeletePersonalAccessTokenArgs = {
  input: DeletePersonalAccessTokenInput;
};


export type MutationDeleteSocialProviderArgs = {
  input: DeleteSocialProviderInput;
};


export type MutationForgotPasswordArgs = {
  input: ForgotPasswordInput;
};


export type MutationSignInArgs = {
  input?: InputMaybe<SignInInput>;
};


export type MutationSignUpArgs = {
  input?: InputMaybe<SignUpInput>;
};


export type MutationSocialProviderCallbackArgs = {
  input: SocialProviderCallbackInput;
};


export type MutationTwoFactorAuthenticationChallengeArgs = {
  input: TwoFactorAuthenticationChallengeInput;
};


export type MutationUpdateAuthenticatedUserArgs = {
  input: UpdateAuthenticatedUserInput;
};


export type MutationUpdateForgottenPasswordArgs = {
  input?: InputMaybe<UpdateForgottenPasswordInput>;
};


export type MutationUpdatePartArgs = {
  input: UpdatePartInput;
};


export type MutationUpdatePasswordArgs = {
  input: UpdatePassword;
};


export type MutationUploadPartImagesArgs = {
  input: UploadPartImagesInput;
};

/** Allows ordering a list of records. */
export type OrderByClause = {
  /** The column that is used for ordering. */
  column: Scalars['String'];
  /** The direction that is used for ordering. */
  order: SortOrder;
};

/** Aggregate functions when ordering by a relation without specifying a column. */
export enum OrderByRelationAggregateFunction {
  /** Amount of items. */
  Count = 'COUNT'
}

/** Aggregate functions when ordering by a relation that may specify a column. */
export enum OrderByRelationWithColumnAggregateFunction {
  /** Average. */
  Avg = 'AVG',
  /** Amount of items. */
  Count = 'COUNT',
  /** Maximum. */
  Max = 'MAX',
  /** Minimum. */
  Min = 'MIN',
  /** Sum. */
  Sum = 'SUM'
}

/** Information about pagination using a Relay style cursor connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** Number of nodes in the current page. */
  count: Scalars['Int'];
  /** Index of the current page. */
  currentPage: Scalars['Int'];
  /** The cursor to continue paginating forwards. */
  endCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** Index of the last available page. */
  lastPage: Scalars['Int'];
  /** The cursor to continue paginating backwards. */
  startCursor?: Maybe<Scalars['String']>;
  /** Total number of nodes in the paginated connection. */
  total: Scalars['Int'];
};

/** Information about pagination using a fully featured paginator. */
export type PaginatorInfo = {
  __typename?: 'PaginatorInfo';
  /** Number of items in the current page. */
  count: Scalars['Int'];
  /** Index of the current page. */
  currentPage: Scalars['Int'];
  /** Index of the first item in the current page. */
  firstItem?: Maybe<Scalars['Int']>;
  /** Are there more pages after this one? */
  hasMorePages: Scalars['Boolean'];
  /** Index of the last item in the current page. */
  lastItem?: Maybe<Scalars['Int']>;
  /** Index of the last available page. */
  lastPage: Scalars['Int'];
  /** Number of items per page. */
  perPage: Scalars['Int'];
  /** Number of total available items. */
  total: Scalars['Int'];
};

export type Part = {
  __typename?: 'Part';
  activeSettingsRecord?: Maybe<PartSettingsRecord>;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  images?: Maybe<Array<PartImage>>;
  manufacturer: Manufacturer;
  name: Scalars['String'];
  settingsRecords?: Maybe<Array<PartSettingsRecord>>;
  specificationsRecord?: Maybe<PartSpecificationsRecord>;
  type: PartType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type PartImage = {
  __typename?: 'PartImage';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  part: Part;
  src?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  url?: Maybe<Scalars['String']>;
};

/** A paginated list of Part items. */
export type PartPaginator = {
  __typename?: 'PartPaginator';
  /** A list of Part items. */
  data: Array<Part>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type PartSettingsRecord = {
  __typename?: 'PartSettingsRecord';
  createdAt: Scalars['DateTime'];
  hscClicks?: Maybe<Scalars['Int']>;
  hsrClicks?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  lscClicks?: Maybe<Scalars['Int']>;
  lsrClicks?: Maybe<Scalars['Int']>;
  nickname?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  part: Part;
  preloadAdjust?: Maybe<Scalars['Int']>;
  psi?: Maybe<Scalars['Float']>;
  sag?: Maybe<Scalars['Float']>;
  springRate?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['DateTime'];
  volumeSpacers?: Maybe<Scalars['Float']>;
};

export type PartSpecificationsRecord = {
  __typename?: 'PartSpecificationsRecord';
  airOrCoil?: Maybe<AirOrCoilType>;
  createdAt: Scalars['DateTime'];
  eyeToEye?: Maybe<Scalars['Float']>;
  hscMaxClicks?: Maybe<Scalars['Int']>;
  hsrMaxClicks?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  lscMaxClicks?: Maybe<Scalars['Int']>;
  lsrMaxClicks?: Maybe<Scalars['Int']>;
  part: Part;
  strokeLength?: Maybe<Scalars['Float']>;
  travel?: Maybe<Scalars['Float']>;
  updatedAt: Scalars['DateTime'];
};

/** Part type */
export enum PartType {
  /** Fork */
  Fork = 'FORK',
  /** Shock */
  Shock = 'SHOCK'
}

export type PersonalAccessToken = {
  __typename?: 'PersonalAccessToken';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  lastUsedAt?: Maybe<Scalars['DateTime']>;
  name: Scalars['String'];
  updatedAt: Scalars['DateTime'];
};

/** A paginated list of PersonalAccessToken items. */
export type PersonalAccessTokenPaginator = {
  __typename?: 'PersonalAccessTokenPaginator';
  /** A list of PersonalAccessToken items. */
  data: Array<PersonalAccessToken>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type Query = {
  __typename?: 'Query';
  authenticatedUser?: Maybe<User>;
  manufacturers: ManufacturerPaginator;
  partById?: Maybe<Part>;
  partsByType: PartPaginator;
  personalAccessTokens: PersonalAccessTokenPaginator;
  socialProviders: Array<SocialProvider>;
};


export type QueryManufacturersArgs = {
  first: Scalars['Int'];
  page?: InputMaybe<Scalars['Int']>;
};


export type QueryPartByIdArgs = {
  id: Scalars['ID'];
};


export type QueryPartsByTypeArgs = {
  first: Scalars['Int'];
  page?: InputMaybe<Scalars['Int']>;
  type: PartType;
};


export type QueryPersonalAccessTokensArgs = {
  first: Scalars['Int'];
  page?: InputMaybe<Scalars['Int']>;
};

export type SignInInput = {
  email?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
};

export type SignInResponse = {
  __typename?: 'SignInResponse';
  twoFactor?: Maybe<Scalars['Boolean']>;
  user?: Maybe<User>;
};

export type SignOutResponse = {
  __typename?: 'SignOutResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type SignUpInput = {
  email?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  password_confirmation?: InputMaybe<Scalars['String']>;
  recaptchaResponse?: InputMaybe<Scalars['String']>;
};

export type SignUpResponse = {
  __typename?: 'SignUpResponse';
  status: Scalars['String'];
};

/** Information about pagination using a simple paginator. */
export type SimplePaginatorInfo = {
  __typename?: 'SimplePaginatorInfo';
  /** Number of items in the current page. */
  count: Scalars['Int'];
  /** Index of the current page. */
  currentPage: Scalars['Int'];
  /** Index of the first item in the current page. */
  firstItem?: Maybe<Scalars['Int']>;
  /** Are there more pages after this one? */
  hasMorePages: Scalars['Boolean'];
  /** Index of the last item in the current page. */
  lastItem?: Maybe<Scalars['Int']>;
  /** Number of items per page. */
  perPage: Scalars['Int'];
};

export type SocialProvider = {
  __typename?: 'SocialProvider';
  id?: Maybe<Scalars['ID']>;
  provider?: Maybe<SocialProviderType>;
};

export type SocialProviderCallbackInput = {
  code?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<SocialProviderType>;
};

export type SocialProviderCallbackResponse = {
  __typename?: 'SocialProviderCallbackResponse';
  callbackType: SocialProviderCallbackType;
  twoFactor?: Maybe<Scalars['Boolean']>;
  user?: Maybe<User>;
};

/** Social provider callback type */
export enum SocialProviderCallbackType {
  /** Connect */
  Connect = 'CONNECT',
  /** Sign in */
  SignIn = 'SIGN_IN'
}

/** Social provider type */
export enum SocialProviderType {
  /** Google */
  Google = 'GOOGLE',
  /** Strava */
  Strava = 'STRAVA'
}

/** Directions for ordering a list of records. */
export enum SortOrder {
  /** Sort records in ascending order. */
  Asc = 'ASC',
  /** Sort records in descending order. */
  Desc = 'DESC'
}

/** Specify if you want to include or exclude trashed results from a query. */
export enum Trashed {
  /** Only return trashed results. */
  Only = 'ONLY',
  /** Return both trashed and non-trashed results. */
  With = 'WITH',
  /** Only return non-trashed results. */
  Without = 'WITHOUT'
}

export type TwoFactorAuthenticationChallengeInput = {
  code?: InputMaybe<Scalars['String']>;
  recoveryCode?: InputMaybe<Scalars['String']>;
};

export type TwoFactorAuthenticationChallengeResponse = {
  __typename?: 'TwoFactorAuthenticationChallengeResponse';
  user?: Maybe<User>;
};

export type TwoFactorAuthenticationQrCodeResponse = {
  __typename?: 'TwoFactorAuthenticationQrCodeResponse';
  svg: Scalars['String'];
};

export type TwoFactorAuthenticationRecoveryCodesResponse = {
  __typename?: 'TwoFactorAuthenticationRecoveryCodesResponse';
  recoveryCodes?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type UpdateActiveSettingsRecordBelongsTo = {
  connect?: InputMaybe<Scalars['ID']>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type UpdateAuthenticatedUserInput = {
  email?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  profileImage?: InputMaybe<Scalars['Upload']>;
};

export type UpdateForgottenPasswordInput = {
  email?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  password_confirmation?: InputMaybe<Scalars['String']>;
  token?: InputMaybe<Scalars['String']>;
};

export type UpdateForgottenPasswordResponse = {
  __typename?: 'UpdateForgottenPasswordResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type UpdatePartImagesHasMany = {
  create: Array<CreatePartImageInput>;
  delete?: InputMaybe<Array<Scalars['ID']>>;
};

export type UpdatePartInput = {
  activeSettingsRecord?: InputMaybe<UpdateActiveSettingsRecordBelongsTo>;
  id: Scalars['ID'];
  images?: InputMaybe<UpdatePartImagesHasMany>;
  manufacturer?: InputMaybe<ManufacturerBelongsTo>;
  name?: InputMaybe<Scalars['String']>;
  settingsRecords?: InputMaybe<UpdateSettingsRecordsHasMany>;
  specificationsRecord?: InputMaybe<UpdateSpecificationsRecordHasOne>;
  type?: InputMaybe<PartType>;
};

export type UpdatePartSettingsRecordInput = {
  hscClicks?: InputMaybe<Scalars['Int']>;
  hsrClicks?: InputMaybe<Scalars['Int']>;
  id: Scalars['ID'];
  lscClicks?: InputMaybe<Scalars['Int']>;
  lsrClicks?: InputMaybe<Scalars['Int']>;
  nickname?: InputMaybe<Scalars['String']>;
  notes?: InputMaybe<Scalars['String']>;
  preloadAdjust?: InputMaybe<Scalars['Int']>;
  psi?: InputMaybe<Scalars['Float']>;
  sag?: InputMaybe<Scalars['Float']>;
  springRate?: InputMaybe<Scalars['Int']>;
  volumeSpacers?: InputMaybe<Scalars['Float']>;
};

export type UpdatePartSpecificationsRecordInput = {
  airOrCoil?: InputMaybe<AirOrCoilType>;
  eyeToEye?: InputMaybe<Scalars['Float']>;
  hscMaxClicks?: InputMaybe<Scalars['Int']>;
  hsrMaxClicks?: InputMaybe<Scalars['Int']>;
  id: Scalars['ID'];
  lscMaxClicks?: InputMaybe<Scalars['Int']>;
  lsrMaxClicks?: InputMaybe<Scalars['Int']>;
  strokeLength?: InputMaybe<Scalars['Float']>;
  travel?: InputMaybe<Scalars['Float']>;
};

export type UpdatePassword = {
  currentPassword?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
  password_confirmation: Scalars['String'];
};

export type UpdatePasswordResponse = {
  __typename?: 'UpdatePasswordResponse';
  message: Scalars['String'];
  status: Scalars['String'];
};

export type UpdateSettingsRecordsHasMany = {
  create: Array<CreatePartSettingsRecordInput>;
  delete?: InputMaybe<Array<Scalars['ID']>>;
  update: Array<UpdatePartSettingsRecordInput>;
};

export type UpdateSpecificationsRecordHasOne = {
  update: UpdatePartSpecificationsRecordInput;
};

export type UploadPartImagesInput = {
  files: Array<Scalars['Upload']>;
};

export type UploadPartImagesResponse = {
  __typename?: 'UploadPartImagesResponse';
  src: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  createdAt: Scalars['DateTime'];
  email?: Maybe<Scalars['String']>;
  hasEnabledTwoFactorAuthentication?: Maybe<Scalars['Boolean']>;
  hasPasswordSet?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  parts: Array<Part>;
  profileImageSrc?: Maybe<Scalars['String']>;
  profileImageUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
};
