import { FunctionComponent } from "@/types/react";

export type SvgIcon = {
  id: string;
};

export interface SvgrComponent
  extends FunctionComponent<React.SVGProps<SVGSVGElement>> {}
