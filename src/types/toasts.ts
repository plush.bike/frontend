import { MouseEvent, ComponentPropsWithRef } from "react";

export type BaseProps = ComponentPropsWithRef<"div"> & {
  show: boolean;
  showFor?: number;
  dataTestid?: string;
  onClose?: (event?: MouseEvent<HTMLButtonElement>) => void;
};
