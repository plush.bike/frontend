import { ComponentPropsWithRef, MouseEvent } from "react";

export type BadgeProps = ComponentPropsWithRef<"span"> & {
  onRemove?: (event?: MouseEvent<HTMLButtonElement>) => void;
};
