import { ApolloClient } from "@apollo/client";
import { AnyAction, Store } from "@reduxjs/toolkit";

export type BaseLayoutProps = {
  reduxStore: Store<any, AnyAction>;
  apolloClient: ApolloClient<any>;
  requireAuthentication?: boolean;
  mainShouldHavePadding?: boolean;
  showNavigation?: boolean;
};
