import { ComponentPropsWithRef } from "react";

import { SvgIcon } from "@/types/svg";

type SharedProps = {
  loading?: boolean;
  icon?: SvgIcon;
};

export type ButtonProps = ComponentPropsWithRef<"button"> & SharedProps;

export type ButtonLinkProps = ComponentPropsWithRef<"a"> &
  SharedProps & {
    href: string;
  };
