import * as GraphQlTypes from "@/types/graphql";

export type User = Pick<
  GraphQlTypes.User,
  | "id"
  | "name"
  | "email"
  | "profileImageUrl"
  | "hasEnabledTwoFactorAuthentication"
  | "hasPasswordSet"
  | "createdAt"
  | "updatedAt"
>;
