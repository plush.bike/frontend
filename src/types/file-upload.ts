import * as Apollo from "@apollo/client";

import { SavedFile } from "@/helpers/file-upload";
import * as GraphQLTypes from "@/types/graphql";

export type FileExtended = File & {
  id: string;
  preview: string;
  key: string;
  loading: boolean;
  src: string;
  error: string;
};

export type FileUploadChangeEvent = (FileExtended | SavedFile)[];

export type FileUploadMutationVariables = GraphQLTypes.Exact<{
  files: Array<GraphQLTypes.Scalars["Upload"]> | GraphQLTypes.Scalars["Upload"];
}>;

export type FileUploadResponse = {
  __typename?: string;
  src: GraphQLTypes.Scalars["String"];
};

export type FileUploadMutation = {
  __typename?: "Mutation";
  [key: string]: GraphQLTypes.Maybe<FileUploadResponse[]> | string;
};

export type FileUploadMutationFn = Apollo.MutationFunction<
  FileUploadMutation,
  FileUploadMutationVariables
>;

export type FileUploadMutationHook = (
  baseOptions?: Apollo.MutationHookOptions<
    FileUploadMutation,
    FileUploadMutationVariables
  >
) => Apollo.MutationTuple<
  FileUploadMutation,
  GraphQLTypes.Exact<{
    files: any;
  }>
>;
