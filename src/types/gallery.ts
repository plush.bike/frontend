export type GalleryItem = {
  src: string;
  thumbnailSrc: string;
  id: number;
};
