import { useRouter } from "next/router";
import { useDispatch } from "react-redux";

import Head from "@/components/Head";
import ForksCreateEdit from "@/components/parts/forks/CreateEdit";
import { useCreatePartMutation } from "@/graphql/part/mutations/create-part.generated";
import { useUpdatePartMutation } from "@/graphql/part/mutations/update-part.generated";
import { formatSettingsRecordsForMutation } from "@/helpers/part";
import { updateToastSuccess } from "@/store/slices/toasts";
import { PartType } from "@/types/graphql";
import { FunctionComponent } from "@/types/react";

const ForksCreate: FunctionComponent = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [createPart] = useCreatePartMutation();
  const [updatePart] = useUpdatePartMutation();

  const onSubmit = async ({
    name,
    manufacturer,
    images = [],
    lscMaxClicks,
    hscMaxClicks,
    lsrMaxClicks,
    hsrMaxClicks,
    travel,
    airOrCoil,
    settingsRecords,
    activeSettingsRecord,
  }) => {
    const {
      data: { createPart: part },
    } = await createPart({
      variables: {
        name,
        type: PartType.Fork,
        manufacturer: { connect: manufacturer.id as string },
        images: { create: images.map((image) => ({ src: image.src })) },
        specificationsRecord: {
          create: {
            lscMaxClicks,
            hscMaxClicks,
            lsrMaxClicks,
            hsrMaxClicks,
            travel,
            airOrCoil,
          },
        },
        settingsRecords: {
          create: formatSettingsRecordsForMutation({
            settingsRecords,
            airOrCoil,
          }),
        },
      },
      update: (cache) => {
        cache.evict({
          id: "ROOT_QUERY",
          fieldName: "partsByType",
        });
      },
    });

    if (part?.settingsRecords?.[activeSettingsRecord]?.id) {
      await updatePart({
        variables: {
          id: part.id,
          activeSettingsRecord: {
            connect: part.settingsRecords[activeSettingsRecord].id,
          },
        },
      });
    }

    dispatch(
      updateToastSuccess({
        show: true,
        message: "Fork successfully added.",
      })
    );
    router.push("/parts/forks");
  };

  return (
    <div className="container">
      <Head pageTitle="Add a fork" />
      <h1>Add a fork</h1>
      <ForksCreateEdit onSubmit={onSubmit} />
    </div>
  );
};

export default ForksCreate;
