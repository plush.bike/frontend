import { range } from "lodash";
import { useRouter } from "next/router";
import { FunctionComponent, useMemo } from "react";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import EmptyState from "@/components/EmptyState";
import Head from "@/components/Head";
import Pagination from "@/components/pagination/Pagination";
import PartCard from "@/components/parts/PartCard";
import { DEFAULT_PER_PAGE } from "@/constants/pagination";
import { usePartsByTypeQuery } from "@/graphql/part/queries/parts-by-type.generated";
import plusIcon from "@/icons/plus.svg";
import ForkPlaceholder from "@/svgs/fork-placeholder.svg";
import { AirOrCoilType, PartType } from "@/types/graphql";

const ForksIndex: FunctionComponent = () => {
  const router = useRouter();

  const page = useMemo(() => {
    return parseInt(router.query.page as string) || 1;
  }, [router]);

  const { loading, error, data } = usePartsByTypeQuery({
    variables: { type: PartType.Fork, first: DEFAULT_PER_PAGE, page },
  });

  const itemsCount = useMemo(() => {
    return data?.partsByType?.paginatorInfo?.total || 0;
  }, [data]);

  const activeSettings = (part) => {
    if (!part.activeSettingsRecord) {
      return [];
    }

    const baseSettings = [
      {
        label: "Sag (mm)",
        value: part.activeSettingsRecord.sag,
      },
      {
        label: "LSC Clicks",
        value: part.activeSettingsRecord.lscClicks,
      },
      {
        label: "HSC Clicks",
        value: part.activeSettingsRecord.hscClicks,
      },
      {
        label: "LSR Clicks",
        value: part.activeSettingsRecord.lsrClicks,
      },
      {
        label: "HSR Clicks",
        value: part.activeSettingsRecord.hsrClicks,
      },
    ];

    return part?.specificationsRecord?.airOrCoil === AirOrCoilType.Air
      ? [
          {
            label: "PSI",
            value: part.activeSettingsRecord.psi,
          },
          {
            label: "Volume Spacers",
            value: part.activeSettingsRecord.volumeSpacers,
          },
          ...baseSettings,
        ]
      : [
          {
            label: "Spring rate",
            value: part.activeSettingsRecord.springRate,
          },
          {
            label: "Preload adjust",
            value: part.activeSettingsRecord.preloadAdjust,
          },
          ...baseSettings,
        ];
  };

  return (
    <div className="container">
      <Head pageTitle="Forks" />
      {error && (
        <AlertError>
          An error occurred loading your forks, please try again.
        </AlertError>
      )}
      <div className="flex items-center justify-between">
        <h1>Forks</h1>
        <ButtonPrimaryLink icon={plusIcon} href="/parts/forks/create">
          New fork
        </ButtonPrimaryLink>
      </div>
      {loading || data?.partsByType?.data?.length ? (
        <ul
          role="list"
          className="grid grid-cols-1 gap-6 mt-4 lg:grid-cols-2 xl:grid-cols-4"
        >
          {loading &&
            range(0, 8).map((part) => <PartCard key={part} loading />)}
          {!loading &&
            data.partsByType.data.map((part) => (
              <PartCard
                key={part.id}
                part={part}
                path="/parts/forks"
                placeholder={ForkPlaceholder}
                activeSettings={activeSettings(part)}
              ></PartCard>
            ))}
        </ul>
      ) : (
        <EmptyState
          title="No forks"
          buttonText="New fork"
          href="/parts/forks/create"
        >
          Create a fork to help keep track of your settings.
        </EmptyState>
      )}
      <Pagination
        className="mt-4"
        itemsCount={itemsCount}
        perPage={DEFAULT_PER_PAGE}
        page={page}
      />
    </div>
  );
};

export default ForksIndex;
