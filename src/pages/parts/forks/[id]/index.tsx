import { createColumnHelper } from "@tanstack/react-table";
import { upperFirst, lowerCase } from "lodash";
import { useRouter } from "next/router";
import { FunctionComponent, useMemo } from "react";

import AlertError from "@/components/alerts/AlertError";
import BadgeGray from "@/components/badges/BadgeGray";
import BadgePrimary from "@/components/badges/BadgePrimary";
import Gallery from "@/components/gallery/Gallery";
import Head from "@/components/Head";
import Loader from "@/components/Loader";
import Table from "@/components/table/Table";
import Truncate from "@/components/Truncate";
import { usePartByIdQuery } from "@/graphql/part/queries/part-by-id.generated";
import { imageUrl } from "@/helpers/url";
import { AirOrCoilType } from "@/types/graphql";

const ForksShow: FunctionComponent = () => {
  const router = useRouter();
  const { id } = router.query;

  const { loading, error, data } = usePartByIdQuery({
    variables: {
      id: id as string,
    },
  });

  const partById = useMemo(() => {
    return data?.partById;
  }, [data]);

  const name = `${partById?.manufacturer?.name} ${partById?.name}`;
  const settingsRecords = partById?.settingsRecords || [];
  const activeSettingsRecordId = partById?.activeSettingsRecord?.id;
  const specificationRecord = partById?.specificationsRecord;

  const columnHelper = createColumnHelper<(typeof settingsRecords)[0]>();

  const settingsRecordsTableColumns = useMemo(
    () => [
      columnHelper.accessor("id", {
        header: "Status",
        cell: (info) =>
          info.getValue() === activeSettingsRecordId ? (
            <BadgePrimary>Active</BadgePrimary>
          ) : (
            <BadgeGray>Inactive</BadgeGray>
          ),
      }),
      columnHelper.accessor("nickname", {
        header: "Nickname",
      }),
      ...(specificationRecord?.airOrCoil === AirOrCoilType.Air
        ? [
            columnHelper.accessor("psi", {
              header: "PSI",
            }),
            columnHelper.accessor("volumeSpacers", {
              header: "Volume spacers",
            }),
          ]
        : [
            columnHelper.accessor("springRate", {
              header: "Spring rate",
            }),
            columnHelper.accessor("preloadAdjust", {
              header: () => "Preload adjust",
            }),
          ]),
      columnHelper.accessor("sag", {
        header: "Sag (mm)",
      }),
      columnHelper.accessor("lscClicks", {
        header: "LSC clicks",
      }),
      columnHelper.accessor("hscClicks", {
        header: "HSC clicks",
      }),
      columnHelper.accessor("lsrClicks", {
        header: "LSR clicks",
      }),
      columnHelper.accessor("hsrClicks", {
        header: "HSR clicks",
      }),
      columnHelper.accessor("notes", {
        header: "Notes",
        cell: (info) => <Truncate>{info.getValue()}</Truncate>,
        meta: {
          cellClassName: "max-w-lg",
        },
      }),
    ],
    [activeSettingsRecordId, specificationRecord]
  );

  return (
    <div className="container">
      <Head pageTitle={loading ? "Fork" : name} />
      {error && (
        <AlertError>
          An error occurred loading your fork, please try again.
        </AlertError>
      )}
      {loading ? (
        <Loader fullPage />
      ) : (
        <>
          <h1>{name}</h1>
          {specificationRecord && (
            <div className="px-4 py-5 mt-6 overflow-hidden bg-white rounded-md shadow sm:rounded-lg sm:px-6">
              <h3>Fork specifications</h3>
              <dl className="mt-4 flex flex-wrap -m-4">
                <div className="p-4 w-full sm:w-auto">
                  <dt className="text-sm font-medium text-gray-500">
                    Low speed compression max clicks
                  </dt>
                  <dd className="mt-1 text-xl font-bold text-gray-900">
                    {specificationRecord.lscMaxClicks}
                  </dd>
                </div>
                <div className="p-4 w-full sm:w-auto">
                  <dt className="text-sm font-medium text-gray-500">
                    High speed compression max clicks
                  </dt>
                  <dd className="mt-1 text-xl font-bold text-gray-900">
                    {specificationRecord.hscMaxClicks}
                  </dd>
                </div>
                <div className="p-4 w-full sm:w-auto">
                  <dt className="text-sm font-medium text-gray-500">
                    Low speed rebound max clicks
                  </dt>
                  <dd className="mt-1 text-xl font-bold text-gray-900">
                    {specificationRecord.lsrMaxClicks}
                  </dd>
                </div>
                <div className="p-4 w-full sm:w-auto">
                  <dt className="text-sm font-medium text-gray-500">
                    High speed rebound max clicks
                  </dt>
                  <dd className="mt-1 text-xl font-bold text-gray-900">
                    {specificationRecord.hsrMaxClicks}
                  </dd>
                </div>
                <div className="p-4 w-full sm:w-auto">
                  <dt className="text-sm font-medium text-gray-500">Travel</dt>
                  <dd className="mt-1 text-xl font-bold text-gray-900">
                    {specificationRecord.travel}
                  </dd>
                </div>
                <div className="p-4 w-full sm:w-auto">
                  <dt className="text-sm font-medium text-gray-500">
                    Air or coil
                  </dt>
                  <dd className="mt-1 text-xl font-bold text-gray-900">
                    {upperFirst(lowerCase(specificationRecord.airOrCoil))}
                  </dd>
                </div>
              </dl>
            </div>
          )}
          <div className="px-4 py-5 mt-6 overflow-hidden bg-white rounded-md shadow sm:rounded-lg sm:px-6">
            <h3>Fork settings</h3>
            <Table<(typeof settingsRecords)[0]>
              columns={settingsRecordsTableColumns}
              data={settingsRecords}
              className="mt-4"
              breakpoint="lg"
              emptyContent="No settings records found."
            />
          </div>
          {partById?.images?.length > 0 && (
            <div className="px-4 py-5 mt-6 overflow-hidden bg-white rounded-md shadow sm:rounded-lg sm:px-6">
              <h3>Images</h3>
              <div className="mt-4">
                <Gallery
                  items={partById.images.map((image) => ({
                    id: parseInt(image.id),
                    src: image.url,
                    thumbnailSrc: imageUrl({ url: image.url, width: 300 }),
                  }))}
                />
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default ForksShow;
