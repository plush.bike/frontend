import { partition } from "lodash";
import { useRouter } from "next/router";
import { FunctionComponent, useMemo } from "react";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import Head from "@/components/Head";
import Loader from "@/components/Loader";
import ForksCreateEdit from "@/components/parts/forks/CreateEdit";
import { useUpdatePartMutation } from "@/graphql/part/mutations/update-part.generated";
import { usePartByIdQuery } from "@/graphql/part/queries/part-by-id.generated";
import { SavedFile } from "@/helpers/file-upload";
import { formatSettingsRecordsForMutation } from "@/helpers/part";
import { updateToastSuccess } from "@/store/slices/toasts";

const ForksEdit: FunctionComponent = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { id } = router.query;

  const { loading, error, data } = usePartByIdQuery({
    variables: {
      id: id as string,
    },
  });

  const [updatePart] = useUpdatePartMutation();

  const specificationRecord = useMemo(() => {
    return data?.partById?.specificationsRecord;
  }, [data]);

  const defaultValuesProp = useMemo(() => {
    if (!data) {
      return null;
    }

    const { partById } = data;
    const {
      name,
      manufacturer,
      images,
      settingsRecords,
      activeSettingsRecord,
    } = partById;

    const activeSettingsRecordIndex = settingsRecords.findIndex(
      (settingsRecord) => settingsRecord.id === activeSettingsRecord?.id
    );

    return {
      name,
      manufacturer,
      images: images.map(
        (image) => new SavedFile({ id: image.id, src: image.url })
      ),
      settingsRecords,
      lscMaxClicks: specificationRecord.lscMaxClicks,
      hscMaxClicks: specificationRecord.hscMaxClicks,
      lsrMaxClicks: specificationRecord.lsrMaxClicks,
      hsrMaxClicks: specificationRecord.hsrMaxClicks,
      travel: specificationRecord.travel,
      airOrCoil: specificationRecord.airOrCoil,
      activeSettingsRecord:
        activeSettingsRecordIndex === -1 ? null : activeSettingsRecordIndex,
    };
  }, [data]);

  const onSubmit = async ({
    name,
    manufacturer,
    images,
    lscMaxClicks,
    hscMaxClicks,
    lsrMaxClicks,
    hsrMaxClicks,
    travel,
    airOrCoil,
    settingsRecords,
    activeSettingsRecord,
  }) => {
    const [updateSettingsRecords, createSettingsRecords] = partition(
      settingsRecords,
      (settingsRecord) =>
        Object.prototype.hasOwnProperty.call(settingsRecord, "id")
    );
    const deleteSettingsRecords = defaultValuesProp.settingsRecords
      .filter(
        (settingsRecord) =>
          !updateSettingsRecords.find(
            (updateSettingsRecord) =>
              updateSettingsRecord.id === settingsRecord.id
          )
      )
      .map(({ id }) => id);

    const [savedImages, newImages] = partition(
      images,
      (image) => image.id !== null
    );

    const deleteImages = defaultValuesProp.images
      .filter(
        (image) => !savedImages.find((savedImage) => savedImage.id === image.id)
      )
      .map(({ id }) => id);

    const {
      data: { updatePart: part },
    } = await updatePart({
      variables: {
        id: id as string,
        name,
        manufacturer: { connect: manufacturer.id },
        specificationsRecord: {
          update: {
            id: specificationRecord.id,
            lscMaxClicks,
            hscMaxClicks,
            lsrMaxClicks,
            hsrMaxClicks,
            travel,
            airOrCoil,
          },
        },
        settingsRecords: {
          create: formatSettingsRecordsForMutation({
            settingsRecords: createSettingsRecords,
            airOrCoil,
          }),
          update: formatSettingsRecordsForMutation({
            settingsRecords: updateSettingsRecords,
            airOrCoil,
          }),
          delete: deleteSettingsRecords,
        },
        images: {
          create: newImages.map((image) => ({ src: image.src })),
          delete: deleteImages,
        },
      },
    });

    await updatePart({
      variables: {
        id: part.id,
        activeSettingsRecord: part?.settingsRecords?.[activeSettingsRecord]?.id
          ? {
              connect: part.settingsRecords[activeSettingsRecord].id,
            }
          : { disconnect: true },
      },
      update: (cache) => {
        cache.evict({
          id: "ROOT_QUERY",
          fieldName: "partsByType",
        });
        cache.evict({
          id: "ROOT_QUERY",
          fieldName: "partById",
        });
      },
    });

    dispatch(
      updateToastSuccess({
        show: true,
        message: "Fork successfully updated.",
      })
    );
    router.push("/parts/forks");
  };

  return (
    <div className="container">
      <Head pageTitle="Edit fork" />
      {error && (
        <AlertError>
          An error occurred loading your fork, please try again.
        </AlertError>
      )}
      {loading ? (
        <Loader fullPage />
      ) : (
        <>
          <h1>Edit fork</h1>
          <ForksCreateEdit
            onSubmit={onSubmit}
            defaultValues={defaultValuesProp}
          />
        </>
      )}
    </div>
  );
};

export default ForksEdit;
