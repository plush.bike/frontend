import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import Head from "@/components/Head";
import Icon from "@/components/Icon";
import { useForgotPasswordMutation } from "@/graphql/auth/mutations/forgot-password.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { validateEmail } from "@/helpers/validation-rules";
import checkmarkOutlineIcon from "@/icons/checkmark-outline.svg";
import { PageFunctionComponent } from "@/types/react";

type Inputs = {
  email: string;
};

const ForgotPassword: PageFunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [successIsShown, setSuccessIsShown] = useState(false);
  const [loading, setLoading] = useState(false);

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<Inputs>();

  const [forgotPassword] = useForgotPasswordMutation();

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({ email }) => {
    setLoading(true);

    try {
      await forgotPassword({
        variables: {
          email,
        },
      });

      setSuccessIsShown(true);
      setAlertErrorMessage(null);
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    }

    setLoading(false);
  };

  return (
    <div className="container-sm">
      <Head pageTitle="Forgot password" />
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        {successIsShown ? (
          <div className="text-center">
            <Icon
              svg={checkmarkOutlineIcon}
              size={32}
              className="mx-auto text-emerald-500"
            />
            <h2 className="mt-4">
              Password reset email sent successfully, please check your email.
            </h2>
          </div>
        ) : (
          <>
            <h1>Forgot Password</h1>
            <FormWrapper className="mt-4" onSubmit={handleSubmit(onSubmit)}>
              <FormInput
                label="Email"
                type="email"
                errors={errors.email}
                {...register("email", {
                  required: {
                    value: true,
                    message: "Please enter your email.",
                  },
                  validate: {
                    email: (value) =>
                      validateEmail(value, "Please enter a valid email."),
                  },
                })}
              />
              <ButtonPrimary className="mt-4" type="submit" loading={loading}>
                Send forgot password email
              </ButtonPrimary>
            </FormWrapper>
          </>
        )}
      </div>
    </div>
  );
};

ForgotPassword.getLayoutProps = () => ({ requireAuthentication: false });

export default ForgotPassword;
