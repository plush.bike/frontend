import { useRouter } from "next/router";
import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import Head from "@/components/Head";
import { useUpdateForgottenPasswordMutation } from "@/graphql/auth/mutations/update-forgotten-password.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { getQueryParam } from "@/helpers/url";
import { validateMatch } from "@/helpers/validation-rules";
import { updateToastSuccess } from "@/store/slices/toasts";
import { PageFunctionComponent } from "@/types/react";

type Inputs = {
  password: string;
  passwordConfirmation: string;
};

const ResetPassword: PageFunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [invalidTokenAlertIsShown, setInvalidTokenAlertIsShown] =
    useState(false);
  const [loading, setLoading] = useState(false);

  const {
    register,
    handleSubmit,
    setError,
    watch,
    formState: { errors },
  } = useForm<Inputs>();

  const [updateForgottenPassword] = useUpdateForgottenPasswordMutation();
  const dispatch = useDispatch();
  const router = useRouter();

  const watchPasswordConfirmation = watch("passwordConfirmation", "");

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const handleInvalidTokenAlertClose = () => {
    setInvalidTokenAlertIsShown(false);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({
    password,
    passwordConfirmation,
  }) => {
    const token = getQueryParam("token");
    const email = getQueryParam("email");

    setLoading(true);

    try {
      await updateForgottenPassword({
        variables: {
          email,
          token,
          password,
          passwordConfirmation,
        },
      });

      dispatch(
        updateToastSuccess({
          show: true,
          message: "Password successfully reset.",
        })
      );
      router.push("/sign-in");
    } catch (error) {
      const invalidToken = error?.graphQLErrors?.[0]?.extensions?.errors?.token;

      if (invalidToken) {
        setInvalidTokenAlertIsShown(true);
        setLoading(false);

        return;
      }

      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    }

    setLoading(false);
  };

  return (
    <div className="container-sm">
      <Head pageTitle="Reset password" />
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      {invalidTokenAlertIsShown && (
        <AlertError onClose={handleInvalidTokenAlertClose}>
          <p>
            The password reset link is invalid or expired. Please try{" "}
            <a
              className="text-white underline no-anchor-styles"
              href="/forgot-password"
            >
              resending the password reset email
            </a>
            .
          </p>
        </AlertError>
      )}
      <div className="p-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        <h1>Reset Your Password</h1>
        <FormWrapper
          className="grid grid-cols-1 gap-4 mt-4"
          onSubmit={handleSubmit(onSubmit)}
        >
          <FormInput
            label="Password"
            type="password"
            errors={errors.password}
            {...register("password", {
              required: { value: true, message: "Please enter a password." },
              minLength: {
                value: 8,
                message: "Your password must be at least 8 characters long.",
              },
              validate: {
                match: (value) =>
                  validateMatch(value, "Your passwords do not match.", {
                    otherValue: watchPasswordConfirmation,
                  }),
              },
            })}
          />
          <FormInput
            label="Confirm password"
            type="password"
            errors={errors.passwordConfirmation}
            {...register("passwordConfirmation", {
              required: {
                value: true,
                message: "Please confirm your password.",
              },
            })}
          />
          <ButtonPrimary
            className="justify-self-start"
            type="submit"
            loading={loading}
          >
            Reset password
          </ButtonPrimary>
        </FormWrapper>
      </div>
    </div>
  );
};

ResetPassword.getLayoutProps = () => ({ requireAuthentication: false });

export default ResetPassword;
