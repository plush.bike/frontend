import Image from "next/image";

import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import Head from "@/components/Head";
import { useAuth } from "@/hooks/auth";
import backgroundImage from "@/images/404-background-image.jpg";
import { PageFunctionComponent } from "@/types/react";

const Custom404: PageFunctionComponent = () => {
  const { isAuthenticated } = useAuth();

  const customImageLoader = ({ src }) => {
    return src;
  };

  return (
    <div className="flex flex-col flex-1 min-h-full">
      <Head pageTitle="404 page not found" />
      <Image
        src={backgroundImage}
        alt="background image"
        loader={customImageLoader}
        unoptimized
        fill
        sizes="100vw"
        className="object-cover"
      />
      <div className="relative z-10 flex-1 w-full min-h-full bg-gray-700/30">
        <div className="px-4 py-16 mx-auto text-center max-w-7xl sm:px-6 sm:py-24 lg:px-8 lg:py-48">
          <p className="text-sm font-semibold tracking-wide text-white uppercase">
            404 error
          </p>
          <h1 className="mt-2 text-4xl font-extrabold tracking-tight text-white sm:text-5xl">
            Uh oh! I think you&apos;re lost.
          </h1>
          <p className="mt-2 text-lg font-medium text-white">
            It looks like the page you&apos;re looking for doesn&apos;t exist.
          </p>
          <div className="mt-6">
            <ButtonPrimaryLink href={isAuthenticated ? "/parts/forks" : "/"}>
              Go back home
            </ButtonPrimaryLink>
          </div>
        </div>
      </div>
    </div>
  );
};

Custom404.getLayoutProps = () => ({
  requireAuthentication: false,
  showNavigation: false,
  mainShouldHavePadding: false,
});

export default Custom404;
