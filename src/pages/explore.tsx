import Filter from "@/components/Filter";
import FormInput from "@/components/form/FormInput";
import Head from "@/components/Head";
import searchIcon from "@/icons/search.svg";

const Explore = () => {
  return (
    <div className="container">
      <Head pageTitle="Explore" />
      <h1>Explore</h1>
      <div className="grid grid-cols-4 gap-4 mt-4">
        <div className="col-span-1">
          <FormInput
            icon={searchIcon}
            label="Search"
            labelHidden
            placeholder="Search"
          />
        </div>
        <Filter
          className="col-span-3"
          availableFilters={[
            {
              category: {
                label: "Filter 1",
                key: "filter-1",
              },
              options: [
                {
                  label: "Option 1",
                  key: "option-1",
                },
                {
                  label: "Option 2",
                  key: "option-2",
                },
              ],
            },
            {
              category: {
                label: "Filter 2",
                key: "filter-2",
              },
              options: [
                {
                  label: "Option 1",
                  key: "option-1",
                },
                {
                  label: "Option 2",
                  key: "option-2",
                },
                {
                  label: "Option 3",
                  key: "option-3",
                },
              ],
            },
            {
              category: {
                label: "Filter 3",
                key: "filter-3",
              },
              options: [
                {
                  label: "Option 1",
                  key: "option-1",
                },
                {
                  label: "Option 2",
                  key: "option-2",
                },
                {
                  label: "Option 3",
                  key: "option-3",
                },
                {
                  label: "Option 4",
                  key: "option-4",
                },
              ],
            },
          ]}
        />
      </div>
    </div>
  );
};

export default Explore;
