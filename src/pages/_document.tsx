import Document, { Html, Head, Main, NextScript } from "next/document";
import sprite from "svg-sprite-loader/runtime/sprite.build";

type Props = {
  spriteContent: string;
};

class MyDocument extends Document<Props> {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    const spriteContent = sprite.stringify();
    return { spriteContent, ...initialProps };
  }

  render() {
    return (
      <Html className="h-full bg-gray-100">
        <Head />
        <body className="h-full">
          <div dangerouslySetInnerHTML={{ __html: this.props.spriteContent }} />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
