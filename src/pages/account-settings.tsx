import PersonalAccessTokens from "@/components/account-settings/personal-access-tokens/PersonalAccessTokens";
import SocialProviders from "@/components/account-settings/SocialProviders";
import TwoFactorAuthentication from "@/components/account-settings/two-factor-authentication/TwoFactorAuthentication";
import UpdatePassword from "@/components/account-settings/UpdatePassword";
import UpdateProfile from "@/components/account-settings/UpdateProfile";
import Head from "@/components/Head";

const AccountSettings = () => {
  return (
    <div className="container">
      <Head pageTitle="Account Settings" />
      <h1>Account Settings</h1>
      <UpdateProfile />
      <UpdatePassword />
      <SocialProviders />
      <TwoFactorAuthentication />
      <PersonalAccessTokens />
    </div>
  );
};

export default AccountSettings;
