import { useRouter } from "next/router";
import { useState } from "react";
import {
  useForm,
  SubmitHandler,
  Controller,
  FieldError,
} from "react-hook-form";
import { useDispatch } from "react-redux";

import AlertError from "@/components/alerts/AlertError";
import SocialSignIn from "@/components/auth/SocialSignIn";
import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import FormInput from "@/components/form/FormInput";
import FormWrapper from "@/components/form/FormWrapper";
import Recaptcha from "@/components/form/Recaptcha";
import Head from "@/components/Head";
import { useSignUpMutation } from "@/graphql/auth/mutations/sign-up.generated";
import { parseAndSetServerErrors } from "@/helpers/form";
import { validateEmail, validateMatch } from "@/helpers/validation-rules";
import { updateToastSuccess } from "@/store/slices/toasts";
import { PageFunctionComponent } from "@/types/react";

type Inputs = {
  recaptchaResponse: string;
  name: string;
  email: string;
  password: string;
  passwordConfirmation: string;
};

const SignUp: PageFunctionComponent = () => {
  const [alertErrorMessage, setAlertErrorMessage] = useState<string>(null);
  const [loading, setLoading] = useState(false);

  const {
    register,
    handleSubmit,
    setError,
    watch,
    control,
    formState: { errors },
  } = useForm<Inputs>();

  const [signUp] = useSignUpMutation();
  const dispatch = useDispatch();
  const router = useRouter();

  const watchPasswordConfirmation = watch("passwordConfirmation", "");

  const handleAlertErrorClose = () => {
    setAlertErrorMessage(null);
  };

  const onSubmit: SubmitHandler<Inputs> = async ({
    recaptchaResponse,
    name,
    email,
    password,
    passwordConfirmation,
  }) => {
    setLoading(true);
    try {
      await signUp({
        variables: {
          recaptchaResponse,
          name,
          email,
          password,
          passwordConfirmation,
        },
      });

      dispatch(
        updateToastSuccess({
          show: true,
          message: "Sign up successful, please sign in.",
        })
      );
      router.push("/sign-in");
    } catch (error) {
      parseAndSetServerErrors({
        error,
        setAlertErrorMessage,
        setError,
      });
    }
    setLoading(false);
  };

  return (
    <div className="container-sm">
      <Head pageTitle="Sign up" />
      {alertErrorMessage !== null && (
        <AlertError onClose={handleAlertErrorClose}>
          {alertErrorMessage}
        </AlertError>
      )}
      <div className="p-6 bg-white rounded-md sm:rounded-lg sm:shadow">
        <h1>Sign up</h1>
        <FormWrapper
          className="grid grid-cols-1 gap-4 mt-4"
          onSubmit={handleSubmit(onSubmit)}
        >
          <FormInput
            label="Name"
            type="text"
            errors={errors.name}
            {...register("name", {
              required: { value: true, message: "Please enter your name." },
            })}
          />
          <FormInput
            label="Email"
            type="email"
            errors={errors.email}
            {...register("email", {
              required: { value: true, message: "Please enter your email." },
              validate: {
                email: (value) =>
                  validateEmail(value, "Please enter a valid email."),
              },
            })}
          />
          <FormInput
            label="Password"
            type="password"
            errors={errors.password}
            {...register("password", {
              required: { value: true, message: "Please enter a password." },
              minLength: {
                value: 8,
                message: "Your password must be at least 8 characters long.",
              },
              validate: {
                match: (value) =>
                  validateMatch(value, "Your passwords do not match.", {
                    otherValue: watchPasswordConfirmation,
                  }),
              },
            })}
          />
          <FormInput
            label="Confirm password"
            type="password"
            errors={errors.passwordConfirmation}
            {...register("passwordConfirmation", {
              required: {
                value: true,
                message: "Please confirm your password.",
              },
            })}
          />
          <Controller<Inputs, "recaptchaResponse">
            name="recaptchaResponse"
            control={control}
            rules={{
              required: {
                value: true,
                message: "Please complete the reCAPTCHA.",
              },
            }}
            render={({ field: { onChange }, fieldState: { error } }) => {
              return (
                <Recaptcha
                  onChange={onChange}
                  errors={error as unknown as FieldError}
                />
              );
            }}
          />
          <ButtonPrimary
            className="mt-4 justify-self-start"
            type="submit"
            loading={loading}
          >
            Sign up
          </ButtonPrimary>
        </FormWrapper>
        <SocialSignIn />
      </div>
    </div>
  );
};

SignUp.getLayoutProps = () => ({ requireAuthentication: false });

export default SignUp;
