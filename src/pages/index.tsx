import Image from "next/image";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import ButtonSecondaryLink from "@/components/buttons/ButtonSecondaryLink";
import Head from "@/components/Head";
import { useAuthenticatedUserLazyQuery } from "@/graphql/auth/queries/authenticated-user.generated";
import { useAuth } from "@/hooks/auth";
import landingPageScreenshot from "@/images/landing-page-screenshot.png";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import LandingPageBackground from "@/svgs/landing-page-background.svg";
import LogoIndigo from "@/svgs/logo-indigo.svg";
import { PageFunctionComponent } from "@/types/react";

const LandingPage: PageFunctionComponent = () => {
  const [loadingAuthenticatedUser, setLoadingAuthenticatedUser] =
    useState(true);
  const dispatch = useDispatch();
  const { isAuthenticated, csrfCookieSet } = useAuth();
  const [getAuthenticatedUser] = useAuthenticatedUserLazyQuery({
    fetchPolicy: "no-cache",
    onCompleted: ({ authenticatedUser }) => {
      dispatch(updateAuthenticatedUser(authenticatedUser));
      setLoadingAuthenticatedUser(false);
    },
    onError: () => {
      setLoadingAuthenticatedUser(false);
    },
  });

  useEffect(() => {
    if (!csrfCookieSet) {
      return;
    }

    if (!isAuthenticated) {
      getAuthenticatedUser();

      return;
    }

    setLoadingAuthenticatedUser(false);
  }, [csrfCookieSet]);

  const customImageLoader = ({ src }) => {
    return src;
  };

  return (
    <div className="flex-1 bg-white">
      <Head />
      {loadingAuthenticatedUser ? (
        <div className="h-[4.375rem]" />
      ) : (
        <div className="flex items-center justify-end flex-1 px-4 py-4 space-x-4">
          {isAuthenticated ? (
            <ButtonPrimaryLink href="/parts/forks">Dashboard</ButtonPrimaryLink>
          ) : (
            <>
              <div>
                <ButtonPrimaryLink href="/sign-in">Sign in</ButtonPrimaryLink>
              </div>
              <div>
                <ButtonSecondaryLink href="/sign-up">
                  Sign up
                </ButtonSecondaryLink>
              </div>
            </>
          )}
        </div>
      )}
      <div className="pt-8 overflow-hidden sm:pt-12 lg:relative lg:py-48">
        <div className="max-w-md px-4 mx-auto sm:max-w-3xl sm:px-6 lg:px-8 lg:max-w-7xl lg:grid lg:grid-cols-2 lg:gap-24">
          <div>
            <div>
              <LogoIndigo className="h-auto w-52" />
            </div>
            <div className="mt-10">
              <div className="mt-6 sm:max-w-xl">
                <h1 className="text-4xl font-extrabold tracking-tight text-gray-900 sm:text-5xl">
                  Track your mountain bike suspension settings
                </h1>
                <p className="mt-6 text-xl text-gray-500">
                  Plush.bike helps you remember the settings you liked and the
                  settings you didn&apos;t.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="sm:mx-auto sm:max-w-3xl sm:px-6">
          <div className="py-12 sm:relative sm:mt-12 sm:py-16 lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
            <div className="hidden sm:block">
              <div className="absolute inset-y-0 w-screen left-1/2 bg-gray-50 rounded-l-3xl lg:left-80 lg:right-0 lg:w-full" />
              <LandingPageBackground className="absolute -mr-3 text-gray-200 top-8 right-1/2 lg:m-0 lg:left-0" />
            </div>
            <div className="relative pl-4 -mr-40 sm:mx-auto sm:max-w-3xl sm:px-0 lg:max-w-none lg:h-full lg:pl-12">
              <Image
                className="w-full rounded-md shadow-xl ring-1 ring-black ring-opacity-5 lg:h-full lg:w-auto lg:max-w-none"
                src={landingPageScreenshot}
                alt="plush.bike screenshot"
                loader={customImageLoader}
                unoptimized
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

LandingPage.getLayoutProps = () => ({
  requireAuthentication: false,
  showNavigation: false,
  mainShouldHavePadding: false,
});

export default LandingPage;
