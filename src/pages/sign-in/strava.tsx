import SocialSignInCallback from "@/components/auth/SocialSignInCallback";
import { SocialProviderType } from "@/types/graphql";
import { PageFunctionComponent } from "@/types/react";

const SignInStrava: PageFunctionComponent = () => {
  return (
    <SocialSignInCallback
      providerType={SocialProviderType.Strava}
      providerTitle="Strava"
    />
  );
};

SignInStrava.getLayoutProps = () => ({ requireAuthentication: false });

export default SignInStrava;
