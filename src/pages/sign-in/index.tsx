import { useState } from "react";

import SignInForm from "@/components/auth/SignInForm";
import TwoFactorAuthenticationChallengeForm from "@/components/auth/TwoFactorAuthenticationChallengeForm";
import Head from "@/components/Head";
import { PageFunctionComponent } from "@/types/react";

const SignInIndex: PageFunctionComponent = () => {
  const [
    showTwoFactorAuthenticationChallenge,
    setShowTwoFactorAuthenticationChallenge,
  ] = useState(false);

  const handleOnTwoFactor = () => {
    setShowTwoFactorAuthenticationChallenge(true);
  };

  return (
    <>
      <Head pageTitle="Sign in" />
      {showTwoFactorAuthenticationChallenge ? (
        <TwoFactorAuthenticationChallengeForm />
      ) : (
        <SignInForm onTwoFactor={handleOnTwoFactor} />
      )}
    </>
  );
};

SignInIndex.getLayoutProps = () => ({ requireAuthentication: false });

export default SignInIndex;
