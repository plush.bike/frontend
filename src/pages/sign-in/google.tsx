import SocialSignInCallback from "@/components/auth/SocialSignInCallback";
import { SocialProviderType } from "@/types/graphql";
import { PageFunctionComponent } from "@/types/react";

const SignInGoogle: PageFunctionComponent = () => {
  return (
    <SocialSignInCallback
      providerType={SocialProviderType.Google}
      providerTitle="Google"
    />
  );
};

SignInGoogle.getLayoutProps = () => ({ requireAuthentication: false });

export default SignInGoogle;
