import { NextPage } from "next";
import { AppProps } from "next/app";

import BaseLayout from "@/components/layouts/BaseLayout";
import { useApollo } from "@/graphql/apolloClient";
import store from "@/store";
import "../styles/index.css";
import { GetLayoutProps } from "@/types/react";

type NextPageWithLayout = NextPage<{
  initialApolloState: Record<string, unknown>;
}> &
  GetLayoutProps;

type AppPropsWithLayout = AppProps<{
  initialApolloState: Record<string, unknown>;
}> & {
  Component: NextPageWithLayout;
};

const App = ({ Component, pageProps }: AppPropsWithLayout) => {
  const apolloClient = useApollo(pageProps.initialApolloState || null);

  const getLayoutProps = Component.getLayoutProps ?? (() => ({}));

  return (
    <BaseLayout
      apolloClient={apolloClient}
      reduxStore={store}
      {...getLayoutProps()}
    >
      <Component {...pageProps} />
    </BaseLayout>
  );
};

export default App;
