import { createSlice } from "@reduxjs/toolkit";

// eslint-disable-next-line import/exports-last
export const initialState: {
  success: {
    show: boolean;
    message: string;
  };
  error: {
    show: boolean;
    message: string;
  };
} = {
  success: {
    show: false,
    message: "",
  },
  error: {
    show: false,
    message: "",
  },
};

const toastsSlice = createSlice({
  name: "toasts",
  initialState,
  reducers: {
    updateToastSuccess(state, action) {
      return {
        ...state,
        success: {
          ...state.success,
          ...action.payload,
        },
      };
    },
    updateToastError(state, action) {
      return {
        ...state,
        error: {
          ...state.error,
          ...action.payload,
        },
      };
    },
  },
});

export const { updateToastSuccess, updateToastError } = toastsSlice.actions;

export default toastsSlice.reducer;
