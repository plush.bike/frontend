import { createSlice } from "@reduxjs/toolkit";

import { User } from "@/types/auth";

// eslint-disable-next-line import/exports-last
export const initialState: {
  authenticatedUser: User | null;
  csrfCookieSet: boolean;
} = {
  authenticatedUser: null,
  csrfCookieSet: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    updateAuthenticatedUser(state, action) {
      return {
        ...state,
        authenticatedUser: action.payload,
      };
    },
    updateCsrfCookieSet(state, action) {
      return {
        ...state,
        csrfCookieSet: action.payload,
      };
    },
  },
});

export const { updateAuthenticatedUser, updateCsrfCookieSet } =
  authSlice.actions;

export default authSlice.reducer;
