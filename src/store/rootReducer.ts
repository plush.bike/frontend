import { combineReducers } from "@reduxjs/toolkit";

import authReducer from "@/store/slices/auth";
import toastsReducer from "@/store/slices/toasts";

const rootReducer = combineReducers({
  auth: authReducer,
  toasts: toastsReducer,
});

export default rootReducer;
