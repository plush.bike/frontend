import { configureStore } from "@reduxjs/toolkit";

import rootReducer from "@/store/rootReducer";

const store = configureStore({
  reducer: rootReducer,
  devTools: true,
});

export type AppDispatch = typeof store.dispatch;

export default configureStore({
  reducer: rootReducer,
  devTools: true,
});
