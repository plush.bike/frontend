const path = require("path");

const withPWA = require("@ducanh2912/next-pwa").default({
  dest: "public",
});

module.exports = withPWA({
  images: {
    loader: "custom",
  },
  trailingSlash: true,
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.svg$/,
      include: path.resolve(__dirname, "./src/svgs"),
      exclude: path.resolve(__dirname, "./src/icons"),
      use: [
        {
          loader: "@svgr/webpack",
          options: {
            dimensions: false,
          },
        },
      ],
    });

    config.module.rules.push({
      test: /\.svg$/,
      include: path.resolve(__dirname, "./src/icons"),
      loader: "svg-sprite-loader",
      options: {
        symbolId: "icon-[name]",
        symbolModule: path.resolve(__dirname, "./src/icons/custom-symbol"),
      },
    });

    return config;
  },
});
