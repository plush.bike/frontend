import { ApolloProvider, GraphQLRequest, InMemoryCache } from "@apollo/client";
import { configureStore } from "@reduxjs/toolkit";
import {
  act,
  render,
  queries,
  screen,
  RenderOptions,
  RenderResult,
  BoundFunctions,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { ASTNode, DocumentNode, GraphQLError, Source } from "graphql";
import { Maybe } from "graphql/jsutils/Maybe";
import { isBoolean } from "lodash";
import { createMockClient } from "mock-apollo-client";
import { PropsWithChildren, ReactElement } from "react";
import { Provider as ReduxProvider } from "react-redux";

import * as customQueries from "./custom-queries";
import * as customUserEvents from "./custom-user-events";
import type CustomUserEventTypes from "./custom-user-events";

import rootReducer from "@/store/rootReducer";
import { user as mockUser } from "tests/unit/mock-data/user";

interface CustomRenderOptions extends Omit<RenderOptions, "queries"> {
  store?: {
    initialState?: Record<string, unknown>;
  };
  apollo?: {
    mocks?: {
      request: GraphQLRequest;
      result?: any;
      loading?: boolean;
      error?: Error;
    }[];
    initialCacheQueries?: {
      query: DocumentNode;
      data: any;
      variables?: any;
    }[];
  };
  authenticated?: boolean;
}

const customRender = (
  ui: ReactElement,
  options: CustomRenderOptions = {}
): RenderResult & {
  user: ReturnType<typeof userEvent.setup> & CustomUserEventTypes;
  expectApolloHandlersCalledWithCorrectVariables: () => void;
  apolloCache: InMemoryCache;
} => {
  const {
    store = {},
    apollo: { mocks = [], initialCacheQueries = [] } = {},
    authenticated = false,
  } = options;

  const initialState = authenticated
    ? {
        auth: {
          authenticatedUser: mockUser,
        },
        ...store.initialState,
      }
    : store.initialState;

  const cache = new InMemoryCache();

  for (let index = 0; index < initialCacheQueries.length; index++) {
    const initialCacheQuery = initialCacheQueries[index];

    cache.writeQuery(initialCacheQuery);
  }

  const mockClient = createMockClient({ cache });

  const mocksWithHandlers = mocks.reduce(
    (accumulator, { request, result, loading, error }) => {
      const alreadyDefinedMock = accumulator.find(
        (mock) => mock.request.query === request.query
      );

      const mockWithHandler = {
        request,
        handler: alreadyDefinedMock?.handler || vi.fn(),
        shouldCallSetRequestHandler: !alreadyDefinedMock,
      };

      if (loading) {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        mockWithHandler.handler.mockResolvedValueOnce(new Promise(() => {}));
      } else if (error) {
        mockWithHandler.handler.mockRejectedValueOnce(error);
      } else {
        mockWithHandler.handler.mockResolvedValueOnce(result);
      }

      return [...accumulator, mockWithHandler];
    },
    []
  );

  for (let index = 0; index < mocksWithHandlers.length; index++) {
    const mock = mocksWithHandlers[index];

    if (mock.shouldCallSetRequestHandler) {
      mockClient.setRequestHandler(mock.request.query, mock.handler);
    }
  }

  const baseUser = userEvent.setup();
  const customUser = Object.entries(customUserEvents).reduce(
    (accumulator, [key, customUserEvent]) => {
      return {
        ...accumulator,
        [key]: customUserEvent.bind(null, baseUser),
      };
    },
    {}
  ) as CustomUserEventTypes;

  const user = {
    ...baseUser,
    ...customUser,
  };

  const renderResult = render(
    <ReduxProvider
      store={configureStore({
        reducer: rootReducer,
        preloadedState: initialState,
      })}
    >
      <ApolloProvider client={mockClient}>{ui}</ApolloProvider>
    </ReduxProvider>,
    { queries: { ...queries, ...customQueries }, ...options }
  );

  const expectApolloHandlersCalledWithCorrectVariables = () => {
    for (let index = 0; index < mocksWithHandlers.length; index++) {
      const mock = mocksWithHandlers[index];

      if (mock.request.variables) {
        expect(mock.handler).toHaveBeenCalledWith(mock.request.variables);
      }
    }
  };

  return {
    ...renderResult,
    user,
    expectApolloHandlersCalledWithCorrectVariables,
    apolloCache: cache,
  };
};

const boundQueries = Object.entries(customQueries).reduce(
  (queries, [queryName, queryFn]) => {
    queries[queryName] = queryFn.bind(null, document.body);
    return queries;
  },
  {} as BoundFunctions<typeof customQueries>
);

const customScreen = { ...screen, ...boundQueries };

// re-export everything
// eslint-disable-next-line import/exports-last, import/export
export * from "@testing-library/react";

// override render method
// eslint-disable-next-line import/exports-last, import/export
export { customRender as render };

// override screen method
// eslint-disable-next-line import/exports-last, import/export
export { customScreen as screen };

export const renderAndSubmitForm = async (
  FormComponent: ReactElement,
  options: CustomRenderOptions & {
    submitLabel?: string;
    fields?: { [label: string]: string | boolean };
  } = {}
) => {
  const { submitLabel = "Submit", fields = {}, ...renderOptions } = options;
  const renderResult = customRender(FormComponent, renderOptions);
  const { getByLabelText, getByText, user } = renderResult;

  for (const fieldLabel in fields) {
    const fieldValue = fields[fieldLabel];
    const field = getByLabelText(fieldLabel);

    if (isBoolean(fieldValue)) {
      await user.click(field);

      continue;
    }

    if (fieldValue !== "") {
      await user.type(field, fieldValue);
    }
  }

  await user.click(
    getByText(submitLabel, { selector: 'button[type="submit"]' })
  );

  return renderResult;
};

export const waitForPromises = async (ms = 0) => {
  await act(() => {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  });
};

export const mockNetworkError = ({
  request,
  message = "An error occurred",
}: {
  request: GraphQLRequest;
  message?: string;
}) => ({
  request,
  error: new Error(message),
});

export const mockGraphQLError = ({
  request,
  message,
  nodes,
  source,
  positions,
  path,
  originalError,
  extensions,
}: {
  request: GraphQLRequest;
  message: string;
  nodes?: Maybe<ReadonlyArray<ASTNode> | ASTNode>;
  source?: Maybe<Source>;
  positions?: Maybe<ReadonlyArray<number>>;
  path?: Maybe<ReadonlyArray<string | number>>;
  originalError?: Maybe<Error>;
  extensions?: Maybe<{ [key: string]: any }>;
}) => ({
  request,
  result: {
    errors: [
      new GraphQLError(
        message,
        nodes,
        source,
        positions,
        path,
        originalError,
        extensions
      ),
    ],
  },
});

export const mockWindowLocation = () => {
  delete window.location;
  const location = {
    ...new URL("https://plush.bike"),
    assign: vi.fn(),
    replace: vi.fn(),
    reload: vi.fn(),
    ancestorOrigins: null,
  };

  window.location = location;
};

export const mockComponent = ({
  componentObject,
  componentKey = "default",
  render = () => null,
}: {
  componentObject: {
    [key: string]: any;
  };
  componentKey?: string;
  render?: (props: PropsWithChildren<Record<string, any>>) => JSX.Element;
}) => {
  let props: PropsWithChildren<Record<string, any>> = {};
  const spy = vi
    .spyOn(componentObject, componentKey)
    .mockImplementation((componentProps) => {
      props = componentProps;

      return render(props);
    });

  return { spy, getProps: () => props };
};

export const mockFile = ({
  name = "mock.jpg",
  size = 1024,
  type = "image/jpg",
} = {}) => {
  const blob = new Blob(["a".repeat(size)], { type });

  return new File([blob], name);
};

export const mockTimezone = (timeZone = "America/Los_Angeles") => {
  const dateTimeResolvedOptions = new Intl.DateTimeFormat().resolvedOptions();

  vi.spyOn(Intl.DateTimeFormat.prototype, "resolvedOptions").mockReturnValue({
    ...dateTimeResolvedOptions,
    timeZone,
  });
};
