import {
  buildQueries,
  getAllByRole,
  getNodeText,
  queryAllByText,
} from "@testing-library/dom";
import { makeNormalizer } from "@testing-library/dom/dist/matches";

const queryAllByDescriptionTerm = (container: HTMLElement, name: string) => {
  const terms = getAllByRole(container, "term").filter(
    (term) => getNodeText(term) === name
  );

  const definitions = terms.flatMap((term) =>
    term.nextElementSibling.tagName === "DD" ? [term.nextElementSibling] : []
  );

  return definitions as HTMLElement[];
};

const [
  queryByDescriptionTerm,
  getAllByDescriptionTerm,
  getByDescriptionTerm,
  findAllByDescriptionTerm,
  findByDescriptionTerm,
] = buildQueries(
  queryAllByDescriptionTerm,
  (c, name) => `Found multiple descriptions from term with name of ${name}`,
  (c, name) => `Unable to find a description from term with name of ${name}`
);

const matchNormalizer = makeNormalizer({});
const nodeHasText = (node: HTMLElement, text: string) =>
  matchNormalizer(node.textContent) === text;

const queryAllByTextWithMarkup = (container: HTMLElement, text: string) => {
  return queryAllByText(container, (_, node: HTMLElement) => {
    if (!nodeHasText(node, text)) {
      return false;
    }

    const childrenDontHaveText = Array.from(node.children).every(
      (child) => !nodeHasText(child as HTMLElement, text)
    );

    return childrenDontHaveText;
  });
};

const [
  queryByTextWithMarkup,
  getAllByTextWithMarkup,
  getByTextWithMarkup,
  findAllByTextWithMarkup,
  findByTextWithMarkup,
] = buildQueries(
  queryAllByTextWithMarkup,
  (c, text) => `Found multiple elements with text of ${text}`,
  (c, text) => `Unable to find element with text of ${text}`
);

export {
  queryAllByDescriptionTerm,
  queryByDescriptionTerm,
  getAllByDescriptionTerm,
  getByDescriptionTerm,
  findAllByDescriptionTerm,
  findByDescriptionTerm,
  queryAllByTextWithMarkup,
  queryByTextWithMarkup,
  getAllByTextWithMarkup,
  getByTextWithMarkup,
  findAllByTextWithMarkup,
  findByTextWithMarkup,
};
