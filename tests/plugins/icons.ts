import path from "path";

import type { Plugin } from "vite";

export default function icons(): Plugin {
  return {
    name: "icons",

    transform(src, id) {
      if (/^.+\/src\/icons\/.+\.svg$/.test(id)) {
        const result = {
          id: path.basename(id).replace(".svg", ""),
          viewbox: "0 0 24 24",
        };

        return {
          code: `module.exports = ${JSON.stringify(result)};`,
        };
      }
    },
  };
}
