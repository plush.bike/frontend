import type { Plugin } from "vite";
import { transformWithEsbuild } from "vite";

export default function svgr(): Plugin {
  return {
    name: "svgr",
    async transform(code, id) {
      if (/^.+\/src\/svgs\/.+\.svg$/.test(id)) {
        const res = await transformWithEsbuild(
          `
            import React, { forwardRef } from "react";

            const SvgrMock = forwardRef((props, ref) => (
              <span ref={ref} {...props} />
            ));

            export const ReactComponent = SvgrMock;
            export default SvgrMock;
          `,
          id,
          {
            loader: "jsx",
          }
        );

        return {
          code: res.code,
        };
      }
    },
  };
}
