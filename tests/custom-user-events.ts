/* eslint-disable import/no-unresolved */
import { within } from "@testing-library/dom";
import {
  UserEvent,
  UserEventApi,
} from "@testing-library/user-event/dist/types/setup/setup";

type CustomUserEvents = {
  clearAndType: (
    ...args: Parameters<UserEventApi["type"]>
  ) => ReturnType<UserEventApi["type"]>;
  selectOptionFromSelect: (
    selectButton: Element,
    optionName: string
  ) => Promise<void>;
};

export async function clearAndType(
  user: UserEvent,
  ...args: Parameters<CustomUserEvents["clearAndType"]>
): Promise<void> {
  const [element] = args;

  await user.clear(element);
  await user.type(...args);
}

export async function selectOptionFromSelect(
  user: UserEvent,
  ...args: Parameters<CustomUserEvents["selectOptionFromSelect"]>
): Promise<void> {
  const [selectButton, optionName] = args;

  vi.useFakeTimers({ shouldAdvanceTime: true });

  await user.click(selectButton);
  await user.click(
    within(selectButton.parentElement).getByRole("option", { name: optionName })
  );

  vi.runAllTimers();
  vi.useRealTimers();
}

export default CustomUserEvents;
