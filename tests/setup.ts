import "@testing-library/jest-dom";
import { waitForPromises } from "./utils";

vi.mock("imagekit-javascript", async () =>
  vi.importActual("./mocks/imagekit-javascript")
);

Object.defineProperty(process, "browser", { value: true });

global.ResizeObserver = require("resize-observer-polyfill");

process.env.NEXT_PUBLIC_API_URL = "http://api.plush.test";
process.env.NEXT_PUBLIC_IMAGEKIT_ENDPOINT = "https://ik.imagekit.io";
// https://github.com/tailwindlabs/headlessui/blob/master/packages/%40headlessui-react/src/hooks/use-tracked-pointer.ts#L17
process.env.TEST_BYPASS_TRACKED_POINTER = "true";

afterEach(async () => {
  await waitForPromises();

  if (vi.isMockFunction(setTimeout)) {
    vi.runOnlyPendingTimers();
    vi.useRealTimers();
  }
});
