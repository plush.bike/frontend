export const settingsRecord1 = {
  __typename: "PartSettingsRecord" as const,
  id: "1",
  nickname: "Firm setup",
  lscClicks: 10,
  hscClicks: 9,
  lsrClicks: 6,
  hsrClicks: 7,
  volumeSpacers: 3,
  psi: 85,
  springRate: null,
  preloadAdjust: null,
  sag: 20,
  notes: "Works well in dry dusty conditions",
};

export const settingsRecord2 = {
  __typename: "PartSettingsRecord" as const,
  id: "2",
  nickname: "Soft setup",
  lscClicks: 5,
  hscClicks: 6,
  lsrClicks: 6,
  hsrClicks: 5,
  volumeSpacers: 1,
  psi: 80,
  springRate: null,
  preloadAdjust: null,
  sag: 20,
  notes: "Works well in wet conditions",
};

export const settingsRecord3 = {
  __typename: "PartSettingsRecord" as const,
  id: "3",
  nickname: "Bad setup",
  lscClicks: 2,
  hscClicks: 2,
  lsrClicks: 2,
  hsrClicks: 2,
  volumeSpacers: 0,
  psi: 60,
  springRate: null,
  preloadAdjust: null,
  sag: 25,
  notes: "Setup didn't work",
};

export const settingsRecords = [settingsRecord1, settingsRecord2];
