export const image1 = {
  src: "parts/1/tP9rncM5DmCq4ybR3i9UHpiL9lmMpQKKnYzERJ6a.png",
  __typename: "UploadPartImagesResponse",
};

export const image2 = {
  src: "parts/1/V2UAGLjCDN7Dp0x8ZeaYrPnBBy5Kl4MErMsOIPGl.png",
  __typename: "UploadPartImagesResponse",
};

export const image3 = {
  src: "parts/1/FDSFGSDF5345FDS5354Gdsg543544354FDS543FS.png",
  __typename: "UploadPartImagesResponse",
};

export const images = [image1, image2];
