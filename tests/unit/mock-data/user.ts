export const user = {
  id: "1",
  name: "Peter Hegman",
  email: "peter@plush.bike",
  profileImageUrl: "https://ik.imagekit.io/profile_images/1/foo-bar.png",
  createdAt: "2020-10-27 19:22:53",
  updatedAt: "2020-10-27 19:22:53",
  hasEnabledTwoFactorAuthentication: false,
  hasPasswordSet: true,
  __typename: "User",
};
