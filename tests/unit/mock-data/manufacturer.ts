export const fox = {
  id: "1",
  name: "Fox",
  __typename: "Manufacturer",
};

export const rockShox = {
  id: "2",
  name: "RockShox",
  __typename: "Manufacturer",
};

export const manufacturers = [fox, rockShox];
