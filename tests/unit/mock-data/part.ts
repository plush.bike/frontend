import { fox, rockShox } from "./manufacturer";
import { images } from "./part-image";
import { settingsRecords, settingsRecord1 } from "./part-settings-record";
import { specificationsRecord } from "./part-specifications-record";

import { PartType } from "@/types/graphql";

export const fox38 = {
  __typename: "Part",
  id: "1",
  name: "38",
  type: PartType.Fork,
  manufacturer: fox,
  settingsRecords,
  activeSettingsRecord: settingsRecord1,
  images,
  specificationsRecord,
};

export const rockShoxZeb = {
  __typename: "Part",
  id: "2",
  name: "Zeb",
  type: PartType.Fork,
  manufacturer: rockShox,
  settingsRecords: [settingsRecord1],
  activeSettingsRecord: settingsRecord1,
  images,
  specificationsRecord,
};

export const fox36NoImages = {
  __typename: "Part",
  id: "3",
  name: "36",
  type: PartType.Fork,
  manufacturer: fox,
  settingsRecords: [settingsRecord1],
  activeSettingsRecord: settingsRecord1,
  images: [],
  specificationsRecord,
};

export const forks = [fox38, rockShoxZeb, fox36NoImages];
