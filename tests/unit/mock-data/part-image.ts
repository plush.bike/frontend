export const image1 = {
  __typename: "PartImage",
  id: "1",
  url: "https://ik.imagekit.io/urlbngry1myr/parts/1/7ppaRKSahJGOpHhP7pZ1WLTnLMyOtxm9a8FoRbUi.png",
};

export const image2 = {
  __typename: "PartImage",
  id: "2",
  url: "https://ik.imagekit.io/urlbngry1myr/parts/1/4ay1CNJjoiXe4tozbLf1dKDZpfM3BrV1a8ZL0Cn2.png",
};

export const image3 = {
  __typename: "PartImage",
  id: "3",
  url: "https://ik.imagekit.io/urlbngry1myr/parts/1/fsadfsd4325Fds532fdsFD5643Fdsag43545fdas.png",
};

export const images = [image1, image2];
