export const personalAccessToken = {
  id: "1",
  name: "Foo bar",
  token: "1|GPDuzt9ToI4SJQgk1VBpOjss54FX8hbHztysvbUi",
  lastUsedAt: "2022-09-25 19:38:25",
  createdAt: "2022-09-25 19:38:25",
  updatedAt: "2022-09-25 19:38:25",
  __typename: "CreatePersonalAccessTokenResponse",
};

export const personalAccessToken2 = {
  id: "2",
  name: "Baz",
  token: "2|NunkIpMftqz8sKyiSo85FBHS6BUuhi5RJzKWUgBq",
  lastUsedAt: null,
  createdAt: "2022-09-25 20:19:33",
  updatedAt: "2022-09-25 20:19:33",
  __typename: "CreatePersonalAccessTokenResponse",
};

export const personalAccessTokens = [personalAccessToken, personalAccessToken2];
