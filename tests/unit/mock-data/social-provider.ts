export const strava = {
  id: "1",
  provider: "STRAVA",
  __typename: "SocialProvider",
};

export const google = {
  id: "2",
  provider: "GOOGLE",
  __typename: "SocialProvider",
};

export const socialProviders = [strava, google];
