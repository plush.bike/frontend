import { AirOrCoilType } from "@/types/graphql";

export const specificationsRecord = {
  id: "1",
  lscMaxClicks: 10,
  hscMaxClicks: 10,
  lsrMaxClicks: 10,
  hsrMaxClicks: 10,
  travel: 170,
  airOrCoil: AirOrCoilType.Air,
  __typename: "PartSpecificationsRecord",
};
