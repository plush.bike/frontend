import { AnyAction } from "@reduxjs/toolkit";

import reducer, {
  initialState,
  updateToastSuccess,
  updateToastError,
} from "@/store/slices/toasts";

describe("toasts slice", () => {
  it("returns initial state on first run", () => {
    expect(reducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  describe("updateToastSuccess", () => {
    it("updates `success` state", () => {
      expect(
        reducer(
          initialState,
          updateToastSuccess({
            show: true,
            message: "Profile successfully updated.",
          })
        ).success
      ).toEqual({
        show: true,
        message: "Profile successfully updated.",
      });
    });
  });

  describe("updateToastError", () => {
    it("updates `error` state", () => {
      expect(
        reducer(
          initialState,
          updateToastError({
            show: true,
            message: "An error occurred, please try again.",
          })
        ).error
      ).toEqual({
        show: true,
        message: "An error occurred, please try again.",
      });
    });
  });
});
