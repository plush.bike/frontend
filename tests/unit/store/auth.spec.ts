import { AnyAction } from "@reduxjs/toolkit";

import reducer, {
  initialState,
  updateAuthenticatedUser,
} from "@/store/slices/auth";
import { user as mockUser } from "tests/unit/mock-data/user";

describe("auth slice", () => {
  it("returns initial state on first run", () => {
    expect(reducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  describe("updateAuthenticatedUser", () => {
    it("updates authenticated user", () => {
      expect(
        reducer(initialState, updateAuthenticatedUser(mockUser))
          .authenticatedUser
      ).toEqual(mockUser);
    });
  });
});
