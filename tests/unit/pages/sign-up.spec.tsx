import mockRouter from "next-router-mock";

import * as SocialSignIn from "@/components/auth/SocialSignIn";
import { SignUpDocument } from "@/graphql/auth/mutations/sign-up.generated";
import SignUp from "@/pages/sign-up";
import { updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import {
  renderAndSubmitForm,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
  screen,
  mockComponent,
  render,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock("react-google-recaptcha", () => ({
  default: ({ onChange }: { onChange: (value: string) => void }) => {
    const handleChange = () => {
      onChange("foobar");
    };

    return (
      <div>
        <label htmlFor="recaptcha">reCAPTCHA</label>
        <input onChange={handleChange} id="recaptcha" type="checkbox" />
      </div>
    );
  },
}));

describe("/sign-up", () => {
  const requestVariables = {
    recaptchaResponse: "foobar",
    name: "Peter Hegman",
    email: "peter@plush.bike",
    password: "password",
    passwordConfirmation: "password",
  };

  const baseOptions = {
    submitLabel: "Sign up",
    fields: {
      reCAPTCHA: true,
      Name: requestVariables.name,
      Email: requestVariables.email,
      Password: requestVariables.password,
      "Confirm password": requestVariables.passwordConfirmation,
    },
  };

  const request = {
    query: SignUpDocument,
    variables: requestVariables,
  };

  describe("validation", () => {
    describe.each`
      field                 | value    | expectedError
      ${"Name"}             | ${""}    | ${"Please enter your name."}
      ${"Email"}            | ${""}    | ${"Please enter your email."}
      ${"Email"}            | ${"foo"} | ${"Please enter a valid email."}
      ${"Password"}         | ${""}    | ${"Please enter a password."}
      ${"Confirm password"} | ${""}    | ${"Please confirm your password."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<SignUp />, {
          submitLabel: "Sign up",
          fields: {
            [field]: value,
          },
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });

    describe("when passwords do not match", () => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<SignUp />, {
          submitLabel: "Sign up",
          fields: {
            Password: "password",
            "Confirm password": "password foo",
          },
        });

        expect(
          await screen.findByLabelText("Password")
        ).toHaveAccessibleErrorMessage("Your passwords do not match.");
      });
    });

    describe("when reCAPTCHA is not completed", () => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<SignUp />, {
          submitLabel: "Sign up",
        });

        expect(
          await screen.findByText("Please complete the reCAPTCHA.")
        ).toBeInTheDocument();
      });
    });

    it("renders loading icon when form is submitted", async () => {
      await renderAndSubmitForm(<SignUp />, {
        ...baseOptions,
        apollo: { mocks: [{ request, loading: true }] },
      });

      expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
    });

    describe("when API call is not successful", () => {
      const optionsNetworkError = {
        ...baseOptions,
        apollo: {
          mocks: [mockNetworkError({ request })],
        },
      };

      const optionsGraphQLError = {
        ...baseOptions,
        apollo: {
          mocks: [
            mockGraphQLError({
              request,
              message: "Validation failed for the field [register].",
              extensions: {
                validation: {
                  "input.email": ["Email has already been taken."],
                },
                category: "validation",
              },
              path: ["register"],
            }),
          ],
        },
      };

      describe("when there are no `graphQLErrors`", () => {
        it("displays error alert", async () => {
          await renderAndSubmitForm(<SignUp />, optionsNetworkError);

          const alert = await screen.findByText(
            "An error occurred, please try again."
          );

          expect(alert).toBeInTheDocument();
        });

        it("closes error alert when close button is clicked", async () => {
          const { user } = await renderAndSubmitForm(
            <SignUp />,
            optionsNetworkError
          );

          const alert = await screen.findByText(
            "An error occurred, please try again."
          );

          await user.click(screen.getByLabelText("Close alert"));
          expect(alert).not.toBeInTheDocument();
        });
      });

      describe("when email has already been taken", () => {
        it("displays error message", async () => {
          await renderAndSubmitForm(<SignUp />, optionsGraphQLError);

          await waitForPromises();

          expect(screen.getByLabelText("Email")).toHaveAccessibleErrorMessage(
            "Email has already been taken."
          );
        });
      });
    });
  });

  describe("when API call is successful", () => {
    const options = {
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                signUp: {
                  status: "SUCCESS",
                },
              },
            },
          },
        ],
      },
    };

    it("dispatches `updateToastSuccess` action", async () => {
      mockRouter.push("/sign-up");

      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<SignUp />, options);

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastSuccess({
          show: true,
          message: "Sign up successful, please sign in.",
        })
      );
      expectApolloHandlersCalledWithCorrectVariables();
    });

    it("redirects to `/sign-in`", async () => {
      mockRouter.push("/sign-up");

      await renderAndSubmitForm(<SignUp />, options);

      await waitForPromises();

      expect(mockRouter.pathname).toBe("/sign-in");
    });
  });

  it("renders `SocialSignIn` component", () => {
    const { spy } = mockComponent({ componentObject: SocialSignIn });

    render(<SignUp />);

    expect(spy).toHaveBeenCalled();
  });
});
