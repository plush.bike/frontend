import mockRouter from "next-router-mock";

import { PartsByTypeDocument } from "@/graphql/part/queries/parts-by-type.generated";
import ForksIndex from "@/pages/parts/forks";
import { forks as forksMock } from "tests/unit/mock-data/part";
import { render, screen, waitForPromises } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock("@/constants/pagination", async () => {
  const actual = (await vi.importActual("@/constants/pagination")) as object;

  return {
    ...actual,
    DEFAULT_PER_PAGE: 1,
  };
});

describe("parts/forks - pagination", () => {
  const request = {
    query: PartsByTypeDocument,
    variables: { id: "1" },
  };

  const optionsSuccess = () => ({
    apollo: {
      mocks: [
        {
          request,
          result: {
            data: {
              partsByType: {
                data: forksMock,
                paginatorInfo: {
                  lastItem: 1,
                  total: 3,
                  __typename: "PaginatorInfo",
                },
                __typename: "PartPaginator",
              },
            },
          },
        },
      ],
    },
  });

  beforeEach(() => {
    mockRouter.push("/parts/forks");
  });

  it("displays pagination", async () => {
    const { user } = render(<ForksIndex />, optionsSuccess());

    await waitForPromises();

    expect(
      await screen.findByRole("heading", {
        name: `${forksMock[0].manufacturer.name} ${forksMock[0].name}`,
      })
    ).toBeInTheDocument();

    await user.click(await screen.findByRole("button", { name: "2" }));

    await waitForPromises();

    expect(mockRouter.query).toEqual({ page: 2 });
  });
});
