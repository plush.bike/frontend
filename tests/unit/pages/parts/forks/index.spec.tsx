import mockRouter from "next-router-mock";

import { PartsByTypeDocument } from "@/graphql/part/queries/parts-by-type.generated";
import ForksIndex from "@/pages/parts/forks";
import { forks as forksMock } from "tests/unit/mock-data/part";
import { mockNetworkError, render, screen, waitForPromises } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("parts/forks", () => {
  const request = {
    query: PartsByTypeDocument,
    variables: { id: "1" },
  };

  const optionsSuccess = ({
    forks = forksMock,
  }: {
    forks?: any[];
  } = {}) => ({
    apollo: {
      mocks: [
        {
          request,
          result: {
            data: {
              partsByType: {
                data: forks,
                paginatorInfo: {
                  lastItem: 3,
                  total: 3,
                  __typename: "PaginatorInfo",
                },
                __typename: "PartPaginator",
              },
            },
          },
        },
      ],
    },
  });

  beforeEach(() => {
    mockRouter.push("/parts/forks");
  });

  it("displays page title", async () => {
    render(<ForksIndex />, optionsSuccess());

    await waitForPromises();

    expect(screen.getByRole("heading", { name: "Forks" })).toBeInTheDocument();
  });

  it("displays `New fork` button", async () => {
    render(<ForksIndex />, optionsSuccess());

    await waitForPromises();

    expect(screen.getByRole("link", { name: "New fork" })).toHaveAttribute(
      "href",
      "/parts/forks/create"
    );
  });

  describe("when page is loading", () => {
    it("displays skeleton loader", () => {
      render(<ForksIndex />, {
        apollo: {
          mocks: [{ request, loading: true }],
        },
      });

      expect(screen.getAllByTestId("skeletonLoader").length).toBeGreaterThan(0);
    });
  });

  describe("when API call is successful", () => {
    describe("when there are forks", () => {
      it("displays forks", async () => {
        render(<ForksIndex />, optionsSuccess());

        await waitForPromises();

        expect(
          await screen.findByRole("heading", {
            name: `${forksMock[0].manufacturer.name} ${forksMock[0].name}`,
          })
        ).toBeInTheDocument();
        expect(
          await screen.findByRole("heading", {
            name: `${forksMock[1].manufacturer.name} ${forksMock[1].name}`,
          })
        ).toBeInTheDocument();
        expect(
          await screen.findByRole("heading", {
            name: `${forksMock[2].manufacturer.name} ${forksMock[2].name}`,
          })
        ).toBeInTheDocument();
      });
    });

    describe("when there are no forks", () => {
      it("displays empty state", async () => {
        render(<ForksIndex />, optionsSuccess({ forks: [] }));

        await waitForPromises();

        expect(
          screen.getByRole("heading", { name: "No forks" })
        ).toBeInTheDocument();
      });
    });
  });

  describe("when API call is not successful", () => {
    const optionsError = {
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    it("displays error alert", async () => {
      render(<ForksIndex />, optionsError);

      await waitForPromises();

      const alert = screen.getByText(
        "An error occurred loading your forks, please try again."
      );

      expect(alert).toBeInTheDocument();
    });
  });
});
