import { omit } from "lodash";
import mockRouter from "next-router-mock";

import { ManufacturersDocument } from "@/graphql/manufacturer/queries/manufacturers.generated";
import { CreatePartDocument } from "@/graphql/part/mutations/create-part.generated";
import { UpdatePartDocument } from "@/graphql/part/mutations/update-part.generated";
import { UploadPartImagesDocument } from "@/graphql/part/mutations/upload-part-images.generated";
import { PartsByTypeDocument } from "@/graphql/part/queries/parts-by-type.generated";
import ForksCreate from "@/pages/parts/forks/create";
import { updateToastSuccess } from "@/store/slices/toasts";
import { AirOrCoilType, PartType } from "@/types/graphql";
import { mockDispatch } from "tests/mocks/react-redux";
import { manufacturers } from "tests/unit/mock-data/manufacturer";
import { forks, fox38 } from "tests/unit/mock-data/part";
import { settingsRecord1 } from "tests/unit/mock-data/part-settings-record";
import { specificationsRecord } from "tests/unit/mock-data/part-specifications-record";
import { image1 } from "tests/unit/mock-data/upload-part-image";
import {
  fireEvent,
  mockFile,
  mockGraphQLError,
  mockNetworkError,
  render,
  screen,
  waitForPromises,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("parts/forks/create", () => {
  const manufacturersMock = {
    request: {
      query: ManufacturersDocument,
      variables: {
        first: 25,
      },
    },
    result: {
      data: {
        manufacturers: {
          data: manufacturers,
        },
      },
    },
  };

  beforeAll(() => {
    window.URL.revokeObjectURL = vi.fn();
    window.URL.createObjectURL = vi
      .fn()
      .mockImplementation(
        () => "blob:http://plush.test:3010/a3f96c77-4d20-469e-a737-4a82d3904b5b"
      );
  });

  beforeEach(() => {
    mockRouter.push("/parts/forks/create");
  });

  it("displays page title", async () => {
    render(<ForksCreate />, {
      apollo: {
        mocks: [manufacturersMock],
      },
    });

    await waitForPromises();

    expect(
      screen.getByRole("heading", { name: "Add a fork" })
    ).toBeInTheDocument();
  });

  describe("validation", () => {
    it("requires manufacturer and name fields", async () => {
      const { user } = render(<ForksCreate />, {
        apollo: {
          mocks: [manufacturersMock],
        },
      });

      await waitForPromises();

      await user.click(screen.getByRole("button", { name: "Submit" }));

      await waitForPromises();

      expect(
        await screen.findByRole("button", {
          name: "Manufacturer Select a manufacturer",
        })
      ).toHaveAccessibleErrorMessage("Please select a manufacturer.");
      expect(await screen.findByLabelText("Name")).toHaveAccessibleErrorMessage(
        "Please enter the name of your fork."
      );
    });
  });

  describe("when creating a fork and API call is successful", () => {
    it("redirects to forks index and clears Apollo cache", async () => {
      const mockImage = mockFile({ name: "mock1.jpg" });

      const {
        expectApolloHandlersCalledWithCorrectVariables,
        apolloCache,
        user,
      } = render(<ForksCreate />, {
        apollo: {
          mocks: [
            manufacturersMock,
            {
              request: {
                query: UploadPartImagesDocument,
                variables: {
                  files: [mockImage],
                },
              },
              result: {
                data: {
                  uploadPartImages: [image1],
                },
              },
            },
            {
              request: {
                query: CreatePartDocument,
                variables: {
                  name: fox38.name,
                  type: fox38.type,
                  manufacturer: { connect: fox38.manufacturer.id },
                  images: {
                    create: [
                      {
                        src: image1.src,
                      },
                    ],
                  },
                  specificationsRecord: {
                    create: {
                      lscMaxClicks: specificationsRecord.lscMaxClicks,
                      hscMaxClicks: specificationsRecord.hscMaxClicks,
                      lsrMaxClicks: specificationsRecord.lsrMaxClicks,
                      hsrMaxClicks: specificationsRecord.hsrMaxClicks,
                      travel: specificationsRecord.travel,
                      airOrCoil: AirOrCoilType.Air,
                    },
                  },
                  settingsRecords: {
                    create: [
                      omit(
                        settingsRecord1,
                        "id",
                        "__typename",
                        "springRate",
                        "preloadAdjust"
                      ),
                    ],
                  },
                },
              },
              result: {
                data: {
                  createPart: fox38,
                },
              },
            },
            {
              request: {
                query: UpdatePartDocument,
                variables: {
                  id: fox38.id,
                  activeSettingsRecord: {
                    connect: settingsRecord1.id,
                  },
                },
              },
              result: {
                data: {
                  updatePart: fox38,
                },
              },
            },
          ],
          initialCacheQueries: [
            {
              query: PartsByTypeDocument,
              data: {
                partsByType: {
                  data: forks,
                  paginatorInfo: {
                    lastItem: 25,
                    total: 25,
                  },
                },
              },
              variables: {
                type: PartType.Fork,
              },
            },
          ],
        },
      });

      await waitForPromises();

      await user.selectOptionFromSelect(
        screen.getByRole("button", {
          name: "Manufacturer Select a manufacturer",
        }),
        fox38.manufacturer.name
      );

      await user.type(screen.getByLabelText("Name"), fox38.name);
      await user.type(
        screen.getByLabelText("Low speed compression max clicks"),
        `{backspace}${specificationsRecord.lscMaxClicks}`
      );
      await user.type(
        screen.getByLabelText("High speed compression max clicks"),
        `{backspace}${specificationsRecord.hscMaxClicks}`
      );
      await user.type(
        screen.getByLabelText("Low speed rebound max clicks"),
        `{backspace}${specificationsRecord.lsrMaxClicks}`
      );
      await user.type(
        screen.getByLabelText("High speed rebound max clicks"),
        `{backspace}${specificationsRecord.hsrMaxClicks}`
      );
      await user.type(
        screen.getByLabelText("Travel (mm)"),
        `{backspace}{backspace}{backspace}${specificationsRecord.travel}`
      );

      // Upload image
      fireEvent.drop(screen.getByLabelText("Upload a file"), {
        dataTransfer: {
          files: mockImage,
          items: [
            {
              kind: "file",
              type: mockImage.type,
              getAsFile: () => mockImage,
            },
          ],
          types: ["Files"],
        },
      });
      await waitForPromises();

      // Fill in fork settings
      await user.type(
        screen.getByLabelText("Nickname"),
        settingsRecord1.nickname
      );
      await user.type(
        screen.getByLabelText("PSI"),
        `{backspace}${settingsRecord1.psi}`
      );
      await user.type(
        screen.getByLabelText("Sag (mm)"),
        `{backspace}${settingsRecord1.sag}`
      );
      await user.type(
        screen.getByLabelText("Volume spacers"),
        `{backspace}${settingsRecord1.volumeSpacers}}`
      );
      await user.type(
        screen.getByLabelText("Low speed compression clicks"),
        `{backspace}${settingsRecord1.lscClicks}`
      );
      await user.type(
        screen.getByLabelText("High speed compression clicks"),
        `{backspace}${settingsRecord1.hscClicks}`
      );
      await user.type(
        screen.getByLabelText("Low speed rebound clicks"),
        `{backspace}${settingsRecord1.lsrClicks}`
      );
      await user.type(
        screen.getByLabelText("High speed rebound clicks"),
        `{backspace}${settingsRecord1.hsrClicks}`
      );
      await user.type(screen.getByLabelText("Notes"), settingsRecord1.notes);

      // Submit the form
      await user.click(screen.getByRole("button", { name: "Submit" }));
      await waitForPromises();

      expect(mockRouter.pathname).toBe("/parts/forks");
      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastSuccess({
          show: true,
          message: "Fork successfully added.",
        })
      );
      expectApolloHandlersCalledWithCorrectVariables();
      const partsByTypeQuery = apolloCache.readQuery({
        query: PartsByTypeDocument,
        variables: {
          type: PartType.Fork,
        },
      });

      expect(partsByTypeQuery).toBeNull();
    });
  });

  describe("when creating a fork and API call is not successful", () => {
    const optionsNetworkError = {
      apollo: {
        mocks: [
          manufacturersMock,
          mockNetworkError({
            request: {
              query: CreatePartDocument,
            },
          }),
        ],
      },
    };

    const optionsGraphQLError = {
      apollo: {
        mocks: [
          manufacturersMock,
          mockGraphQLError({
            request: {
              query: CreatePartDocument,
            },
            message: "Validation failed for the field [name].",
            extensions: {
              validation: {
                "input.name": ["Name is invalid."],
              },
              category: "validation",
            },
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        const { user } = render(<ForksCreate />, optionsNetworkError);

        await waitForPromises();

        await user.selectOptionFromSelect(
          screen.getByRole("button", {
            name: "Manufacturer Select a manufacturer",
          }),
          fox38.manufacturer.name
        );

        await user.type(screen.getByLabelText("Name"), fox38.name);

        await user.click(screen.getByRole("button", { name: "Submit" }));
        await waitForPromises();

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when error is a GraphQL error", () => {
      it("displays error message", async () => {
        const { user } = render(<ForksCreate />, optionsGraphQLError);

        await waitForPromises();

        await user.selectOptionFromSelect(
          screen.getByRole("button", {
            name: "Manufacturer Select a manufacturer",
          }),
          fox38.manufacturer.name
        );

        await user.type(screen.getByLabelText("Name"), fox38.name);

        await user.click(screen.getByRole("button", { name: "Submit" }));
        await waitForPromises();

        expect(screen.getByLabelText("Name")).toHaveAccessibleErrorMessage(
          "Name is invalid."
        );
      });
    });
  });
});
