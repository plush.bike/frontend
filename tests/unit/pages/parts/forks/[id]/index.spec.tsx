import { toString } from "lodash";
import mockRouter from "next-router-mock";

import { PartByIdDocument } from "@/graphql/part/queries/part-by-id.generated";
import ForksShow from "@/pages/parts/forks/[id]";
import { fox38 } from "tests/unit/mock-data/part";
import {
  mockNetworkError,
  render,
  screen,
  waitForPromises,
  within,
} from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("parts/forks/[id]", () => {
  const request = {
    query: PartByIdDocument,
    variables: { id: "1" },
  };

  const expectSettingsRecordToBeInTheDocument = (
    index: number,
    expectedStatus: string
  ) => {
    expect(
      screen.getByRole("heading", { name: "Fork settings" })
    ).toBeInTheDocument();

    const row = screen.getAllByRole("row")[index + 1];

    expect(
      within(row).getByRole("cell", { name: `Status ${expectedStatus}` })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `Nickname ${fox38.settingsRecords[index].nickname}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `PSI ${fox38.settingsRecords[index].psi}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `Sag (mm) ${fox38.settingsRecords[index].sag}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `Volume spacers ${fox38.settingsRecords[index].volumeSpacers}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `LSC clicks ${fox38.settingsRecords[index].lscClicks}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `HSC clicks ${fox38.settingsRecords[index].hscClicks}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `LSR clicks ${fox38.settingsRecords[index].lsrClicks}`,
      })
    ).toBeInTheDocument();

    expect(
      within(row).getByRole("cell", {
        name: `HSR clicks ${fox38.settingsRecords[index].hsrClicks}`,
      })
    ).toBeInTheDocument();
  };

  beforeEach(() => {
    mockRouter.push("/parts/forks/1");
  });

  describe("when page is loading", () => {
    it("shows loading icon", () => {
      render(<ForksShow />, {
        apollo: {
          mocks: [{ request, loading: true }],
        },
      });

      expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const optionsSuccess = {
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                partById: fox38,
              },
            },
          },
        ],
      },
    };

    it("renders fork specifications", async () => {
      render(<ForksShow />, optionsSuccess);

      await waitForPromises();

      expect(
        screen.getByRole("heading", { name: "Fork specifications" })
      ).toBeInTheDocument();

      expect(
        screen.getByDescriptionTerm("Low speed compression max clicks")
      ).toHaveTextContent(toString(fox38.specificationsRecord.lscMaxClicks));
      expect(
        screen.getByDescriptionTerm("High speed compression max clicks")
      ).toHaveTextContent(toString(fox38.specificationsRecord.hscMaxClicks));
      expect(
        screen.getByDescriptionTerm("Low speed rebound max clicks")
      ).toHaveTextContent(toString(fox38.specificationsRecord.lsrMaxClicks));
      expect(
        screen.getByDescriptionTerm("High speed rebound max clicks")
      ).toHaveTextContent(toString(fox38.specificationsRecord.hsrMaxClicks));
      expect(screen.getByDescriptionTerm("Travel")).toHaveTextContent(
        toString(fox38.specificationsRecord.travel)
      );
    });

    it("renders fork settings", async () => {
      render(<ForksShow />, optionsSuccess);

      await waitForPromises();

      expectSettingsRecordToBeInTheDocument(0, "Active");
      expectSettingsRecordToBeInTheDocument(1, "Inactive");
    });

    it("renders fork images", async () => {
      render(<ForksShow />, optionsSuccess);

      await waitForPromises();

      const images = screen.getAllByRole("img");

      expect(images[0]).toHaveAttribute("src", `${fox38.images[0].url}?w=300`);
      expect(images[1]).toHaveAttribute("src", `${fox38.images[1].url}?w=300`);
    });
  });

  describe("when API call is not successful", () => {
    const optionsError = {
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    it("displays error alert", async () => {
      render(<ForksShow />, optionsError);

      await waitForPromises();

      const alert = screen.getByText(
        "An error occurred loading your fork, please try again."
      );

      expect(alert).toBeInTheDocument();
    });
  });
});
