import { omit } from "lodash";
import mockRouter from "next-router-mock";

import { ManufacturersDocument } from "@/graphql/manufacturer/queries/manufacturers.generated";
import { UpdatePartDocument } from "@/graphql/part/mutations/update-part.generated";
import { UploadPartImagesDocument } from "@/graphql/part/mutations/upload-part-images.generated";
import { PartByIdDocument } from "@/graphql/part/queries/part-by-id.generated";
import { PartsByTypeDocument } from "@/graphql/part/queries/parts-by-type.generated";
import ForksEdit from "@/pages/parts/forks/[id]/edit";
import { updateToastSuccess } from "@/store/slices/toasts";
import { PartType } from "@/types/graphql";
import { mockDispatch } from "tests/mocks/react-redux";
import { manufacturers } from "tests/unit/mock-data/manufacturer";
import { forks, fox38 } from "tests/unit/mock-data/part";
import {
  image1 as partImage1,
  image2 as partImage2,
  image3 as partImage3,
} from "tests/unit/mock-data/part-image";
import {
  settingsRecord1,
  settingsRecord2,
  settingsRecord3,
} from "tests/unit/mock-data/part-settings-record";
import { image3 as uploadPartImage3 } from "tests/unit/mock-data/upload-part-image";
import {
  render,
  waitForPromises,
  screen,
  within,
  mockFile,
  fireEvent,
  mockNetworkError,
  mockGraphQLError,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("parts/forks/[id]/edit", () => {
  const manufacturersMock = {
    request: {
      query: ManufacturersDocument,
      variables: {
        first: 25,
      },
    },
    result: {
      data: {
        manufacturers: {
          data: manufacturers,
        },
      },
    },
  };

  const partByIdMock = {
    request: {
      query: PartByIdDocument,
      variables: {
        id: "1",
      },
    },
    result: {
      data: {
        partById: fox38,
      },
    },
  };

  const updatedSettingsRecord = {
    ...settingsRecord2,
    nickname: "Super soft setup",
    lscClicks: 3,
    hscClicks: 3,
    lsrClicks: 3,
    hsrClicks: 3,
    volumeSpacers: 0,
    psi: 75,
    sag: 25,
    notes: "Works really well in wet conditions",
  };

  const editedFox38 = {
    ...fox38,
    name: "thirty eight",
    settingsRecords: [updatedSettingsRecord, settingsRecord3],
    images: [partImage1, partImage3],
    specificationsRecord: {
      ...fox38.specificationsRecord,
      lscMaxClicks: 11,
      hscMaxClicks: 11,
      lsrMaxClicks: 11,
      hsrMaxClicks: 11,
      travel: 180,
    },
    activeSettingsRecord: settingsRecord3,
  };

  beforeAll(() => {
    window.URL.revokeObjectURL = vi.fn();
    window.URL.createObjectURL = vi
      .fn()
      .mockImplementation(
        () => "blob:http://plush.test:3010/a3f96c77-4d20-469e-a737-4a82d3904b5b"
      );
  });

  beforeEach(() => {
    mockRouter.push({
      pathname: "/parts/forks/1/edit",
      query: { id: "1" },
    });
  });

  const expectSettingsRecordFieldsToHaveValues = ({
    container,
    active,
    nickname,
    lscClicks,
    hscClicks,
    lsrClicks,
    hsrClicks,
    volumeSpacers,
    psi,
    sag,
    notes,
  }: {
    container: HTMLElement;
    active: boolean;
    nickname: string;
    lscClicks: number;
    hscClicks: number;
    lsrClicks: number;
    hsrClicks: number;
    volumeSpacers: number;
    psi: number;
    sag: number;
    notes: string;
  }) => {
    const withinSettingsRecord = within(container);

    if (active) {
      expect(
        withinSettingsRecord.getByRole("switch", { name: "Active" })
      ).toBeChecked();
    } else {
      expect(
        withinSettingsRecord.getByRole("switch", { name: "Active" })
      ).not.toBeChecked();
    }

    expect(withinSettingsRecord.getByLabelText("Nickname")).toHaveValue(
      nickname
    );
    expect(withinSettingsRecord.getByLabelText("PSI")).toHaveValue(psi);
    expect(withinSettingsRecord.getByLabelText("Sag (mm)")).toHaveValue(sag);
    expect(withinSettingsRecord.getByLabelText("Volume spacers")).toHaveValue(
      volumeSpacers
    );
    expect(
      withinSettingsRecord.getByLabelText("Low speed compression clicks")
    ).toHaveValue(lscClicks);
    expect(
      withinSettingsRecord.getByLabelText("High speed compression clicks")
    ).toHaveValue(hscClicks);
    expect(
      withinSettingsRecord.getByLabelText("Low speed rebound clicks")
    ).toHaveValue(lsrClicks);
    expect(
      withinSettingsRecord.getByLabelText("High speed rebound clicks")
    ).toHaveValue(hsrClicks);
    expect(withinSettingsRecord.getByLabelText("Notes")).toHaveValue(notes);
  };

  it("displays page title", async () => {
    render(<ForksEdit />, {
      apollo: {
        mocks: [partByIdMock, manufacturersMock],
      },
    });

    await waitForPromises();

    expect(
      screen.getByRole("heading", { name: "Edit fork" })
    ).toBeInTheDocument();
  });

  describe("when page is loading", () => {
    it("shows loading icon", async () => {
      render(<ForksEdit />, {
        apollo: {
          mocks: [
            { request: partByIdMock.request, loading: true },
            manufacturersMock,
          ],
        },
      });

      expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();

      await waitForPromises();
    });
  });

  describe("when `forksById` query is successful", () => {
    it("displays fields with current values", async () => {
      render(<ForksEdit />, {
        apollo: {
          mocks: [partByIdMock, manufacturersMock],
        },
      });

      await waitForPromises();

      expect(screen.getByLabelText("Manufacturer")).toHaveTextContent(
        fox38.manufacturer.name
      );
      expect(screen.getByLabelText("Name")).toHaveValue(fox38.name);
      expect(
        screen.getByLabelText("Low speed compression max clicks")
      ).toHaveValue(fox38.specificationsRecord.lscMaxClicks);
      expect(
        screen.getByLabelText("High speed compression max clicks")
      ).toHaveValue(fox38.specificationsRecord.hscMaxClicks);
      expect(screen.getByLabelText("Low speed rebound max clicks")).toHaveValue(
        fox38.specificationsRecord.lsrMaxClicks
      );
      expect(
        screen.getByLabelText("High speed rebound max clicks")
      ).toHaveValue(fox38.specificationsRecord.hsrMaxClicks);
      expect(screen.getByLabelText("Travel (mm)")).toHaveValue(
        fox38.specificationsRecord.travel
      );

      expectSettingsRecordFieldsToHaveValues({
        container: screen.getByTestId("settingsRecord0"),
        active: true,
        ...fox38.settingsRecords[0],
      });
      expectSettingsRecordFieldsToHaveValues({
        container: screen.getByTestId("settingsRecord1"),
        active: false,
        ...fox38.settingsRecords[1],
      });
    });

    describe("when editing fork and API call is successful", () => {
      it("redirects to forks index", async () => {
        const mockImage = mockFile({ name: "mock1.jpg" });

        const {
          expectApolloHandlersCalledWithCorrectVariables,
          apolloCache,
          user,
        } = render(<ForksEdit />, {
          apollo: {
            mocks: [
              partByIdMock,
              manufacturersMock,
              {
                request: {
                  query: UploadPartImagesDocument,
                  variables: {
                    files: [mockImage],
                  },
                },
                result: {
                  data: {
                    uploadPartImages: [uploadPartImage3],
                  },
                },
              },
              {
                request: {
                  query: UpdatePartDocument,
                  variables: {
                    id: editedFox38.id,
                    name: editedFox38.name,
                    manufacturer: { connect: editedFox38.manufacturer.id },
                    settingsRecords: {
                      create: [
                        omit(
                          settingsRecord3,
                          "id",
                          "__typename",
                          "springRate",
                          "preloadAdjust"
                        ),
                      ],
                      update: [
                        omit(
                          updatedSettingsRecord,
                          "__typename",
                          "springRate",
                          "preloadAdjust"
                        ),
                      ],
                      delete: [settingsRecord1.id],
                    },
                    specificationsRecord: {
                      update: omit(
                        editedFox38.specificationsRecord,
                        "__typename"
                      ),
                    },
                    images: {
                      create: [omit(uploadPartImage3, "__typename")],
                      delete: [partImage2.id],
                    },
                  },
                },
                result: {
                  data: {
                    updatePart: {
                      ...editedFox38,
                      activeSettingsRecord: null,
                    },
                  },
                },
              },
              {
                request: {
                  query: UpdatePartDocument,
                  variables: {
                    id: editedFox38.id,
                    activeSettingsRecord: {
                      connect: settingsRecord3.id,
                    },
                  },
                },
                result: {
                  data: {
                    updatePart: editedFox38,
                  },
                },
              },
            ],
            initialCacheQueries: [
              {
                query: PartsByTypeDocument,
                data: {
                  partsByType: {
                    data: forks,
                    paginatorInfo: {
                      lastItem: 25,
                      total: 25,
                    },
                  },
                },
                variables: {
                  type: PartType.Fork,
                },
              },
            ],
          },
        });

        await waitForPromises();

        // Fill in fork specifications
        await user.clearAndType(
          screen.getByLabelText("Name"),
          editedFox38.name
        );

        await user.clearAndType(
          screen.getByLabelText("Low speed compression max clicks"),
          editedFox38.specificationsRecord.lscMaxClicks.toString()
        );

        await user.clearAndType(
          screen.getByLabelText("High speed compression max clicks"),
          editedFox38.specificationsRecord.hscMaxClicks.toString()
        );

        await user.clearAndType(
          screen.getByLabelText("Low speed rebound max clicks"),
          editedFox38.specificationsRecord.lsrMaxClicks.toString()
        );

        await user.clearAndType(
          screen.getByLabelText("High speed rebound max clicks"),
          editedFox38.specificationsRecord.hsrMaxClicks.toString()
        );

        await user.clearAndType(
          screen.getByLabelText("Travel (mm)"),
          editedFox38.specificationsRecord.travel.toString()
        );

        // Remove second image
        await user.click(
          within(screen.getByTestId("fileThumbnailContainer1")).getByRole(
            "button",
            {
              name: "Remove file",
            }
          )
        );

        // Upload image
        fireEvent.drop(screen.getByLabelText("Upload a file"), {
          dataTransfer: {
            files: mockImage,
            items: [
              {
                kind: "file",
                type: mockImage.type,
                getAsFile: () => mockImage,
              },
            ],
            types: ["Files"],
          },
        });
        await waitForPromises();

        // Delete first settings record
        await user.click(
          within(screen.getByTestId("settingsRecord0")).getByRole("button", {
            name: "Delete settings record",
          })
        );
        await user.click(
          within(
            screen.getByRole("dialog", {
              name: "Delete settings record",
            })
          ).getByRole("button", { name: "Delete" })
        );

        // Update now first settings record
        const withinFirstSettingsRecord = within(
          screen.getByTestId("settingsRecord0")
        );

        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("Nickname"),
          updatedSettingsRecord.nickname
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("PSI"),
          updatedSettingsRecord.psi.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("Sag (mm)"),
          updatedSettingsRecord.sag.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("Volume spacers"),
          updatedSettingsRecord.volumeSpacers.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText(
            "Low speed compression clicks"
          ),
          updatedSettingsRecord.lscClicks.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText(
            "High speed compression clicks"
          ),
          updatedSettingsRecord.hscClicks.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("Low speed rebound clicks"),
          updatedSettingsRecord.lsrClicks.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("High speed rebound clicks"),
          updatedSettingsRecord.hsrClicks.toString()
        );
        await user.clearAndType(
          withinFirstSettingsRecord.getByLabelText("Notes"),
          updatedSettingsRecord.notes
        );

        // Create new settings record
        await user.click(
          screen.getByRole("button", { name: "New settings record" })
        );

        const withinSecondSettingsRecord = within(
          screen.getByTestId("settingsRecord1")
        );

        await user.click(
          withinSecondSettingsRecord.getByRole("switch", {
            name: "Active",
            checked: false,
          })
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText("Nickname"),
          settingsRecord3.nickname.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText("PSI"),
          settingsRecord3.psi.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText("Sag (mm)"),
          settingsRecord3.sag.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText("Volume spacers"),
          settingsRecord3.volumeSpacers.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText(
            "Low speed compression clicks"
          ),
          settingsRecord3.lscClicks.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText(
            "High speed compression clicks"
          ),
          settingsRecord3.hscClicks.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText("Low speed rebound clicks"),
          settingsRecord3.lsrClicks.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText(
            "High speed rebound clicks"
          ),
          settingsRecord3.hsrClicks.toString()
        );
        await user.clearAndType(
          withinSecondSettingsRecord.getByLabelText("Notes"),
          settingsRecord3.notes
        );

        // Submit the form
        await user.click(screen.getByRole("button", { name: "Submit" }));
        await waitForPromises();

        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastSuccess({
            show: true,
            message: "Fork successfully updated.",
          })
        );
        expect(mockRouter.pathname).toBe("/parts/forks");
        expectApolloHandlersCalledWithCorrectVariables();

        const partsByTypeQuery = apolloCache.readQuery({
          query: PartsByTypeDocument,
          variables: {
            type: PartType.Fork,
          },
        });
        expect(partsByTypeQuery).toBeNull();

        const partsByIdQuery = apolloCache.readQuery({
          query: PartByIdDocument,
        });
        expect(partsByIdQuery).toBeNull();
      });
    });

    describe("when editing a fork and API call is not successful", () => {
      const optionsNetworkError = {
        apollo: {
          mocks: [
            partByIdMock,
            manufacturersMock,
            mockNetworkError({
              request: {
                query: UpdatePartDocument,
              },
            }),
          ],
        },
      };

      const optionsGraphQLError = {
        apollo: {
          mocks: [
            partByIdMock,
            manufacturersMock,
            mockGraphQLError({
              request: {
                query: UpdatePartDocument,
              },
              message: "Validation failed for the field [lscMaxClicks].",
              extensions: {
                validation: {
                  "input.lscMaxClicks": [
                    "Low speed compression max clicks must be a number.",
                  ],
                },
                category: "validation",
              },
              path: ["updateSpecificationsRecord"],
            }),
          ],
        },
      };

      describe("when error is a network error", () => {
        it("displays error alert", async () => {
          const { user } = render(<ForksEdit />, optionsNetworkError);

          await waitForPromises();

          await user.click(screen.getByRole("button", { name: "Submit" }));
          await waitForPromises();

          const alert = await screen.findByText(
            "An error occurred, please try again."
          );

          expect(alert).toBeInTheDocument();
        });
      });

      describe("when error is a GraphQL error", () => {
        it("displays error message", async () => {
          const { user } = render(<ForksEdit />, optionsGraphQLError);

          await waitForPromises();

          await user.click(screen.getByRole("button", { name: "Submit" }));
          await waitForPromises();

          expect(
            screen.getByLabelText("Low speed compression max clicks")
          ).toHaveAccessibleErrorMessage(
            "Low speed compression max clicks must be a number."
          );
        });
      });
    });
  });
});
