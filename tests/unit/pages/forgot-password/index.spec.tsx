import mockRouter from "next-router-mock";

import { ForgotPasswordDocument } from "@/graphql/auth/mutations/forgot-password.generated";
import ForgotPassword from "@/pages/forgot-password";
import {
  renderAndSubmitForm,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
  screen,
} from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("/forgot-password", () => {
  const requestVariables = {
    email: "peter@plush.bike",
  };

  const baseOptions = {
    submitLabel: "Send forgot password email",
    fields: {
      Email: requestVariables.email,
    },
  };

  const request = {
    query: ForgotPasswordDocument,
    variables: requestVariables,
  };

  beforeEach(() => {
    mockRouter.push("/forgot-password");
  });

  describe("validation", () => {
    describe.each`
      field      | value    | expectedError
      ${"Email"} | ${""}    | ${"Please enter your email."}
      ${"Email"} | ${"foo"} | ${"Please enter a valid email."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<ForgotPassword />, {
          submitLabel: "Send forgot password email",
          fields: {
            [field]: value,
          },
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<ForgotPassword />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            message: "We can't find a user with that email address.",
            path: ["forgotPassword"],
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<ForgotPassword />, optionsNetworkError);

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when error is incorrect email or password", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<ForgotPassword />, optionsGraphQLError);

        const alert = await screen.findByText(
          "We can't find a user with that email address."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    it("closes error alert when close button is clicked", async () => {
      const { user } = await renderAndSubmitForm(
        <ForgotPassword />,
        optionsNetworkError
      );

      const alert = await screen.findByText(
        "An error occurred, please try again."
      );

      await user.click(screen.getByLabelText("Close alert"));
      expect(alert).not.toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const options = {
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                forgotPassword: {
                  message: "We have emailed your password reset link!",
                  status: "EMAIL_SENT",
                },
              },
            },
          },
        ],
      },
    };

    it("shows success message", async () => {
      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<ForgotPassword />, options);

      await waitForPromises();

      expect(screen.getByTestId("iconSvgCheckmarkOutline")).toBeInTheDocument();
      expect(
        screen.getByText(
          "Password reset email sent successfully, please check your email."
        )
      ).toBeInTheDocument();
      expectApolloHandlersCalledWithCorrectVariables();
    });
  });
});
