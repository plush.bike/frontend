import mockRouter from "next-router-mock";

import { UpdateForgottenPasswordDocument } from "@/graphql/auth/mutations/update-forgotten-password.generated";
import ResetPassword from "@/pages/forgot-password/reset";
import { updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import {
  renderAndSubmitForm,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
  mockWindowLocation,
  screen,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("/forgot-password/reset", () => {
  const requestVariables = {
    email: "peter@plush.bike",
    token: "05f6a08faf716e4438d175baa13d3fa23467a5898f65d3a60fb89a3af00ce4ef",
    password: "foo bar baz",
    passwordConfirmation: "foo bar baz",
  };

  const baseOptions = {
    submitLabel: "Reset password",
    fields: {
      Password: requestVariables.password,
      "Confirm password": requestVariables.passwordConfirmation,
    },
  };

  const request = {
    query: UpdateForgottenPasswordDocument,
    variables: requestVariables,
  };

  beforeEach(() => {
    mockRouter.push("/forgot-password/reset");
    mockWindowLocation();

    window.location.search = `?token=${encodeURIComponent(
      requestVariables.token
    )}&email=${encodeURIComponent(requestVariables.email)}`;
  });

  describe("validation", () => {
    describe.each`
      field                 | value    | expectedError
      ${"Password"}         | ${""}    | ${"Please enter a password."}
      ${"Password"}         | ${"foo"} | ${"Your password must be at least 8 characters long."}
      ${"Confirm password"} | ${""}    | ${"Please confirm your password."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<ResetPassword />, {
          submitLabel: "Reset password",
          fields: {
            [field]: value,
          },
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });

    describe("when passwords do not match", () => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<ResetPassword />, {
          submitLabel: "Reset password",
          fields: {
            Password: "password",
            "Confirm password": "password foo",
          },
        });

        expect(
          await screen.findByLabelText("Password")
        ).toHaveAccessibleErrorMessage("Your passwords do not match.");
      });
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<ResetPassword />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            message: "An error has occurred while resetting the password",
            extensions: {
              errors: {
                token: "This password reset token is invalid.",
              },
              category: "validation",
            },
            path: ["updateForgottenPassword"],
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<ResetPassword />, optionsNetworkError);

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when error is invalid token", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<ResetPassword />, optionsGraphQLError);

        const alert = await screen.findByText(
          "The password reset link is invalid or expired.",
          { exact: false }
        );

        expect(alert).toBeInTheDocument();
      });
    });

    it("closes error alert when close button is clicked", async () => {
      const { user } = await renderAndSubmitForm(
        <ResetPassword />,
        optionsNetworkError
      );

      const alert = await screen.findByText(
        "An error occurred, please try again."
      );

      await user.click(screen.getByLabelText("Close alert"));
      expect(alert).not.toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const options = {
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                updateForgottenPassword: {
                  status: "PASSWORD_UPDATED",
                  message: "Your password has been reset!",
                },
              },
            },
          },
        ],
      },
    };

    it("dispatches `updateToastSuccess` action", async () => {
      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<ResetPassword />, options);

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastSuccess({
          show: true,
          message: "Password successfully reset.",
        })
      );
      expectApolloHandlersCalledWithCorrectVariables();
    });

    it("redirects to `/sign-in`", async () => {
      await renderAndSubmitForm(<ResetPassword />, options);

      await waitForPromises();

      expect(mockRouter.pathname).toBe("/sign-in");
    });
  });
});
