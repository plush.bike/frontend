import mockRouter from "next-router-mock";

import * as SocialSignInCallback from "@/components/auth/SocialSignInCallback";
import SignInGoogle from "@/pages/sign-in/google";
import { SocialProviderType } from "@/types/graphql";
import { mockComponent, render } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("/sign-in/google", () => {
  beforeEach(() => {
    mockRouter.push("/sign-in/google");
  });

  it("renders `SocialSignIn` component", () => {
    const { spy } = mockComponent({ componentObject: SocialSignInCallback });

    render(<SignInGoogle />);

    expect(spy).toHaveBeenCalledWith(
      {
        providerType: SocialProviderType.Google,
        providerTitle: "Google",
      },
      {}
    );
  });
});
