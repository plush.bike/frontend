import mockRouter from "next-router-mock";

import { SignInDocument } from "@/graphql/auth/mutations/sign-in.generated";
import SignIn from "@/pages/sign-in/index";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  render,
  renderAndSubmitForm,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
  screen,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("/sign-in", () => {
  const requestVariables = {
    email: "peter@plush.bike",
    password: "foobar",
  };

  const baseOptions = {
    submitLabel: "Sign in",
    fields: {
      Email: requestVariables.email,
      Password: requestVariables.password,
    },
  };

  const request = {
    query: SignInDocument,
    variables: requestVariables,
  };

  beforeEach(() => {
    mockRouter.push("/sign-in");
  });

  describe("validation", () => {
    describe.each`
      field         | value    | expectedError
      ${"Email"}    | ${""}    | ${"Please enter your email."}
      ${"Email"}    | ${"foo"} | ${"Please enter a valid email."}
      ${"Password"} | ${""}    | ${"Please enter your password."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<SignIn />, {
          submitLabel: "Sign in",
          fields: {
            [field]: value,
          },
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<SignIn />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            message: "Incorrect username or password.",
            path: ["sign-in"],
          }),
        ],
      },
    };

    it("clears password input and focuses on it", async () => {
      await renderAndSubmitForm(<SignIn />, optionsNetworkError);

      await waitForPromises();

      const passwordInput = await screen.findByLabelText("Password");

      expect(passwordInput).toHaveValue("");
      expect(passwordInput).toHaveFocus();
    });

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<SignIn />, optionsNetworkError);

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when error is incorrect email or password", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<SignIn />, optionsGraphQLError);

        const alert = await screen.findByText(
          "Incorrect email or password, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    it("closes error alert when close button is clicked", async () => {
      const { user } = await renderAndSubmitForm(
        <SignIn />,
        optionsNetworkError
      );

      const alert = await screen.findByText(
        "An error occurred, please try again."
      );

      await user.click(screen.getByLabelText("Close alert"));
      expect(alert).not.toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const options = ({ twoFactor = false } = {}) => ({
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                signIn: {
                  user: mockUser,
                  twoFactor,
                },
              },
            },
          },
        ],
      },
    });

    it("dispatches `updateAuthenticatedUser` action", async () => {
      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<SignIn />, options());

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateAuthenticatedUser(mockUser)
      );
      expectApolloHandlersCalledWithCorrectVariables();
    });

    it("redirects to `/parts/forks`", async () => {
      await renderAndSubmitForm(<SignIn />, options());

      await waitForPromises();

      expect(mockRouter.pathname).toBe("/parts/forks");
    });

    describe("when user has two factor authentication enabled", () => {
      it("it shows two factor authentication challenge form", async () => {
        await renderAndSubmitForm(<SignIn />, options({ twoFactor: true }));

        await waitForPromises();

        expect(screen.getByLabelText("One time password")).toBeInTheDocument();
      });
    });
  });

  it("renders forgot password link", () => {
    render(<SignIn />);

    expect(
      screen.getByText("Forgot password?", {
        selector: 'a[href="/forgot-password"]',
      })
    ).toBeInTheDocument();
  });
});
