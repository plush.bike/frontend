import mockRouter from "next-router-mock";

import * as SocialSignInCallback from "@/components/auth/SocialSignInCallback";
import SignInStrava from "@/pages/sign-in/strava";
import { SocialProviderType } from "@/types/graphql";
import { mockComponent, render } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("/sign-in/strava", () => {
  beforeEach(() => {
    mockRouter.push("/sign-in/strava");
  });

  it("renders `SocialSignIn` component", () => {
    const { spy } = mockComponent({ componentObject: SocialSignInCallback });

    render(<SignInStrava />);

    expect(spy).toHaveBeenCalledWith(
      {
        providerType: SocialProviderType.Strava,
        providerTitle: "Strava",
      },
      {}
    );
  });
});
