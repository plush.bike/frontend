import mockRouter from "next-router-mock";

import * as PersonalAccessTokens from "@/components/account-settings/personal-access-tokens/PersonalAccessTokens";
import * as SocialProviders from "@/components/account-settings/SocialProviders";
import * as TwoFactorAuthentication from "@/components/account-settings/two-factor-authentication/TwoFactorAuthentication";
import * as UpdatePassword from "@/components/account-settings/UpdatePassword";
import * as UpdateProfile from "@/components/account-settings/UpdateProfile";
import { PersonalAccessTokensDocument } from "@/graphql/auth/personal-access-tokens/queries/personal-access-tokens.generated";
import { SocialProvidersDocument } from "@/graphql/auth/social-providers/queries/social-providers.generated";
import AccountSettings from "@/pages/account-settings";
import { personalAccessTokens } from "tests/unit/mock-data/personal-access-token";
import { strava } from "tests/unit/mock-data/social-provider";
import { render, mockComponent, waitForPromises } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("/account-settings", () => {
  const mockPersonalAccessTokenQuery = {
    request: {
      query: PersonalAccessTokensDocument,
    },
    result: {
      data: {
        personalAccessTokens: {
          data: personalAccessTokens,
          paginatorInfo: {
            lastItem: 2,
            total: 2,
            __typename: "PaginatorInfo",
          },
        },
      },
    },
  };

  const mockSocialProvidersQuery = {
    request: {
      query: SocialProvidersDocument,
    },
    result: {
      data: {
        socialProviders: [strava],
      },
    },
  };

  beforeEach(() => {
    mockRouter.push("/account-settings");
  });

  it("renders `Profile` component", async () => {
    const { spy } = mockComponent({ componentObject: UpdateProfile });
    render(<AccountSettings />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery, mockSocialProvidersQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });

  it("renders `UpdatePassword` component", async () => {
    const { spy } = mockComponent({ componentObject: UpdatePassword });

    render(<AccountSettings />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery, mockSocialProvidersQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });

  it("renders `SocialProviders` component", async () => {
    const { spy } = mockComponent({ componentObject: SocialProviders });

    render(<AccountSettings />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery, mockSocialProvidersQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });

  it("renders `TwoFactorAuthentication` component", async () => {
    const { spy } = mockComponent({ componentObject: TwoFactorAuthentication });

    render(<AccountSettings />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery, mockSocialProvidersQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });

  it("renders `PersonalAccessTokens` component", async () => {
    const { spy } = mockComponent({ componentObject: PersonalAccessTokens });

    render(<AccountSettings />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery, mockSocialProvidersQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });
});
