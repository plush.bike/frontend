import { ApolloError } from "@apollo/client";
import { GraphQLError } from "graphql";

import {
  descriptionInputAttributes,
  emailIsValid,
  errorInputAttributes,
  hasErrors,
  parseAlertErrorMessage,
  parseAndSetServerErrors,
  parseServerErrorFields,
} from "@/helpers/form";

describe("emailIsValid", () => {
  it.each`
    email                    | expected
    ${"peter@plush.bike"}    | ${true}
    ${"example@gmail.com"}   | ${true}
    ${"foobar@gmail.co"}     | ${true}
    ${"foo.bar@test.com"}    | ${true}
    ${"peter"}               | ${false}
    ${"peter@fadsfasdfasdf"} | ${false}
  `("returns $expected for $email", ({ email, expected }) => {
    expect(emailIsValid(email)).toBe(expected);
  });
});

describe("hasErrors", () => {
  it.each`
    value                                                       | expected
    ${undefined}                                                | ${false}
    ${null}                                                     | ${false}
    ${[]}                                                       | ${false}
    ${[{ type: "server", message: "This field is required." }]} | ${true}
    ${{ type: "server", message: "This field is required." }}   | ${true}
  `("returns $expected for $value", ({ value, expected }) => {
    expect(hasErrors(value)).toBe(expected);
  });
});

describe("errorInputAttributes", () => {
  describe("when input has an error", () => {
    it("returns object with `aria-invalid` and `aria-errormessage` attributes set", () => {
      expect(
        errorInputAttributes({
          errors: { type: "server", message: "This field is required." },
          id: "email",
        })
      ).toEqual({ "aria-invalid": true, "aria-errormessage": "email-error" });
    });
  });

  describe("descriptionInputAttributes", () => {
    describe("when input has a description", () => {
      const description = "This field does the thing";

      it("returns object with `aria-describedby` attribute set", () => {
        expect(
          descriptionInputAttributes({ id: "email", description })
        ).toEqual({ "aria-describedby": "email-description" });
      });
    });

    describe("when input does not have a description", () => {
      it("returns object with `aria-describedby` attribute set as `null`", () => {
        expect(
          descriptionInputAttributes({ id: "email", description: undefined })
        ).toEqual({ "aria-describedby": null });
      });
    });
  });

  describe("when input does not have an error", () => {
    it("returns an object with `aria-invalid` and `aria-errormessage` attributes set to `null`", () => {
      expect(
        errorInputAttributes({
          errors: undefined,
          id: "email",
        })
      ).toEqual({ "aria-invalid": null, "aria-errormessage": null });
    });
  });
});

describe("parseServerErrorFields", () => {
  describe("when there are no GraphQL validation errors", () => {
    it("returns an empty object", () => {
      expect(
        parseServerErrorFields(new ApolloError({ graphQLErrors: [] }))
      ).toEqual({});
    });
  });

  describe("when there are GraphQL validation errors", () => {
    it("parses and returns object of validation errors", () => {
      expect(
        parseServerErrorFields(
          new ApolloError({
            graphQLErrors: [
              new GraphQLError(
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                {
                  validation: {
                    "input.first_name": ["First name is required."],
                    "input.last_name": ["Last name is required"],
                  },
                }
              ),
            ],
          })
        )
      ).toEqual({
        firstName: ["First name is required."],
        lastName: ["Last name is required"],
      });
    });
  });
});

describe("parseAlertErrorMessage", () => {
  const error1 = new ApolloError({
    graphQLErrors: [
      new GraphQLError(
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        {
          category: "validation",
        }
      ),
    ],
  });

  const error2 = new ApolloError({
    graphQLErrors: [
      new GraphQLError(
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        {
          category: "validation",
          errors: ["Foo bar error message"],
        }
      ),
    ],
  });

  const error3 = new ApolloError({
    graphQLErrors: [
      new GraphQLError(
        "Foo bar baz error message",
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined
      ),
    ],
  });

  it.each`
    error     | expected
    ${error1} | ${undefined}
    ${error2} | ${"Foo bar error message"}
    ${error3} | ${"Foo bar baz error message"}
  `("returns $expected", ({ error, expected }) => {
    expect(parseAlertErrorMessage(error)).toBe(expected);
  });
});

describe("parseAndSetServerErrors", () => {
  const setAlertErrorMessage = vi.fn();
  const setError = vi.fn();

  describe("when fields have validation errors", () => {
    beforeEach(() => {
      parseAndSetServerErrors({
        error: new ApolloError({
          graphQLErrors: [
            new GraphQLError(
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              undefined,
              {
                validation: {
                  "input.first_name": ["First name is required."],
                  "input.last_name": ["Last name is required"],
                },
              }
            ),
          ],
        }),
        setAlertErrorMessage,
        setError,
      });
    });

    it("calls `setError` on each field", () => {
      expect(setError).toHaveBeenCalledWith("firstName", {
        type: "server",
        message: "First name is required.",
      });
      expect(setError).toHaveBeenCalledWith("lastName", {
        type: "server",
        message: "Last name is required",
      });
    });
  });

  describe("when fields do not have validation errors", () => {
    describe("when server returns an error message", () => {
      const error = new ApolloError({
        graphQLErrors: [
          new GraphQLError(
            "Foo bar baz error message",
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined
          ),
        ],
      });

      describe("when `customAlertErrorMessages` argument is passed", () => {
        it("uses custom error message", () => {
          parseAndSetServerErrors({
            error,
            setAlertErrorMessage,
            setError,
            customAlertErrorMessages: {
              "Foo bar baz error message": "Custom error message",
            },
          });

          expect(setAlertErrorMessage).toHaveBeenCalledWith(
            "Custom error message"
          );
        });
      });

      describe("when `customAlertErrorMessages` argument is not passed", () => {
        it("uses server error message", () => {
          parseAndSetServerErrors({
            error,
            setAlertErrorMessage,
            setError,
          });

          expect(setAlertErrorMessage).toHaveBeenCalledWith(
            "Foo bar baz error message"
          );
        });
      });
    });

    describe("when server does not return an error message", () => {
      const error = new ApolloError({
        graphQLErrors: [],
      });

      it("uses generic error message", () => {
        parseAndSetServerErrors({
          error,
          setAlertErrorMessage,
          setError,
        });

        expect(setAlertErrorMessage).toHaveBeenCalledWith(
          "An error occurred, please try again."
        );
      });
    });
  });
});
