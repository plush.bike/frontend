import { omit } from "lodash";

import { formatSettingsRecordsForMutation } from "@/helpers/part";
import { AirOrCoilType } from "@/types/graphql";
import { settingsRecord1 } from "tests/unit/mock-data/part-settings-record";

describe("formatSettingsRecordsForMutation", () => {
  describe(`when \`airOrCoil\` is \`${AirOrCoilType.Air}\``, () => {
    it("has `psi` and `volumeSpacers` but does not have `springRate`, `preloadAdjust` and `__typename`", () => {
      expect(
        formatSettingsRecordsForMutation({
          airOrCoil: AirOrCoilType.Air,
          settingsRecords: [settingsRecord1],
        })
      ).toEqual([
        omit(settingsRecord1, "springRate", "preloadAdjust", "__typename"),
      ]);
    });
  });

  describe(`when \`airOrCoil\` is \`${AirOrCoilType.Coil}\``, () => {
    it("has `springRate`, `preloadAdjust` but does not have `psi`, `volumeSpacers` and `__typename`", () => {
      expect(
        formatSettingsRecordsForMutation({
          airOrCoil: AirOrCoilType.Coil,
          settingsRecords: [settingsRecord1],
        })
      ).toEqual([omit(settingsRecord1, "psi", "volumeSpacers", "__typename")]);
    });
  });
});
