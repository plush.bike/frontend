import { pascalCase } from "@/helpers/string";

describe("pascalCase", () => {
  it.each`
    text
    ${"fooBar"}
    ${"foo bar"}
    ${"foo_bar"}
    ${"foo-bar"}
  `("converts $text to PascalCase", ({ text }) => {
    expect(pascalCase(text)).toBe("FooBar");
  });
});
