import { SavedFile } from "@/helpers/file-upload";

vi.mock("lodash", () => ({
  uniqueId: vi.fn(() => 1),
}));

describe("SavedFile", () => {
  it("correctly sets class attributes", () => {
    const savedFile = new SavedFile({ id: 1, src: "foo.png" });

    expect(savedFile.id).toBe(1);
    expect(savedFile.preview).toBe(null);
    expect(savedFile.key).toBe(1);
    expect(savedFile.loading).toBe(false);
    expect(savedFile.src).toBe("foo.png");
    expect(savedFile.error).toBe(null);
  });
});
