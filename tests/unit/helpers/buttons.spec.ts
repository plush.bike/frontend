import { buttonBaseSharedProps } from "@/helpers/buttons";

describe("buttonBaseSharedProps", () => {
  describe.each`
    disabled     | loading      | expected
    ${true}      | ${false}     | ${true}
    ${true}      | ${true}      | ${true}
    ${false}     | ${false}     | ${false}
    ${false}     | ${true}      | ${true}
    ${undefined} | ${undefined} | ${false}
    ${true}      | ${undefined} | ${true}
    ${undefined} | ${true}      | ${true}
  `(
    "when `disabled` argument is `$disabled` and `loading` argument is `$loading`",
    ({ disabled, loading, expected }) => {
      it(`sets \`disabled\` prop as \`${expected}\``, () => {
        expect(buttonBaseSharedProps({ disabled, loading }).disabled).toBe(
          expected
        );
      });

      const { className } = buttonBaseSharedProps({ disabled, loading });

      if (expected) {
        it("adds disabled CSS classes", () => {
          expect(className).toContain("disabled:cursor-not-allowed");
          expect(className).toContain("disabled:opacity-40");
        });
      } else {
        it("does not add disabled CSS classes", () => {
          expect(className).not.toContain("disabled:cursor-not-allowed");
          expect(className).not.toContain("disabled:opacity-40");
        });
      }
    }
  );

  describe("when `className` argument is passed", () => {
    it("adds class name", () => {
      expect(
        buttonBaseSharedProps({ className: "foo-bar" }).className
      ).toContain("foo-bar");
    });
  });
});
