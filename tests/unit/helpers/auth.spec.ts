import { fetchCsrfCookie } from "@/helpers/auth";

describe("fetchCsrfCookie", () => {
  beforeAll(() => {
    window.fetch = vi.fn().mockResolvedValue("");
  });

  it("calls `window.fetch` and returns promise", () => {
    expect(fetchCsrfCookie()).resolves.toBe("");
    expect(window.fetch).toHaveBeenCalledWith(
      "http://api.plush.test/sanctum/csrf-cookie",
      { credentials: "include" }
    );
  });
});
