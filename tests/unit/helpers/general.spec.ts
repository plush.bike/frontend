import { isPromise, classToObject } from "@/helpers/general";

describe("isPromise", () => {
  /* eslint-disable @typescript-eslint/no-empty-function */
  it.each`
    value                    | expected
    ${"foo bar"}             | ${false}
    ${1}                     | ${false}
    ${{ foo: "bar" }}        | ${false}
    ${["foo", "bar"]}        | ${false}
    ${new Promise(() => {})} | ${true}
  `("returns $expected for $value", ({ value, expected }) => {
    expect(isPromise(value)).toBe(expected);
  });
  /* eslint-enable @typescript-eslint/no-empty-function */
});

describe("classToObject", () => {
  it("converts the class to an object", () => {
    expect(classToObject(Object.create({ foo: "bar", bar: "baz" }))).toEqual({
      foo: "bar",
      bar: "baz",
    });
  });
});
