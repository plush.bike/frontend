import { getQueryParam, imageUrl } from "@/helpers/url";
import { mockWindowLocation } from "tests/utils";

describe("getQueryParam", () => {
  describe.each`
    param    | queryString               | expected
    ${"foo"} | ${"?foo=bar"}             | ${"bar"}
    ${"foo"} | ${"?foo=bar&baz=testing"} | ${"bar"}
  `("when query string is $queryString", ({ param, queryString, expected }) => {
    beforeEach(() => {
      mockWindowLocation();
    });

    it(`returns ${expected} for ${param}`, () => {
      window.location.search = queryString;

      expect(getQueryParam(param)).toBe(expected);
    });
  });
});

describe("imageUrl", () => {
  it("returns ImageKit url", () => {
    expect(
      imageUrl({
        url: "https://ik.imagekit.io/profile_images/1/foo-bar.jpg",
        height: 100,
        width: 200,
      })
    ).toBe(
      "https://ik.imagekit.io/profile_images/1/foo-bar.jpg?height=100&width=200"
    );
  });
});
