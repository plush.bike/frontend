import { copyToClipboard } from "@/helpers/copy-to-clipboard";

describe("copyToClipboard", () => {
  it("copies the value", async () => {
    document.execCommand = vi.fn().mockImplementation(() => true);

    await copyToClipboard("foo");

    expect(document.execCommand).toHaveBeenCalledWith("copy");
  });
});
