import { Mock } from "vitest";

import { emailIsValid } from "@/helpers/form";
import {
  validateEmail,
  validateListBoxRequired,
  validateMatch,
} from "@/helpers/validation-rules";

vi.mock("@/helpers/form");

describe("validateListBoxRequired", () => {
  describe("when value is truthy", () => {
    it("returns `true`", () => {
      expect(
        validateListBoxRequired(
          {
            id: 1,
            name: "Foo bar",
          },
          "This field is required"
        )
      ).toBe(true);
    });
  });

  describe("when value is falsy", () => {
    it("returns error message", () => {
      expect(validateListBoxRequired(undefined, "This field is required")).toBe(
        "This field is required"
      );
    });
  });
});

describe("validateEmail", () => {
  describe("when email is valid", () => {
    beforeEach(() => {
      (emailIsValid as Mock).mockReturnValueOnce(true);
    });

    it("returns `true`", () => {
      expect(
        validateEmail("peter@plush.bike", "Please enter a valid email")
      ).toBe(true);
    });
  });

  describe("when email is not valid", () => {
    beforeEach(() => {
      (emailIsValid as Mock).mockReturnValueOnce(false);
    });

    it("returns error message", () => {
      expect(
        validateEmail("peter@plush.bike", "Please enter a valid email")
      ).toBe("Please enter a valid email");
    });
  });
});

describe("validateMatch", () => {
  describe("when values match", () => {
    it("returns `true`", () => {
      expect(
        validateMatch("Foo bar", "These values must match", {
          otherValue: "Foo bar",
        })
      ).toBe(true);
    });
  });

  describe("when values do not match", () => {
    it("returns error message", () => {
      expect(
        validateMatch("Foo bar", "These values must match", {
          otherValue: "Foo bar baz",
        })
      ).toBe("These values must match");
    });
  });
});
