import { BadgeProps } from "@/types/badges";
import { FunctionComponent } from "@/types/react";
import { render, screen } from "tests/utils";

export const badgeShared = (BadgeComponent: FunctionComponent<BadgeProps>) => {
  describe("props", () => {
    it("merges `className` prop with CSS classes", () => {
      const { container } = render(<BadgeComponent className="foo-bar" />);

      expect(container.firstChild).toHaveClass("foo-bar");
    });

    describe("when remove button is clicked", () => {
      it("calls `onRemove` prop", async () => {
        const onRemove = vi.fn();

        const { user } = render(<BadgeComponent onRemove={onRemove} />);

        await user.click(screen.getByRole("button", { name: "Remove" }));

        expect(onRemove).toHaveBeenCalled();
      });
    });
  });
};
