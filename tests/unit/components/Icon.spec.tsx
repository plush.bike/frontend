import React from "react";

import Icon from "@/components/Icon";
import { render, screen } from "tests/utils";

describe("Icon", () => {
  const defaultProps = { svg: { id: "clipboard" }, title: "Copy" };

  describe("props", () => {
    it("renders svg based on `svg` prop", () => {
      render(<Icon {...defaultProps} />);

      expect(screen.getByTestId("iconUse")).toHaveAttribute(
        "xlink:href",
        "#clipboard"
      );
    });

    describe("when `size` prop is passed", () => {
      it("adds corresponding CSS classes", () => {
        const { container } = render(<Icon {...defaultProps} size={5} />);

        expect(container.firstChild).toHaveClass("w-5", "h-5");
      });
    });

    describe("when `size` prop is not passed", () => {
      it("defaults to size `4`", () => {
        const { container } = render(<Icon {...defaultProps} />);

        expect(container.firstChild).toHaveClass("w-4", "h-4");
      });
    });
  });
});
