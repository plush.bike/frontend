import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import Tooltip from "@/components/Tooltip";
import { render, screen } from "tests/utils";

describe("Tooltip", () => {
  const defaultProps = {
    content: "Tooltip content",
  };

  it("renders tooltip when hovering over button", async () => {
    const { user } = render(
      <Tooltip {...defaultProps}>
        <ButtonPrimary>Hover over me</ButtonPrimary>
      </Tooltip>
    );

    await user.hover(screen.getByRole("button", { name: "Hover over me" }));
    expect(
      screen.getByRole("tooltip", { name: defaultProps.content })
    ).toBeInTheDocument();
    await user.unhover(screen.getByRole("button", { name: "Hover over me" }));
  });

  describe("when button is disabled", () => {
    it("renders div with tooltip", () => {
      const { container } = render(
        <Tooltip {...defaultProps}>
          <ButtonPrimary disabled>Hover over me</ButtonPrimary>
        </Tooltip>
      );

      expect(container.firstElementChild.tagName).toBe("DIV");
    });

    it("adds display CSS class to the `div`", () => {
      const { container } = render(
        <Tooltip {...defaultProps}>
          <ButtonPrimary disabled className="inline-flex">
            Hover over me
          </ButtonPrimary>
        </Tooltip>
      );

      expect(container.firstChild).toHaveClass("inline-flex");
    });

    it("renders tooltip when hovering over div", async () => {
      const { container, user } = render(
        <Tooltip {...defaultProps}>
          <ButtonPrimary disabled>Hover over me</ButtonPrimary>
        </Tooltip>
      );

      await user.hover(container.firstElementChild);
      expect(
        screen.getByRole("tooltip", { name: defaultProps.content })
      ).toBeInTheDocument();
    });
  });
});
