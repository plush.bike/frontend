import { createColumnHelper } from "@tanstack/react-table";

import Table from "@/components/table/Table";
import { render, screen } from "tests/utils";

describe("Table", () => {
  const data = [
    {
      id: 1,
      name: "foo",
    },
    {
      id: 2,
      name: "bar",
    },
    {
      id: 3,
      name: "baz",
    },
    {
      id: 4,
      name: "foo bar baz",
    },
  ];

  const columnHelper = createColumnHelper<(typeof data)[0]>();

  const columns = [
    columnHelper.accessor("id", {
      header: "ID",
    }),
    columnHelper.accessor("name", {
      header: "Name",
    }),
  ];

  it("renders table with headers and data", () => {
    render(<Table columns={columns} data={data} />);

    expect(
      screen.getByRole("columnheader", { name: "ID" })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("columnheader", { name: "Name" })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("cell", { name: `ID ${data[0].id}` })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("cell", { name: `Name ${data[0].name}` })
    ).toBeInTheDocument();
  });

  describe("when `loading` prop is `true`", () => {
    it("displays 10 rows of loading skeletons", () => {
      render(<Table columns={columns} data={[]} loading />);

      expect(screen.getAllByTestId("skeletonLoader")).toHaveLength(20);
    });
  });

  describe("when table has no data", () => {
    it("displays empty content", () => {
      const emptyContent = "No personal access tokens.";
      render(<Table columns={columns} data={[]} emptyContent={emptyContent} />);

      expect(screen.getByRole("cell", { name: emptyContent }));
    });
  });
});
