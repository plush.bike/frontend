import React from "react";

import ToastSuccess from "@/components/toasts/ToastSuccess";
import { render, fireEvent, within, screen } from "tests/utils";

describe("ToastSuccess", () => {
  const defaultProps = { show: true };

  describe("props", () => {
    describe("when close button is clicked", () => {
      it("calls handler passed as `onClose` prop", () => {
        const onClose = vi.fn();

        render(<ToastSuccess {...defaultProps} onClose={onClose} />);

        fireEvent.click(screen.getByLabelText("Close notification"));

        expect(onClose).toHaveBeenCalled();
      });
    });

    it("merges `className` prop", () => {
      render(<ToastSuccess {...defaultProps} className="foo-bar" />);

      expect(screen.getByTestId("toastSuccess")).toHaveClass(
        "foo-bar",
        "toast"
      );
    });
  });

  it('renders "Checkmark outline" icon', () => {
    render(<ToastSuccess {...defaultProps} />);

    const icon = screen.getByTestId("toastIcon");

    expect(within(icon).getByTestId("iconUse")).toHaveAttribute(
      "xlink:href",
      "#checkmark-outline"
    );
  });

  it("renders children", () => {
    render(<ToastSuccess {...defaultProps}>Foo bar</ToastSuccess>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
