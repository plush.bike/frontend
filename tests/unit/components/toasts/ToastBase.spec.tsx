import React from "react";

import ToastBase from "@/components/toasts/ToastBase";
import { render, within, fireEvent, screen } from "tests/utils";

describe("ToastBase", () => {
  const defaultProps = { show: true, icon: { id: "checkmark-outline" } };

  beforeEach(() => {
    vi.useFakeTimers({ shouldAdvanceTime: true });
    vi.spyOn(global, "setTimeout");
  });

  describe("props", () => {
    describe("when `showFor` prop is a number", () => {
      it("shows toast for that amount of time", () => {
        const onClose = vi.fn();

        const { rerender } = render(
          <ToastBase
            {...defaultProps}
            show={false}
            showFor={1000}
            onClose={onClose}
          />
        );

        expect(setTimeout).not.toHaveBeenCalledWith(expect.any(Function), 1000);

        rerender(
          <ToastBase {...defaultProps} showFor={1000} onClose={onClose} />
        );

        vi.runAllTimers();

        expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 1000);
        expect(onClose).toHaveBeenCalled();
      });
    });

    describe("when `showFor` prop is `null`", () => {
      it("does not hide toast", () => {
        const onClose = vi.fn();

        const { rerender } = render(
          <ToastBase
            {...defaultProps}
            show={false}
            showFor={null}
            onClose={onClose}
          />
        );

        expect(setTimeout).not.toHaveBeenCalledWith(expect.any(Function), 1000);

        rerender(
          <ToastBase {...defaultProps} showFor={null} onClose={onClose} />
        );

        vi.runAllTimers();

        expect(setTimeout).not.toHaveBeenCalledWith(expect.any(Function), 1000);
        expect(onClose).not.toHaveBeenCalled();
      });
    });

    it("renders svg passed as `icon` prop", async () => {
      render(<ToastBase {...defaultProps} />);

      const icon = screen.getByTestId("toastIcon");

      expect(within(icon).getByTestId("iconUse")).toHaveAttribute(
        "xlink:href",
        "#checkmark-outline"
      );
      expect(within(icon).getByTestId("iconSvgCheckmarkOutline")).toHaveClass(
        "w-5",
        "h-5"
      );
    });

    it("merges `className` prop", () => {
      render(<ToastBase {...defaultProps} className="bg-emerald-50" />);

      expect(screen.getByTestId("toastBase")).toHaveClass(
        "bg-emerald-50",
        "toast"
      );
    });

    describe("when close button is clicked", () => {
      it("calls handler passed as `onClose` prop", () => {
        const onClose = vi.fn();

        render(<ToastBase {...defaultProps} onClose={onClose} />);

        fireEvent.click(screen.getByLabelText("Close notification"));

        expect(onClose).toHaveBeenCalled();
      });
    });
  });

  it("renders children", () => {
    render(<ToastBase {...defaultProps}>Foo bar</ToastBase>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
