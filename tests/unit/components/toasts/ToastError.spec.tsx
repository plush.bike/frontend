import React from "react";

import ToastError from "@/components/toasts/ToastError";
import { render, fireEvent, within, screen } from "tests/utils";

describe("ToastError", () => {
  const defaultProps = { show: true };

  describe("props", () => {
    describe("when close button is clicked", () => {
      it("calls handler passed as `onClose` prop", () => {
        const onClose = vi.fn();

        render(<ToastError {...defaultProps} onClose={onClose} />);

        fireEvent.click(screen.getByLabelText("Close notification"));

        expect(onClose).toHaveBeenCalled();
      });
    });

    it("merges `className` prop", () => {
      render(<ToastError {...defaultProps} className="foo-bar" />);

      expect(screen.getByTestId("toastError")).toHaveClass("foo-bar", "toast");
    });
  });

  it('renders "Close solid" icon', () => {
    render(<ToastError {...defaultProps} />);

    const icon = screen.getByTestId("toastIcon");

    expect(within(icon).getByTestId("iconUse")).toHaveAttribute(
      "xlink:href",
      "#close-solid"
    );
  });

  it("renders children", () => {
    render(<ToastError {...defaultProps}>Foo bar</ToastError>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
