import RadioGroup from "@/components/form/RadioGroup";
import RadioInput from "@/components/form/RadioInput";
import { render, screen } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("RadioGroup", () => {
  const defaultProps = {
    label: "Air or coil",
    description: "Choose one",
  };

  describe("props", () => {
    it("renders `radiogroup` with label", () => {
      render(<RadioGroup {...defaultProps} />);

      expect(
        screen.getByRole("radiogroup", { name: defaultProps.label })
      ).toBeInTheDocument();
    });

    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <RadioGroup
            {...defaultProps}
            errors={{
              type: "server",
              message: "This field is required",
            }}
          />
        );

        const input = screen.getByLabelText(defaultProps.label);

        expect(input).toHaveAccessibleErrorMessage("This field is required");
      });
    });

    describe("when `description` prop is passed", () => {
      const description = "This input does the thing";

      it("displays description", () => {
        render(<RadioGroup {...defaultProps} description={description} />);

        expect(
          screen.getByRole("radiogroup", { name: defaultProps.label })
        ).toHaveAccessibleDescription(description);
      });
    });

    describe("when `labelHidden` prop is `true`", () => {
      it("adds `sr-only` CSS class to label", () => {
        render(<RadioGroup {...defaultProps} labelHidden />);

        expect(
          screen.getByText(defaultProps.label, { selector: "label" })
        ).toHaveClass("sr-only");
      });
    });
  });

  it("renders children", () => {
    render(
      <RadioGroup {...defaultProps}>
        <RadioInput label="Air" value="air" name="airOrCoil" />
        <RadioInput label="Coil" value="coil" name="airOrCoil" />
      </RadioGroup>
    );

    expect(screen.getByRole("radio", { name: "Air" })).toBeInTheDocument();
    expect(screen.getByRole("radio", { name: "Coil" })).toBeInTheDocument();
  });
});
