import Textarea from "@/components/form/Textarea";
import { render, screen } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("Textarea", () => {
  const defaultProps = {
    label: "Notes",
  };

  describe("props", () => {
    it("renders `label` prop as label", () => {
      render(<Textarea {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.label, { selector: "label" })
      ).toBeInTheDocument();
    });

    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <Textarea
            {...defaultProps}
            errors={{
              type: "server",
              message: "Notes is required",
            }}
          />
        );

        const textarea = screen.getByLabelText(defaultProps.label);

        expect(textarea).toHaveAccessibleErrorMessage("Notes is required");
        expect(textarea).toHaveClass("border-red-300");
        expect(
          screen.getByTestId("iconSvgExclamationSolid")
        ).toBeInTheDocument();
      });
    });

    describe("when `errors` prop is not passed", () => {
      it("does not display error message", () => {
        render(<Textarea {...defaultProps} />);

        const textarea = screen.getByLabelText(defaultProps.label);

        expect(textarea).not.toHaveAccessibleErrorMessage("Notes is required");
        expect(textarea).not.toHaveClass("border-red-300");
      });
    });

    describe("when `description` prop is passed", () => {
      const description = "This input does the thing";

      it("displays description", () => {
        render(<Textarea {...defaultProps} description={description} />);

        expect(
          screen.getByLabelText(defaultProps.label)
        ).toHaveAccessibleDescription(description);
      });
    });

    describe("when `labelHidden` prop is `true`", () => {
      it("adds `sr-only` CSS class to label", () => {
        render(<Textarea {...defaultProps} labelHidden />);

        expect(
          screen.getByText(defaultProps.label, { selector: "label" })
        ).toHaveClass("sr-only");
      });
    });

    describe("when `wrapperClassName` is passed", () => {
      it("adds classes to wrapper", () => {
        const { container } = render(
          <Textarea {...defaultProps} wrapperClassName="mt-4" />
        );

        expect(container.firstChild).toHaveClass("mt-4");
      });
    });
  });

  it("forwards ref", () => {
    let textareaEl: HTMLTextAreaElement;

    render(<Textarea {...defaultProps} ref={(el) => (textareaEl = el)} />);

    expect(textareaEl).toBeInTheDocument();
    expect(textareaEl.tagName).toBe("TEXTAREA");
  });
});
