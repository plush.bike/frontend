import { noop } from "lodash";
import { useForm } from "react-hook-form";

import NumberInput from "@/components/form/NumberInput";
import clockwiseIcon from "@/icons/clockwise.svg";
import counterclockwiseIcon from "@/icons/counterclockwise.svg";
import { FunctionComponent } from "@/types/react";
import { render, screen, within } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("NumberInput", () => {
  const defaultProps = {
    label: "Travel (mm)",
    onChange: noop,
  };

  describe("props", () => {
    it("renders `label` prop as label", () => {
      render(<NumberInput {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.label, { selector: "label" })
      ).toBeInTheDocument();
    });

    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <NumberInput
            {...defaultProps}
            errors={{
              type: "server",
              message: "Travel (mm) is required",
            }}
          />
        );

        const input = screen.getByLabelText(defaultProps.label);

        expect(input).toHaveAccessibleErrorMessage("Travel (mm) is required");
        expect(input).toHaveClass("border-red-300");
      });
    });

    describe("when `errors` prop is not passed", () => {
      it("does not display error message", () => {
        render(<NumberInput {...defaultProps} />);

        const input = screen.getByLabelText(defaultProps.label);

        expect(input).not.toHaveAccessibleErrorMessage(
          "Travel (mm) is required"
        );
        expect(input).not.toHaveClass("border-red-300");
      });
    });

    describe("when `description` prop is passed", () => {
      const description = "This input does the thing";

      it("displays description", () => {
        render(<NumberInput {...defaultProps} description={description} />);

        expect(
          screen.getByLabelText(defaultProps.label)
        ).toHaveAccessibleDescription(description);
      });
    });

    describe("when `labelHidden` prop is `true`", () => {
      it("adds `sr-only` CSS class to label", () => {
        render(<NumberInput {...defaultProps} labelHidden />);

        expect(
          screen.getByText(defaultProps.label, { selector: "label" })
        ).toHaveClass("sr-only");
      });
    });

    describe("when `wrapperClassName` prop is passed", () => {
      it("adds classes to wrapper", () => {
        const { container } = render(
          <NumberInput {...defaultProps} wrapperClassName="mt-4" />
        );

        expect(container.firstChild).toHaveClass("mt-4");
      });
    });

    describe("when `min` prop is passed", () => {
      it("disables decrement button when min has been reached", () => {
        render(<NumberInput {...defaultProps} min={0} />);

        expect(screen.getByLabelText("Decrement by 1")).toBeDisabled();
      });

      it("adds `min` attribute to input", () => {
        render(<NumberInput {...defaultProps} min={0} />);

        expect(screen.getByLabelText(defaultProps.label)).toHaveAttribute(
          "min",
          "0"
        );
      });

      it("does not allow manually typing a value lower than the min value", async () => {
        const { user } = render(<NumberInput {...defaultProps} min={0} />);

        const input = screen.getByLabelText(defaultProps.label);

        await user.type(input, "{backspace}-1");
        expect(input).toHaveValue(0);
      });

      describe("when `min` prop is changed", () => {
        it("changes input value if current value is less than min value", async () => {
          const NumberInputsWrapper: FunctionComponent = () => {
            type Inputs = {
              fooBar: number;
              fooBarBaz: number;
            };

            const { register, watch } = useForm<Inputs>({
              defaultValues: {
                fooBar: -5,
                fooBarBaz: -5,
              },
            });

            const fooBar = watch("fooBar");

            return (
              <>
                <NumberInput
                  label="Foo bar"
                  {...register("fooBar", { valueAsNumber: true })}
                />
                <NumberInput
                  label="Foo bar baz"
                  min={fooBar}
                  {...register("fooBarBaz", {
                    valueAsNumber: true,
                  })}
                />
              </>
            );
          };

          const { user } = render(<NumberInputsWrapper />);

          const fooBarInput = screen.getByLabelText("Foo bar");
          const fooBarBazInput = screen.getByLabelText("Foo bar baz");

          expect(fooBarBazInput).toHaveValue(-5);

          await user.type(fooBarInput, "{backspace}2");

          expect(fooBarInput).toHaveValue(-2);
        });
      });
    });

    describe("when `max` prop is passed", () => {
      it("disables increment button when max has been reached", async () => {
        const { user } = render(<NumberInput {...defaultProps} max={1} />);

        const incrementButton = screen.getByLabelText("Increment by 1");

        await user.click(incrementButton);

        expect(incrementButton).toBeDisabled();
      });

      it("adds `max` attribute to input", () => {
        render(<NumberInput {...defaultProps} max={1} />);

        expect(screen.getByLabelText(defaultProps.label)).toHaveAttribute(
          "max",
          "1"
        );
      });

      it("does not allow manually typing a value higher than the max value", async () => {
        const { user } = render(<NumberInput {...defaultProps} max={1} />);

        const input = screen.getByLabelText(defaultProps.label);

        await user.type(input, "{backspace}2");
        expect(input).toHaveValue(1);
      });

      describe("when `max` prop is changed", () => {
        it("changes input value if current value is greater than max value", async () => {
          const NumberInputsWrapper: FunctionComponent = () => {
            type Inputs = {
              lscMaxClicks: number;
              lscClicks: number;
            };

            const { register, watch } = useForm<Inputs>({
              defaultValues: {
                lscMaxClicks: 5,
                lscClicks: 5,
              },
            });

            const lscMaxClicks = watch("lscMaxClicks");

            return (
              <>
                <NumberInput
                  label="Low speed compression max clicks"
                  min={0}
                  {...register("lscMaxClicks", { valueAsNumber: true })}
                />
                <NumberInput
                  label="Low speed compression clicks"
                  min={0}
                  max={lscMaxClicks}
                  {...register("lscClicks", {
                    valueAsNumber: true,
                  })}
                />
              </>
            );
          };

          const { user } = render(<NumberInputsWrapper />);

          const lscMaxClicksInput = screen.getByLabelText(
            "Low speed compression max clicks"
          );
          const lscClicksInput = screen.getByLabelText(
            "Low speed compression clicks"
          );

          expect(lscClicksInput).toHaveValue(5);

          await user.type(lscMaxClicksInput, "{backspace}2");

          expect(lscClicksInput).toHaveValue(2);
        });
      });
    });

    describe("when `step` prop is passed", () => {
      it("increments and decrements input by passed step", async () => {
        const { user } = render(<NumberInput {...defaultProps} step={5} />);

        const input = screen.getByLabelText(defaultProps.label);
        const incrementButton = screen.getByLabelText("Increment by 5");

        await user.click(incrementButton);
        await user.click(incrementButton);

        expect(input).toHaveValue(10);

        await user.click(screen.getByLabelText("Decrement by 5"));

        expect(input).toHaveValue(5);
      });

      it("rounds to the nearest step", async () => {
        const { user } = render(<NumberInput {...defaultProps} step={5} />);

        const input = screen.getByLabelText(defaultProps.label);

        await user.type(input, "7");

        await user.click(screen.getByLabelText("Increment by 5"));

        expect(input).toHaveValue(10);

        await user.clear(input);
        await user.type(input, "13");

        await user.click(screen.getByLabelText("Decrement by 5"));

        expect(input).toHaveValue(10);
      });
    });

    describe("when `decrementLabel` prop is passed", () => {
      it("uses label for decrement button", async () => {
        const label = "Firmer";
        const { user } = render(
          <NumberInput {...defaultProps} decrementLabel={label} />
        );

        await user.click(screen.getByRole("button", { name: label }));

        expect(screen.getByLabelText(defaultProps.label)).toHaveValue(-1);
      });
    });

    describe("when `incrementLabel` prop is passed", () => {
      it("uses label for increment button", async () => {
        const label = "Softer";
        const { user } = render(
          <NumberInput {...defaultProps} incrementLabel={label} />
        );

        await user.click(screen.getByRole("button", { name: label }));

        expect(screen.getByLabelText(defaultProps.label)).toHaveValue(1);
      });
    });

    describe("when `decrementTooltipContent` prop is passed", () => {
      it("adds tooltip to decrement button", async () => {
        const content = "Clockwise";
        const { user } = render(
          <NumberInput {...defaultProps} decrementTooltipContent={content} />
        );

        await user.hover(
          screen.getByRole("button", { name: "Decrement by 1" })
        );

        expect(
          screen.getByRole("tooltip", { name: content })
        ).toBeInTheDocument();
      });
    });

    describe("when `incrementTooltipContent` prop is passed", () => {
      it("adds tooltip to increment button", async () => {
        const content = "Counterclockwise";
        const { user } = render(
          <NumberInput {...defaultProps} incrementTooltipContent={content} />
        );

        await user.hover(
          screen.getByRole("button", { name: "Increment by 1" })
        );

        expect(
          screen.getByRole("tooltip", { name: content })
        ).toBeInTheDocument();
      });
    });

    describe("when `decrementIcon` prop is passed", () => {
      it("adds icon to decrement button", () => {
        render(<NumberInput {...defaultProps} decrementIcon={clockwiseIcon} />);

        expect(
          within(
            screen.getByRole("button", { name: "Decrement by 1" })
          ).getByTestId("iconSvgClockwise")
        ).toBeInTheDocument();
      });
    });

    describe("when `incrementIcon` prop is passed", () => {
      it("adds icon to increment button", () => {
        render(
          <NumberInput {...defaultProps} incrementIcon={counterclockwiseIcon} />
        );

        expect(
          within(
            screen.getByRole("button", { name: "Increment by 1" })
          ).getByTestId("iconSvgCounterclockwise")
        ).toBeInTheDocument();
      });
    });
  });

  it("forwards ref", () => {
    let inputEl: HTMLInputElement;

    render(<NumberInput {...defaultProps} ref={(el) => (inputEl = el)} />);

    expect(inputEl).toBeInTheDocument();
    expect(inputEl.tagName).toBe("INPUT");
  });
});
