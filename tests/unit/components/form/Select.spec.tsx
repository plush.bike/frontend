import { FunctionComponent, useState } from "react";

import Select from "@/components/form/Select";
import { ListboxChangeEvent, ListboxOption } from "@/types/form";
import { render, within, screen, fireEvent } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("Select", () => {
  const defaultProps = {
    label: "Manufacturer",
    buttonLabel: "Select a manufacturer",
    options: [
      {
        id: "1",
        name: "Fox",
      },
      {
        id: "2",
        name: "RockShox",
      },
    ],
    value: null,
    onChange: vi.fn(),
  };

  describe("props", () => {
    it("renders `label` prop as label", () => {
      render(<Select {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.label, { selector: "label" })
      ).toBeInTheDocument();
    });

    it("renders `buttonLabel` as button label", () => {
      render(<Select {...defaultProps} />);

      expect(
        screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
        })
      ).toBeInTheDocument();
    });

    it("renders select options", async () => {
      const { user } = render(<Select {...defaultProps} />);

      await user.click(
        screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
        })
      );

      expect(
        screen.getByRole("option", { name: defaultProps.options[0].name })
      ).toBeInTheDocument();
      expect(
        screen.getByRole("option", { name: defaultProps.options[1].name })
      ).toBeInTheDocument();
    });

    describe("when `disabled` prop is `true`", () => {
      it("disables select", () => {
        render(<Select {...defaultProps} disabled />);

        expect(
          screen.getByRole("button", {
            name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
          })
        ).toBeDisabled();
      });
    });

    describe("when `loading` prop is `true`", () => {
      it("renders loading bars", async () => {
        const { user } = render(<Select {...defaultProps} loading />);

        await user.click(
          screen.getByRole("button", {
            name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
          })
        );

        const options = screen.getAllByRole("option");

        expect(options).toHaveLength(6);
        options.forEach((option) => {
          expect(option).toHaveAttribute("aria-disabled", "true");
          expect(option.firstChild).toHaveClass("animate-pulse");
        });
      });
    });

    describe("when `value` prop is passed", () => {
      it("default selects the option", async () => {
        const { user } = render(
          <Select {...defaultProps} value={defaultProps.options[0]} />
        );

        const toggleButton = screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.options[0].name}`,
        });

        expect(toggleButton).toBeInTheDocument();

        await user.click(toggleButton);

        expect(
          screen.getByRole("option", { selected: true })
        ).toHaveTextContent(defaultProps.options[0].name);
      });
    });

    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <Select
            {...defaultProps}
            errors={{
              type: "server",
              message: "Manufacturer is required.",
            }}
          />
        );

        expect(
          screen.getByRole("button", {
            name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
          })
        ).toHaveAccessibleErrorMessage("Manufacturer is required.");
      });
    });

    describe("when `errors` prop is not passed", () => {
      it("does not display error message", () => {
        render(<Select {...defaultProps} />);

        expect(
          screen.queryByText("Manufacturer is required.")
        ).not.toBeInTheDocument();
      });
    });

    describe("when `description` prop is passed", () => {
      const description = "This input does the thing";

      it("displays description", () => {
        render(<Select {...defaultProps} description={description} />);

        expect(
          screen.getByLabelText(defaultProps.label)
        ).toHaveAccessibleDescription(description);
      });
    });

    it("calls `onChange` prop when option is selected", async () => {
      const { user } = render(<Select {...defaultProps} />);

      await user.selectOptionFromSelect(
        screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
        }),
        defaultProps.options[0].name
      );

      expect(defaultProps.onChange).toHaveBeenCalledWith({
        target: {
          value: defaultProps.options[0],
        },
      });
    });
  });

  describe("when option is active", () => {
    it("adds background color to option", async () => {
      const { user } = render(<Select {...defaultProps} />);

      await user.click(
        screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
        })
      );

      fireEvent.pointerMove(
        screen.getByRole("option", { name: defaultProps.options[1].name })
      );

      expect(
        screen.getByRole("option", { name: defaultProps.options[1].name })
      ).toHaveClass("text-white", "bg-indigo-600");
    });
  });

  describe("when option is selected", () => {
    const SelectWrapper: FunctionComponent = () => {
      const [value, setValue] = useState<ListboxOption>(null);

      const handleOnChange = (event: ListboxChangeEvent) => {
        setValue(event.target.value);
      };

      return (
        <Select {...defaultProps} value={value} onChange={handleOnChange} />
      );
    };

    it("bolds the name and shows checkmark", async () => {
      const { user } = render(<SelectWrapper />);

      await user.selectOptionFromSelect(
        screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.buttonLabel}`,
        }),
        defaultProps.options[1].name
      );

      await user.click(
        screen.getByRole("button", {
          name: `${defaultProps.label} ${defaultProps.options[1].name}`,
        })
      );

      const selectedOption = screen.getByRole("option", { selected: true });

      expect(selectedOption.querySelector("span")).toHaveClass("font-semibold");
      expect(
        within(selectedOption).getByTestId("iconSvgCheckmark")
      ).toBeInTheDocument();
    });
  });
});
