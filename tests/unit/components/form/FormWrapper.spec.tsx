import React from "react";

import FormWrapper from "@/components/form/FormWrapper";
import { render, fireEvent, screen } from "tests/utils";

describe("FormWrapper", () => {
  describe("props", () => {
    describe("when `submit` event is fired", () => {
      it("calls handler passed as `onSubmit` prop", () => {
        const onSubmit = vi.fn();

        const { container } = render(
          <FormWrapper onSubmit={onSubmit}>Foo bar</FormWrapper>
        );

        fireEvent.submit(container.firstChild);

        expect(onSubmit).toHaveBeenCalled();
      });
    });
  });

  describe("when `submit` event is fired", () => {
    it("prevents default", () => {
      const { container } = render(<FormWrapper>Foo bar</FormWrapper>);

      const isPrevented = !fireEvent.submit(container.firstChild);

      expect(isPrevented).toBe(true);
    });
  });

  it("renders children", () => {
    render(<FormWrapper>Foo bar</FormWrapper>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
