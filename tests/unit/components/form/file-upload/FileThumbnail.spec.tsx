import FileThumbnail from "@/components/form/file-upload/FileThumbnail";
import { SavedFile } from "@/helpers/file-upload";
import { FileExtended } from "@/types/file-upload";
import { mockFile, render, screen } from "tests/utils";

describe("FileThumbnail", () => {
  const defaultProps = {
    file: new SavedFile({
      id: 1,
      src: "https://ik.imagekit.io/parts/1/foo-bar1.png",
    }),
    onRemove: vi.fn(),
  };

  it("renders thumbnail", () => {
    render(<FileThumbnail {...defaultProps} />);

    expect(screen.getByTestId("fileThumbnail")).toHaveStyle({
      backgroundImage: `url("${defaultProps.file.src}")`,
    });
  });

  describe("when file is removed", () => {
    it("calls `onRemove` prop", async () => {
      const { user } = render(<FileThumbnail {...defaultProps} />);

      await user.click(screen.getByRole("button", { name: "Remove file" }));

      expect(defaultProps.onRemove).toHaveBeenCalledWith(defaultProps.file);
    });
  });

  describe("when file is loading", () => {
    it("renders loading icon", () => {
      const file = {
        ...mockFile(),
        loading: true,
        preview:
          "blob:http://plush.test:3010/a3f96c77-4d20-469e-a737-4a82d3904b5b",
      } as FileExtended;

      render(<FileThumbnail {...defaultProps} file={file} />);

      expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
    });
  });

  describe("when file is not loading", () => {
    it("renders checkmark", () => {
      render(<FileThumbnail {...defaultProps} />);

      expect(screen.getByTestId("iconSvgCheckmark")).toBeInTheDocument();
    });
  });
});
