import FileUpload from "@/components/form/file-upload/FileUpload";
import {
  UploadPartImagesDocument,
  useUploadPartImagesMutation,
} from "@/graphql/part/mutations/upload-part-images.generated";
import { SavedFile } from "@/helpers/file-upload";
import { images } from "tests/unit/mock-data/upload-part-image";
import {
  fireEvent,
  mockFile,
  render,
  screen,
  within,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
} from "tests/utils";

describe("FileUpload", () => {
  const defaultProps = {
    label: "Images",
    onChange: vi.fn(),
    useMutation: useUploadPartImagesMutation,
    mutationKey: "uploadPartImages",
  };

  const mockFiles = [
    {
      file: mockFile({ name: "mock1.jpg" }),
      previewUrl:
        "blob:http://plush.test:3010/a3f96c77-4d20-469e-a737-4a82d3904b5b",
    },
    {
      file: mockFile({ name: "mock2.jpg" }),
      previewUrl:
        "blob:http://plush.test:3010/a3f54334-4d30-462e-a743-4a82d390432f",
    },
  ];

  const initialFiles = [
    new SavedFile({
      id: 1,
      src: "https://ik.imagekit.io/parts/1/foo-bar1.png",
    }),
    new SavedFile({
      id: 2,
      src: "https://ik.imagekit.io/parts/1/foo-bar2.png",
    }),
  ];

  const request = {
    query: UploadPartImagesDocument,
    variables: {
      files: mockFiles.map((mockFile) => mockFile.file),
    },
  };

  const optionsSuccess = {
    apollo: {
      mocks: [
        {
          request,
          result: {
            data: {
              [defaultProps.mutationKey]: images,
            },
          },
        },
      ],
    },
  };

  const fireDropEvent = () => {
    fireEvent.drop(screen.getByLabelText("Upload a file"), {
      dataTransfer: {
        files: mockFiles.map((mockFile) => mockFile.file),
        items: mockFiles.map((mockFile) => ({
          kind: "file",
          type: mockFile.file.type,
          getAsFile: () => mockFile.file,
        })),
        types: ["Files"],
      },
    });
  };

  const getThumbnailContainer = (index: number) => {
    return screen.queryByTestId(`fileThumbnailContainer${index}`);
  };

  const getThumbnail = (index: number) => {
    return within(getThumbnailContainer(index)).getByTestId("fileThumbnail");
  };

  const getThumbnailLoader = (index: number) => {
    return within(getThumbnailContainer(index)).getByTestId("iconSvgLoading");
  };

  const getThumbnailCheckmarkIcon = (index: number) => {
    return within(getThumbnailContainer(index)).getByTestId("iconSvgCheckmark");
  };

  const getThumbnailRemoveFileButton = (index: number) => {
    return within(getThumbnailContainer(index)).getByRole("button", {
      name: "Remove file",
    });
  };

  beforeAll(() => {
    window.URL.revokeObjectURL = vi.fn();
    window.URL.createObjectURL = vi
      .fn()
      .mockImplementation(
        (file: File) =>
          mockFiles.find((mockFile) => mockFile.file.name === file.name)
            .previewUrl
      );
  });

  describe("when files are added", () => {
    describe("when loading", () => {
      const options = {
        apollo: {
          mocks: [
            {
              request,
              loading: true,
            },
          ],
        },
      };

      it("shows thumbnail of files with loading icon", async () => {
        render(<FileUpload {...defaultProps} />, options);

        fireDropEvent();

        await waitForPromises();

        expect(getThumbnail(0)).toHaveStyle({
          backgroundImage: `url("${mockFiles[0].previewUrl}")`,
        });
        expect(getThumbnail(1)).toHaveStyle({
          backgroundImage: `url("${mockFiles[1].previewUrl}")`,
        });

        expect(getThumbnailLoader(0)).toBeInTheDocument();
        expect(getThumbnailLoader(1)).toBeInTheDocument();
      });
    });

    describe("when API call is successful", () => {
      it("shows thumbnail of files with checkmark icon", async () => {
        render(<FileUpload {...defaultProps} />, optionsSuccess);

        fireDropEvent();

        await waitForPromises();

        expect(getThumbnail(0)).toHaveStyle({
          backgroundImage: `url("${mockFiles[0].previewUrl}")`,
        });
        expect(getThumbnail(1)).toHaveStyle({
          backgroundImage: `url("${mockFiles[1].previewUrl}")`,
        });

        expect(getThumbnailCheckmarkIcon(0)).toBeInTheDocument();
        expect(getThumbnailCheckmarkIcon(1)).toBeInTheDocument();
      });

      it("calls `onChange` prop", async () => {
        render(<FileUpload {...defaultProps} />, optionsSuccess);

        fireDropEvent();

        await waitForPromises();

        expect(defaultProps.onChange).toHaveBeenCalledWith(
          images.map((image) => expect.objectContaining({ src: image.src }))
        );
      });
    });
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            message: "Validation failed for the field [uploadPartImages].",
            extensions: {
              validation: {
                "input.files.0": ["File must be less than 10MB."],
                "input.files.1": ["File must be less than 10MB."],
              },
              category: "validation",
            },
            path: ["uploadPartImages"],
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays generic error alert", async () => {
        render(<FileUpload {...defaultProps} />, optionsNetworkError);

        fireDropEvent();

        await waitForPromises();

        expect(
          screen.getByText("An error occurred, please try again.")
        ).toBeInTheDocument();
      });
    });

    describe("when error is a GraphQL error", () => {
      it("displays error messages", async () => {
        render(<FileUpload {...defaultProps} />, optionsGraphQLError);

        fireDropEvent();

        await waitForPromises();

        expect(
          screen.getByText(
            `${mockFiles[0].file.name}: File must be less than 10MB.`
          )
        ).toBeInTheDocument();
        expect(
          screen.getByText(
            `${mockFiles[1].file.name}: File must be less than 10MB.`
          )
        ).toBeInTheDocument();
      });
    });
  });

  describe("when `initialFiles` prop is passed", () => {
    it("shows thumbnail of files with checkmark icon", () => {
      render(<FileUpload {...defaultProps} initialFiles={initialFiles} />);

      expect(getThumbnail(0)).toHaveStyle({
        backgroundImage: `url("${initialFiles[0].src}")`,
      });
      expect(getThumbnail(1)).toHaveStyle({
        backgroundImage: `url("${initialFiles[1].src}")`,
      });

      expect(getThumbnailCheckmarkIcon(0)).toBeInTheDocument();
      expect(getThumbnailCheckmarkIcon(1)).toBeInTheDocument();
    });

    it("still shows initial files after successfully uploading new files", async () => {
      render(
        <FileUpload {...defaultProps} initialFiles={initialFiles} />,
        optionsSuccess
      );

      fireDropEvent();

      await waitForPromises();

      expect(getThumbnail(0)).toHaveStyle({
        backgroundImage: `url("${initialFiles[0].src}")`,
      });
      expect(getThumbnail(1)).toHaveStyle({
        backgroundImage: `url("${initialFiles[1].src}")`,
      });
      expect(getThumbnail(2)).toHaveStyle({
        backgroundImage: `url("${mockFiles[0].previewUrl}")`,
      });
      expect(getThumbnail(3)).toHaveStyle({
        backgroundImage: `url("${mockFiles[1].previewUrl}")`,
      });
    });
  });

  describe("when a file is removed", () => {
    it("removes thumbnail", async () => {
      const { user } = render(
        <FileUpload {...defaultProps} initialFiles={[initialFiles[0]]} />
      );

      await user.click(getThumbnailRemoveFileButton(0));

      expect(getThumbnailContainer(0)).not.toBeInTheDocument();
    });

    it("calls `onChange` prop", async () => {
      const { user } = render(
        <FileUpload {...defaultProps} initialFiles={[initialFiles[0]]} />
      );

      await user.click(getThumbnailRemoveFileButton(0));

      expect(defaultProps.onChange).toHaveBeenCalledWith([]);
    });
  });
});
