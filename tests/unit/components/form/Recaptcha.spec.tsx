import * as ReactReCAPTCHA from "react-google-recaptcha";

import Recaptcha from "@/components/form/Recaptcha";
import { mockComponent, render, screen } from "tests/utils";

describe("Recaptcha", () => {
  const defaultProps = {
    onChange: vi.fn(),
  };

  it("renders `react-google-recaptcha` component", () => {
    const { spy } = mockComponent({
      componentObject: ReactReCAPTCHA.default,
      componentKey: "render",
    });

    render(<Recaptcha {...defaultProps} />);

    expect(spy).toHaveBeenCalledWith(
      {
        sitekey: "6Lfff0wfAAAAAJRD1t9tnpngbWbh4du0sktWENbV",
        className: "md:scale-100 scale-[0.8] origin-top-left",
        onChange: expect.any(Function),
      },
      null
    );
  });

  it("calls `onChange` prop when ReactReCAPTCHA `onChange` is called", () => {
    const { getProps } = mockComponent({
      componentObject: ReactReCAPTCHA.default,
      componentKey: "render",
    });

    render(<Recaptcha {...defaultProps} />);

    const recaptchaResponse = "foobar";

    getProps().onChange(recaptchaResponse);

    expect(defaultProps.onChange).toHaveBeenCalledWith({
      target: { value: recaptchaResponse },
    });
  });

  it("renders error", () => {
    mockComponent({
      componentObject: ReactReCAPTCHA.default,
      componentKey: "render",
    });

    const message = "reCAPTCHA was invalid, please try again.";

    render(
      <Recaptcha
        {...defaultProps}
        errors={{
          type: "server",
          message,
        }}
      />
    );

    expect(screen.getByText(message)).toBeInTheDocument();
  });
});
