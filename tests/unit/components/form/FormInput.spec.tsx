import FormInput from "@/components/form/FormInput";
import envelopeIcon from "@/icons/envelope.svg";
import { render, screen } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("FormInput", () => {
  const defaultProps = {
    label: "Email",
    type: "email",
  };

  describe("props", () => {
    it("sets `value` prop as `value` attribute", () => {
      render(<FormInput {...defaultProps} defaultValue="test@testing.com" />);

      expect(screen.getByLabelText(defaultProps.label)).toHaveValue(
        "test@testing.com"
      );
    });

    it("renders `label` prop as label", () => {
      render(<FormInput {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.label, { selector: "label" })
      ).toBeInTheDocument();
    });

    it("sets `type` prop as `type` attribute", () => {
      render(<FormInput {...defaultProps} />);

      expect(screen.getByLabelText(defaultProps.label)).toHaveAttribute(
        "type",
        "email"
      );
    });

    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <FormInput
            {...defaultProps}
            errors={{
              type: "server",
              message: "Email is required",
            }}
          />
        );

        const input = screen.getByLabelText(defaultProps.label);

        expect(input).toHaveAccessibleErrorMessage("Email is required");
        expect(input).toHaveClass("border-red-300");
        expect(
          screen.getByTestId("iconSvgExclamationSolid")
        ).toBeInTheDocument();
      });
    });

    describe("when `errors` prop is not passed", () => {
      it("does not display error message", () => {
        render(<FormInput {...defaultProps} />);

        const input = screen.getByLabelText(defaultProps.label);

        expect(input).not.toHaveAccessibleErrorMessage("Email is required");
        expect(input).not.toHaveClass("border-red-300");
      });
    });

    describe("when `description` prop is passed", () => {
      const description = "This input does the thing";

      it("displays description", () => {
        render(<FormInput {...defaultProps} description={description} />);

        expect(
          screen.getByLabelText(defaultProps.label)
        ).toHaveAccessibleDescription(description);
      });
    });

    describe("when `labelHidden` prop is `true`", () => {
      it("adds `sr-only` CSS class to label", () => {
        render(<FormInput {...defaultProps} labelHidden />);

        expect(
          screen.getByText(defaultProps.label, { selector: "label" })
        ).toHaveClass("sr-only");
      });
    });

    describe("when `wrapperClassName` is passed", () => {
      it("adds classes to wrapper", () => {
        const { container } = render(
          <FormInput {...defaultProps} wrapperClassName="mt-4" />
        );

        expect(container.firstChild).toHaveClass("mt-4");
      });
    });

    describe("when `icon` prop is passed", () => {
      it("displays icon and adjusts input padding", () => {
        render(<FormInput {...defaultProps} icon={envelopeIcon} />);

        expect(screen.getByTestId("iconSvgEnvelope")).toBeInTheDocument();
        expect(screen.getByLabelText(defaultProps.label)).toHaveClass("pl-10");
      });
    });
  });

  it("forwards ref", () => {
    let inputEl: HTMLInputElement;

    render(<FormInput {...defaultProps} ref={(el) => (inputEl = el)} />);

    expect(inputEl).toBeInTheDocument();
    expect(inputEl.tagName).toBe("INPUT");
  });
});
