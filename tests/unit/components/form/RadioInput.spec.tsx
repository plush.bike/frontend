import RadioInput from "@/components/form/RadioInput";
import { render, screen } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("RadioInput", () => {
  const defaultProps = {
    label: "Email",
    value: "air",
    name: "airOrCoil",
  };

  describe("props", () => {
    it("sets `value` prop as `value` attribute", () => {
      render(<RadioInput {...defaultProps} />);

      // eslint-disable-next-line jest-dom/prefer-to-have-value
      expect(screen.getByLabelText(defaultProps.label)).toHaveAttribute(
        "value",
        defaultProps.value
      );
    });

    it("renders `label` prop as label", () => {
      render(<RadioInput {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.label, { selector: "label" })
      ).toBeInTheDocument();
    });
  });

  it("forwards ref", () => {
    let inputEl: HTMLInputElement;

    render(<RadioInput {...defaultProps} ref={(el) => (inputEl = el)} />);

    expect(inputEl).toBeInTheDocument();
    expect(inputEl.tagName).toBe("INPUT");
  });
});
