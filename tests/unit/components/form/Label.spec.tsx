import Label from "@/components/form/Label";
import { render, screen } from "tests/utils";

describe("Label", () => {
  describe("props", () => {
    it("merges `className`", () => {
      const { container } = render(<Label className="mt-4">Foo bar</Label>);

      expect(container.firstChild).toHaveClass("mt-4");
    });

    describe("when `hidden` prop is `true`", () => {
      it("adds `sr-only` class", () => {
        const { container } = render(<Label hidden>Foo bar</Label>);

        expect(container.firstChild).toHaveClass("sr-only");
      });
    });
  });

  it("renders children", () => {
    render(<Label>Foo bar</Label>);

    expect(
      screen.getByText("Foo bar", { selector: "label" })
    ).toBeInTheDocument();
  });
});
