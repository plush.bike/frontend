import { FunctionComponent, useState } from "react";

import Toggle from "@/components/form/Toggle";
import { render, screen } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));

describe("Toggle", () => {
  const defaultProps = {
    label: "Active",
    onChange: vi.fn(),
    value: false,
  };

  describe("props", () => {
    it("renders `label` prop as label", () => {
      render(<Toggle {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.label, { selector: "label" })
      ).toBeInTheDocument();
    });

    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <Toggle
            {...defaultProps}
            errors={{
              type: "server",
              message: "Settings record can not be active.",
            }}
          />
        );

        const errorMessage = screen.getByText(
          "Settings record can not be active."
        );

        expect(errorMessage).toBeInTheDocument();
        expect(errorMessage).toHaveAttribute("id", "active-1-error");
      });
    });

    describe("when `errors` prop is not passed", () => {
      it("does not display error message", () => {
        render(<Toggle {...defaultProps} />);

        expect(
          screen.queryByText("Settings record can not be active.")
        ).not.toBeInTheDocument();
      });
    });

    describe("when `labelHidden` prop is `true`", () => {
      it("adds `sr-only` CSS class to label", () => {
        render(<Toggle {...defaultProps} labelHidden />);

        expect(
          screen.getByText(defaultProps.label, { selector: "label" })
        ).toHaveClass("sr-only");
      });
    });

    describe("when `wrapperClassName` is passed", () => {
      it("adds classes to wrapper", () => {
        const { container } = render(
          <Toggle {...defaultProps} wrapperClassName="mt-4" />
        );

        expect(container.firstChild).toHaveClass("mt-4");
      });
    });
  });

  describe("when toggle is clicked", () => {
    it("changes toggle value", async () => {
      const ToggleWrapper: FunctionComponent = () => {
        const [value, setValue] = useState(false);

        const handleOnChange = (event: { target: { value: boolean } }) => {
          setValue(event.target.value);
        };

        return (
          <Toggle {...defaultProps} value={value} onChange={handleOnChange} />
        );
      };

      const { user } = render(<ToggleWrapper />);

      const toggle = screen.getByRole("switch", { name: defaultProps.label });

      expect(toggle).not.toBeChecked();

      await user.click(toggle);

      expect(toggle).toBeChecked();
    });
  });
});
