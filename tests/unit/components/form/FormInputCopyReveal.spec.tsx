import FormInputCopyReveal from "@/components/form/FormInputCopyReveal";
import { render, screen, fireEvent } from "tests/utils";

describe("FormInputCopyReveal", () => {
  const defaultProps = {
    label: "Personal access token",
    value: "1|CsQ1t6gZpJ1N4hxEB5Qr7fUvyb3DL8zUM5Rdmu6M",
  };

  it("renders input with hidden value", () => {
    render(<FormInputCopyReveal {...defaultProps} />);

    expect(screen.getByLabelText(defaultProps.label)).toHaveValue(
      "********************"
    );
  });

  describe("when input is focused", () => {
    it("calls `select` on input", async () => {
      const { user } = render(<FormInputCopyReveal {...defaultProps} />);

      const input = screen.getByLabelText(defaultProps.label);
      const spy = vi.spyOn(input as HTMLInputElement, "select");

      await user.click(input);

      expect(spy).toHaveBeenCalled();
    });
  });

  describe("when value is manually copied", () => {
    it("calls `clipboardData.setData`", () => {
      render(<FormInputCopyReveal {...defaultProps} />);

      const event = {
        clipboardData: {
          setData: vi.fn(),
        },
      };

      fireEvent.copy(screen.getByLabelText(defaultProps.label), event);

      expect(event.clipboardData.setData).toHaveBeenCalledWith(
        "text/plain",
        defaultProps.value
      );
    });
  });

  describe("when `Reveal value` button is clicked and then `Hide value` button is clicked", () => {
    it("reveals value then hides it again", async () => {
      const { user } = render(<FormInputCopyReveal {...defaultProps} />);

      await user.click(screen.getByRole("button", { name: "Reveal value" }));

      expect(screen.getByLabelText(defaultProps.label)).toHaveValue(
        defaultProps.value
      );

      await user.click(screen.getByRole("button", { name: "Hide value" }));

      expect(screen.getByLabelText(defaultProps.label)).toHaveValue(
        "********************"
      );
    });
  });

  describe("when `errors` prop is passed", () => {
    it("displays error message", () => {
      render(
        <FormInputCopyReveal
          {...defaultProps}
          errors={{
            type: "server",
            message: "This field is required",
          }}
        />
      );

      const input = screen.getByLabelText(defaultProps.label);

      expect(input).toHaveAccessibleErrorMessage("This field is required");
      expect(input).toHaveClass("border-red-300");
    });
  });

  describe("when `description` prop is passed", () => {
    const description = "This input does the thing";

    it("displays description", () => {
      render(
        <FormInputCopyReveal {...defaultProps} description={description} />
      );

      expect(
        screen.getByLabelText(defaultProps.label)
      ).toHaveAccessibleDescription(description);
    });
  });

  describe("when `wrapperClassName` is passed", () => {
    it("adds classes to wrapper", () => {
      const { container } = render(
        <FormInputCopyReveal {...defaultProps} wrapperClassName="mt-4" />
      );

      expect(container.firstChild).toHaveClass("mt-4");
    });
  });

  it("forwards ref", () => {
    let inputEl: HTMLInputElement;

    render(
      <FormInputCopyReveal {...defaultProps} ref={(el) => (inputEl = el)} />
    );

    expect(inputEl).toBeInTheDocument();
    expect(inputEl.tagName).toBe("INPUT");
  });
});
