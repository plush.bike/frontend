import React from "react";

import FormInputWrapper from "@/components/form/FormInputWrapper";
import { render, screen } from "tests/utils";

describe("FormInputWrapper", () => {
  const defaultProps = { id: "email" };

  describe("props", () => {
    describe("when `errors` prop is passed", () => {
      it("displays error message", () => {
        render(
          <FormInputWrapper
            {...defaultProps}
            errors={{
              type: "server",
              message: "Email is required",
            }}
          >
            Foo bar
          </FormInputWrapper>
        );

        const errorMessage = screen.getByText("Email is required");

        expect(errorMessage).toBeInTheDocument();
        expect(errorMessage).toHaveAttribute("id", "email-error");
      });
    });

    describe("when `errors` prop is an array", () => {
      it("displays multiple error messages", () => {
        render(
          <FormInputWrapper
            {...defaultProps}
            errors={[
              {
                type: "server",
                message: "Email is required",
              },
              {
                type: "server",
                message: "Email is invalid",
              },
            ]}
          >
            Foo bar
          </FormInputWrapper>
        );

        expect(screen.getByText("Email is required")).toBeInTheDocument();
        expect(screen.getByText("Email is invalid")).toBeInTheDocument();
      });
    });

    describe("when `errors` prop is not passed", () => {
      it("does not display error message", () => {
        render(<FormInputWrapper {...defaultProps}>Foo bar</FormInputWrapper>);

        expect(screen.queryByText("Email is required")).not.toBeInTheDocument();
      });
    });

    describe("when `className` is passed", () => {
      it("adds `className` to wrapper", () => {
        const { container } = render(
          <FormInputWrapper {...defaultProps} className="mt-4">
            Foo bar
          </FormInputWrapper>
        );

        expect(container.firstChild).toHaveClass("mt-4");
      });
    });

    describe("when `description` prop is passed", () => {
      const description = "This input does the thing";

      it.each`
        descriptionProp
        ${description}
        ${() => <>{description}</>}
      `("renders description", ({ descriptionProp }) => {
        render(
          <FormInputWrapper {...defaultProps} description={descriptionProp}>
            Foo bar
          </FormInputWrapper>
        );

        expect(screen.getByText(description)).toBeInTheDocument();
      });
    });

    describe("when `descriptionClassName` prop is passed", () => {
      const description = "This input does the thing";
      const cssClass = "foo-bar";

      it("adds class to description", () => {
        render(
          <FormInputWrapper
            {...defaultProps}
            description={description}
            descriptionClassName={cssClass}
          >
            Foo bar
          </FormInputWrapper>
        );

        expect(screen.getByText(description)).toHaveClass("foo-bar");
      });
    });
  });

  it("renders children", () => {
    render(<FormInputWrapper {...defaultProps}>Foo bar</FormInputWrapper>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
