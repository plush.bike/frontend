import DateTime from "@/components/DateTime";
import { mockTimezone, render, screen } from "tests/utils";

describe("DateTime", () => {
  beforeEach(() => {
    mockTimezone();
  });

  describe("when date time is valid", () => {
    it("formats and renders date time", () => {
      render(<DateTime dateTime="2022-09-07 23:22:22" />);

      expect(screen.getByText("Sep 7, 2022, 4:22 PM")).toBeInTheDocument();
    });
  });

  describe("when date time is not valid", () => {
    it("renders null value", () => {
      render(<DateTime dateTime={null} />);

      expect(screen.getByText("Never")).toBeInTheDocument();
    });
  });
});
