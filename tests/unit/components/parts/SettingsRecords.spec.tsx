import {
  useFieldArray,
  UseFieldArrayReturn,
  useForm,
  UseFormReturn,
} from "react-hook-form";

import SettingsRecords from "@/components/parts/SettingsRecords";
import { AirOrCoilType } from "@/types/graphql";
import { FunctionComponent } from "@/types/react";
import { render, within, screen } from "tests/utils";

describe("SettingsRecords", () => {
  type Inputs = {
    lscMaxClicks: number;
    hscMaxClicks: number;
    lsrMaxClicks: number;
    hsrMaxClicks: number;
    airOrCoil: AirOrCoilType;
    settingsRecords: {
      nickname: string;
      lscClicks: number;
      hscClicks: number;
      lsrClicks: number;
      hsrClicks: number;
      volumeSpacers: number;
      psi: number;
      sag: number;
      springRate: number;
      preloadAdjust: number;
    }[];
    activeSettingsRecord: number;
  };

  type WrapperProps = {
    defaultValues?: {
      lscMaxClicks?: number;
      hscMaxClicks?: number;
      lsrMaxClicks?: number;
      hsrMaxClicks?: number;
      airOrCoil?: AirOrCoilType;
      settingsRecords: {
        nickname?: string;
        lscClicks?: number;
        hscClicks?: number;
        lsrClicks?: number;
        hsrClicks?: number;
        volumeSpacers?: number;
        psi?: number;
        sag?: number;
        springRate?: number;
        preloadAdjust?: number;
      }[];
      activeSettingsRecord?: number;
    };
  };

  const Wrapper: FunctionComponent<WrapperProps> = ({ defaultValues }) => {
    const useFormReturn = useForm<Inputs>({ defaultValues });

    const useFieldArrayReturn = useFieldArray<Inputs, "settingsRecords", "key">(
      {
        control: useFormReturn.control,
        name: "settingsRecords",
        keyName: "key",
      }
    );

    return (
      <SettingsRecords
        useFormReturn={useFormReturn as unknown as UseFormReturn}
        useFieldArrayReturn={
          useFieldArrayReturn as unknown as UseFieldArrayReturn<
            Record<string, unknown>,
            never,
            "key"
          >
        }
      />
    );
  };

  describe("when there are no settings records", () => {
    it("renders empty state", () => {
      render(<Wrapper />);

      expect(
        screen.getByRole("heading", { name: "No settings records" })
      ).toBeInTheDocument();
      expect(
        screen.getByText(
          "Create a settings record to help keep track of the settings you like."
        )
      ).toBeInTheDocument();
      expect(
        screen.getByRole("button", { name: "New settings record" })
      ).toBeInTheDocument();
    });

    describe("when `New settings record` button is clicked", () => {
      it("adds a new settings record", async () => {
        const { user } = render(
          <Wrapper
            defaultValues={{
              airOrCoil: AirOrCoilType.Air,
              settingsRecords: [],
            }}
          />
        );

        await user.click(
          screen.getByRole("button", { name: "New settings record" })
        );

        expect(
          screen.getByRole("switch", { name: "Active" })
        ).toBeInTheDocument();
        expect(screen.getByLabelText("Nickname")).toBeInTheDocument();
        expect(screen.getByLabelText("Volume spacers")).toBeInTheDocument();
        expect(
          screen.getByLabelText("Low speed compression clicks")
        ).toBeInTheDocument();
        expect(
          screen.getByLabelText("High speed compression clicks")
        ).toBeInTheDocument();
        expect(
          screen.getByLabelText("Low speed rebound clicks")
        ).toBeInTheDocument();
        expect(
          screen.getByLabelText("High speed rebound clicks")
        ).toBeInTheDocument();
        expect(screen.getByLabelText("Notes")).toBeInTheDocument();
      });
    });

    describe(`when \`airOrCoil\` is \`${AirOrCoilType.Air}\``, () => {
      it("renders `PSI` and `Volume spacers` fields", async () => {
        const { user } = render(
          <Wrapper
            defaultValues={{
              airOrCoil: AirOrCoilType.Air,
              settingsRecords: [],
            }}
          />
        );

        await user.click(
          screen.getByRole("button", { name: "New settings record" })
        );

        expect(screen.getByLabelText("PSI")).toBeInTheDocument();
        expect(screen.getByLabelText("Sag (mm)")).toBeInTheDocument();
      });
    });

    describe(`when \`airOrCoil\` is \`${AirOrCoilType.Coil}\``, () => {
      it("renders `Spring rate` and `Preload adjust` fields", async () => {
        const { user } = render(
          <Wrapper
            defaultValues={{
              airOrCoil: AirOrCoilType.Coil,
              settingsRecords: [],
            }}
          />
        );

        await user.click(
          screen.getByRole("button", { name: "New settings record" })
        );

        expect(screen.getByLabelText("Spring rate")).toBeInTheDocument();
        expect(screen.getByLabelText("Preload adjust")).toBeInTheDocument();
      });
    });
  });

  describe("when there are multiple settings records", () => {
    it("renders multiple settings records", () => {
      render(
        <Wrapper
          defaultValues={{
            airOrCoil: AirOrCoilType.Air,
            settingsRecords: [{}, {}],
          }}
        />
      );

      expect(screen.getAllByRole("switch", { name: "Active" })).toHaveLength(2);
      expect(screen.getAllByLabelText("Nickname")).toHaveLength(2);
      expect(screen.getAllByLabelText("PSI")).toHaveLength(2);
      expect(screen.getAllByLabelText("Sag (mm)")).toHaveLength(2);
      expect(screen.getAllByLabelText("Volume spacers")).toHaveLength(2);
      expect(
        screen.getAllByLabelText("Low speed compression clicks")
      ).toHaveLength(2);
      expect(
        screen.getAllByLabelText("High speed compression clicks")
      ).toHaveLength(2);
      expect(screen.getAllByLabelText("Low speed rebound clicks")).toHaveLength(
        2
      );
      expect(
        screen.getAllByLabelText("High speed rebound clicks")
      ).toHaveLength(2);
      expect(screen.getAllByLabelText("Notes")).toHaveLength(2);
    });

    it("only allows one settings record to be active", async () => {
      const { user } = render(
        <Wrapper
          defaultValues={{
            settingsRecords: [{}, {}],
            activeSettingsRecord: 0,
          }}
        />
      );

      expect(
        within(screen.getByTestId("settingsRecord0")).getByRole("switch", {
          name: "Active",
          checked: true,
        })
      ).toBeInTheDocument();
      expect(
        within(screen.getByTestId("settingsRecord1")).getByRole("switch", {
          name: "Active",
          checked: false,
        })
      ).toBeInTheDocument();

      await user.click(
        within(screen.getByTestId("settingsRecord1")).getByRole("switch", {
          name: "Active",
          checked: false,
        })
      );

      expect(
        within(screen.getByTestId("settingsRecord0")).getByRole("switch", {
          name: "Active",
          checked: false,
        })
      ).toBeInTheDocument();
      expect(
        within(screen.getByTestId("settingsRecord1")).getByRole("switch", {
          name: "Active",
          checked: true,
        })
      ).toBeInTheDocument();
    });
  });

  it("asks user to confirm before deleting settings record", async () => {
    const { user } = render(<Wrapper />);

    await user.click(
      screen.getByRole("button", { name: "New settings record" })
    );
    await user.click(
      screen.getByRole("button", { name: "Delete settings record" })
    );

    const modal = screen.getByRole("dialog", {
      name: "Delete settings record",
    });

    expect(modal).toBeInTheDocument();

    await user.click(within(modal).getByRole("button", { name: "Delete" }));

    expect(
      screen.queryByRole("switch", { name: "Active" })
    ).not.toBeInTheDocument();
  });

  it("controls LSC, HSC, LSR, HSR plus buttons disabled state based on the corresponding max clicks setting", async () => {
    const getSettingPlusButton = (fieldLabel: string, buttonLabel: string) => {
      return within(screen.getByLabelText(fieldLabel).parentElement).getByRole(
        "button",
        {
          name: buttonLabel,
        }
      );
    };

    const { user } = render(
      <Wrapper
        defaultValues={{
          lscMaxClicks: 1,
          hscMaxClicks: 1,
          lsrMaxClicks: 1,
          hsrMaxClicks: 1,
          settingsRecords: [{}],
        }}
      />
    );

    expect(
      getSettingPlusButton("Low speed compression clicks", "Softer")
    ).toBeEnabled();
    expect(
      getSettingPlusButton("High speed compression clicks", "Softer")
    ).toBeEnabled();
    expect(
      getSettingPlusButton("Low speed rebound clicks", "Faster")
    ).toBeEnabled();
    expect(
      getSettingPlusButton("High speed rebound clicks", "Faster")
    ).toBeEnabled();

    await user.click(
      getSettingPlusButton("Low speed compression clicks", "Softer")
    );
    await user.click(
      getSettingPlusButton("High speed compression clicks", "Softer")
    );
    await user.click(
      getSettingPlusButton("Low speed rebound clicks", "Faster")
    );
    await user.click(
      getSettingPlusButton("High speed rebound clicks", "Faster")
    );

    expect(
      getSettingPlusButton("Low speed compression clicks", "Softer")
    ).toBeDisabled();
    expect(
      getSettingPlusButton("High speed compression clicks", "Softer")
    ).toBeDisabled();
    expect(
      getSettingPlusButton("Low speed rebound clicks", "Faster")
    ).toBeDisabled();
    expect(
      getSettingPlusButton("High speed rebound clicks", "Faster")
    ).toBeDisabled();
  });
});
