import { ApolloError } from "@apollo/client";
import { GraphQLError } from "graphql";

import CreateEdit from "@/components/parts/forks/CreateEdit";
import { ManufacturersDocument } from "@/graphql/manufacturer/queries/manufacturers.generated";
import { AirOrCoilType } from "@/types/graphql";
import { manufacturers } from "tests/unit/mock-data/manufacturer";
import { render, screen, waitForPromises } from "tests/utils";

describe("CreateEdit", () => {
  const defaultProps = {
    onSubmit: vi.fn(),
  };

  const manufacturersRequest = {
    query: ManufacturersDocument,
  };

  const optionsAPISuccess = {
    apollo: {
      mocks: [
        {
          request: manufacturersRequest,
          result: {
            data: {
              manufacturers: {
                data: manufacturers,
              },
            },
          },
        },
      ],
    },
  };

  const expectFieldsToHaveValues = ({
    name,
    manufacturer,
    lscMaxClicks,
    hscMaxClicks,
    lsrMaxClicks,
    hsrMaxClicks,
    travel,
    active,
    nickname,
    lscClicks,
    hscClicks,
    lsrClicks,
    hsrClicks,
    volumeSpacers,
    psi,
    sag,
    notes,
  }: {
    name: string;
    manufacturer: string;
    lscMaxClicks: number;
    hscMaxClicks: number;
    lsrMaxClicks: number;
    hsrMaxClicks: number;
    travel: number;
    active: boolean;
    nickname: string;
    lscClicks: number;
    hscClicks: number;
    lsrClicks: number;
    hsrClicks: number;
    volumeSpacers: number;
    psi: number;
    sag: number;
    notes: string;
  }) => {
    expect(screen.getByLabelText("Manufacturer")).toHaveTextContent(
      manufacturer
    );
    expect(screen.getByLabelText("Name")).toHaveValue(name);
    expect(
      screen.getByLabelText("Low speed compression max clicks")
    ).toHaveValue(lscMaxClicks);
    expect(
      screen.getByLabelText("High speed compression max clicks")
    ).toHaveValue(hscMaxClicks);
    expect(screen.getByLabelText("Low speed rebound max clicks")).toHaveValue(
      lsrMaxClicks
    );
    expect(screen.getByLabelText("High speed rebound max clicks")).toHaveValue(
      hsrMaxClicks
    );
    expect(screen.getByLabelText("Travel (mm)")).toHaveValue(travel);

    if (active) {
      expect(screen.getByRole("switch", { name: "Active" })).toBeChecked();
    } else {
      expect(screen.getByRole("switch", { name: "Active" })).not.toBeChecked();
    }

    expect(screen.getByLabelText("Nickname")).toHaveValue(nickname);
    expect(screen.getByLabelText("PSI")).toHaveValue(psi);
    expect(screen.getByLabelText("Sag (mm)")).toHaveValue(sag);
    expect(screen.getByLabelText("Volume spacers")).toHaveValue(volumeSpacers);
    expect(screen.getByLabelText("Low speed compression clicks")).toHaveValue(
      lscClicks
    );
    expect(screen.getByLabelText("High speed compression clicks")).toHaveValue(
      hscClicks
    );
    expect(screen.getByLabelText("Low speed rebound clicks")).toHaveValue(
      lsrClicks
    );
    expect(screen.getByLabelText("High speed rebound clicks")).toHaveValue(
      hsrClicks
    );
    expect(screen.getByLabelText("Notes")).toHaveValue(notes);
  };

  describe("when no default values are passed", () => {
    it("renders expected fields with default values", async () => {
      render(<CreateEdit {...defaultProps} />, optionsAPISuccess);

      await waitForPromises();

      expectFieldsToHaveValues({
        name: "",
        manufacturer: "Select a manufacturer",
        lscMaxClicks: 0,
        hscMaxClicks: 0,
        lsrMaxClicks: 0,
        hsrMaxClicks: 0,
        travel: 150,
        active: true,
        nickname: "",
        lscClicks: 0,
        hscClicks: 0,
        lsrClicks: 0,
        hsrClicks: 0,
        volumeSpacers: 0,
        psi: 0,
        sag: 0,
        notes: "",
      });
    });
  });

  describe("when default values are passed", () => {
    it("renders expected fields with passed values", async () => {
      const defaultValues = {
        name: "38",
        manufacturer: {
          id: "1",
          name: "Fox",
        },
        lscMaxClicks: 12,
        hscMaxClicks: 12,
        lsrMaxClicks: 12,
        hsrMaxClicks: 12,
        travel: 170,
        airOrCoil: AirOrCoilType.Air,
        settingsRecords: [
          {
            nickname: "Firm setup",
            lscClicks: 8,
            hscClicks: 9,
            lsrClicks: 7,
            hsrClicks: 6,
            volumeSpacers: 4,
            psi: 80,
            sag: 20,
            notes: "Good in dry dusty conditions",
          },
        ],
        activeSettingsRecord: 0,
      };

      render(
        <CreateEdit {...defaultProps} defaultValues={defaultValues} />,
        optionsAPISuccess
      );

      await waitForPromises();

      expectFieldsToHaveValues({
        name: defaultValues.name,
        manufacturer: defaultValues.manufacturer.name,
        lscMaxClicks: defaultValues.lscMaxClicks,
        hscMaxClicks: defaultValues.hscMaxClicks,
        lsrMaxClicks: defaultValues.lsrMaxClicks,
        hsrMaxClicks: defaultValues.hsrMaxClicks,
        travel: defaultValues.travel,
        active: true,
        nickname: defaultValues.settingsRecords[0].nickname,
        lscClicks: defaultValues.settingsRecords[0].lscClicks,
        hscClicks: defaultValues.settingsRecords[0].hscClicks,
        lsrClicks: defaultValues.settingsRecords[0].lsrClicks,
        hsrClicks: defaultValues.settingsRecords[0].hsrClicks,
        volumeSpacers: defaultValues.settingsRecords[0].volumeSpacers,
        psi: defaultValues.settingsRecords[0].psi,
        sag: defaultValues.settingsRecords[0].sag,
        notes: defaultValues.settingsRecords[0].notes,
      });
    });
  });

  describe(`when \`Air or coil\` is \`${AirOrCoilType.Coil}\` and then changed back to \`${AirOrCoilType.Air}\``, () => {
    it("renders `Spring rate` and `Preload adjust` fields and then `PSI` and `Volume spacers` fields", async () => {
      const defaultValues = {
        name: "38",
        manufacturer: {
          id: "1",
          name: "Fox",
        },
        lscMaxClicks: 12,
        hscMaxClicks: 12,
        lsrMaxClicks: 12,
        hsrMaxClicks: 12,
        travel: 170,
        airOrCoil: AirOrCoilType.Coil,
        settingsRecords: [
          {
            nickname: "Firm setup",
            lscClicks: 8,
            hscClicks: 9,
            lsrClicks: 7,
            hsrClicks: 6,
            springRate: 450,
            preloadAdjust: 10,
            volumeSpacers: 4,
            psi: 80,
            sag: 20,
            notes: "Good in dry dusty conditions",
          },
        ],
        activeSettingsRecord: 0,
      };

      const { user } = render(
        <CreateEdit {...defaultProps} defaultValues={defaultValues} />,
        optionsAPISuccess
      );

      await waitForPromises();

      expect(screen.getByLabelText("Spring rate")).toHaveValue(
        defaultValues.settingsRecords[0].springRate
      );
      expect(screen.getByLabelText("Preload adjust")).toHaveValue(
        defaultValues.settingsRecords[0].preloadAdjust
      );

      await user.click(screen.getByRole("radio", { name: "Air" }));

      expect(screen.getByLabelText("PSI")).toHaveValue(
        defaultValues.settingsRecords[0].psi
      );
      expect(screen.getByLabelText("Volume spacers")).toHaveValue(
        defaultValues.settingsRecords[0].volumeSpacers
      );
    });
  });

  describe("validation", () => {
    it("displays validation for required fields", async () => {
      const { user } = render(
        <CreateEdit {...defaultProps} />,
        optionsAPISuccess
      );

      await waitForPromises();

      await user.click(screen.getByRole("button", { name: "Submit" }));

      await waitForPromises();

      expect(
        await screen.findByRole("button", {
          name: "Manufacturer Select a manufacturer",
        })
      ).toHaveAccessibleErrorMessage("Please select a manufacturer.");
      expect(await screen.findByLabelText("Name")).toHaveAccessibleErrorMessage(
        "Please enter the name of your fork."
      );
    });

    describe("when there is a network error", () => {
      it("displays error alert", async () => {
        const { user } = render(
          <CreateEdit
            onSubmit={vi.fn().mockRejectedValue("An error occurred")}
          />,
          optionsAPISuccess
        );

        await waitForPromises();

        await user.selectOptionFromSelect(
          screen.getByRole("button", {
            name: "Manufacturer Select a manufacturer",
          }),
          manufacturers[0].name
        );

        await user.type(await screen.findByLabelText("Name"), "36");

        await user.click(screen.getByRole("button", { name: "Submit" }));

        await waitForPromises();

        expect(
          await screen.findByText("An error occurred, please try again.")
        ).toBeInTheDocument();
      });
    });

    describe("when there is a GraphQL error", () => {
      it("parses and sets error messages", async () => {
        const { user } = render(
          <CreateEdit
            onSubmit={vi.fn().mockRejectedValue(
              new ApolloError({
                graphQLErrors: [
                  new GraphQLError(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    {
                      validation: {
                        "input.manufacturer": ["Manufacturer is invalid."],
                        "input.name": ["Name is invalid."],
                      },
                    }
                  ),
                ],
              })
            )}
          />,
          optionsAPISuccess
        );

        await waitForPromises();

        await user.selectOptionFromSelect(
          screen.getByRole("button", {
            name: "Manufacturer Select a manufacturer",
          }),
          manufacturers[0].name
        );

        await user.type(await screen.findByLabelText("Name"), "36");

        await user.click(screen.getByRole("button", { name: "Submit" }));

        await waitForPromises();

        expect(
          await screen.findByRole("button", {
            name: `Manufacturer ${manufacturers[0].name}`,
          })
        ).toHaveAccessibleErrorMessage("Manufacturer is invalid.");
        expect(
          await screen.findByLabelText("Name")
        ).toHaveAccessibleErrorMessage("Name is invalid.");
      });
    });
  });
});
