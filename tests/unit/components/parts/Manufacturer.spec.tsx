import Manufacturer from "@/components/parts/Manufacturer";
import { ManufacturersDocument } from "@/graphql/manufacturer/queries/manufacturers.generated";
import { updateToastError } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { manufacturers } from "tests/unit/mock-data/manufacturer";
import { mockNetworkError, render, waitForPromises, screen } from "tests/utils";

vi.mock("@reach/auto-id", () => ({
  useId: () => 1,
}));
vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("Manufacturer", () => {
  const defaultProps = {
    onChange: vi.fn(),
  };

  const request = {
    query: ManufacturersDocument,
  };

  const optionsAPISuccess = {
    apollo: {
      mocks: [
        {
          request,
          result: {
            data: {
              manufacturers: {
                data: manufacturers,
              },
            },
          },
        },
      ],
    },
  };

  describe("props", () => {
    describe("onChange", () => {
      describe("when manufacturer is selected", () => {
        it("calls `onChange`", async () => {
          const { user } = render(
            <Manufacturer {...defaultProps} />,
            optionsAPISuccess
          );

          await waitForPromises();

          const optionToSelect = manufacturers[0];

          await user.selectOptionFromSelect(
            screen.getByRole("button", {
              name: "Manufacturer Select a manufacturer",
            }),
            optionToSelect.name
          );

          expect(defaultProps.onChange).toHaveBeenCalledWith({
            target: {
              value: optionToSelect,
            },
          });
        });
      });
    });

    describe("error", () => {
      it("renders error", async () => {
        render(
          <Manufacturer
            {...defaultProps}
            errors={{
              type: "server",
              message: "Manufacturer is required.",
            }}
          />,
          optionsAPISuccess
        );

        await waitForPromises();

        const errorMessage = screen.getByText("Manufacturer is required.");

        expect(errorMessage).toBeInTheDocument();
        expect(errorMessage).toHaveAttribute("id", "manufacturer-1-error");
      });
    });

    describe("value", () => {
      it("selects option with matching value", async () => {
        const selectedOption = manufacturers[0];

        const { user } = render(
          <Manufacturer {...defaultProps} value={selectedOption} />,
          optionsAPISuccess
        );

        await waitForPromises();

        await user.click(
          screen.getByRole("button", {
            name: `Manufacturer ${selectedOption.name}`,
          })
        );

        expect(
          screen.getByRole("option", {
            name: selectedOption.name,
            selected: true,
          })
        ).toBeInTheDocument();
      });
    });
  });

  describe("when manufacturer query is loading", () => {
    it("renders select in loading state", async () => {
      const { user } = render(<Manufacturer {...defaultProps} />, {
        apollo: {
          mocks: [
            {
              request,
              loading: true,
            },
          ],
        },
      });

      await user.click(
        screen.getByRole("button", {
          name: "Manufacturer Select a manufacturer",
        })
      );

      const options = screen.getAllByRole("option");

      expect(options).toHaveLength(6);
      options.forEach((option) => {
        expect(option).toHaveAttribute("aria-disabled", "true");
        expect(option.firstChild).toHaveClass("animate-pulse");
      });
    });
  });

  describe("when manufacturer query is successful", () => {
    it("renders select with manufacturers", async () => {
      const { user } = render(
        <Manufacturer {...defaultProps} />,
        optionsAPISuccess
      );

      await waitForPromises();

      await user.click(
        screen.getByRole("button", {
          name: "Manufacturer Select a manufacturer",
        })
      );

      expect(
        screen.getByRole("option", { name: manufacturers[0].name })
      ).toBeInTheDocument();
      expect(
        screen.getByRole("option", { name: manufacturers[1].name })
      ).toBeInTheDocument();
    });
  });

  describe("when manufacturer query is not successful", () => {
    it("displays error toast", async () => {
      render(<Manufacturer {...defaultProps} />, {
        apollo: {
          mocks: [mockNetworkError({ request })],
        },
      });

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastError({
          show: true,
          message:
            "An error occurred loading the manufacturers, please try again.",
        })
      );
    });
  });
});
