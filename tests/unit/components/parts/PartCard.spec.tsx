import { toString } from "lodash";

import * as Image from "@/components/Image";
import PartCard from "@/components/parts/PartCard";
import { DeletePartDocument } from "@/graphql/part/mutations/delete-part.generated";
import {
  PartsByTypeDocument,
  PartsByTypeQuery,
} from "@/graphql/part/queries/parts-by-type.generated";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import ForkPlaceholder from "@/svgs/fork-placeholder.svg";
import { PartType } from "@/types/graphql";
import { mockDispatch } from "tests/mocks/react-redux";
import { forks, fox36NoImages, fox38 } from "tests/unit/mock-data/part";
import {
  mockComponent,
  mockNetworkError,
  render,
  waitForPromises,
  within,
  screen,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("PartCard", () => {
  const defaultProps = {
    part: fox38,
    path: "/parts/forks",
  };

  describe("when `loading` prop is `true`", () => {
    it("renders card in loading state", () => {
      render(<PartCard {...defaultProps} loading />);

      expect(screen.getAllByTestId("skeletonLoader")).toHaveLength(4);
    });
  });

  describe("when `loading` prop is `false`", () => {
    describe("part actions", () => {
      it("renders dropdown with `View`, `Edit`, and `Delete` actions", async () => {
        const { user } = render(<PartCard {...defaultProps} />);

        await user.click(
          screen.getByRole("button", { name: "Open part options" })
        );

        const view = screen.getByRole("menuitem", { name: "View" });
        const edit = screen.getByRole("menuitem", { name: "Edit" });

        expect(view).toBeInTheDocument();
        expect(view).toHaveAttribute(
          "href",
          `${defaultProps.path}/${fox38.id}`
        );

        expect(edit).toBeInTheDocument();
        expect(edit).toHaveAttribute(
          "href",
          `${defaultProps.path}/${fox38.id}/edit`
        );

        expect(
          screen.getByRole("menuitem", { name: "Delete" })
        ).toBeInTheDocument();
      });

      describe("when `delete` action is clicked", () => {
        it("opens confirmation modal", async () => {
          const { user } = render(<PartCard {...defaultProps} />);

          await user.click(
            screen.getByRole("button", { name: "Open part options" })
          );

          await user.click(screen.getByRole("menuitem", { name: "Delete" }));

          expect(
            screen.getByRole("dialog", {
              name: `Delete ${defaultProps.part.manufacturer.name} ${defaultProps.part.name}`,
            })
          ).toBeInTheDocument();
        });

        describe("when `Delete` button in confirmation modal is clicked", () => {
          const request = {
            query: DeletePartDocument,
            variables: {
              id: defaultProps.part.id,
            },
          };

          describe("when API call is successful", () => {
            it("shows success toast and updates apollo cache", async () => {
              const {
                expectApolloHandlersCalledWithCorrectVariables,
                apolloCache,
                user,
              } = render(<PartCard {...defaultProps} />, {
                apollo: {
                  mocks: [
                    {
                      request,
                      result: {
                        data: {
                          deletePart: {
                            id: defaultProps.part.id,
                            __typename: "Part",
                          },
                        },
                      },
                    },
                  ],
                  initialCacheQueries: [
                    {
                      query: PartsByTypeDocument,
                      data: {
                        partsByType: {
                          data: forks,
                          paginatorInfo: {
                            lastItem: 25,
                            total: 25,
                          },
                        },
                      },
                      variables: {
                        type: PartType.Fork,
                      },
                    },
                  ],
                },
              });

              await user.click(
                screen.getByRole("button", { name: "Open part options" })
              );

              await user.click(
                screen.getByRole("menuitem", { name: "Delete" })
              );

              await user.click(
                within(
                  screen.getByRole("dialog", {
                    name: `Delete ${defaultProps.part.manufacturer.name} ${defaultProps.part.name}`,
                  })
                ).getByRole("button", { name: "Delete" })
              );

              await waitForPromises();

              expect(mockDispatch).toHaveBeenCalledWith(
                updateToastSuccess({
                  show: true,
                  message: `${defaultProps.part.manufacturer.name} ${defaultProps.part.name} successfully deleted.`,
                })
              );
              expectApolloHandlersCalledWithCorrectVariables();

              const partsByTypeQuery = apolloCache.readQuery({
                query: PartsByTypeDocument,
                variables: {
                  type: PartType.Fork,
                },
              }) as PartsByTypeQuery;

              expect(partsByTypeQuery.partsByType.data).toHaveLength(2);
            });
          });

          describe("when API call is not successful", () => {
            it("shows error toast", async () => {
              const { user } = render(<PartCard {...defaultProps} />, {
                apollo: {
                  mocks: [mockNetworkError({ request })],
                },
              });

              await user.click(
                screen.getByRole("button", { name: "Open part options" })
              );

              await user.click(
                screen.getByRole("menuitem", { name: "Delete" })
              );

              await user.click(
                within(
                  screen.getByRole("dialog", {
                    name: `Delete ${defaultProps.part.manufacturer.name} ${defaultProps.part.name}`,
                  })
                ).getByRole("button", { name: "Delete" })
              );

              await waitForPromises();

              expect(mockDispatch).toHaveBeenCalledWith(
                updateToastError({
                  show: true,
                  message: "An error occurred, please try again.",
                })
              );
            });
          });
        });
      });
    });

    describe("when part has images", () => {
      it("renders the first image", () => {
        mockComponent({
          componentObject: Image,
          render: ({ url, alt }) => <img src={url} alt={alt} />,
        });

        render(<PartCard {...defaultProps} />);

        const image = screen.getByRole("img", {
          name: `${defaultProps.part.manufacturer.name} ${defaultProps.part.name} image`,
        });

        expect(image).toHaveAttribute("src", defaultProps.part.images[0].url);
      });
    });

    describe("when part does not have images", () => {
      it("renders `placeholder` prop", () => {
        render(
          <PartCard
            {...defaultProps}
            part={fox36NoImages}
            placeholder={ForkPlaceholder}
          />
        );

        expect(screen.getByTestId("partCardPlaceholder")).toBeInTheDocument();
      });
    });

    describe("when part has active settings", () => {
      it("renders active settings", () => {
        render(
          <PartCard
            {...defaultProps}
            activeSettings={[
              {
                label: "PSI",
                value: defaultProps.part.activeSettingsRecord.psi,
              },
              {
                label: "Sag (mm)",
                value: defaultProps.part.activeSettingsRecord.sag,
              },
              {
                label: "Volume spacers",
                value: defaultProps.part.activeSettingsRecord.volumeSpacers,
              },
              {
                label: "LSC clicks",
                value: defaultProps.part.activeSettingsRecord.lscClicks,
              },
              {
                label: "HSC clicks",
                value: defaultProps.part.activeSettingsRecord.hscClicks,
              },
            ]}
          />
        );

        expect(screen.getByDescriptionTerm("PSI")).toHaveTextContent(
          toString(defaultProps.part.activeSettingsRecord.psi)
        );

        expect(screen.getByDescriptionTerm("Sag (mm)")).toHaveTextContent(
          toString(defaultProps.part.activeSettingsRecord.sag)
        );

        expect(screen.getByDescriptionTerm("Volume spacers")).toHaveTextContent(
          toString(defaultProps.part.activeSettingsRecord.volumeSpacers)
        );

        expect(screen.getByDescriptionTerm("LSC clicks")).toHaveTextContent(
          toString(defaultProps.part.activeSettingsRecord.lscClicks)
        );

        expect(screen.getByDescriptionTerm("HSC clicks")).toHaveTextContent(
          toString(defaultProps.part.activeSettingsRecord.hscClicks)
        );
      });
    });

    it("renders `Edit` button", () => {
      render(<PartCard {...defaultProps} />);

      const editButton = screen.getByRole("link", { name: "Edit" });

      expect(editButton).toBeInTheDocument();
      expect(editButton).toHaveAttribute(
        "href",
        `${defaultProps.path}/${fox38.id}/edit`
      );
    });

    it("renders `View` button", () => {
      render(<PartCard {...defaultProps} />);

      const viewButton = screen.getByRole("link", { name: "View" });

      expect(viewButton).toBeInTheDocument();
      expect(viewButton).toHaveAttribute(
        "href",
        `${defaultProps.path}/${fox38.id}`
      );
    });
  });
});
