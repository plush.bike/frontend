import React from "react";

import AlertBase from "@/components/alerts/AlertBase";
import { render, within, screen } from "tests/utils";

describe("AlertBase", () => {
  const defaultProps = { icon: { id: "announcement" } };

  describe("props", () => {
    it("renders svg passed as `icon` prop", () => {
      render(<AlertBase {...defaultProps} />);

      const icon = screen.getByTestId("alertIcon");

      expect(within(icon).getByTestId("iconUse")).toHaveAttribute(
        "xlink:href",
        "#announcement"
      );
      expect(within(icon).getByTestId(/^iconSvg/)).toHaveClass("w-5", "h-5");
    });

    it("merges `className` prop with CSS classes", () => {
      const { container } = render(
        <AlertBase {...defaultProps} className="bg-danger-default" />
      );

      expect(container.firstChild).toHaveClass("bg-danger-default");
    });

    describe("when close button is clicked", () => {
      it("calls handler passed as `onClose` prop", async () => {
        const onClose = vi.fn();

        const { user } = render(
          <AlertBase {...defaultProps} onClose={onClose} />
        );

        await user.click(screen.getByLabelText("Close alert"));

        expect(onClose).toHaveBeenCalled();
      });
    });

    describe("when `onClose` prop is not passed", () => {
      it("does not render close button", () => {
        render(<AlertBase {...defaultProps} onClose={null} />);

        expect(screen.queryByLabelText("Close alert")).not.toBeInTheDocument();
      });
    });
  });

  it("renders children", () => {
    render(<AlertBase {...defaultProps}>Foo bar</AlertBase>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
