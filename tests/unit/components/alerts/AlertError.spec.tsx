import React from "react";

import AlertError from "@/components/alerts/AlertError";
import { render, within, screen } from "tests/utils";

describe("AlertError", () => {
  describe("props", () => {
    describe("when close button is clicked", () => {
      it("calls handler passed as `onClose` prop", async () => {
        const onClose = vi.fn();

        const { user } = render(<AlertError onClose={onClose} />);

        await user.click(screen.getByLabelText("Close alert"));

        expect(onClose).toHaveBeenCalled();
      });
    });
  });

  it('renders "Close solid" icon', () => {
    render(<AlertError />);

    const icon = screen.getByTestId("alertIcon");

    expect(within(icon).getByTestId("iconUse")).toHaveAttribute(
      "xlink:href",
      "#close-solid"
    );
  });

  it("renders children", () => {
    render(<AlertError>Foo bar</AlertError>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
