import React from "react";

import AlertSuccess from "@/components/alerts/AlertSuccess";
import { render, within, screen } from "tests/utils";

describe("AlertSuccess", () => {
  describe("props", () => {
    describe("when close button is clicked", () => {
      it("calls handler passed as `onClose` prop", async () => {
        const onClose = vi.fn();

        const { user } = render(<AlertSuccess onClose={onClose} />);

        await user.click(screen.getByLabelText("Close alert"));

        expect(onClose).toHaveBeenCalled();
      });
    });
  });

  it('renders "Checkmark outline" icon', () => {
    render(<AlertSuccess />);

    const icon = screen.getByTestId("alertIcon");

    expect(within(icon).getByTestId("iconUse")).toHaveAttribute(
      "xlink:href",
      "#checkmark-outline"
    );
  });

  it("renders children", () => {
    render(<AlertSuccess>Foo bar</AlertSuccess>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
