import React from "react";

import Loader from "@/components/Loader";
import { render, screen } from "tests/utils";

describe("Icon", () => {
  describe("props", () => {
    it("loading svg", () => {
      render(<Loader />);

      expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
    });

    describe("when `size` prop is passed", () => {
      it("adds corresponding CSS classes", () => {
        const { container } = render(<Loader size={6} />);

        expect(container.firstChild).toHaveClass("w-6", "h-6");
      });
    });

    describe("when `size` prop is not passed", () => {
      it("defaults to size `5`", () => {
        const { container } = render(<Loader />);

        expect(container.firstChild).toHaveClass("w-5", "h-5");
      });
    });

    describe("when `fullPage` prop is set to `true`", () => {
      it("it renders size 16 loader in a `div` centered on the page", () => {
        const { container } = render(<Loader fullPage />);

        expect(container.firstChild).toHaveClass(
          "absolute",
          "-translate-x-1/2",
          "-translate-y-1/2",
          "top-1/2",
          "left-1/2"
        );
        expect(screen.getByTestId("iconSvgLoading")).toHaveClass(
          "w-16",
          "h-16"
        );
      });
    });

    it("merges `className` prop", () => {
      const { container } = render(<Loader className="text-white" />);

      expect(container.firstChild).toHaveClass("text-white", "animate-spin");
    });
  });
});
