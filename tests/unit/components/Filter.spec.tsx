import Filter from "@/components/Filter";
import { render, screen } from "tests/utils";

describe("Filter", () => {
  const defaultProps = {
    availableFilters: [
      {
        category: {
          label: "Filter 1",
          key: "filter-1",
        },
        options: [
          {
            label: "Option 1",
            key: "option-1",
          },
          {
            label: "Option 2",
            key: "option-2",
          },
        ],
      },
      {
        category: {
          label: "Filter 2",
          key: "filter-2",
        },
        options: [
          {
            label: "Option 1",
            key: "option-1",
          },
          {
            label: "Option 2",
            key: "option-2",
          },
          {
            label: "Option 3",
            key: "option-3",
          },
        ],
      },
      {
        category: {
          label: "Filter 3",
          key: "filter-3",
        },
        options: [
          {
            label: "Option 1",
            key: "option-1",
          },
          {
            label: "Option 2",
            key: "option-2",
          },
          {
            label: "Option 3",
            key: "option-3",
          },
          {
            label: "Option 4",
            key: "option-4",
          },
        ],
      },
    ],
  };

  it("displays available filter categories", async () => {
    const { user } = render(<Filter {...defaultProps} />);

    const expectedCategories = defaultProps.availableFilters.map(
      (availableFilter) => availableFilter.category.label
    );

    await user.click(screen.getByRole("button", { name: "Filter" }));

    expectedCategories.forEach((name) => {
      expect(screen.getByRole("menuitem", { name })).toBeInTheDocument();
    });
  });

  describe("when filter category is clicked", () => {
    const [filter] = defaultProps.availableFilters;

    const setup = async () => {
      const view = render(<Filter {...defaultProps} />);
      const { user } = view;

      await user.click(screen.getByRole("button", { name: "Filter" }));

      await user.click(
        screen.getByRole("menuitem", {
          name: filter.category.label,
        })
      );

      return view;
    };

    it("displays available options", async () => {
      await setup();

      filter.options.forEach(({ label }) => {
        expect(
          screen.getByRole("menuitem", { name: label })
        ).toBeInTheDocument();
      });
    });

    describe("when `Back to filters` button is clicked", () => {
      it("displays available filter categories", async () => {
        const { user } = await setup();

        await user.click(
          screen.getByRole("button", { name: "Back to filters" })
        );

        const expectedCategories = defaultProps.availableFilters.map(
          (availableFilter) => availableFilter.category.label
        );

        expectedCategories.forEach((name) => {
          expect(screen.getByRole("menuitem", { name })).toBeInTheDocument();
        });
      });
    });
  });

  describe("when filter option is clicked", () => {
    const [filter] = defaultProps.availableFilters;

    const setup = async () => {
      const view = render(<Filter {...defaultProps} />);
      const { user } = view;

      await user.click(screen.getByRole("button", { name: "Filter" }));

      await user.click(
        screen.getByRole("menuitem", {
          name: filter.category.label,
        })
      );
      await user.click(
        screen.getByRole("menuitem", {
          name: filter.options[0].label,
        })
      );

      return view;
    };

    it("displays badge with filter", async () => {
      await setup();

      expect(
        screen.getByTextWithMarkup("Filter 1 is Option 1")
      ).toBeInTheDocument();
    });

    it("removes filter category", async () => {
      const { user } = await setup();

      await user.click(screen.getByRole("button", { name: "Filter" }));

      expect(
        screen.queryByRole("menuitem", {
          name: filter.category.label,
        })
      ).not.toBeInTheDocument();
    });

    describe("when selected filter is removed", () => {
      it("removes badge and displays filter category again", async () => {
        const { user } = await setup();

        await user.click(screen.getByRole("button", { name: "Remove" }));

        expect(
          screen.queryByTextWithMarkup("Filter 1 is Option 1")
        ).not.toBeInTheDocument();

        await user.click(screen.getByRole("button", { name: "Filter" }));

        expect(
          screen.getByRole("menuitem", {
            name: filter.category.label,
          })
        ).toBeInTheDocument();
      });
    });
  });
});
