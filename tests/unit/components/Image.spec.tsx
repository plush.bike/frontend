import * as IKReact from "imagekitio-react";

import Image from "@/components/Image";
import { mockComponent, render } from "tests/utils";

describe("Image", () => {
  const defaultProps = {
    url: "https://ik.imagekit.io/profile_images/1/foo-bar.jpg",
    height: 400,
    width: 400,
    alt: "Profile image",
    className: "foo-bar",
  };

  it("renders `IKImage` component with correct props", () => {
    const { spy } = mockComponent({
      componentObject: IKReact,
      componentKey: "IKImage",
    });

    render(<Image {...defaultProps} />);

    expect(spy).toHaveBeenCalledWith(
      {
        urlEndpoint: "https://ik.imagekit.io",
        className: defaultProps.className,
        src: defaultProps.url,
        transformation: [{ height: "400", width: "400" }],
        lqip: { active: true },
        loading: "lazy",
        height: defaultProps.height,
        width: defaultProps.width,
        alt: defaultProps.alt,
      },
      {}
    );
  });
});
