import mockRouter from "next-router-mock";

import Sidebar from "@/components/nav/Sidebar";
import { render, within, screen } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("Sidebar", () => {
  const defaultProps = {
    open: true,
    onClose: vi.fn(),
  };

  beforeEach(() => {
    mockRouter.push("/parts/forks");
  });

  describe("mobile nav", () => {
    it("renders mobile nav", () => {
      render(<Sidebar {...defaultProps} />);

      expect(screen.getByTestId("mobileNav")).toBeInTheDocument();
    });

    it("renders sidebar links", () => {
      render(<Sidebar {...defaultProps} />);

      expect(
        within(screen.getByTestId("mobileNav")).getByRole("link", {
          name: "Forks",
        })
      ).toBeInTheDocument();
    });

    it("renders link to `/parts/forks`", () => {
      render(<Sidebar {...defaultProps} />);

      expect(
        within(screen.getByTestId("mobileNav")).getByRole("link", {
          name: "Home",
        })
      ).toHaveAttribute("href", "/parts/forks");
    });

    describe("when `Close sidebar` button is clicked", () => {
      it("calls `onClose` prop", async () => {
        const { user } = render(<Sidebar {...defaultProps} />);

        await user.click(
          within(screen.getByTestId("mobileNav")).getByRole("button", {
            name: "Close sidebar",
          })
        );

        expect(defaultProps.onClose).toHaveBeenCalled();
      });
    });

    describe("when `open` prop is `false`", () => {
      it("does not show nav", () => {
        render(<Sidebar {...defaultProps} open={false} />);

        expect(screen.queryByTestId("mobileNav")).not.toBeInTheDocument();
      });
    });
  });

  describe("desktop nav", () => {
    it("renders desktop nav", () => {
      render(<Sidebar {...defaultProps} />);

      expect(screen.getByTestId("desktopNav")).toBeInTheDocument();
    });

    it("renders sidebar links", () => {
      render(<Sidebar {...defaultProps} />);

      expect(
        within(screen.getByTestId("desktopNav")).getByRole("link", {
          name: "Forks",
          hidden: true,
        })
      ).toBeInTheDocument();
    });

    it("renders link to `/parts/forks`", () => {
      render(<Sidebar {...defaultProps} />);

      expect(
        within(screen.getByTestId("desktopNav")).getByRole("link", {
          name: "Home",
          hidden: true,
        })
      ).toHaveAttribute("href", "/parts/forks");
    });
  });
});
