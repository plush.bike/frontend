import mockRouter from "next-router-mock";
import React from "react";

import ActiveLink from "@/components/nav/ActiveLink";
import { render } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("ActiveLink", () => {
  const defaultProps = { href: "/sign-in" };

  beforeEach(() => {
    mockRouter.push("/sign-in");
  });

  describe("props", () => {
    it("sets `href` prop as `href` attribute", () => {
      const { container } = render(<ActiveLink {...defaultProps} />);

      expect(container.firstChild).toHaveAttribute("href", "/sign-in");
    });

    it("merges `className` prop with CSS classes", () => {
      const { container } = render(
        <ActiveLink {...defaultProps} className="font-bold" />
      );

      expect(container.firstChild).toHaveClass("font-bold");
    });

    describe("when `href` matches active path", () => {
      it("merges `activeClassname` prop with CSS classes", () => {
        const { container } = render(
          <ActiveLink
            {...defaultProps}
            className="font-bold"
            activeClassName="border-b-2"
          />
        );

        expect(container.firstChild).toHaveClass("border-b-2");
      });
    });
  });
});
