import mockRouter from "next-router-mock";

import SidebarLinks from "@/components/nav/SidebarLinks";
import { render, screen } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("SidebarLinks", () => {
  beforeEach(() => {
    mockRouter.push("/parts/forks");
  });

  it("renders `Forks` link", () => {
    render(<SidebarLinks />);

    expect(screen.getByRole("link", { name: "Forks" })).toHaveAttribute(
      "href",
      "/parts/forks"
    );
  });
});
