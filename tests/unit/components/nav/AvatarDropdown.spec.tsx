import mockRouter from "next-router-mock";

import * as Image from "@/components/Image";
import AvatarDropdown from "@/components/nav/AvatarDropdown";
import { SignOutDocument } from "@/graphql/auth/mutations/sign-out.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  mockComponent,
  mockNetworkError,
  render,
  waitForPromises,
  within,
  screen,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("AvatarDropdown", () => {
  describe("when authenticated user has a profile image", () => {
    it("renders profile image as dropdown toggle", () => {
      mockComponent({
        componentObject: Image,
        render: ({ url }) => <img src={url} />,
      });

      render(<AvatarDropdown />, { authenticated: true });

      const dropdownToggle = screen.getByRole("button", {
        name: "Open user menu",
      });

      expect(within(dropdownToggle).getByRole("img")).toHaveAttribute(
        "src",
        mockUser.profileImageUrl
      );
    });
  });

  describe("when authenticated user does not have a profile image", () => {
    it("renders user icon as dropdown toggle", () => {
      render(<AvatarDropdown />, {
        store: {
          initialState: {
            auth: {
              authenticatedUser: {
                ...mockUser,
                profileImageUrl: null,
              },
            },
          },
        },
      });

      const dropdownToggle = screen.getByRole("button", {
        name: "Open user menu",
      });

      expect(
        within(dropdownToggle).getByTestId("iconSvgUserSolidCircle")
      ).toBeInTheDocument();
    });
  });

  describe("when dropdown toggle is clicked", () => {
    it("displays dropdown", async () => {
      const { user } = render(<AvatarDropdown />, {
        authenticated: true,
      });

      await user.click(screen.getByRole("button", { name: "Open user menu" }));

      expect(screen.getByText("Signed in as")).toBeInTheDocument();
      expect(screen.getByText(mockUser.name)).toBeInTheDocument();
      expect(
        screen.getByRole("menuitem", { name: "Account settings" })
      ).toHaveAttribute("href", "/account-settings");
      expect(
        screen.getByRole("menuitem", { name: "Sign out" })
      ).toBeInTheDocument();
    });
  });

  describe("when `Sign out` item is clicked", () => {
    const request = {
      query: SignOutDocument,
    };

    describe("when `Sign Out` button is clicked", () => {
      describe("when API call is successful", () => {
        const options = {
          authenticated: true,
          apollo: {
            mocks: [
              {
                request,
                result: {
                  data: {
                    signOut: {
                      status: "SUCCESS",
                      message: "Your session has been terminated",
                    },
                  },
                },
              },
            ],
          },
        };

        it("dispatches `updateAuthenticatedUser` action", async () => {
          const { user } = render(<AvatarDropdown />, options);

          await user.click(
            screen.getByRole("button", { name: "Open user menu" })
          );
          await user.click(screen.getByRole("menuitem", { name: "Sign out" }));

          await waitForPromises();

          expect(mockDispatch).toHaveBeenCalledWith(
            updateAuthenticatedUser(null)
          );
        });

        it("redirects to sign in page", async () => {
          mockRouter.push("/parts/forks");

          const { user } = render(<AvatarDropdown />, options);

          await user.click(
            screen.getByRole("button", { name: "Open user menu" })
          );
          await user.click(screen.getByRole("menuitem", { name: "Sign out" }));

          await waitForPromises();

          expect(mockRouter.pathname).toBe("/sign-in");
        });
      });

      describe("when API call is not successful", () => {
        const options = {
          authenticated: true,
          apollo: {
            mocks: [mockNetworkError({ request })],
          },
        };

        it("displays error toast", async () => {
          const { user } = render(<AvatarDropdown />, options);

          await user.click(
            screen.getByRole("button", { name: "Open user menu" })
          );
          await user.click(screen.getByRole("menuitem", { name: "Sign out" }));

          await waitForPromises();

          expect(mockDispatch).toHaveBeenCalledWith(
            updateToastError({
              show: true,
              message: "An error occurred, please try again.",
            })
          );
        });
      });
    });
  });
});
