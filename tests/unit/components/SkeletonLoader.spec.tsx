import SkeletonLoader from "@/components/SkeletonLoader";
import { render } from "tests/utils";

describe("SkeletonLoader", () => {
  describe("props", () => {
    describe("wrapperClassName", () => {
      it("adds wrapper class to wrapping `div`", () => {
        const { container } = render(
          <SkeletonLoader wrapperClassName="foo-bar" />
        );

        expect(container.firstChild).toHaveClass("foo-bar");
      });
    });

    describe("className", () => {
      it("adds class to child `div`", () => {
        const { container } = render(<SkeletonLoader className="foo-bar" />);

        expect(container.firstChild.firstChild).toHaveClass("foo-bar");
      });
    });
  });

  it("renders `div` with `animate-pulse` class and child `div` with `bg-gray-300` class", () => {
    const { container } = render(<SkeletonLoader />);

    expect(container.firstChild).toHaveClass("animate-pulse");
    expect(container.firstChild.firstChild).toHaveClass("bg-gray-300");
  });
});
