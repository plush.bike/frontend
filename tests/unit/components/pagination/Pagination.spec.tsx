import { range } from "lodash";
import mockRouter from "next-router-mock";

import Pagination from "@/components/pagination/Pagination";
import { render, within, screen } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("Pagination", () => {
  const defaultProps = {
    itemsCount: 25,
    page: 1,
    perPage: 5,
  };

  describe.each`
    itemsCount | page  | visiblePageButtons          | prevPageEnabled | nextPageEnabled | expectedShowingText
    ${6}       | ${1}  | ${range(1, 3)}              | ${false}        | ${true}         | ${"Showing 1 to 5 of 6 results"}
    ${6}       | ${2}  | ${range(1, 3)}              | ${true}         | ${false}        | ${"Showing 6 of 6 results"}
    ${25}      | ${1}  | ${range(1, 6)}              | ${false}        | ${true}         | ${"Showing 1 to 5 of 25 results"}
    ${50}      | ${1}  | ${range(1, 11)}             | ${false}        | ${true}         | ${"Showing 1 to 5 of 50 results"}
    ${50}      | ${5}  | ${range(1, 11)}             | ${true}         | ${true}         | ${"Showing 21 to 25 of 50 results"}
    ${50}      | ${10} | ${range(1, 11)}             | ${true}         | ${false}        | ${"Showing 46 to 50 of 50 results"}
    ${100}     | ${1}  | ${[...range(1, 8), 20]}     | ${false}        | ${true}         | ${"Showing 1 to 5 of 100 results"}
    ${100}     | ${7}  | ${[1, ...range(5, 10), 20]} | ${true}         | ${true}         | ${"Showing 31 to 35 of 100 results"}
  `(
    "when `itemsCount` is `$itemsCount` and `page` is `$page`",
    ({
      itemsCount,
      page,
      visiblePageButtons,
      prevPageEnabled,
      nextPageEnabled,
      expectedShowingText,
    }) => {
      it(`renders previous page button as ${
        prevPageEnabled ? "enabled" : "disabled"
      }`, () => {
        render(<Pagination itemsCount={itemsCount} page={page} perPage={5} />);

        const desktopPagination = screen.getByTestId("desktopPagination");
        const mobilePagination = screen.getByTestId("mobilePagination");

        const previousButtonDesktop = within(desktopPagination).getByRole(
          "button",
          {
            name: "Previous",
          }
        );
        const previousButtonMobile = within(mobilePagination).getByRole(
          "button",
          {
            name: "Previous",
          }
        );

        if (prevPageEnabled) {
          expect(previousButtonDesktop).toBeEnabled();
          expect(previousButtonMobile).toBeEnabled();
        } else {
          expect(previousButtonDesktop).toBeDisabled();
          expect(previousButtonMobile).toBeDisabled();
        }
      });

      it(`renders next page button as ${
        nextPageEnabled ? "enabled" : "disabled"
      }`, () => {
        render(<Pagination itemsCount={itemsCount} page={page} perPage={5} />);

        const desktopPagination = screen.getByTestId("desktopPagination");
        const mobilePagination = screen.getByTestId("mobilePagination");

        const nextButtonDesktop = within(desktopPagination).getByRole(
          "button",
          {
            name: "Next",
          }
        );
        const nextButtonMobile = within(mobilePagination).getByRole("button", {
          name: "Next",
        });

        if (nextPageEnabled) {
          expect(nextButtonDesktop).toBeEnabled();
          expect(nextButtonMobile).toBeEnabled();
        } else {
          expect(nextButtonDesktop).toBeDisabled();
          expect(nextButtonMobile).toBeDisabled();
        }
      });

      it(`renders buttons for the following pages: ${visiblePageButtons.join(
        ", "
      )}`, () => {
        render(<Pagination itemsCount={itemsCount} page={page} perPage={5} />);

        visiblePageButtons.forEach((visiblePageButton) => {
          expect(
            within(screen.getByTestId("desktopPagination")).getByRole(
              "button",
              {
                name: visiblePageButton,
              }
            )
          ).toBeInTheDocument();
        });
      });

      it(`renders page ${page} as current page`, () => {
        render(<Pagination itemsCount={itemsCount} page={page} perPage={5} />);

        expect(
          within(screen.getByTestId("desktopPagination")).getByRole("button", {
            name: page,
            current: "page",
          })
        ).toBeInTheDocument();
      });

      it(`renders \`${expectedShowingText}\``, () => {
        render(<Pagination itemsCount={itemsCount} page={page} perPage={5} />);

        expect(
          within(screen.getByTestId("desktopPagination")).getByTestId(
            "showingText"
          )
        ).toHaveTextContent(expectedShowingText);
      });
    }
  );

  describe("desktop pagination", () => {
    beforeEach(() => {
      mockRouter.push("/parts/forks");
    });

    describe("when `Next` button is clicked", () => {
      it("updates `page` query string parameter", async () => {
        const { user } = render(<Pagination {...defaultProps} />);

        const desktopPagination = screen.getByTestId("desktopPagination");
        const nextButtonDesktop = within(desktopPagination).getByRole(
          "button",
          {
            name: "Next",
          }
        );

        await user.click(nextButtonDesktop);

        expect(mockRouter.query).toEqual({ page: 2 });
      });
    });

    describe("when `Previous` button is clicked", () => {
      describe("when previous page is the first page", () => {
        it("removes the `page` query string parameter", async () => {
          const { user } = render(<Pagination {...defaultProps} page={2} />);

          const desktopPagination = screen.getByTestId("desktopPagination");
          const previousButtonDesktop = within(desktopPagination).getByRole(
            "button",
            {
              name: "Previous",
            }
          );

          await user.click(previousButtonDesktop);

          expect(mockRouter.pathname).toBe("/parts/forks");
        });
      });

      describe("when previous page is not the first page", () => {
        it("updates `page` query string parameter", async () => {
          const { user } = render(<Pagination {...defaultProps} page={3} />);

          const desktopPagination = screen.getByTestId("desktopPagination");
          const previousButtonDesktop = within(desktopPagination).getByRole(
            "button",
            {
              name: "Previous",
            }
          );

          await user.click(previousButtonDesktop);

          expect(mockRouter.query).toEqual({ page: 2 });
        });
      });
    });

    describe("when a number button is clicked", () => {
      it("updates `page` query string parameter", async () => {
        const { user } = render(<Pagination {...defaultProps} page={3} />);

        const desktopPagination = screen.getByTestId("desktopPagination");
        const numberButton = within(desktopPagination).getByRole("button", {
          name: "3",
        });

        await user.click(numberButton);

        expect(mockRouter.query).toEqual({ page: 3 });
      });
    });
  });

  describe("mobile pagination", () => {
    beforeEach(() => {
      mockRouter.push("/parts/forks");
    });

    describe("when `Next` button is clicked", () => {
      it("updates `page` query string parameter", async () => {
        const { user } = render(<Pagination {...defaultProps} />);

        const mobilePagination = screen.getByTestId("mobilePagination");
        const nextButtonMobile = within(mobilePagination).getByRole("button", {
          name: "Next",
        });

        await user.click(nextButtonMobile);

        expect(mockRouter.query).toEqual({ page: 2 });
      });
    });

    describe("when `Previous` button is clicked", () => {
      describe("when previous page is the first page", () => {
        it("removes the `page` query string parameter", async () => {
          const { user } = render(<Pagination {...defaultProps} page={2} />);

          const mobilePagination = screen.getByTestId("mobilePagination");
          const previousButtonMobile = within(mobilePagination).getByRole(
            "button",
            {
              name: "Previous",
            }
          );

          await user.click(previousButtonMobile);

          expect(mockRouter.pathname).toBe("/parts/forks");
        });
      });

      describe("when previous page is not the first page", () => {
        it("updates `page` query string parameter", async () => {
          const { user } = render(<Pagination {...defaultProps} page={3} />);

          const mobilePagination = screen.getByTestId("mobilePagination");
          const previousButtonMobile = within(mobilePagination).getByRole(
            "button",
            {
              name: "Previous",
            }
          );

          await user.click(previousButtonMobile);

          expect(mockRouter.query).toEqual({ page: 2 });
        });
      });
    });
  });
});
