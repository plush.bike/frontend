import PaginationItem from "@/components/pagination/PaginationItem";
import { render, screen } from "tests/utils";

describe("PaginationItem", () => {
  const defaultProps = {
    page: 2,
    onClick: vi.fn(),
  };

  describe("when `item` is a string", () => {
    it("renders a `span` tag", () => {
      render(<PaginationItem {...defaultProps} item={"..."} />);

      expect(screen.getByText("...", { selector: "span" })).toBeInTheDocument();
    });
  });

  describe("when `item` is a number", () => {
    it("renders a button", () => {
      render(<PaginationItem {...defaultProps} item={1} />);

      expect(screen.getByRole("button", { name: "1" })).toBeInTheDocument();
    });

    describe("when button is clicked", () => {
      it("calls `onClick` prop", async () => {
        const { user } = render(<PaginationItem {...defaultProps} item={1} />);

        await user.click(screen.getByRole("button", { name: "1" }));

        expect(defaultProps.onClick).toHaveBeenCalled();
      });
    });

    describe("when item is active", () => {
      it("adds a purple border", () => {
        render(<PaginationItem {...defaultProps} item={2} />);

        expect(screen.getByRole("button", { name: "2" })).toHaveClass(
          "z-10",
          "bg-indigo-50",
          "border-indigo-500",
          "text-indigo-600"
        );
      });
    });
  });
});
