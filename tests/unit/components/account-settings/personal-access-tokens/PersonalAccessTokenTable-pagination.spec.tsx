import mockRouter from "next-router-mock";

import PersonalAccessTokensTable from "@/components/account-settings/personal-access-tokens/PersonalAccessTokensTable";
import { PersonalAccessTokensDocument } from "@/graphql/auth/personal-access-tokens/queries/personal-access-tokens.generated";
import { personalAccessTokens } from "tests/unit/mock-data/personal-access-token";
import { mockTimezone, render, screen, waitForPromises } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock("@/constants/pagination", async () => {
  const actual = (await vi.importActual("@/constants/pagination")) as object;

  return {
    ...actual,
    DEFAULT_PER_PAGE: 1,
  };
});

describe("PersonalAccessTokenTable", () => {
  beforeEach(() => {
    mockRouter.push("/account-settings");

    mockTimezone();
  });

  it("displays pagination", async () => {
    const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
      <PersonalAccessTokensTable />,
      {
        apollo: {
          mocks: [
            {
              request: {
                query: PersonalAccessTokensDocument,
                variables: {
                  first: 1,
                  page: 1,
                },
              },
              result: {
                data: {
                  personalAccessTokens: {
                    data: personalAccessTokens,
                    paginatorInfo: {
                      lastItem: 1,
                      total: 2,
                      __typename: "PaginatorInfo",
                    },
                  },
                },
              },
            },
            {
              request: {
                query: PersonalAccessTokensDocument,
                variables: {
                  first: 1,
                  page: 2,
                },
              },
              result: {
                data: {
                  personalAccessTokens: {
                    data: personalAccessTokens,
                    paginatorInfo: {
                      lastItem: 2,
                      total: 3,
                      __typename: "PaginatorInfo",
                    },
                  },
                },
              },
            },
          ],
        },
      }
    );

    await waitForPromises();

    await user.click(await screen.findByRole("button", { name: "2" }));

    await waitForPromises();

    expect(mockRouter).toMatchObject({
      pathname: "/account-settings",
      query: { patPage: 2 },
    });

    expectApolloHandlersCalledWithCorrectVariables();
  });
});
