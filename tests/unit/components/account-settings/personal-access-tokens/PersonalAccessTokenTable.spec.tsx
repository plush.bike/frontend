import mockRouter from "next-router-mock";

import PersonalAccessTokensTable from "@/components/account-settings/personal-access-tokens/PersonalAccessTokensTable";
import { DeletePersonalAccessTokenDocument } from "@/graphql/auth/personal-access-tokens/mutations/delete-personal-access-token.generated";
import { PersonalAccessTokensDocument } from "@/graphql/auth/personal-access-tokens/queries/personal-access-tokens.generated";
import { updateToastError } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import {
  personalAccessToken,
  personalAccessToken2,
  personalAccessTokens,
} from "tests/unit/mock-data/personal-access-token";
import {
  mockNetworkError,
  mockTimezone,
  render,
  screen,
  waitForPromises,
  within,
} from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("PersonalAccessTokenTable", () => {
  const personalAccessTokensRequest = {
    query: PersonalAccessTokensDocument,
  };
  const deletePersonalAccessTokenRequest = {
    query: DeletePersonalAccessTokenDocument,
    variables: {
      id: personalAccessToken.id,
    },
  };

  beforeEach(() => {
    mockRouter.push("/account-settings");

    mockTimezone();
  });

  it("renders skeleton loader when personal access tokens are loading", async () => {
    render(<PersonalAccessTokensTable />, {
      apollo: {
        mocks: [{ request: personalAccessTokensRequest, loading: true }],
      },
    });

    expect(screen.getAllByTestId("skeletonLoader")).toHaveLength(40);
  });

  describe("when API call is successful", () => {
    const mockPersonalAccessTokenQuery = {
      request: personalAccessTokensRequest,
      result: {
        data: {
          personalAccessTokens: {
            data: personalAccessTokens,
            paginatorInfo: {
              lastItem: 2,
              total: 2,
              __typename: "PaginatorInfo",
            },
          },
        },
      },
    };

    test.each([
      {
        ...personalAccessToken,
        expectedLastUsed: "Sep 25, 2022, 12:38 PM",
        expectedCreated: "Sep 25, 2022, 12:38 PM",
      },
      {
        ...personalAccessToken2,
        expectedLastUsed: "Never",
        expectedCreated: "Sep 25, 2022, 1:19 PM",
      },
    ])(
      "displays `$name` personal access token",
      async ({ name, expectedLastUsed, expectedCreated }) => {
        render(<PersonalAccessTokensTable />, {
          apollo: {
            mocks: [mockPersonalAccessTokenQuery],
          },
        });

        await waitForPromises();

        expect(
          screen.getByRole("cell", { name: `Name ${name}` })
        ).toBeInTheDocument();
        expect(
          screen.getByRole("cell", { name: `Last used ${expectedLastUsed}` })
        ).toBeInTheDocument();
        expect(
          screen.getByRole("cell", { name: `Created ${expectedCreated}` })
        ).toBeInTheDocument();
      }
    );

    describe("when a personal access token is successfully deleted", () => {
      const mockDeletePersonalAccessTokenMutation = {
        request: deletePersonalAccessTokenRequest,
        result: {
          data: {
            deletePersonalAccessToken: personalAccessToken,
          },
        },
      };

      it("removes it from the table", async () => {
        const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
          <PersonalAccessTokensTable />,
          {
            apollo: {
              mocks: [
                mockPersonalAccessTokenQuery,
                mockDeletePersonalAccessTokenMutation,
              ],
            },
          }
        );

        await waitForPromises();

        await user.click(
          within(screen.getByRole("row", { name: /Name Foo bar/ })).getByRole(
            "button",
            { name: "Delete" }
          )
        );

        await user.click(
          within(
            screen.getByRole("dialog", {
              name: "Delete personal access token",
            })
          ).getByRole("button", { name: "Delete" })
        );

        await waitForPromises();
        await waitForPromises();

        expect(
          screen.queryByRole("cell", {
            name: `Name ${personalAccessToken.name}`,
          })
        ).not.toBeInTheDocument();
        expectApolloHandlersCalledWithCorrectVariables();
      });
    });

    describe("when a personal access token is not successfully deleted", () => {
      const mockDeletePersonalAccessTokenMutation = mockNetworkError({
        request: deletePersonalAccessTokenRequest,
      });

      it("dispatches `updateToastError`", async () => {
        const { user } = render(<PersonalAccessTokensTable />, {
          apollo: {
            mocks: [
              mockPersonalAccessTokenQuery,
              mockDeletePersonalAccessTokenMutation,
            ],
          },
        });

        await waitForPromises();

        await user.click(
          within(screen.getByRole("row", { name: /Name Foo bar/ })).getByRole(
            "button",
            { name: "Delete" }
          )
        );

        await user.click(
          within(
            screen.getByRole("dialog", {
              name: "Delete personal access token",
            })
          ).getByRole("button", { name: "Delete" })
        );

        await waitForPromises();

        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastError({
            show: true,
            message: "An error occurred, please try again.",
          })
        );
      });
    });
  });

  describe("when API call is not successful", () => {
    it("displays error alert", async () => {
      render(<PersonalAccessTokensTable />, {
        apollo: {
          mocks: [mockNetworkError({ request: personalAccessTokensRequest })],
        },
      });

      const alert = await screen.findByText(
        "An error occurred loading your personal access tokens, please try again."
      );

      expect(alert).toBeInTheDocument();
    });
  });
});
