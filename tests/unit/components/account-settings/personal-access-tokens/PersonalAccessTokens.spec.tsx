import mockRouter from "next-router-mock";

import * as CreatePersonalAccessTokenForm from "@/components/account-settings/personal-access-tokens/CreatePersonalAccessTokenForm";
import PersonalAccessTokens from "@/components/account-settings/personal-access-tokens/PersonalAccessTokens";
import * as PersonalAccessTokensTable from "@/components/account-settings/personal-access-tokens/PersonalAccessTokensTable";
import { PersonalAccessTokensDocument } from "@/graphql/auth/personal-access-tokens/queries/personal-access-tokens.generated";
import { personalAccessTokens } from "tests/unit/mock-data/personal-access-token";
import { mockComponent, render, waitForPromises } from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("PersonalAccessTokens", () => {
  const mockPersonalAccessTokenQuery = {
    request: {
      query: PersonalAccessTokensDocument,
    },
    result: {
      data: {
        personalAccessTokens: {
          data: personalAccessTokens,
          paginatorInfo: {
            lastItem: 2,
            total: 2,
            __typename: "PaginatorInfo",
          },
        },
      },
    },
  };

  beforeEach(() => {
    mockRouter.push("/account-settings");
  });

  it("renders `CreatePersonalAccessTokenForm`", async () => {
    const { spy } = mockComponent({
      componentObject: CreatePersonalAccessTokenForm,
    });
    render(<PersonalAccessTokens />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });

  it("renders `PersonalAccessTokensTable`", async () => {
    const { spy } = mockComponent({
      componentObject: PersonalAccessTokensTable,
    });
    render(<PersonalAccessTokens />, {
      apollo: {
        mocks: [mockPersonalAccessTokenQuery],
      },
    });

    await waitForPromises();

    expect(spy).toHaveBeenCalled();
  });
});
