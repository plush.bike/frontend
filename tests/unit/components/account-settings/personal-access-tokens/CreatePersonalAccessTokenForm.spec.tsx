import CreatePersonalAccessTokenForm from "@/components/account-settings/personal-access-tokens/CreatePersonalAccessTokenForm";
import { CreatePersonalAccessTokenDocument } from "@/graphql/auth/personal-access-tokens/mutations/create-personal-access-token.generated";
import {
  PersonalAccessTokensDocument,
  PersonalAccessTokensQuery,
} from "@/graphql/auth/personal-access-tokens/queries/personal-access-tokens.generated";
import { personalAccessToken } from "tests/unit/mock-data/personal-access-token";
import {
  mockGraphQLError,
  mockNetworkError,
  renderAndSubmitForm,
  screen,
  waitForPromises,
} from "tests/utils";

describe("CreatePersonalAccessTokenForm", () => {
  const requestVariables = {
    name: "Foo bar",
  };

  const baseOptions = {
    submitLabel: "Create personal access token",
    fields: {
      Name: requestVariables.name,
    },
  };

  const request = {
    query: CreatePersonalAccessTokenDocument,
    variables: requestVariables,
  };

  describe("validation", () => {
    it("requires `Name` field", async () => {
      await renderAndSubmitForm(<CreatePersonalAccessTokenForm />, {
        submitLabel: "Create personal access token",
        fields: {
          Name: "",
        },
      });

      expect(await screen.findByLabelText("Name")).toHaveAccessibleErrorMessage(
        "Please enter a name for your personal access token."
      );
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<CreatePersonalAccessTokenForm />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            message: "Validation failed for the field [name].",
            extensions: {
              validation: {
                "input.name": ["Name is required."],
              },
              category: "validation",
            },
            path: ["createPersonalAccessToken"],
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(
          <CreatePersonalAccessTokenForm />,
          optionsNetworkError
        );

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when error is a GraphQL error", () => {
      it("displays error message", async () => {
        await renderAndSubmitForm(
          <CreatePersonalAccessTokenForm />,
          optionsGraphQLError
        );

        await waitForPromises();

        expect(screen.getByLabelText("Name")).toHaveAccessibleErrorMessage(
          "Name is required."
        );
      });
    });

    it("closes error alert when close button is clicked", async () => {
      const { user } = await renderAndSubmitForm(
        <CreatePersonalAccessTokenForm />,
        optionsNetworkError
      );

      const alert = await screen.findByText(
        "An error occurred, please try again."
      );

      await user.click(screen.getByLabelText("Close alert"));
      expect(alert).not.toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const options = {
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                createPersonalAccessToken: personalAccessToken,
              },
            },
          },
        ],
        initialCacheQueries: [
          {
            query: PersonalAccessTokensDocument,
            data: {
              personalAccessTokens: {
                data: [personalAccessToken],
                paginatorInfo: {
                  lastItem: 25,
                  total: 25,
                },
              },
            },
          },
        ],
      },
    };

    it("it displays hidden personal access token and allows the user to reveal it", async () => {
      const {
        expectApolloHandlersCalledWithCorrectVariables,
        apolloCache,
        user,
      } = await renderAndSubmitForm(<CreatePersonalAccessTokenForm />, options);

      await waitForPromises();

      expect(
        screen.getByLabelText("Your new personal access token")
      ).toHaveValue("********************");

      await user.click(screen.getByRole("button", { name: "Reveal value" }));

      expect(
        screen.getByLabelText("Your new personal access token")
      ).toHaveValue(personalAccessToken.token);

      expectApolloHandlersCalledWithCorrectVariables();
      const personalAccessTokensQuery = apolloCache.readQuery({
        query: PersonalAccessTokensDocument,
      }) as PersonalAccessTokensQuery;

      expect(personalAccessTokensQuery).toBeNull();
    });
  });
});
