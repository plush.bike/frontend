import UpdatePassword from "@/components/account-settings/UpdatePassword";
import { CURRENT_PASSWORD_IS_INCORRECT } from "@/constants/auth";
import { UpdatePasswordDocument } from "@/graphql/auth/mutations/update-password.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  renderAndSubmitForm,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
  screen,
  render,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("UpdatePassword", () => {
  const requestVariables = {
    currentPassword: "super secure password",
    password: "foo bar baz",
    passwordConfirmation: "foo bar baz",
  };

  const baseOptions = {
    submitLabel: "Update",
    fields: {
      "Current password": requestVariables.currentPassword,
      "New password": requestVariables.passwordConfirmation,
      "Confirm new password": requestVariables.passwordConfirmation,
    },
    authenticated: true,
  };

  const request = {
    query: UpdatePasswordDocument,
    variables: requestVariables,
  };

  describe("validation", () => {
    describe.each`
      field                     | value    | expectedError
      ${"Current password"}     | ${""}    | ${"Please enter your current password."}
      ${"New password"}         | ${""}    | ${"Please enter your new password."}
      ${"New password"}         | ${"foo"} | ${"Your password must be at least 8 characters long."}
      ${"Confirm new password"} | ${""}    | ${"Please confirm your new password."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<UpdatePassword />, {
          submitLabel: "Update",
          fields: {
            [field]: value,
          },
          authenticated: true,
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });

    describe("when passwords do not match", () => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<UpdatePassword />, {
          submitLabel: "Update",
          fields: {
            "Current password": requestVariables.currentPassword,
            "New password": requestVariables.passwordConfirmation,
            "Confirm new password": "Foo bar bazzzzz",
          },
          authenticated: true,
        });

        expect(
          await screen.findByLabelText("New password")
        ).toHaveAccessibleErrorMessage("Your passwords do not match.");
      });
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<UpdatePassword />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            extensions: {
              errors: { currentPassword: CURRENT_PASSWORD_IS_INCORRECT },
              category: "validation",
            },
            message: "Validation Exception",
            path: ["updatePassword"],
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<UpdatePassword />, optionsNetworkError);

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when current password is incorrect", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<UpdatePassword />, optionsGraphQLError);

        const alert = await screen.findByText(
          "Your current password is incorrect, please try again."
        );

        expect(alert).toBeInTheDocument();
      });

      it("clears inputs", async () => {
        await renderAndSubmitForm(<UpdatePassword />, optionsGraphQLError);

        expect(
          await screen.findByLabelText("Current password")
        ).not.toHaveValue();
        expect(await screen.findByLabelText("New password")).not.toHaveValue();
        expect(
          await screen.findByLabelText("Confirm new password")
        ).not.toHaveValue();
      });
    });

    it("closes error alert when close button is clicked", async () => {
      const { user } = await renderAndSubmitForm(
        <UpdatePassword />,
        optionsNetworkError
      );

      const alert = await screen.findByText(
        "An error occurred, please try again."
      );

      await user.click(screen.getByLabelText("Close alert"));
      expect(alert).not.toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const options = {
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                updatePassword: {
                  status: "SUCCESS",
                  message: "Your password has been updated",
                },
              },
            },
          },
        ],
      },
    };

    it("dispatches `updateToastSuccess` action and updates `hasPasswordSet` on authenticated user", async () => {
      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<UpdatePassword />, options);

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastSuccess({
          show: true,
          message: "Password successfully updated.",
        })
      );
      expect(mockDispatch).toHaveBeenCalledWith(
        updateAuthenticatedUser({
          ...mockUser,
          hasPasswordSet: true,
        })
      );
      expectApolloHandlersCalledWithCorrectVariables();
    });
  });

  describe("when user does not have a password set", () => {
    it("does not show `Current password` field", () => {
      render(<UpdatePassword />, {
        store: {
          initialState: {
            auth: {
              authenticatedUser: {
                ...mockUser,
                hasPasswordSet: false,
              },
            },
          },
        },
      });

      expect(
        screen.queryByLabelText("Current password")
      ).not.toBeInTheDocument();
    });
  });
});
