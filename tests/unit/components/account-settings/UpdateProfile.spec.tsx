import UpdateProfile from "@/components/account-settings/UpdateProfile";
import * as UploadProfileImage from "@/components/account-settings/UploadProfileImage";
import { UpdateAuthenticatedUserDocument } from "@/graphql/user/mutations/update-authenticated-user.generated";
import { updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  act,
  mockComponent,
  mockFile,
  mockGraphQLError,
  mockNetworkError,
  render,
  renderAndSubmitForm,
  waitForPromises,
  screen,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("UpdateProfile", () => {
  const requestVariables = {
    name: "Peter Hegman",
    email: "peter@plush.bike",
  };

  const baseOptions = {
    submitLabel: "Update",
    fields: {},
    authenticated: true,
  };

  const request = {
    query: UpdateAuthenticatedUserDocument,
    variables: requestVariables,
  };

  describe("validation", () => {
    describe.each`
      field      | value    | expectedError
      ${"Name"}  | ${""}    | ${"Please enter your name."}
      ${"Email"} | ${""}    | ${"Please enter your email."}
      ${"Email"} | ${"foo"} | ${"Please enter a valid email."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<UpdateProfile />, {
          submitLabel: "Update",
          fields: {
            [field]: value,
          },
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });
  });

  it("renders pre-filled inputs", async () => {
    render(<UpdateProfile />, {
      authenticated: true,
    });

    expect(await screen.findByLabelText("Name")).toHaveValue(mockUser.name);
    expect(await screen.findByLabelText("Email")).toHaveValue(mockUser.email);
    expect(await screen.findByTestId("profileImage")).toHaveStyle({
      backgroundImage: `url("${mockUser.profileImageUrl}?height=400&width=400")`,
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<UpdateProfile />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            extensions: {
              validation: {
                "input.profileImage": ["File must be less than 10MB."],
              },
              category: "validation",
            },
            message:
              "Validation failed for the field [updateAuthenticatedUser].",
            path: ["updateAuthenticatedUser"],
          }),
        ],
      },
    };

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(<UpdateProfile />, optionsNetworkError);

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });

      it("closes error alert when close button is clicked", async () => {
        const { user } = await renderAndSubmitForm(
          <UpdateProfile />,
          optionsNetworkError
        );

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        await user.click(screen.getByLabelText("Close alert"));
        expect(alert).not.toBeInTheDocument();
      });
    });

    describe("when error is a GraphQL error", () => {
      it("displays error", async () => {
        await renderAndSubmitForm(<UpdateProfile />, optionsGraphQLError);

        expect(
          await screen.findByText("File must be less than 10MB.")
        ).toBeInTheDocument();
      });
    });
  });

  describe("when API call is successful", () => {
    it("dispatches `updateToastSuccess` action", async () => {
      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<UpdateProfile />, {
          ...baseOptions,
          apollo: {
            mocks: [
              {
                request,
                result: {
                  data: {
                    updateAuthenticatedUser: mockUser,
                  },
                },
              },
            ],
          },
        });

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastSuccess({
          show: true,
          message: "Profile successfully updated.",
        })
      );
      expectApolloHandlersCalledWithCorrectVariables();
    });

    describe("when profile image has been updated", () => {
      it("passes profile image to GraphQL request", async () => {
        const mockProfileImage = mockFile();

        const { getProps } = mockComponent({
          componentObject: UploadProfileImage,
        });

        const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
          <UpdateProfile />,
          {
            ...baseOptions,
            apollo: {
              mocks: [
                {
                  request: {
                    query: UpdateAuthenticatedUserDocument,
                    variables: {
                      ...requestVariables,
                      profileImage: mockProfileImage,
                    },
                  },
                  result: {
                    data: {
                      updateAuthenticatedUser: mockUser,
                    },
                  },
                },
              ],
            },
          }
        );

        await act(async () => {
          getProps().onChange(mockProfileImage);
        });

        await user.click(
          screen.getByText("Update", { selector: 'button[type="submit"]' })
        );

        await waitForPromises();

        expectApolloHandlersCalledWithCorrectVariables();
      });
    });
  });
});
