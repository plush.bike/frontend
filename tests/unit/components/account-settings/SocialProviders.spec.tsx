import SocialProviders from "@/components/account-settings/SocialProviders";
import { DeleteSocialProviderDocument } from "@/graphql/auth/social-providers/mutations/delete-social-provider.generated";
import { SocialProvidersDocument } from "@/graphql/auth/social-providers/queries/social-providers.generated";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { google, strava } from "tests/unit/mock-data/social-provider";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  mockNetworkError,
  render,
  screen,
  waitForPromises,
  within,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("SocialProviders", () => {
  const socialProvidersRequest = {
    query: SocialProvidersDocument,
  };

  const mockSocialProvidersQuery = (
    providers: { id: string; provider: string; __typename: string }[] = [strava]
  ) => ({
    request: socialProvidersRequest,
    result: {
      data: {
        socialProviders: providers,
      },
    },
  });

  describe("when API request is loading", () => {
    it("displays skeleton loaders", () => {
      render(<SocialProviders />, {
        apollo: {
          mocks: [
            {
              request: socialProvidersRequest,
              loading: true,
            },
          ],
        },
      });

      expect(screen.getAllByTestId("skeletonLoader")).toHaveLength(3);
    });
  });

  describe("when API request is successful", () => {
    describe("when user has password set", () => {
      it("renders connected providers as enabled buttons", async () => {
        render(<SocialProviders />, {
          apollo: { mocks: [mockSocialProvidersQuery()] },
          authenticated: true,
        });

        await waitForPromises();

        const stravaButton = screen.getByRole("button", {
          name: "Disconnect Strava",
        });

        expect(stravaButton).toBeInTheDocument();
        expect(stravaButton).toBeEnabled();
      });
    });

    describe("when user does not have password set and there are no other connected providers", () => {
      it("renders connected providers as disabled buttons", async () => {
        const { user } = render(<SocialProviders />, {
          apollo: { mocks: [mockSocialProvidersQuery()] },
          store: {
            initialState: {
              auth: {
                authenticatedUser: {
                  ...mockUser,
                  hasPasswordSet: false,
                },
              },
            },
          },
        });

        await waitForPromises();

        const stravaButton = screen.getByRole("button", {
          name: "Disconnect Strava",
        });

        expect(stravaButton).toBeInTheDocument();
        expect(stravaButton).toBeDisabled();

        await user.hover(stravaButton.parentElement);

        expect(
          screen.getByRole("tooltip", {
            name: "This service can not be disconnected until you set a password or connect another service.",
          })
        ).toBeInTheDocument();
      });
    });

    describe("when user does not have password set but there are other connected providers", () => {
      it("renders connected providers as enabled buttons", async () => {
        render(<SocialProviders />, {
          apollo: { mocks: [mockSocialProvidersQuery([strava, google])] },
          store: {
            initialState: {
              auth: {
                authenticatedUser: {
                  ...mockUser,
                  hasPasswordSet: false,
                },
              },
            },
          },
        });

        await waitForPromises();

        const stravaButton = screen.getByRole("button", {
          name: "Disconnect Strava",
        });

        expect(stravaButton).toBeInTheDocument();
        expect(stravaButton).toBeEnabled();

        const googleButton = screen.getByRole("button", {
          name: "Disconnect Google",
        });

        expect(googleButton).toBeInTheDocument();
        expect(googleButton).toBeEnabled();
      });
    });

    it("renders disconnected providers", async () => {
      render(<SocialProviders />, {
        apollo: { mocks: [mockSocialProvidersQuery()] },
        authenticated: true,
      });

      await waitForPromises();

      const googleButton = screen.getByRole("button", {
        name: "Connect Google",
      });

      expect(googleButton).toBeInTheDocument();
      expect(googleButton.parentElement).toHaveAttribute(
        "action",
        "http://api.plush.test/social-provider/google/redirect"
      );
    });
  });

  describe("when API request is not successful", () => {
    it("displays error alert", async () => {
      render(<SocialProviders />, {
        apollo: {
          mocks: [mockNetworkError({ request: socialProvidersRequest })],
        },
      });

      const alert = await screen.findByText(
        "An error occurred loading sign in services, please refresh the page to try again."
      );

      expect(alert).toBeInTheDocument();
    });
  });

  describe("disconnecting a social provider", () => {
    const deleteSocialProvidersRequest = {
      query: DeleteSocialProviderDocument,
    };

    describe("when API request is loading", () => {
      it("renders loading icon", async () => {
        const { user } = render(<SocialProviders />, {
          apollo: {
            mocks: [
              mockSocialProvidersQuery([strava, google]),
              {
                request: deleteSocialProvidersRequest,
                loading: true,
              },
            ],
          },
        });

        await waitForPromises();

        const stravaButton = screen.getByRole("button", {
          name: "Disconnect Strava",
        });

        await user.click(stravaButton);

        expect(
          within(stravaButton).getByTestId("iconSvgLoading")
        ).toBeInTheDocument();
      });
    });

    describe("when API request is successful", () => {
      it("updates apollo cache and displays success toast", async () => {
        const { user } = render(<SocialProviders />, {
          apollo: {
            mocks: [
              mockSocialProvidersQuery([strava, google]),
              {
                request: deleteSocialProvidersRequest,
                result: {
                  data: {
                    deleteSocialProvider: strava,
                  },
                },
              },
            ],
          },
        });

        await waitForPromises();

        await user.click(
          screen.getByRole("button", {
            name: "Disconnect Strava",
          })
        );

        await waitForPromises();

        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastSuccess({
            show: true,
            message: "Strava successfully disconnected.",
          })
        );
        expect(
          screen.getByRole("button", { name: "Disconnect Google" })
        ).toBeDisabled();
      });
    });

    describe("when API request is not successful", () => {
      it("displays error toast", async () => {
        const { user } = render(<SocialProviders />, {
          apollo: {
            mocks: [
              mockSocialProvidersQuery([strava, google]),
              mockNetworkError({ request: deleteSocialProvidersRequest }),
            ],
          },
        });

        await waitForPromises();

        await user.click(
          screen.getByRole("button", {
            name: "Disconnect Strava",
          })
        );

        await waitForPromises();

        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastError({
            show: true,
            message:
              "An error occurred disconnecting Strava, please try again.",
          })
        );

        expect(screen.queryByTestId("iconSvgLoading")).not.toBeInTheDocument();
      });
    });
  });
});
