import UploadProfileImage from "@/components/account-settings/UploadProfileImage";
import {
  render,
  fireEvent,
  mockFile,
  waitForPromises,
  screen,
} from "tests/utils";

describe("UploadProfileImage", () => {
  const defaultProps = {
    label: "Profile image",
    onChange: vi.fn(),
  };

  const previewURL =
    "blob:http://plush.test:3010/a3f96c77-4d20-469e-a737-4a82d3904b5b";

  const mockProfileImage = mockFile();

  const fireDropEvent = () => {
    fireEvent.drop(screen.getByLabelText("Upload a file"), {
      dataTransfer: {
        files: [mockProfileImage],
        items: [
          {
            kind: "file",
            type: mockProfileImage.type,
            getAsFile: () => mockProfileImage,
          },
        ],
        types: ["Files"],
      },
    });
  };

  beforeAll(() => {
    window.URL.revokeObjectURL = vi.fn();
    window.URL.createObjectURL = vi.fn().mockImplementation(() => previewURL);
  });

  describe("when image has not been added added", () => {
    it("displays upload input", () => {
      render(<UploadProfileImage {...defaultProps} />);

      const input = screen.getByLabelText("Upload a file");

      expect(input).toBeInTheDocument();
      expect(input).not.toHaveAttribute("style");
    });

    it("displays photo icon", () => {
      render(<UploadProfileImage {...defaultProps} />);

      expect(screen.getByTestId("iconSvgPhoto")).toBeInTheDocument();
    });

    it("displays message", () => {
      render(<UploadProfileImage {...defaultProps} />);

      expect(screen.getByText("or drag and drop")).toBeInTheDocument();
      expect(screen.getByText("PNG, JPG, GIF up to 10MB")).toBeInTheDocument();
    });
  });

  describe("when drag is active", () => {
    it("displays drop message", async () => {
      render(<UploadProfileImage {...defaultProps} />);

      fireEvent.dragEnter(screen.getByTestId("dropzone"), {
        dataTransfer: {
          files: [mockProfileImage],
          items: [
            {
              kind: "file",
              type: mockProfileImage.type,
              getAsFile: () => mockProfileImage,
            },
          ],
          types: ["Files"],
        },
      });

      expect(
        await screen.findByText("Drop the files here...")
      ).toBeInTheDocument();
    });
  });

  describe("when image is added", () => {
    it("displays preview", async () => {
      render(<UploadProfileImage {...defaultProps} />);

      fireDropEvent();

      expect(await screen.findByTestId("profileImage")).toHaveStyle({
        backgroundImage: `url("${previewURL}")`,
      });
    });

    it("fires `onChange` event", async () => {
      render(<UploadProfileImage {...defaultProps} />);

      fireDropEvent();

      await waitForPromises();

      expect(defaultProps.onChange).toHaveBeenCalledWith(mockProfileImage);
    });

    it("allows user to remove image", async () => {
      const { user } = render(<UploadProfileImage {...defaultProps} />);

      fireDropEvent();

      const removeImageButton = await screen.findByLabelText(
        "Remove profile image"
      );

      expect(removeImageButton).toBeInTheDocument();

      await user.click(removeImageButton);

      expect(await screen.findByLabelText("Upload a file")).toBeInTheDocument();
      expect(defaultProps.onChange).toHaveBeenCalledWith(null);
    });
  });

  describe("when error prop is passed", () => {
    it("displays error message", () => {
      const errorMessage = "File must be less than 10MB.";

      render(
        <UploadProfileImage
          {...defaultProps}
          errors={{ type: "server", message: errorMessage }}
        />
      );

      expect(screen.getByText(errorMessage)).toBeInTheDocument();
    });
  });

  describe("when initial value is passed", () => {
    it("displays profile image preview", async () => {
      const initialValue =
        "https://ik.imagekit.io/profile_images/1/foo-bar.jpg";

      const { user } = render(
        <UploadProfileImage {...defaultProps} initialValue={initialValue} />
      );

      expect(screen.getByTestId("profileImage")).toHaveStyle({
        backgroundImage: `url("${initialValue}?height=400&width=400")`,
      });

      await user.click(screen.getByLabelText("Remove profile image"));

      expect(await screen.findByLabelText("Upload a file")).toBeInTheDocument();
      expect(defaultProps.onChange).toHaveBeenCalledWith(null);
    });
  });
});
