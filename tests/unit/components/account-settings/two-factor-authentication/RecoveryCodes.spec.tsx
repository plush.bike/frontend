import RecoveryCodes from "@/components/account-settings/two-factor-authentication/RecoveryCodes";
import { render, screen } from "tests/utils";

describe("RecoveryCodes", () => {
  const defaultProps = {
    recoveryCodes: ["fake-recovery-code-1", "fake-recovery-code-2"],
    onContinue: vi.fn(),
  };

  describe("when there are recovery codes", () => {
    it("renders recovery codes", () => {
      render(<RecoveryCodes {...defaultProps} />);

      expect(
        screen.getByText(defaultProps.recoveryCodes[0])
      ).toBeInTheDocument();
      expect(
        screen.getByText(defaultProps.recoveryCodes[1])
      ).toBeInTheDocument();
    });

    describe("when continue button is clicked", () => {
      it("calls `onContinue` prop", async () => {
        const { user } = render(<RecoveryCodes {...defaultProps} />);

        await user.click(screen.getByRole("button", { name: "Continue" }));

        expect(defaultProps.onContinue).toHaveBeenCalled();
      });
    });
  });

  describe("when there are no recovery codes", () => {
    it("does not render anything", () => {
      const { container } = render(
        <RecoveryCodes {...defaultProps} recoveryCodes={[]} />
      );

      expect(container).toBeEmptyDOMElement();
    });
  });
});
