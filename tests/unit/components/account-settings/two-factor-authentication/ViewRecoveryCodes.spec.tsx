import ViewRecoveryCodes from "@/components/account-settings/two-factor-authentication/ViewRecoveryCodes";
import { ConfirmPasswordDocument } from "@/graphql/auth/mutations/confirm-password.generated";
import { TwoFactorAuthenticationRecoveryCodesDocument } from "@/graphql/auth/mutations/two-factor-authentication-recovery-codes.generated";
import { updateToastError } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import {
  mockGraphQLError,
  mockNetworkError,
  render,
  screen,
  waitForPromises,
  within,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("ViewRecoveryCodes", () => {
  const twoFactorAuthenticationRecoveryCodesRequest = {
    query: TwoFactorAuthenticationRecoveryCodesDocument,
  };

  const confirmPasswordRequest = {
    query: ConfirmPasswordDocument,
    variables: {
      password: "password",
    },
  };

  const requirePasswordConfirmationMocks = [
    mockGraphQLError({
      request: twoFactorAuthenticationRecoveryCodesRequest,
      message: "Please enter your password to continue.",
      path: ["twoFactorAuthenticationRecoveryCodes"],
    }),
  ];

  const twoFactorAuthenticationRecoveryCodesLoadingMocks = [
    { request: twoFactorAuthenticationRecoveryCodesRequest, loading: true },
  ];

  const twoFactorAuthenticationRecoveryCodesSuccessMocks = [
    {
      request: twoFactorAuthenticationRecoveryCodesRequest,
      result: {
        data: {
          twoFactorAuthenticationRecoveryCodes: {
            recoveryCodes: ["fake-recovery-code-1", "fake-recovery-code-2"],
          },
        },
      },
    },
  ];

  const twoFactorAuthenticationRecoveryCodesNetworkErrorMocks = [
    mockNetworkError({ request: twoFactorAuthenticationRecoveryCodesRequest }),
  ];

  const confirmPasswordSuccessMocks = [
    {
      request: confirmPasswordRequest,
      result: {
        data: {
          confirmPassword: {
            status: "SUCCESS",
            message: "Password confirmed successfully.",
          },
        },
      },
    },
  ];

  const confirmPasswordGraphQLErrorMocks = [
    mockGraphQLError({
      request: confirmPasswordRequest,
      message: "Password is incorrect.",
      extensions: {
        validation: {
          "input.password": ["Password is incorrect."],
        },
        category: "validation",
      },
      path: ["confirmPassword"],
    }),
  ];

  describe("when request is loading", () => {
    it("displays loading icon", async () => {
      const { user } = render(<ViewRecoveryCodes />, {
        apollo: { mocks: twoFactorAuthenticationRecoveryCodesLoadingMocks },
      });

      const button = screen.getByRole("button", {
        name: "View recovery codes",
      });

      await user.click(button);

      expect(within(button).getByTestId("iconSvgLoading")).toBeInTheDocument();
    });
  });

  describe("when request requires password confirmation", () => {
    it("displays password confirmation modal", async () => {
      const { user } = render(<ViewRecoveryCodes />, {
        apollo: { mocks: requirePasswordConfirmationMocks },
      });

      await user.click(
        screen.getByRole("button", {
          name: "View recovery codes",
        })
      );

      await waitForPromises();

      expect(
        screen.getByRole("dialog", { name: "Confirm password" })
      ).toBeInTheDocument();
    });

    describe("when password is confirmed successfully", () => {
      it("renders recovery codes", async () => {
        const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
          <ViewRecoveryCodes />,
          {
            apollo: {
              mocks: [
                ...requirePasswordConfirmationMocks,
                ...confirmPasswordSuccessMocks,
                ...twoFactorAuthenticationRecoveryCodesSuccessMocks,
              ],
            },
          }
        );

        await user.click(
          screen.getByRole("button", {
            name: "View recovery codes",
          })
        );

        await waitForPromises();

        await user.type(
          screen.getByLabelText("Password"),
          confirmPasswordRequest.variables.password
        );

        await user.click(
          screen.getByRole("button", {
            name: "Continue",
          })
        );

        await waitForPromises();

        expect(screen.getByText("fake-recovery-code-1")).toBeInTheDocument();
        expect(screen.getByText("fake-recovery-code-2")).toBeInTheDocument();
        expect(
          screen.queryByRole("button", { name: "View recovery codes" })
        ).not.toBeInTheDocument();

        await user.click(
          within(screen.getByTestId("recoveryCodes")).getByRole("button", {
            name: "Continue",
          })
        );

        expect(
          screen.queryByText("fake-recovery-code-1")
        ).not.toBeInTheDocument();
        expect(
          screen.queryByText("fake-recovery-code-2")
        ).not.toBeInTheDocument();
        expect(
          screen.getByRole("button", { name: "View recovery codes" })
        ).toBeInTheDocument();
        expectApolloHandlersCalledWithCorrectVariables();
      });
    });

    describe("when password is not confirmed", () => {
      it("displays error message", async () => {
        const { user } = render(<ViewRecoveryCodes />, {
          apollo: {
            mocks: [
              ...requirePasswordConfirmationMocks,
              ...confirmPasswordGraphQLErrorMocks,
            ],
          },
        });

        await user.click(
          screen.getByRole("button", {
            name: "View recovery codes",
          })
        );

        await waitForPromises();

        await user.type(
          screen.getByLabelText("Password"),
          confirmPasswordRequest.variables.password
        );

        await user.click(
          screen.getByRole("button", {
            name: "Continue",
          })
        );

        await waitForPromises();

        expect(screen.getByLabelText("Password")).toHaveAccessibleErrorMessage(
          "Password is incorrect."
        );
      });
    });
  });

  describe("when view two factor authentication recovery codes request is not successful", () => {
    it("shows error toast", async () => {
      const { user } = render(<ViewRecoveryCodes />, {
        apollo: {
          mocks: twoFactorAuthenticationRecoveryCodesNetworkErrorMocks,
        },
      });

      await user.click(
        screen.getByRole("button", {
          name: "View recovery codes",
        })
      );

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    });
  });
});
