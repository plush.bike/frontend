import DisableTwoFactorAuthentication from "@/components/account-settings/two-factor-authentication/DisableTwoFactorAuthentication";
import { ConfirmPasswordDocument } from "@/graphql/auth/mutations/confirm-password.generated";
import { DisableTwoFactorAuthenticationDocument } from "@/graphql/auth/mutations/disable-two-factor-authentication.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  mockGraphQLError,
  mockNetworkError,
  render,
  screen,
  waitForPromises,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

const disableTwoFactorAuthenticationRequest = {
  query: DisableTwoFactorAuthenticationDocument,
};

const confirmPasswordRequest = {
  query: ConfirmPasswordDocument,
  variables: {
    password: "password",
  },
};

const requirePasswordConfirmationMocks = [
  mockGraphQLError({
    request: disableTwoFactorAuthenticationRequest,
    message: "Please enter your password to continue.",
    path: ["disableTwoFactorAuthentication"],
  }),
];

const disableTwoFactorAuthenticationSuccessMocks = [
  {
    request: disableTwoFactorAuthenticationRequest,
    result: {
      data: {
        disableTwoFactorAuthentication: {
          status: "SUCCESS",
          message: "Two factor authentication disabled.",
        },
      },
    },
  },
];

const disableTwoFactorAuthenticationNetworkErrorMocks = [
  mockNetworkError({ request: disableTwoFactorAuthenticationRequest }),
];

const confirmPasswordSuccessMocks = [
  {
    request: confirmPasswordRequest,
    result: {
      data: {
        confirmPassword: {
          status: "SUCCESS",
          message: "Password confirmed successfully.",
        },
      },
    },
  },
];

const confirmPasswordGraphQLErrorMocks = [
  mockGraphQLError({
    request: confirmPasswordRequest,
    message: "Password is incorrect.",
    extensions: {
      validation: {
        "input.password": ["Password is incorrect."],
      },
      category: "validation",
    },
    path: ["confirmPassword"],
  }),
];

describe("DisableTwoFactorAuthentication", () => {
  it("displays confirmation modal", async () => {
    const { user } = render(<DisableTwoFactorAuthentication />);

    await user.click(
      screen.getByRole("button", { name: "Disable two factor authentication" })
    );

    expect(
      screen.getByRole("dialog", {
        name: "Disable two factor authentication",
      })
    ).toBeInTheDocument();
  });

  describe("when disabling two factor authentication requires password confirmation", () => {
    it("displays password field", async () => {
      const { user } = render(<DisableTwoFactorAuthentication />, {
        apollo: { mocks: requirePasswordConfirmationMocks },
      });

      await user.click(
        screen.getByRole("button", {
          name: "Disable two factor authentication",
        })
      );

      await user.click(
        screen.getByRole("button", {
          name: "Disable",
        })
      );

      await waitForPromises();

      expect(screen.getByLabelText("Password")).toBeInTheDocument();
    });

    describe("when password is successfully confirmed", () => {
      it("disables two factor authentication", async () => {
        const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
          <DisableTwoFactorAuthentication />,
          {
            apollo: {
              mocks: [
                ...requirePasswordConfirmationMocks,
                ...confirmPasswordSuccessMocks,
                ...disableTwoFactorAuthenticationSuccessMocks,
              ],
            },
            authenticated: true,
          }
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable two factor authentication",
          })
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable",
          })
        );

        await waitForPromises();

        await user.type(
          screen.getByLabelText("Password"),
          confirmPasswordRequest.variables.password
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable",
          })
        );

        await waitForPromises();

        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastSuccess({
            show: true,
            message: "Two factor authentication disabled successfully.",
          })
        );

        expect(mockDispatch).toHaveBeenCalledWith(
          updateAuthenticatedUser({
            ...mockUser,
            hasEnabledTwoFactorAuthentication: false,
          })
        );
        expectApolloHandlersCalledWithCorrectVariables();
      });
    });

    describe("when password is not successfully confirmed", () => {
      it("displays error message", async () => {
        const { user } = render(<DisableTwoFactorAuthentication />, {
          apollo: {
            mocks: [
              ...requirePasswordConfirmationMocks,
              ...confirmPasswordGraphQLErrorMocks,
            ],
          },
        });

        await user.click(
          screen.getByRole("button", {
            name: "Disable two factor authentication",
          })
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable",
          })
        );

        await waitForPromises();

        await user.type(
          screen.getByLabelText("Password"),
          confirmPasswordRequest.variables.password
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable",
          })
        );

        await waitForPromises();

        expect(screen.getByLabelText("Password")).toHaveAccessibleErrorMessage(
          "Password is incorrect."
        );
      });
    });
  });

  describe("when disabling two factor authentication request is not successful", () => {
    it("shows error toast", async () => {
      const { user } = render(<DisableTwoFactorAuthentication />, {
        apollo: {
          mocks: disableTwoFactorAuthenticationNetworkErrorMocks,
        },
      });

      await user.click(
        screen.getByRole("button", {
          name: "Disable two factor authentication",
        })
      );

      await user.click(
        screen.getByRole("button", {
          name: "Disable",
        })
      );

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    });
  });
});
