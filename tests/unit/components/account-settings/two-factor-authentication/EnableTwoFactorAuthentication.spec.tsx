import EnableTwoFactorAuthentication from "@/components/account-settings/two-factor-authentication/EnableTwoFactorAuthentication";
import { ConfirmTwoFactorAuthenticationDocument } from "@/graphql/auth/mutations/confirm-two-factor-authentication.generated";
import { EnableTwoFactorAuthenticationDocument } from "@/graphql/auth/mutations/enable-two-factor-authentication.generated";
import { TwoFactorAuthenticationQrCodeDocument } from "@/graphql/auth/mutations/two-factor-authentication-qr-code.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  mockGraphQLError,
  mockNetworkError,
  render,
  screen,
  waitForPromises,
  within,
} from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("EnableTwoFactorAuthentication", () => {
  const defaultProps = {
    setAlertErrorMessage: vi.fn(),
  };

  const requestEnableTwoFactorAuthentication = {
    query: EnableTwoFactorAuthenticationDocument,
  };

  const requestTwoFactorAuthenticationQrCode = {
    query: TwoFactorAuthenticationQrCodeDocument,
  };

  const requestConfirmTwoFactorAuthentication = {
    query: ConfirmTwoFactorAuthenticationDocument,
    variables: {
      code: "12345",
    },
  };

  const enableTwoFactorAuthenticationLoadingMocks = [
    {
      request: requestEnableTwoFactorAuthentication,
      loading: true,
    },
  ];

  const enableTwoFactorAuthenticationSuccessMocks = [
    {
      request: requestEnableTwoFactorAuthentication,
      result: {
        data: {
          enableTwoFactorAuthentication: {
            status: "SUCCESS",
            message: "Two factor authentication enabled.",
          },
        },
      },
    },
    {
      request: requestTwoFactorAuthenticationQrCode,
      result: {
        data: {
          twoFactorAuthenticationQrCode: {
            svg: "<svg />",
          },
        },
      },
    },
  ];

  const enableTwoFactorAuthenticationErrorMocks = [
    mockNetworkError({
      request: requestEnableTwoFactorAuthentication,
    }),
  ];

  const confirmTwoFactorAuthenticationLoadingMocks = [
    {
      request: requestConfirmTwoFactorAuthentication,
      loading: true,
    },
  ];

  const confirmTwoFactorAuthenticationSuccessMocks = [
    {
      request: requestConfirmTwoFactorAuthentication,
      result: {
        data: {
          confirmTwoFactorAuthentication: {
            status: "SUCCESS",
            message: "Two factor authentication confirmed.",
            recoveryCodes: ["fake-recovery-code-1", "fake-recovery-code-2"],
          },
        },
      },
    },
  ];

  const confirmTwoFactorAuthenticationGraphQLErrorMocks = [
    mockGraphQLError({
      request: requestConfirmTwoFactorAuthentication,
      message:
        "An error has occurred while enabling two factor authentication.",
      extensions: {
        validation: {
          "input.code": ["The provided one time password was invalid."],
        },
        category: "validation",
      },
      path: ["confirmTwoFactorAuthentication"],
    }),
  ];

  const confirmTwoFactorAuthenticationNetworkErrorMocks = [
    mockNetworkError({ request: requestConfirmTwoFactorAuthentication }),
  ];

  describe("when enable two factor authentication request is loading", () => {
    it("shows loading icon", async () => {
      const { user } = render(
        <EnableTwoFactorAuthentication {...defaultProps} />,
        {
          apollo: { mocks: enableTwoFactorAuthenticationLoadingMocks },
        }
      );

      const button = screen.getByRole("button", {
        name: "Enable two factor authentication",
      });

      await user.click(button);

      expect(within(button).getByTestId("iconSvgLoading")).toBeInTheDocument();
    });
  });

  describe("when enable two factor authentication request is successful", () => {
    it("fetches QR code and renders form to confirm two factor authentication", async () => {
      const { user } = render(
        <EnableTwoFactorAuthentication {...defaultProps} />,
        {
          apollo: { mocks: enableTwoFactorAuthenticationSuccessMocks },
        }
      );

      await user.click(
        screen.getByRole("button", {
          name: "Enable two factor authentication",
        })
      );

      await waitForPromises();

      expect(screen.getByLabelText("One time password")).toBeInTheDocument();
      expect(screen.getByRole("img")).toHaveAttribute(
        "src",
        "data:image/svg+xml;utf8,%3Csvg%20%2F%3E"
      );
    });

    describe("when confirm two factor authentication is loading", () => {
      it("it shows loading icon", async () => {
        const { user } = render(
          <EnableTwoFactorAuthentication {...defaultProps} />,
          {
            apollo: {
              mocks: [
                ...enableTwoFactorAuthenticationSuccessMocks,
                ...confirmTwoFactorAuthenticationLoadingMocks,
              ],
            },
          }
        );

        await user.click(
          screen.getByRole("button", {
            name: "Enable two factor authentication",
          })
        );

        await waitForPromises();

        await user.type(
          screen.getByLabelText("One time password"),
          requestConfirmTwoFactorAuthentication.variables.code
        );

        const confirmButton = screen.getByRole("button", {
          name: "Confirm",
        });

        await user.click(confirmButton);

        expect(
          await within(confirmButton).findByTestId("iconSvgLoading")
        ).toBeInTheDocument();
      });
    });

    describe("when confirm two factor authentication is successful", () => {
      it("displays recovery codes and allows user to continue", async () => {
        const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
          <EnableTwoFactorAuthentication {...defaultProps} />,
          {
            apollo: {
              mocks: [
                ...enableTwoFactorAuthenticationSuccessMocks,
                ...confirmTwoFactorAuthenticationSuccessMocks,
              ],
            },
            authenticated: true,
          }
        );

        await user.click(
          screen.getByRole("button", {
            name: "Enable two factor authentication",
          })
        );

        await waitForPromises();

        await user.type(
          screen.getByLabelText("One time password"),
          requestConfirmTwoFactorAuthentication.variables.code
        );

        await user.click(
          screen.getByRole("button", {
            name: "Confirm",
          })
        );

        await waitForPromises();

        expect(screen.getByText("fake-recovery-code-1")).toBeInTheDocument();
        expect(screen.getByText("fake-recovery-code-2")).toBeInTheDocument();

        await user.click(
          screen.getByRole("button", {
            name: "Continue",
          })
        );

        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastSuccess({
            show: true,
            message: "Two factor authentication enabled successfully.",
          })
        );
        expect(mockDispatch).toHaveBeenCalledWith(
          updateAuthenticatedUser({
            ...mockUser,
            hasEnabledTwoFactorAuthentication: true,
          })
        );
        expectApolloHandlersCalledWithCorrectVariables();
      });
    });

    describe("when confirm two factor authentication is not successful", () => {
      describe("when error is a GraphQL error", () => {
        it("renders error message", async () => {
          const { user } = render(
            <EnableTwoFactorAuthentication {...defaultProps} />,
            {
              apollo: {
                mocks: [
                  ...enableTwoFactorAuthenticationSuccessMocks,
                  ...confirmTwoFactorAuthenticationGraphQLErrorMocks,
                ],
              },
              authenticated: true,
            }
          );

          await user.click(
            screen.getByRole("button", {
              name: "Enable two factor authentication",
            })
          );

          await waitForPromises();

          await user.type(
            screen.getByLabelText("One time password"),
            requestConfirmTwoFactorAuthentication.variables.code
          );

          await user.click(
            screen.getByRole("button", {
              name: "Confirm",
            })
          );

          await waitForPromises();

          expect(
            screen.getByLabelText("One time password")
          ).toHaveAccessibleErrorMessage(
            "The provided one time password was invalid."
          );
        });
      });

      describe("when error is a network error", () => {
        it("calls `setAlertErrorMessage` prop", async () => {
          const { user } = render(
            <EnableTwoFactorAuthentication {...defaultProps} />,
            {
              apollo: {
                mocks: [
                  ...enableTwoFactorAuthenticationSuccessMocks,
                  ...confirmTwoFactorAuthenticationNetworkErrorMocks,
                ],
              },
              authenticated: true,
            }
          );

          await user.click(
            screen.getByRole("button", {
              name: "Enable two factor authentication",
            })
          );

          await waitForPromises();

          await user.type(
            screen.getByLabelText("One time password"),
            requestConfirmTwoFactorAuthentication.variables.code
          );

          await user.click(
            screen.getByRole("button", {
              name: "Confirm",
            })
          );

          await waitForPromises();

          expect(defaultProps.setAlertErrorMessage).toHaveBeenCalled();
        });
      });
    });
  });

  describe("when enable two factor authentication request is not successful", () => {
    it("shows error toast", async () => {
      const { user } = render(
        <EnableTwoFactorAuthentication {...defaultProps} />,
        {
          apollo: { mocks: enableTwoFactorAuthenticationErrorMocks },
        }
      );

      await user.click(
        screen.getByRole("button", {
          name: "Enable two factor authentication",
        })
      );

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateToastError({
          show: true,
          message: "An error occurred, please try again.",
        })
      );
    });
  });
});
