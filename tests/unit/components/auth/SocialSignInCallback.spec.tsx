import mockRouter from "next-router-mock";

import SocialSignInCallback from "@/components/auth/SocialSignInCallback";
import * as TwoFactorAuthenticationChallengeForm from "@/components/auth/TwoFactorAuthenticationChallengeForm";
import { SocialProviderCallbackDocument } from "@/graphql/auth/social-providers/mutations/social-provider-callback.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import {
  SocialProviderCallbackType,
  SocialProviderType,
} from "@/types/graphql";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  mockComponent,
  mockGraphQLError,
  render,
  screen,
  waitForPromises,
} from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("SocialSignInCallback", () => {
  const defaultProps = {
    providerType: SocialProviderType.Strava,
    providerTitle: "Strava",
  };
  const code = "fake-code";
  const socialProviderCallbackRequest = {
    query: SocialProviderCallbackDocument,
    variables: {
      code,
      provider: defaultProps.providerType,
    },
  };

  beforeEach(() => {
    mockRouter.push({
      pathname: "/sign-in/strava",
      query: { code },
    });
  });

  describe("when API request is loading", () => {
    it("displays loading icon", () => {
      render(<SocialSignInCallback {...defaultProps} />, {
        apollo: {
          mocks: [
            {
              request: socialProviderCallbackRequest,
              loading: true,
            },
          ],
        },
      });

      expect(
        screen.getByRole("heading", {
          name: `Authorizing with ${defaultProps.providerTitle}`,
        })
      ).toBeInTheDocument();
      expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
    });
  });

  describe("when API request is successful", () => {
    describe("when callback type is `SignIn` and `twoFactor` is `false`", () => {
      it("redirects to dashboard and updates authenticated user", async () => {
        const { expectApolloHandlersCalledWithCorrectVariables } = render(
          <SocialSignInCallback {...defaultProps} />,
          {
            apollo: {
              mocks: [
                {
                  request: socialProviderCallbackRequest,
                  result: {
                    data: {
                      socialProviderCallback: {
                        user: mockUser,
                        twoFactor: false,
                        callbackType: SocialProviderCallbackType.SignIn,
                      },
                    },
                  },
                },
              ],
            },
          }
        );

        await waitForPromises();

        expect(mockRouter.pathname).toBe("/parts/forks");
        expect(mockDispatch).toHaveBeenCalledWith(
          updateAuthenticatedUser(mockUser)
        );
        expectApolloHandlersCalledWithCorrectVariables();
      });
    });

    describe("when callback type is `SignIn` and `twoFactor` is `true`", () => {
      it("renders `TwoFactorAuthenticationChallengeForm` component", async () => {
        const { spy } = mockComponent({
          componentObject: TwoFactorAuthenticationChallengeForm,
        });

        render(<SocialSignInCallback {...defaultProps} />, {
          apollo: {
            mocks: [
              {
                request: socialProviderCallbackRequest,
                result: {
                  data: {
                    socialProviderCallback: {
                      user: null,
                      twoFactor: true,
                      callbackType: SocialProviderCallbackType.SignIn,
                    },
                  },
                },
              },
            ],
          },
        });

        await waitForPromises();

        expect(spy).toHaveBeenCalled();
      });
    });

    describe("when callback type is `Connect`", () => {
      it("redirects to account settings and shows success toast", async () => {
        render(<SocialSignInCallback {...defaultProps} />, {
          apollo: {
            mocks: [
              {
                request: socialProviderCallbackRequest,
                result: {
                  data: {
                    socialProviderCallback: {
                      user: null,
                      twoFactor: null,
                      callbackType: SocialProviderCallbackType.Connect,
                    },
                  },
                },
              },
            ],
          },
        });

        await waitForPromises();

        expect(mockRouter.pathname).toBe("/account-settings");
        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastSuccess({
            show: true,
            message: "Strava successfully connected.",
          })
        );
      });
    });
  });

  describe("when API request is not successful", () => {
    describe("when callback type is `SignIn`", () => {
      it("redirects to sign in page and shows error toast", async () => {
        const message = "Could not communicate with Strava.";

        render(<SocialSignInCallback {...defaultProps} />, {
          apollo: {
            mocks: [
              mockGraphQLError({
                request: socialProviderCallbackRequest,
                extensions: {
                  callbackType: SocialProviderCallbackType.SignIn,
                },
                message,
                path: ["socialProviderCallback"],
              }),
            ],
          },
        });

        await waitForPromises();

        expect(mockRouter.pathname).toBe("/sign-in");
        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastError({
            show: true,
            message,
          })
        );
      });
    });

    describe("when callback type is `Connect`", () => {
      it("redirects to account settings page and shows error toast", async () => {
        const message = "Could not communicate with Strava.";

        render(<SocialSignInCallback {...defaultProps} />, {
          apollo: {
            mocks: [
              mockGraphQLError({
                request: socialProviderCallbackRequest,
                extensions: {
                  callbackType: SocialProviderCallbackType.Connect,
                },
                message,
                path: ["socialProviderCallback"],
              }),
            ],
          },
        });

        await waitForPromises();

        expect(mockRouter.pathname).toBe("/account-settings");
        expect(mockDispatch).toHaveBeenCalledWith(
          updateToastError({
            show: true,
            message,
          })
        );
      });
    });
  });
});
