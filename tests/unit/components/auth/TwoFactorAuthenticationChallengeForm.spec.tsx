import mockRouter from "next-router-mock";

import TwoFactorAuthenticationChallengeForm from "@/components/auth/TwoFactorAuthenticationChallengeForm";
import { TwoFactorAuthenticationChallengeDocument } from "@/graphql/auth/mutations/two-factor-authentication-challenge.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  mockGraphQLError,
  render,
  screen,
  waitForPromises,
  within,
} from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("TwoFactorAuthenticationChallengeForm", () => {
  const requestOneTimePassword = {
    query: TwoFactorAuthenticationChallengeDocument,
    variables: {
      code: "12345",
    },
  };

  const requestRecoveryCode = {
    query: TwoFactorAuthenticationChallengeDocument,
    variables: {
      recoveryCode: "fake-recovery-code",
    },
  };

  const optionsAPILoading = {
    apollo: {
      mocks: [
        {
          request: requestOneTimePassword,
          loading: true,
        },
      ],
    },
  };

  const optionsOneTimePasswordAPISuccess = {
    apollo: {
      mocks: [
        {
          request: requestOneTimePassword,
          result: {
            data: {
              twoFactorAuthenticationChallenge: {
                user: mockUser,
              },
            },
          },
        },
      ],
    },
  };

  const optionsRecoveryCodeAPISuccess = {
    apollo: {
      mocks: [
        {
          request: requestRecoveryCode,
          result: {
            data: {
              twoFactorAuthenticationChallenge: {
                user: mockUser,
              },
            },
          },
        },
      ],
    },
  };

  const optionsOneTimePasswordAPIGraphQLError = {
    apollo: {
      mocks: [
        mockGraphQLError({
          request: requestOneTimePassword,
          message: "Invalid one time password.",
          path: ["twoFactorAuthenticationChallenge"],
        }),
      ],
    },
  };

  const optionsRecoveryCodeAPIGraphQLError = {
    apollo: {
      mocks: [
        mockGraphQLError({
          request: requestRecoveryCode,
          message: "Invalid recovery code.",
          path: ["twoFactorAuthenticationChallenge"],
        }),
      ],
    },
  };

  describe("when request is loading", () => {
    it("shows loading icon", async () => {
      const { user } = render(
        <TwoFactorAuthenticationChallengeForm />,
        optionsAPILoading
      );

      const button = screen.getByRole("button", { name: "Sign in" });

      await user.type(
        screen.getByLabelText("One time password"),
        requestOneTimePassword.variables.code
      );
      await user.click(button);

      await waitForPromises();

      expect(within(button).getByTestId("iconSvgLoading")).toBeInTheDocument();
    });
  });

  describe("when `Sign in` button is clicked before entering one time password", () => {
    it("displays error message", async () => {
      const { user } = render(<TwoFactorAuthenticationChallengeForm />);

      await user.click(screen.getByRole("button", { name: "Sign in" }));

      expect(
        await screen.findByLabelText("One time password")
      ).toHaveAccessibleErrorMessage("Please enter your one time password.");
    });
  });

  describe("when one time password is correct", () => {
    it("sets authenticated user and redirects to `/parts/forks`", async () => {
      mockRouter.push("/sign-in");

      const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
        <TwoFactorAuthenticationChallengeForm />,
        optionsOneTimePasswordAPISuccess
      );

      await user.type(
        screen.getByLabelText("One time password"),
        requestOneTimePassword.variables.code
      );

      await user.click(screen.getByRole("button", { name: "Sign in" }));

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateAuthenticatedUser(mockUser)
      );
      expect(mockRouter.pathname).toBe("/parts/forks");
      expectApolloHandlersCalledWithCorrectVariables();
    });
  });

  describe("when one time password is incorrect", () => {
    it("displays alert", async () => {
      const { user } = render(
        <TwoFactorAuthenticationChallengeForm />,
        optionsOneTimePasswordAPIGraphQLError
      );

      await user.type(
        screen.getByLabelText("One time password"),
        requestOneTimePassword.variables.code
      );

      await user.click(screen.getByRole("button", { name: "Sign in" }));

      await waitForPromises();

      expect(
        screen.getByText("Invalid one time password.")
      ).toBeInTheDocument();
    });
  });

  describe("when `Sign in` button is clicked before entering recovery code", () => {
    it("displays error message", async () => {
      const { user } = render(<TwoFactorAuthenticationChallengeForm />);

      await user.click(
        screen.getByRole("button", { name: "Use recovery code" })
      );
      await user.click(screen.getByRole("button", { name: "Sign in" }));

      expect(
        await screen.findByLabelText("Recovery code")
      ).toHaveAccessibleErrorMessage("Please enter your recovery code.");
    });
  });

  describe("when recovery code is correct", () => {
    it("sets authenticated user and redirects to `/parts/forks`", async () => {
      mockRouter.push("/sign-in");

      const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
        <TwoFactorAuthenticationChallengeForm />,
        optionsRecoveryCodeAPISuccess
      );

      await user.click(
        screen.getByRole("button", { name: "Use recovery code" })
      );

      await user.type(
        screen.getByLabelText("Recovery code"),
        requestRecoveryCode.variables.recoveryCode
      );

      await user.click(screen.getByRole("button", { name: "Sign in" }));

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateAuthenticatedUser(mockUser)
      );
      expect(mockRouter.pathname).toBe("/parts/forks");
      expectApolloHandlersCalledWithCorrectVariables();
    });
  });

  describe("when recovery code is incorrect", () => {
    it("displays alert", async () => {
      const { user } = render(
        <TwoFactorAuthenticationChallengeForm />,
        optionsRecoveryCodeAPIGraphQLError
      );

      await user.click(
        screen.getByRole("button", { name: "Use recovery code" })
      );

      await user.type(
        screen.getByLabelText("Recovery code"),
        requestRecoveryCode.variables.recoveryCode
      );

      await user.click(screen.getByRole("button", { name: "Sign in" }));

      await waitForPromises();

      expect(screen.getByText("Invalid recovery code.")).toBeInTheDocument();
    });
  });
});
