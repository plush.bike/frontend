import mockRouter from "next-router-mock";

import SignInForm from "@/components/auth/SignInForm";
import * as SocialSignIn from "@/components/auth/SocialSignIn";
import { SignInDocument } from "@/graphql/auth/mutations/sign-in.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import {
  render,
  renderAndSubmitForm,
  waitForPromises,
  mockNetworkError,
  mockGraphQLError,
  screen,
  mockComponent,
} from "tests/utils";

vi.mock("next/router", async () => await vi.importActual("next-router-mock"));
vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);

describe("SignInForm", () => {
  const defaultProps = {
    onTwoFactor: vi.fn(),
  };

  const requestVariables = {
    email: "peter@plush.bike",
    password: "foobar",
  };

  const baseOptions = {
    submitLabel: "Sign in",
    fields: {
      Email: requestVariables.email,
      Password: requestVariables.password,
    },
  };

  const request = {
    query: SignInDocument,
    variables: requestVariables,
  };

  beforeEach(() => {
    mockRouter.push("/sign-in");
  });

  describe("validation", () => {
    describe.each`
      field         | value    | expectedError
      ${"Email"}    | ${""}    | ${"Please enter your email."}
      ${"Email"}    | ${"foo"} | ${"Please enter a valid email."}
      ${"Password"} | ${""}    | ${"Please enter your password."}
    `("when $field value is $value", ({ field, value, expectedError }) => {
      it("displays error message", async () => {
        await renderAndSubmitForm(<SignInForm {...defaultProps} />, {
          submitLabel: "Sign in",
          fields: {
            [field]: value,
          },
        });

        expect(
          await screen.findByLabelText(field)
        ).toHaveAccessibleErrorMessage(expectedError);
      });
    });
  });

  it("renders loading icon when form is submitted", async () => {
    await renderAndSubmitForm(<SignInForm {...defaultProps} />, {
      ...baseOptions,
      apollo: { mocks: [{ request, loading: true }] },
    });

    expect(await screen.findByTestId("iconSvgLoading")).toBeInTheDocument();
  });

  describe("when API call is not successful", () => {
    const optionsNetworkError = {
      ...baseOptions,
      apollo: {
        mocks: [mockNetworkError({ request })],
      },
    };

    const optionsGraphQLError = {
      ...baseOptions,
      apollo: {
        mocks: [
          mockGraphQLError({
            request,
            message: "Incorrect username or password.",
            path: ["sign-in"],
          }),
        ],
      },
    };

    it("clears password input and focuses on it", async () => {
      await renderAndSubmitForm(
        <SignInForm {...defaultProps} />,
        optionsNetworkError
      );

      await waitForPromises();

      const passwordInput = await screen.findByLabelText("Password");

      expect(passwordInput).toHaveValue("");
      expect(passwordInput).toHaveFocus();
    });

    describe("when error is a network error", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(
          <SignInForm {...defaultProps} />,
          optionsNetworkError
        );

        const alert = await screen.findByText(
          "An error occurred, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    describe("when error is incorrect email or password", () => {
      it("displays error alert", async () => {
        await renderAndSubmitForm(
          <SignInForm {...defaultProps} />,
          optionsGraphQLError
        );

        const alert = await screen.findByText(
          "Incorrect email or password, please try again."
        );

        expect(alert).toBeInTheDocument();
      });
    });

    it("closes error alert when close button is clicked", async () => {
      const { user } = await renderAndSubmitForm(
        <SignInForm {...defaultProps} />,
        optionsNetworkError
      );

      const alert = await screen.findByText(
        "An error occurred, please try again."
      );

      await user.click(screen.getByLabelText("Close alert"));
      expect(alert).not.toBeInTheDocument();
    });
  });

  describe("when API call is successful", () => {
    const options = ({ twoFactor = false } = {}) => ({
      ...baseOptions,
      apollo: {
        mocks: [
          {
            request,
            result: {
              data: {
                signIn: {
                  twoFactor,
                  user: mockUser,
                },
              },
            },
          },
        ],
      },
    });

    it("dispatches `updateAuthenticatedUser` action", async () => {
      const { expectApolloHandlersCalledWithCorrectVariables } =
        await renderAndSubmitForm(<SignInForm {...defaultProps} />, options());

      await waitForPromises();

      expect(mockDispatch).toHaveBeenCalledWith(
        updateAuthenticatedUser(mockUser)
      );
      expectApolloHandlersCalledWithCorrectVariables();
    });

    it("redirects to `/parts/forks`", async () => {
      await renderAndSubmitForm(<SignInForm {...defaultProps} />, options());

      await waitForPromises();

      expect(mockRouter.pathname).toBe("/parts/forks");
    });

    describe("when user has two factor authentication enabled", () => {
      it("calls `onTwoFactor` prop", async () => {
        await renderAndSubmitForm(
          <SignInForm {...defaultProps} />,
          options({ twoFactor: true })
        );

        await waitForPromises();

        expect(defaultProps.onTwoFactor).toHaveBeenCalled();
      });
    });
  });

  it("renders forgot password link", () => {
    render(<SignInForm {...defaultProps} />);

    expect(
      screen.getByText("Forgot password?", {
        selector: 'a[href="/forgot-password"]',
      })
    ).toBeInTheDocument();
  });

  it("renders `SocialSignIn` component", () => {
    const { spy } = mockComponent({ componentObject: SocialSignIn });

    render(<SignInForm {...defaultProps} />);

    expect(spy).toHaveBeenCalled();
  });
});
