import SocialSignIn from "@/components/auth/SocialSignIn";
import { SOCIAL_PROVIDERS } from "@/constants/auth";
import { render, screen } from "tests/utils";

describe("SocialSignIn", () => {
  it.each(SOCIAL_PROVIDERS)("renders $label button", ({ label, type }) => {
    render(<SocialSignIn />);

    const button = screen.getByRole("button", { name: label });

    expect(button).toBeInTheDocument();
    expect(button.parentElement).toHaveAttribute(
      "action",
      `http://api.plush.test/social-provider/${type.toLowerCase()}/redirect`
    );
  });
});
