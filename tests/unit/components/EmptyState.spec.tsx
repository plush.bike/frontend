import EmptyState from "@/components/EmptyState";
import cogIcon from "@/icons/cog.svg";
import { render, screen } from "tests/utils";

describe("EmptyState", () => {
  const defaultProps = {
    title: "No forks",
    buttonText: "New fork",
    onButtonClick: vi.fn(),
  };

  describe("props", () => {
    describe("icon", () => {
      describe("when passed", () => {
        it("renders icon", () => {
          render(<EmptyState {...defaultProps} icon={cogIcon} />);

          expect(screen.getByTestId("iconSvgCog")).toBeInTheDocument();
        });
      });
    });

    describe("title", () => {
      it("renders title", () => {
        render(<EmptyState {...defaultProps} />);

        expect(
          screen.getByRole("heading", { name: defaultProps.title })
        ).toBeInTheDocument();
      });
    });

    describe("buttonText", () => {
      it("renders button with button text", () => {
        render(<EmptyState {...defaultProps} />);

        expect(
          screen.getByRole("button", { name: defaultProps.buttonText })
        ).toBeInTheDocument();
      });
    });

    describe("onButtonClick", () => {
      it("calls `onButtonClick` when button is clicked", async () => {
        const { user } = render(<EmptyState {...defaultProps} />);

        await user.click(
          screen.getByRole("button", { name: defaultProps.buttonText })
        );

        expect(defaultProps.onButtonClick).toHaveBeenCalled();
      });
    });

    describe("href", () => {
      it("renders anchor tag with `href` attribute", () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { onButtonClick, ...defaultPropsWithoutOnButtonClick } =
          defaultProps;
        const href = "/forks/create";

        render(
          <EmptyState {...defaultPropsWithoutOnButtonClick} href={href} />
        );

        const link = screen.getByRole("link", {
          name: defaultPropsWithoutOnButtonClick.buttonText,
        });

        expect(link).toBeInTheDocument();
        expect(link).toHaveAttribute("href", href);
      });
    });
  });

  it("renders children", () => {
    render(
      <EmptyState {...defaultProps}>
        Create a fork to help keep track of your settings.
      </EmptyState>
    );

    expect(
      screen.getByText("Create a fork to help keep track of your settings.")
    ).toBeInTheDocument();
  });
});
