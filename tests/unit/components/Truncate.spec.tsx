import Truncate from "@/components/Truncate";
import { render } from "tests/utils";

describe("Truncate", () => {
  it("truncates text", () => {
    const { container } = render(<Truncate lines={4}>Foo bar</Truncate>);

    expect(container.firstChild).toHaveClass("line-clamp-4");
  });
});
