import Dropdown from "@/components/dropdown/Dropdown";
import DropdownItem from "@/components/dropdown/DropdownItem";
import dotsHorizontalTripleIcon from "@/icons/dots-horizontal-triple.svg";
import { render, within, screen } from "tests/utils";

describe("Dropdown", () => {
  const buttonText = "Open options";

  describe("props", () => {
    describe("when `buttonText` prop is passed", () => {
      it("renders button text with chevron down icon", () => {
        render(<Dropdown buttonText={buttonText} />);

        const toggleButton = screen.getByRole("button", { name: buttonText });

        expect(toggleButton).toBeInTheDocument();
        expect(
          within(toggleButton).getByTestId("iconSvgCheveronDown")
        ).toBeInTheDocument();
      });
    });

    describe("when `buttonIcon` prop is passed", () => {
      it("renders icon with screen reader text", () => {
        render(<Dropdown buttonIcon={dotsHorizontalTripleIcon} />);

        const toggleButton = screen.getByRole("button", {
          name: "Open options",
        });

        expect(toggleButton).toBeInTheDocument();
        expect(
          within(toggleButton).getByTestId("iconSvgDotsHorizontalTriple")
        ).toBeInTheDocument();
      });
    });

    describe("when `disabled` prop is `true`", () => {
      it("disables menu button", () => {
        render(<Dropdown buttonText={buttonText} disabled />);

        expect(screen.getByRole("button", { name: buttonText })).toBeDisabled();
      });
    });

    describe("when `alignment` prop is `right`", () => {
      it("adds `right-0` class to dropdown", async () => {
        const { user } = render(<Dropdown buttonText={buttonText} />);

        await user.click(screen.getByRole("button", { name: buttonText }));

        expect(screen.getByRole("menu")).toHaveClass("right-0");
      });
    });

    describe("when `alignment` prop is `left`", () => {
      it("adds `left-0` class to dropdown", async () => {
        const { user } = render(
          <Dropdown buttonText={buttonText} alignment="left" />
        );

        await user.click(screen.getByRole("button", { name: buttonText }));

        expect(screen.getByRole("menu")).toHaveClass("left-0");
      });
    });

    describe("when `preventCloseWhenItemIsSelected` is `true`", () => {
      it("does not close dropdown when item is selected with enter key", async () => {
        const handleOnClick = vi.fn();

        const { user } = render(
          <Dropdown buttonText={buttonText} preventCloseWhenItemIsSelected>
            <DropdownItem onClick={handleOnClick} preventCloseWhenSelected>
              Foo bar
            </DropdownItem>
          </Dropdown>
        );

        await user.click(screen.getByRole("button", { name: buttonText }));

        await user.keyboard("{ArrowDown}");
        await user.keyboard("{Enter}");

        expect(handleOnClick).toHaveBeenCalled();
        expect(
          screen.getByRole("menuitem", { name: "Foo bar" })
        ).toBeInTheDocument();
      });
    });

    describe("when `preventCloseWhenItemIsSelected` is `false`", () => {
      it("closes dropdown when item is selected with enter key", async () => {
        const handleOnClick = vi.fn();

        const { user } = render(
          <Dropdown buttonText={buttonText}>
            <DropdownItem onClick={handleOnClick}>Foo bar</DropdownItem>
          </Dropdown>
        );

        await user.click(screen.getByRole("button", { name: buttonText }));

        await user.keyboard("{ArrowDown}");
        await user.keyboard("{Enter}");

        expect(handleOnClick).toHaveBeenCalled();
        expect(
          screen.queryByRole("menuitem", { name: "Foo bar" })
        ).not.toBeInTheDocument();
      });
    });
  });

  it("renders children when dropdown is opened", async () => {
    const { user } = render(
      <Dropdown buttonText={buttonText}>
        <DropdownItem href="#">Foo bar</DropdownItem>
      </Dropdown>
    );

    expect(
      screen.queryByRole("menuitem", { name: "Foo bar" })
    ).not.toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: buttonText }));

    expect(
      screen.getByRole("menuitem", { name: "Foo bar" })
    ).toBeInTheDocument();
  });
});
