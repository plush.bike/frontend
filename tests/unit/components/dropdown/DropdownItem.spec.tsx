import { Menu } from "@headlessui/react";

import DropdownItem from "@/components/dropdown/DropdownItem";
import trashIcon from "@/icons/trash.svg";
import { render, within, screen, fireEvent, createEvent } from "tests/utils";

describe("DropdownItem", () => {
  const name = "Foo bar";

  describe("props", () => {
    describe("when `href` prop is passed", () => {
      const href = "/foo-bar";

      it("renders an anchor tag", () => {
        render(
          <Menu>
            <DropdownItem href={href}>{name}</DropdownItem>
          </Menu>
        );

        const dropdownItem = screen.getByRole("menuitem", { name });

        expect(dropdownItem.tagName).toBe("A");
        expect(dropdownItem).toHaveAttribute("href", href);
      });

      describe("when `icon` prop is passed", () => {
        it("renders icon", () => {
          render(
            <Menu>
              <DropdownItem href={href} icon={trashIcon}>
                {name}
              </DropdownItem>
            </Menu>
          );

          expect(
            within(screen.getByRole("menuitem", { name })).getByTestId(
              "iconSvgTrash"
            )
          ).toBeInTheDocument();
        });
      });
    });

    describe("when `onClick` prop is passed", () => {
      const handleOnClick = vi.fn();

      it("renders a button tag", async () => {
        const { user } = render(
          <Menu>
            <DropdownItem onClick={handleOnClick}>{name}</DropdownItem>
          </Menu>
        );

        const dropdownItem = screen.getByRole("menuitem", { name });

        expect(dropdownItem.tagName).toBe("BUTTON");

        await user.click(dropdownItem);

        expect(handleOnClick).toHaveBeenCalled();
      });

      describe("when `icon` prop is passed", () => {
        it("renders icon", () => {
          render(
            <Menu>
              <DropdownItem onClick={handleOnClick} icon={trashIcon}>
                {name}
              </DropdownItem>
            </Menu>
          );

          expect(
            within(screen.getByRole("menuitem", { name })).getByTestId(
              "iconSvgTrash"
            )
          ).toBeInTheDocument();
        });
      });
    });

    describe("when `preventCloseWhenSelected` prop is `true`", () => {
      it("calls `preventDefault` and `stopPropagation`", () => {
        const handleOnClick = vi.fn();

        render(
          <Menu>
            <DropdownItem onClick={handleOnClick} preventCloseWhenSelected>
              {name}
            </DropdownItem>
          </Menu>
        );

        const dropdownItem = screen.getByRole("menuitem", { name });
        const event = createEvent.click(screen.getByRole("menuitem", { name }));
        const preventDefaultSpy = vi.spyOn(event, "preventDefault");
        const stopPropagationSpy = vi.spyOn(event, "stopPropagation");

        fireEvent(dropdownItem, event);

        expect(preventDefaultSpy).toHaveBeenCalled();
        expect(stopPropagationSpy).toHaveBeenCalled();
        expect(handleOnClick).toHaveBeenCalled();
      });
    });
  });

  describe("when dropdown item is active", () => {
    it("changes background color", async () => {
      render(
        <Menu>
          <DropdownItem href="#" icon={trashIcon}>
            {name}
          </DropdownItem>
        </Menu>
      );

      const dropdownItem = screen.getByRole("menuitem", { name });

      fireEvent.pointerMove(dropdownItem);

      expect(dropdownItem).toHaveClass("bg-gray-100", "text-gray-900");
    });
  });
});
