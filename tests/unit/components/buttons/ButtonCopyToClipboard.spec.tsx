import ButtonCopyToClipboard from "@/components/buttons/ButtonCopyToClipboard";
import { copyToClipboard } from "@/helpers/copy-to-clipboard";
import { render, screen, act } from "tests/utils";

vi.mock("@/helpers/copy-to-clipboard", () => ({
  copyToClipboard: vi.fn().mockImplementation(() => Promise.resolve()),
}));

describe("ButtonCopyToClipboard", () => {
  const defaultProps = {
    contentToCopy: "Foo bar",
  };

  beforeEach(() => {
    vi.useFakeTimers({ shouldAdvanceTime: true });
    vi.spyOn(global, "setTimeout");
  });

  describe("when button is clicked", () => {
    it("displays `Copied` tooltip for one second, copies text, and focuses on button", async () => {
      const { user } = render(<ButtonCopyToClipboard {...defaultProps} />);

      const button = screen.getByRole("button", { name: "Copy" });

      await user.click(button);

      expect(copyToClipboard).toHaveBeenCalledWith(defaultProps.contentToCopy);
      expect(
        await screen.findByRole("tooltip", { name: "Copied" })
      ).toBeInTheDocument();

      act(() => {
        vi.runAllTimers();
      });

      expect(
        screen.queryByRole("tooltip", { name: "Copied" })
      ).not.toBeInTheDocument();
      expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 1000);
      expect(button).toHaveFocus();
    });
  });

  describe("when `renderButton` prop is passed", () => {
    it("displays `Copied` tooltip for one second, copies text, and focuses on button", async () => {
      const { user } = render(
        <ButtonCopyToClipboard
          {...defaultProps}
          renderButton={({ onClick }) => (
            <button onClick={onClick} type="button">
              Copy
            </button>
          )}
        />
      );

      const button = screen.getByRole("button", { name: "Copy" });

      await user.click(button);

      expect(copyToClipboard).toHaveBeenCalledWith(defaultProps.contentToCopy);
      expect(
        await screen.findByRole("tooltip", { name: "Copied" })
      ).toBeInTheDocument();

      act(() => {
        vi.runAllTimers();
      });

      expect(
        screen.queryByRole("tooltip", { name: "Copied" })
      ).not.toBeInTheDocument();
      expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 1000);
      expect(button).toHaveFocus();
    });
  });
});
