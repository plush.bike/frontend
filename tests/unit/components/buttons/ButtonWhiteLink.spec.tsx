import React from "react";

import { buttonLinkShared } from "./shared";

import ButtonWhiteLink from "@/components/buttons/ButtonWhiteLink";
import { render } from "tests/utils";

describe("ButtonWhiteLink", () => {
  buttonLinkShared(ButtonWhiteLink);

  it("has correct CSS classes", () => {
    const { container } = render(<ButtonWhiteLink href="https://foo.bar" />);

    expect(container.firstChild).toHaveClass(
      "text-gray-700 bg-white hover:bg-gray-50 border-gray-300"
    );
  });
});
