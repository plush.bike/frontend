import React from "react";

import { buttonShared } from "./shared";

import ButtonSecondary from "@/components/buttons/ButtonSecondary";
import { render } from "tests/utils";

describe("ButtonSecondary", () => {
  buttonShared(ButtonSecondary);

  it("has correct CSS classes", () => {
    const { container } = render(<ButtonSecondary />);

    expect(container.firstChild).toHaveClass(
      "text-indigo-700 bg-indigo-100 hover:bg-indigo-200"
    );
  });
});
