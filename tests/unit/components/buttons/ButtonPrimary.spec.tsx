import { buttonShared } from "./shared";

import ButtonPrimary from "@/components/buttons/ButtonPrimary";
import { render } from "tests/utils";

describe("ButtonPrimary", () => {
  buttonShared(ButtonPrimary);

  it("has correct CSS classes", () => {
    const { container } = render(<ButtonPrimary />);

    expect(container.firstChild).toHaveClass(
      "text-white bg-indigo-600 hover:bg-indigo-700"
    );
  });
});
