import { buttonShared } from "./shared";

import ButtonRed from "@/components/buttons/ButtonRed";
import { render } from "tests/utils";

describe("ButtonRed", () => {
  buttonShared(ButtonRed);

  it("has correct CSS classes", () => {
    const { container } = render(<ButtonRed />);

    expect(container.firstChild).toHaveClass(
      "text-white bg-red-600 hover:bg-red-700"
    );
  });
});
