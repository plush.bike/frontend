import plusIcon from "@/icons/plus.svg";
import { ButtonLinkProps, ButtonProps } from "@/types/buttons";
import { FunctionComponent } from "@/types/react";
import { render, screen } from "tests/utils";

export const buttonShared = (
  ButtonComponent: FunctionComponent<ButtonProps>
) => {
  describe("props", () => {
    it("merges `className` prop with CSS classes", () => {
      const { container } = render(
        <ButtonComponent className="text-white bg-indigo-600 hover:bg-indigo-700" />
      );

      expect(container.firstChild).toHaveClass(
        "text-white bg-indigo-600 hover:bg-indigo-700"
      );
    });

    describe("when `type` prop is passed", () => {
      it("sets `type` attribute", () => {
        const { container } = render(<ButtonComponent type="submit" />);

        expect(container.firstChild).toHaveAttribute("type", "submit");
      });
    });

    describe("when `type` prop is not passed", () => {
      it("sets `type` attribute to `button`", () => {
        const { container } = render(<ButtonComponent />);

        expect(container.firstChild).toHaveAttribute("type", "button");
      });
    });

    describe("when `loading` prop is `true`", () => {
      it("displays loading icon", () => {
        render(<ButtonComponent loading />);

        expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
      });

      it("disables button", () => {
        const { container } = render(<ButtonComponent loading />);

        expect(container.firstChild).toBeDisabled();
      });
    });

    describe("when `icon` prop is passed", () => {
      it("displays icon", () => {
        render(<ButtonComponent icon={plusIcon} />);

        expect(screen.getByTestId("iconSvgPlus")).toBeInTheDocument();
      });
    });
  });

  it("renders children", () => {
    render(<ButtonComponent>Foo bar</ButtonComponent>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
};

export const buttonLinkShared = (
  ButtonComponent: FunctionComponent<ButtonLinkProps>
) => {
  describe("props", () => {
    it("merges `className` prop with CSS classes", () => {
      const { container } = render(
        <ButtonComponent
          href="https://foo.bar"
          className="text-white bg-indigo-600 hover:bg-indigo-700"
        />
      );

      expect(container.firstChild).toHaveClass(
        "text-white bg-indigo-600 hover:bg-indigo-700"
      );
    });

    describe("when `loading` prop is `true`", () => {
      it("displays loading icon", () => {
        render(<ButtonComponent href="https://foo.bar" loading />);

        expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
      });
    });

    describe("when `icon` prop is passed", () => {
      it("displays icon", () => {
        render(<ButtonComponent href="https://foo.bar" icon={plusIcon} />);

        expect(screen.getByTestId("iconSvgPlus")).toBeInTheDocument();
      });
    });
  });

  it("renders children", () => {
    render(<ButtonComponent href="https://foo.bar">Foo bar</ButtonComponent>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
};
