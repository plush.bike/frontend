import React from "react";

import { buttonLinkShared } from "./shared";

import ButtonSecondaryLink from "@/components/buttons/ButtonSecondaryLink";
import { render } from "tests/utils";

describe("ButtonSecondaryLink", () => {
  buttonLinkShared(ButtonSecondaryLink);

  it("has correct CSS classes", () => {
    const { container } = render(
      <ButtonSecondaryLink href="https://foo.bar" />
    );

    expect(container.firstChild).toHaveClass(
      "text-indigo-700 bg-indigo-100 hover:bg-indigo-200"
    );
  });
});
