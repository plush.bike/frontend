import React from "react";

import { buttonShared } from "./shared";

import ButtonWhite from "@/components/buttons/ButtonWhite";
import { render } from "tests/utils";

describe("ButtonWhite", () => {
  buttonShared(ButtonWhite);

  it("has correct CSS classes", () => {
    const { container } = render(<ButtonWhite />);

    expect(container.firstChild).toHaveClass(
      "text-gray-700 bg-white hover:bg-gray-50 border-gray-300"
    );
  });
});
