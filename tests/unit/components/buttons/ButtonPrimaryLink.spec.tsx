import { buttonLinkShared } from "./shared";

import ButtonPrimaryLink from "@/components/buttons/ButtonPrimaryLink";
import { render } from "tests/utils";

describe("ButtonPrimaryLink", () => {
  buttonLinkShared(ButtonPrimaryLink);

  it("has correct CSS classes", () => {
    const { container } = render(<ButtonPrimaryLink href="https://foo.bar" />);

    expect(container.firstChild).toHaveClass(
      "text-white bg-indigo-600 hover:bg-indigo-700"
    );
  });
});
