import Link from "@/components/Link";
import { render, screen } from "tests/utils";

describe("Link", () => {
  const defaultProps = {
    href: "/forks/create",
    className: "foo-bar",
  };

  it("renders an anchor tag with `href` attribute", () => {
    render(<Link {...defaultProps}>New fork</Link>);

    const link = screen.getByRole("link", { name: "New fork" });

    expect(link).toBeInTheDocument();
    expect(link).toHaveAttribute("href", defaultProps.href);
    expect(link).toHaveClass(defaultProps.className);
  });
});
