import ModalConfirmPassword from "@/components/modals/ModalConfirmPassword";
import { INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE } from "@/constants/auth";
import { ConfirmPasswordDocument } from "@/graphql/auth/mutations/confirm-password.generated";
import { mockGraphQLError, render, screen, waitForPromises } from "tests/utils";

describe("ModalConfirmPassword", () => {
  const errorRequiresPasswordConfirmation = {
    message: INCORRECT_CONFIRM_PASSWORD_ERROR_MESSAGE,
  };
  const onPasswordConfirmed = vi
    .fn()
    .mockImplementation(() => Promise.resolve());
  const defaultProps = (error: any) => {
    return {
      modalTrigger: ({ onError }) => {
        const handleButtonClick = () => {
          onError(error);
        };

        return (
          <button type="button" onClick={handleButtonClick}>
            Disable two factor authentication
          </button>
        );
      },
      onPasswordConfirmed,
    };
  };

  const variables = {
    password: "password",
  };
  const request = {
    query: ConfirmPasswordDocument,
    variables,
  };

  const optionsAPISuccess = {
    apollo: {
      mocks: [
        {
          request,
          result: {
            data: {
              confirmPassword: {
                status: "SUCCESS",
                message: "Password confirmed successfully.",
              },
            },
          },
        },
      ],
    },
  };

  const optionsAPIGraphQLError = {
    apollo: {
      mocks: [
        mockGraphQLError({
          request,
          message: "Password is incorrect.",
          extensions: {
            validation: {
              "input.password": ["Password is incorrect."],
            },
            category: "validation",
          },
          path: ["confirmPassword"],
        }),
      ],
    },
  };

  describe("when error requires password confirmation", () => {
    it("opens the modal", async () => {
      const { user } = render(
        <ModalConfirmPassword
          {...defaultProps(errorRequiresPasswordConfirmation)}
        />
      );

      await user.click(
        screen.getByRole("button", {
          name: "Disable two factor authentication",
        })
      );

      expect(
        screen.getByRole("heading", { name: "Confirm password" })
      ).toBeInTheDocument();
    });

    describe("when password is confirmed successfully by clicking continue button", () => {
      it("confirms password and the calls `onPasswordConfirmed`", async () => {
        const { expectApolloHandlersCalledWithCorrectVariables, user } = render(
          <ModalConfirmPassword
            {...defaultProps(errorRequiresPasswordConfirmation)}
          />,
          optionsAPISuccess
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable two factor authentication",
          })
        );

        await user.type(screen.getByLabelText("Password"), variables.password);

        await user.click(
          screen.getByRole("button", {
            name: "Continue",
          })
        );

        await waitForPromises();

        expect(onPasswordConfirmed).toHaveBeenCalled();
        expectApolloHandlersCalledWithCorrectVariables();
      });
    });

    describe("when password is confirmed successfully by submitting the form", () => {
      it("confirms password and the calls `onPasswordConfirmed`", async () => {
        const { user } = render(
          <ModalConfirmPassword
            {...defaultProps(errorRequiresPasswordConfirmation)}
          />,
          optionsAPISuccess
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable two factor authentication",
          })
        );

        await user.type(
          screen.getByLabelText("Password"),
          `${variables.password}{enter}`
        );

        await waitForPromises();

        expect(onPasswordConfirmed).toHaveBeenCalled();
      });
    });

    describe("when form is submitted without entering password", () => {
      it("displays error message", async () => {
        const { user } = render(
          <ModalConfirmPassword
            {...defaultProps(errorRequiresPasswordConfirmation)}
          />,
          optionsAPISuccess
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable two factor authentication",
          })
        );

        await user.click(
          screen.getByRole("button", {
            name: "Continue",
          })
        );

        expect(
          await screen.findByLabelText("Password")
        ).toHaveAccessibleErrorMessage("Please enter your password.");
      });
    });

    describe("when password is incorrect", () => {
      it("displays error message and keeps the modal open", async () => {
        const { user } = render(
          <ModalConfirmPassword
            {...defaultProps(errorRequiresPasswordConfirmation)}
          />,
          optionsAPIGraphQLError
        );

        await user.click(
          screen.getByRole("button", {
            name: "Disable two factor authentication",
          })
        );

        await user.type(
          screen.getByLabelText("Password"),
          `${variables.password}{enter}`
        );

        await waitForPromises();

        expect(screen.getByLabelText("Password")).toHaveAccessibleErrorMessage(
          "Password is incorrect."
        );
      });
    });
  });
});
