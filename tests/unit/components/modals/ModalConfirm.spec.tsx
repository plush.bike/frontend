import { noop } from "lodash";
import { Dispatch, FunctionComponent, SetStateAction, useRef } from "react";

import Icon from "@/components/Icon";
import ModalConfirm from "@/components/modals/ModalConfirm";
import { MODAL_VARIANT_PRIMARY, MODAL_VARIANT_RED } from "@/constants/modal";
import trashIcon from "@/icons/trash.svg";
import { render, act, screen } from "tests/utils";

describe("ModalConfirm", () => {
  const defaultProps = {
    title: "Are you sure?",
    modalTrigger: ({ onClick }) => (
      <button onClick={onClick} type="button" aria-label="Delete fork">
        <Icon size={4} svg={trashIcon} />
      </button>
    ),
    onContinue: vi.fn(),
  };

  const modalContent = "Are you sure you want to delete your Fox 36?";

  const expectModalToOpenAndClose = async (buttonText: string) => {
    const { user } = render(
      <ModalConfirm {...defaultProps}>{modalContent}</ModalConfirm>
    );

    await user.click(screen.getByRole("button", { name: "Delete fork" }));

    expect(
      screen.getByRole("heading", { name: defaultProps.title })
    ).toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: buttonText }));

    expect(
      screen.queryByRole("heading", { name: defaultProps.title })
    ).not.toBeInTheDocument();
  };

  describe("props", () => {
    describe(`when \`variant\` prop is ${MODAL_VARIANT_RED}`, () => {
      it("renders red button and red icon", async () => {
        const { user } = render(
          <ModalConfirm {...defaultProps} variant={MODAL_VARIANT_RED}>
            {modalContent}
          </ModalConfirm>
        );

        await user.click(screen.getByRole("button", { name: "Delete fork" }));

        expect(screen.getByRole("button", { name: "Continue" })).toHaveClass(
          "bg-red-600"
        );
        expect(screen.getByTestId("iconSvgWarning")).toHaveClass(
          "text-red-500"
        );
      });
    });

    describe(`when \`variant\` prop is ${MODAL_VARIANT_PRIMARY}`, () => {
      it("renders primary button and primary color icon", async () => {
        const { user } = render(
          <ModalConfirm {...defaultProps}>{modalContent}</ModalConfirm>
        );

        await user.click(screen.getByRole("button", { name: "Delete fork" }));

        expect(screen.getByRole("button", { name: "Continue" })).toHaveClass(
          "bg-indigo-600"
        );
        expect(screen.getByTestId("iconSvgWarning")).toHaveClass(
          "text-indigo-500"
        );
      });
    });

    describe("when `continueButton` prop is passed", () => {
      it("renders as continue button text", async () => {
        const { user } = render(
          <ModalConfirm
            {...defaultProps}
            continueButton="Continue deleting fork"
          >
            {modalContent}
          </ModalConfirm>
        );

        await user.click(screen.getByRole("button", { name: "Delete fork" }));

        expect(
          screen.getByRole("button", { name: "Continue deleting fork" })
        ).toBeInTheDocument();
      });
    });

    describe("when `cancelButton` prop is passed", () => {
      it("renders as cancel button text", async () => {
        const { user } = render(
          <ModalConfirm {...defaultProps} continueButton="Cancel deleting fork">
            {modalContent}
          </ModalConfirm>
        );

        await user.click(screen.getByRole("button", { name: "Delete fork" }));

        expect(
          screen.getByRole("button", { name: "Cancel deleting fork" })
        ).toBeInTheDocument();
      });
    });
  });

  it("opens modal when trigger is clicked", async () => {
    const { user } = render(
      <ModalConfirm {...defaultProps}>{modalContent}</ModalConfirm>
    );

    expect(
      screen.queryByRole("heading", { name: defaultProps.title })
    ).not.toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: "Delete fork" }));

    expect(
      screen.getByRole("heading", { name: defaultProps.title })
    ).toBeInTheDocument();
    expect(screen.getByText(modalContent)).toBeInTheDocument();
    expect(screen.getByTestId("iconSvgWarning")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Continue" })
    ).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "Cancel" })).toBeInTheDocument();
  });

  it("opens modal when `setShow` is called", async () => {
    const ModalWrapper: FunctionComponent = () => {
      const setModalShow = useRef<Dispatch<SetStateAction<boolean>>>(null);

      const handleButtonOnClick = () => {
        setModalShow.current(true);
      };

      return (
        <>
          <ModalConfirm
            {...defaultProps}
            modalTrigger={({ setShow }) => {
              setModalShow.current = setShow;

              return null;
            }}
          >
            {modalContent}
          </ModalConfirm>
          <button onClick={handleButtonOnClick}>Open modal</button>
        </>
      );
    };

    const { user } = render(<ModalWrapper />);

    expect(
      screen.queryByRole("heading", { name: defaultProps.title })
    ).not.toBeInTheDocument();

    await user.click(screen.getByRole("button", { name: "Open modal" }));

    expect(
      screen.getByRole("heading", { name: defaultProps.title })
    ).toBeInTheDocument();
  });

  it("calls continue when `fireContinue` is called", async () => {
    const ModalWrapper: FunctionComponent = () => {
      const fireModalContinue = useRef<() => void>(noop);

      const handleButtonOnClick = () => {
        fireModalContinue.current();
      };

      return (
        <>
          <ModalConfirm
            {...defaultProps}
            modalTrigger={({ fireContinue, onClick }) => {
              fireModalContinue.current = fireContinue;

              return (
                <button
                  onClick={onClick}
                  type="button"
                  aria-label="Delete fork"
                >
                  <Icon size={4} svg={trashIcon} />
                </button>
              );
            }}
          >
            {modalContent}
            <button onClick={handleButtonOnClick}>Custom continue</button>
          </ModalConfirm>
        </>
      );
    };

    const { user } = render(<ModalWrapper />);

    await user.click(screen.getByRole("button", { name: "Delete fork" }));

    await user.click(screen.getByRole("button", { name: "Custom continue" }));

    expect(defaultProps.onContinue).toHaveBeenCalled();
  });

  it("closes modal when close button is clicked", async () => {
    await expectModalToOpenAndClose("Close");
  });

  it("closes modal when `Cancel` button is clicked", async () => {
    await expectModalToOpenAndClose("Cancel");
  });

  describe("when `Continue` button is clicked", () => {
    it("calls `onContinue` prop", async () => {
      const { user } = render(
        <ModalConfirm {...defaultProps}>{modalContent}</ModalConfirm>
      );

      await user.click(screen.getByRole("button", { name: "Delete fork" }));

      await user.click(screen.getByRole("button", { name: "Continue" }));

      expect(defaultProps.onContinue).toHaveBeenCalled();
    });

    describe("when `onContinue` returns a Promise", () => {
      describe("when resolved promise doesn't have any data", () => {
        it("displays loading icon and then closes modal when Promise resolves", async () => {
          let resolvePromise: (value: void | PromiseLike<void>) => void;
          const { user } = render(
            <ModalConfirm
              {...defaultProps}
              // eslint-disable-next-line @typescript-eslint/no-empty-function
              onContinue={() =>
                new Promise((resolve) => {
                  resolvePromise = resolve;
                })
              }
            >
              {modalContent}
            </ModalConfirm>
          );

          await user.click(screen.getByRole("button", { name: "Delete fork" }));

          await user.click(screen.getByRole("button", { name: "Continue" }));

          expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();

          await act(async () => {
            resolvePromise();
          });

          expect(
            screen.queryByTestId("iconSvgLoading")
          ).not.toBeInTheDocument();

          expect(
            screen.queryByRole("heading", { name: defaultProps.title })
          ).not.toBeInTheDocument();
        });
      });

      describe("when resolved promise has data with `shouldHide` set to `true`", () => {
        it("displays loading icon and then keeps modal open when promise resolves", async () => {
          let resolvePromise: (value: { shouldHide: boolean }) => void;
          const { user } = render(
            <ModalConfirm
              {...defaultProps}
              // eslint-disable-next-line @typescript-eslint/no-empty-function
              onContinue={() =>
                new Promise((resolve) => {
                  resolvePromise = resolve;
                })
              }
            >
              {modalContent}
            </ModalConfirm>
          );

          await user.click(screen.getByRole("button", { name: "Delete fork" }));

          await user.click(screen.getByRole("button", { name: "Continue" }));

          expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();

          await act(async () => {
            resolvePromise({ shouldHide: false });
          });

          expect(
            screen.queryByTestId("iconSvgLoading")
          ).not.toBeInTheDocument();

          expect(
            screen.getByRole("heading", { name: defaultProps.title })
          ).toBeInTheDocument();
        });
      });
    });

    describe("when `onContinue` does not return a Promise", () => {
      it("closes the modal", async () => {
        await expectModalToOpenAndClose("Continue");
      });
    });
  });
});
