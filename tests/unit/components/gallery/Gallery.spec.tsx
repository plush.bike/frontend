import PhotoswipeJS from "photoswipe";

import Gallery from "@/components/gallery/Gallery";
import { render, within, screen } from "tests/utils";

vi.mock("photoswipe", () => ({
  default: vi
    .fn()
    .mockImplementation(() => ({ init: vi.fn(), listen: vi.fn() })),
}));

describe("Gallery", () => {
  const items = [
    {
      src: "https://ik.imagekit.io/parts/1/foo-bar.png",
      thumbnailSrc: "https://ik.imagekit.io/parts/1/foo-bar-thumbnail.png",
      id: 1,
    },
    {
      src: "https://ik.imagekit.io/parts/1/foo-bar2.png",
      thumbnailSrc: "https://ik.imagekit.io/parts/1/foo-bar2-thumbnail.png",
      id: 2,
    },
  ];

  it("displays gallery images", () => {
    render(<Gallery items={items} />);

    const listItems = screen.getAllByRole("listitem");

    expect(listItems).toHaveLength(2);

    listItems.forEach((listItem, index) => {
      expect(within(listItem).getByRole("img")).toHaveAttribute(
        "src",
        items[index].thumbnailSrc
      );
      expect(
        within(listItem).getByRole("button", { name: "View image" })
      ).toBeInTheDocument();
    });
  });

  describe("when gallery image is clicked", () => {
    it("opens photswipe gallery", async () => {
      const { user } = render(<Gallery items={items} />);

      const listItems = screen.getAllByRole("listitem");

      await user.click(
        within(listItems[0]).getByRole("button", { name: "View image" })
      );

      expect(PhotoswipeJS).toHaveBeenCalledWith(
        expect.any(HTMLDivElement),
        expect.any(Function),
        [
          {
            src: "https://ik.imagekit.io/parts/1/foo-bar.png",
            msrc: "https://ik.imagekit.io/parts/1/foo-bar-thumbnail.png",
            w: expect.any(Number),
            h: expect.any(Number),
          },
          {
            src: "https://ik.imagekit.io/parts/1/foo-bar2.png",
            msrc: "https://ik.imagekit.io/parts/1/foo-bar2-thumbnail.png",
            w: expect.any(Number),
            h: expect.any(Number),
          },
        ],
        {
          galleryUID: 1,
          getThumbBoundsFn: expect.any(Function),
          showHideOpacity: true,
          index: 0,
        }
      );
    });
  });
});
