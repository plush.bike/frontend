import GalleryThumbnail from "@/components/gallery/GalleryThumbnail";
import { render, screen } from "tests/utils";

describe("GalleryThumbnail", () => {
  const defaultProps = {
    item: {
      src: "https://ik.imagekit.io/parts/1/foo-bar.png",
      thumbnailSrc: "https://ik.imagekit.io/parts/1/foo-bar-thumbnail.png",
      id: 1,
    },
    index: 0,
    onClick: vi.fn(),
  };

  it("renders image thumbnail", () => {
    render(<GalleryThumbnail {...defaultProps} />);

    expect(screen.getByRole("img")).toHaveAttribute(
      "src",
      defaultProps.item.thumbnailSrc
    );
  });

  describe("when thumbnail is clicked", () => {
    it("calls `onClick` prop with index", async () => {
      const { user } = render(<GalleryThumbnail {...defaultProps} />);

      await user.click(screen.getByRole("button", { name: "View image" }));

      expect(defaultProps.onClick).toHaveBeenCalledWith(defaultProps.index);
    });
  });

  it("forwards ref", () => {
    let imageEl: HTMLImageElement;

    render(<GalleryThumbnail {...defaultProps} ref={(el) => (imageEl = el)} />);

    expect(imageEl).toBeInTheDocument();
    expect(imageEl.tagName).toBe("IMG");
  });
});
