import PageLayout from "@/components/layouts/PageLayout";
import * as AvatarDropdown from "@/components/nav/AvatarDropdown";
import * as Sidebar from "@/components/nav/Sidebar";
import * as ToastError from "@/components/toasts/ToastError";
import * as ToastSuccess from "@/components/toasts/ToastSuccess";
import { updateCsrfCookieSet } from "@/store/slices/auth";
import { updateToastError, updateToastSuccess } from "@/store/slices/toasts";
import { mockDispatch } from "tests/mocks/react-redux";
import { mockComponent, render, screen, waitForPromises } from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("PageLayout", () => {
  const defaultProps = {
    showNavigation: true,
    mainShouldHavePadding: true,
  };

  beforeAll(() => {
    window.fetch = vi.fn().mockResolvedValue("");
  });

  it("fetches CSRF cookie then updates store", async () => {
    render(<PageLayout {...defaultProps}>Foo bar</PageLayout>);

    await waitForPromises();

    expect(window.fetch).toHaveBeenCalledWith(
      "http://api.plush.test/sanctum/csrf-cookie",
      { credentials: "include" }
    );
    expect(mockDispatch).toHaveBeenCalledWith(updateCsrfCookieSet(true));
  });

  describe("when user is authenticated", () => {
    it("renders `Sidebar` component", () => {
      const { spy } = mockComponent({ componentObject: Sidebar });

      render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
        authenticated: true,
      });

      expect(spy).toHaveBeenCalled();
    });

    it("renders button to open the sidebar", () => {
      render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
        authenticated: true,
      });

      expect(
        screen.getByRole("button", { name: "Open sidebar" })
      ).toBeInTheDocument();
    });

    it("renders `AvatarDropdown` component", () => {
      const { spy } = mockComponent({ componentObject: AvatarDropdown });

      render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
        authenticated: true,
      });

      expect(spy).toHaveBeenCalled();
    });

    describe("when open sidebar button is clicked", () => {
      it("opens the sidebar", async () => {
        const { spy } = mockComponent({ componentObject: Sidebar });

        const { user } = render(
          <PageLayout {...defaultProps}>Foo bar</PageLayout>,
          {
            authenticated: true,
          }
        );

        await user.click(screen.getByRole("button", { name: "Open sidebar" }));

        expect(spy).toHaveBeenLastCalledWith(
          expect.objectContaining({ open: true }),
          {}
        );
      });
    });
  });

  describe("when user is not authenticated", () => {
    it("does not render `Sidebar` component", () => {
      const { spy } = mockComponent({ componentObject: Sidebar });

      render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
        authenticated: false,
      });

      expect(spy).not.toHaveBeenCalled();
    });

    it("does not render button to open the sidebar", () => {
      render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
        authenticated: false,
      });

      expect(
        screen.queryByRole("button", { name: "Open sidebar" })
      ).not.toBeInTheDocument();
    });

    it("renders `Sign in` and `Sign up` buttons instead of `AvatarDropdown` component", () => {
      const { spy } = mockComponent({ componentObject: AvatarDropdown });

      render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
        authenticated: false,
      });

      expect(spy).not.toHaveBeenCalled();
      expect(screen.getByRole("link", { name: "Sign in" })).toBeInTheDocument();
      expect(screen.getByRole("link", { name: "Sign up" })).toBeInTheDocument();
    });
  });

  describe("when `showNavigation` prop is `false`", () => {
    it("does not render `Sidebar` component", () => {
      const { spy } = mockComponent({ componentObject: Sidebar });

      render(
        <PageLayout {...defaultProps} showNavigation={false}>
          Foo bar
        </PageLayout>,
        {
          authenticated: true,
        }
      );

      expect(spy).not.toHaveBeenCalled();
    });

    it("does not render top navigation", () => {
      render(
        <PageLayout {...defaultProps} showNavigation={false}>
          Foo bar
        </PageLayout>,
        {
          authenticated: true,
        }
      );

      expect(screen.queryByTestId("topNavigation")).not.toBeInTheDocument();
    });
  });

  describe("when `mainShouldHavePadding` prop is `false`", () => {
    it("does not render padding on main content area", () => {
      render(
        <PageLayout {...defaultProps} mainShouldHavePadding={false}>
          Foo bar
        </PageLayout>
      );

      expect(screen.getByRole("main")).not.toHaveClass("py-6");
    });
  });

  describe("toasts", () => {
    describe("when `toasts.success.show` Redux state is `true`", () => {
      it("renders success toast with `toasts.success.message`", () => {
        const { spy } = mockComponent({ componentObject: ToastSuccess });

        const message = "Profile successfully updated.";

        render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
          store: {
            initialState: {
              toasts: {
                success: {
                  show: true,
                  message,
                },
                error: {
                  show: false,
                  message: "",
                },
              },
            },
          },
        });

        expect(spy).toHaveBeenCalledWith(
          expect.objectContaining({
            show: true,
            children: message,
          }),
          {}
        );
      });

      describe("when success toast is closed", () => {
        it("dispatches `updateToastSuccess`", () => {
          const { getProps } = mockComponent({ componentObject: ToastSuccess });

          render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
            store: {
              initialState: {
                toasts: {
                  success: {
                    show: true,
                    message: "Profile successfully updated.",
                  },
                  error: {
                    show: false,
                    message: "",
                  },
                },
              },
            },
          });

          const { onClose } = getProps();

          onClose();

          expect(mockDispatch).toHaveBeenCalledWith(
            updateToastSuccess({ show: false })
          );
        });
      });
    });

    describe("when `toasts.error.show` Redux state is `true`", () => {
      it("renders error toast with `toasts.error.message`", () => {
        const { spy } = mockComponent({ componentObject: ToastError });

        const message = "An error occurred, please try again.";

        render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
          store: {
            initialState: {
              toasts: {
                success: {
                  show: false,
                  message: "",
                },
                error: {
                  show: true,
                  message,
                },
              },
            },
          },
        });

        expect(spy).toHaveBeenCalledWith(
          expect.objectContaining({
            show: true,
            children: message,
          }),
          {}
        );
      });

      describe("when error toast is closed", () => {
        it("dispatches `updateToastError`", () => {
          const { getProps } = mockComponent({ componentObject: ToastError });

          render(<PageLayout {...defaultProps}>Foo bar</PageLayout>, {
            store: {
              initialState: {
                toasts: {
                  success: {
                    show: false,
                    message: "",
                  },
                  error: {
                    show: true,
                    message: "An error occurred, please try again.",
                  },
                },
              },
            },
          });

          const { onClose } = getProps();

          onClose();

          expect(mockDispatch).toHaveBeenCalledWith(
            updateToastError({ show: false })
          );
        });
      });
    });
  });

  it("renders children", () => {
    render(<PageLayout {...defaultProps}>Foo bar</PageLayout>);

    expect(screen.getByText("Foo bar")).toBeInTheDocument();
  });
});
