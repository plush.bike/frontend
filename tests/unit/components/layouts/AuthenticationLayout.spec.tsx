import mockRouter from "next-router-mock";
import React from "react";

import AuthenticationLayout from "@/components/layouts/AuthenticationLayout";
import { AuthenticatedUserDocument } from "@/graphql/auth/queries/authenticated-user.generated";
import { updateAuthenticatedUser } from "@/store/slices/auth";
import { mockDispatch } from "tests/mocks/react-redux";
import { user as mockUser } from "tests/unit/mock-data/user";
import { render, screen, waitForPromises } from "tests/utils";

vi.mock(
  "react-redux",
  async () => await vi.importActual("tests/mocks/react-redux")
);
vi.mock("next/router", async () => await vi.importActual("next-router-mock"));

describe("AuthenticationLayout", () => {
  describe("when user is already authenticated", () => {
    beforeEach(() => {
      mockRouter.push("/parts/forks");
    });

    it("renders children", () => {
      render(<AuthenticationLayout>Foo bar</AuthenticationLayout>, {
        authenticated: true,
        store: {
          initialState: {
            auth: {
              csrfCookieSet: true,
            },
          },
        },
      });

      expect(screen.getByText("Foo bar")).toBeInTheDocument();
    });
  });

  describe("when current route requires authentication and user is not authenticated", () => {
    const request = {
      query: AuthenticatedUserDocument,
    };

    const mockSuccess = [
      {
        request,
        result: {
          data: {
            authenticatedUser: mockUser,
          },
        },
      },
    ];

    const mockError = [
      {
        request,
        error: new Error("An error occurred"),
      },
    ];

    describe("when API call is successful", () => {
      it("renders loading icon, dispatches `updateAuthenticatedUser` action and then renders children", async () => {
        mockRouter.push("/parts/forks");

        render(<AuthenticationLayout>Foo bar</AuthenticationLayout>, {
          apollo: { mocks: mockSuccess },
          store: {
            initialState: {
              auth: {
                authenticatedUser: null,
                csrfCookieSet: true,
              },
            },
          },
        });

        expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
        await waitForPromises();
        expect(mockDispatch).toHaveBeenCalledWith(
          updateAuthenticatedUser(mockUser)
        );
        expect(screen.getByText("Foo bar")).toBeInTheDocument();
      });
    });

    describe("when API call is not successful", () => {
      it("renders loading icon and then renders children", async () => {
        render(<AuthenticationLayout>Foo bar</AuthenticationLayout>, {
          apollo: { mocks: mockError },
          authenticated: false,
          store: {
            initialState: {
              auth: {
                authenticatedUser: null,
                csrfCookieSet: true,
              },
            },
          },
        });

        expect(screen.getByTestId("iconSvgLoading")).toBeInTheDocument();
        await waitForPromises();
        expect(screen.getByText("Foo bar")).toBeInTheDocument();
      });
    });
  });
});
