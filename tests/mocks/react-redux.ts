// eslint-disable-next-line @typescript-eslint/no-var-requires
const redux = require("react-redux");

const mockDispatch = vi.fn();

// eslint-disable-next-line no-undef
module.exports = {
  ...redux,
  useDispatch: vi.fn().mockImplementation(() => mockDispatch),
  mockDispatch,
};

export { mockDispatch };
