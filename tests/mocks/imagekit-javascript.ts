const mock = vi.fn().mockImplementation(() => {
  return {
    url: vi.fn().mockImplementation(({ src, transformation }) => {
      if (transformation?.[0]?.height && transformation?.[0]?.width) {
        return `${src}?height=${transformation[0].height}&width=${transformation[0].width}`;
      }

      if (transformation?.[0]?.height) {
        return `${src}?h=${transformation[0].height}`;
      }

      if (transformation?.[0]?.width) {
        return `${src}?w=${transformation[0].width}`;
      }

      return src;
    }),
  };
});

export default mock;
