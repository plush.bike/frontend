import path from "path";

import react from "@vitejs/plugin-react";
import { defineConfig } from "vitest/config";

import icons from "./tests/plugins/icons";
import svgr from "./tests/plugins/svgr";

export default defineConfig({
  plugins: [react(), icons(), svgr()],
  css: {
    postcss: {
      plugins: [],
    },
  },
  test: {
    include: ["./tests/**/*.spec.?(c|m)[jt]s?(x)"],
    exclude: [
      "**/node_modules/**",
      "**/.next/**",
      "**/dist/**",
      "**/.{idea,git,cache,output,temp}/**",
      "**/{webpack,vite,vitest,babel,postcss,tailwind}.config.*",
    ],
    environment: "jsdom",
    setupFiles: ["./tests/setup.ts"],
    globals: true,
    clearMocks: true,
    alias: {
      "@": path.resolve(__dirname, "./src"),
      tests: path.resolve(__dirname, "./tests"),
      "^@/svgs/.+.svg$": path.resolve(__dirname, "./tests/mocks/svgr.tsx"),
    },
    css: false,
    watch: false,
  },
});
